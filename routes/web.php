<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'user\WelcomeController@index')->name('home');
Route::get('/home', 'user\WelcomeController@index')->name('home2');
Route::get('/aboutus', 'DashboardController@about')->name('aboutus');
Route::get('/tips', 'DashboardController@tips')->name('tips');
Route::get('/tips/{id}', 'DashboardController@tip')->name('tipsdetail');
Route::get('/kontak', 'user\WelcomeController@kontak')->name('kontak');
Route::get('/produk', 'user\ProdukController@index')->name('user.produk');
Route::get('/produk/cari', 'user\ProdukController@cari')->name('user.produk.cari');
Route::get('/kategori/{id}', 'KategoriController@produkByKategori')->name('user.kategori');
Route::get('/produk/{id}', 'user\ProdukController@detail')->name('user.produk.detail');
Route::get('/cari', 'user\ProdukController@loadData')->name('cari');
Route::get('livesearch','user\ProdukController@livesearch');


Route::get('/pelanggan', function () {
    return 'Pelanggan';
});

// Route::group(['middleware' => ['auth', 'checkRole:admin']], function () {
//     //pengaturan
//     Route::get('/admin', 'DashboardController@index')->name('admin.dashboard');
//     Route::get('/dashboard', 'DashboardController@index');
//     Route::get('/admin_admincust', 'DashboardController@indexcust');
//     Route::get('/dashboard_admincust', 'DashboardController@indexcust');
//     Route::get('/admin_adminkat', 'DashboardController@indexkat');
//     Route::get('/dashboard_adminkat', 'DashboardController@indexkat');
//     Route::get('/admin_adminstok', 'DashboardController@indexstok');
//     Route::get('/dashboard_adminstok', 'DashboardController@indexstok');
//     Route::get('/admin_admintrx', 'DashboardController@indextrx');
//     Route::get('/dashboard_admintrx', 'DashboardController@indextrx');
//     Route::get('/pengaturan/alamat', 'admin\PengaturanController@aturalamat')->name('admin.pengaturan.alamat');
//     Route::get('/pengaturan/ubahalamat/{id}', 'admin\PengaturanController@ubahalamat')->name('admin.pengaturan.ubahalamat');
//     Route::get('/pengaturan/alamat/getcity/{id}', 'admin\PengaturanController@getCity')->name('admin.pengaturan.getCity');
//     Route::post('pengaturan/simpanalamat', 'admin\PengaturanController@simpanalamat')->name('admin.pengaturan.simpanalamat');
//     Route::post('pengaturan/updatealamat/{id}', 'admin\PengaturanController@updatealamat')->name('admin.pengaturan.updatealamat');

//     Route::get('/admin/rekening', 'admin\RekeningController@index')->name('admin.rekening');
//     Route::get('/admin/rekening/edit/{id}', 'admin\RekeningController@edit')->name('admin.rekening.edit');
//     Route::get('/admin/rekening/tambah', 'admin\RekeningController@tambah')->name('admin.rekening.tambah');
//     Route::post('/admin/rekening/store', 'admin\RekeningController@store')->name('admin.rekening.store');
//     Route::get('/admin/rekening/delete/{id}', 'admin\RekeningController@delete')->name('admin.rekening.delete');
//     Route::post('/admin/rekening/update/{id}', 'admin\RekeningController@update')->name('admin.rekening.update');

//     Route::get('/dashboard_owner', 'DashboardController@indexown');
//     Route::get('/admin_owner', 'DashboardController@indexown');
//     Route::get('/admin/dataadmin', 'UserController@index')->name('admin.dataadmin');
//     Route::get('/admin/tambahadmin', 'UserController@tambah')->name('admin.tambahadmin');
//     Route::post('/admin/tambahadmin/store', 'UserController@store')->name('admin.tambahadmin.store');
//     Route::get('/admin/deladmin/{id}', 'UserController@delete')->name('admin.delete');



//     //kategori
//     Route::get('/admin/categories', 'admin\CategoriesController@index')->name('admin.categories');
//     Route::get('/admin/categories/tambah', 'admin\CategoriesController@tambah')->name('admin.categories.tambah');
//     Route::post('/admin/categories/store', 'admin\CategoriesController@store')->name('admin.categories.store');
//     Route::post('/admin/categories/update/{id}', 'admin\CategoriesController@update')->name('admin.categories.update');
//     Route::get('/admin/categories/edit/{id}', 'admin\CategoriesController@edit')->name('admin.categories.edit');
//     Route::get('/admin/categories/delete/{id}', 'admin\CategoriesController@delete')->name('admin.categories.delete');


//     //produk
//     Route::get('/admin/product', 'admin\ProductController@index')->name('admin.product');
//     Route::get('/admin/product/tambah', 'admin\ProductController@tambah')->name('admin.product.tambah');
//     Route::post('/admin/product/store', 'admin\ProductController@store')->name('admin.product.store');
//     Route::get('/admin/product/edit/{id}', 'admin\ProductController@edit')->name('admin.product.edit');
//     Route::get('/admin/product/detail/{id}', 'admin\ProductController@detail')->name('admin.product.detail');
//     Route::get('/admin/product/stok/{id}', 'admin\ProductController@stok')->name('admin.product.stok');
//     Route::get('/admin/product/delete/{id}', 'admin\ProductController@delete')->name('admin.product.delete');
//     Route::post('/admin/product/update/{id}', 'admin\ProductController@update')->name('admin.product.update');
//     Route::post('/admin/product/updatestok/{id}', 'admin\ProductController@updatestok')->name('admin.product.updatestok');
//     Route::get('/admin/product/cetak', 'admin\ProductController@cetak')->name('admin.product.cetak');


//     //transaksi
//     Route::get('/admin/transaksi', 'admin\TransaksiController@index')->name('admin.transaksi');
//     Route::get('/admin/transaksi/perludicek', 'admin\TransaksiController@perludicek')->name('admin.transaksi.perludicek');
//     Route::get('/admin/transaksi/perludikirim', 'admin\TransaksiController@perludikirim')->name('admin.transaksi.perludikirim');
//     Route::get('/admin/transaksi/dikirim', 'admin\TransaksiController@dikirim')->name('admin.transaksi.dikirim');
//     Route::get('/admin/transaksi/detail/{id}', 'admin\TransaksiController@detail')->name('admin.transaksi.detail');
//     Route::get('/admin/transaksi/konfirmasi/{id}', 'admin\TransaksiController@konfirmasi')->name('admin.transaksi.konfirmasi');
//     Route::post('/admin/transaksi/inputresi/{id}', 'admin\TransaksiController@inputresi')->name('admin.transaksi.inputresi');
//     Route::get('/admin/transaksi/selesai', 'admin\TransaksiController@selesai')->name('admin.transaksi.selesai');
//     Route::get('/admin/transaksi/gagal', 'admin\TransaksiController@gagal')->name('admin.transaksi.gagal');
//     Route::get('/admin/transaksi/dibatalkan', 'admin\TransaksiController@dibatalkan')->name('admin.transaksi.dibatalkan');


//     //laporan
//     Route::get('/admin/laporantrx', 'admin\TransaksiController@lapselesai')->name('admin.transaksi.lapselesai');
//     Route::get('/admin/laporantrx/tanggal:{tgl}', 'admin\TransaksiController@ambiltgl')->name('admin.transaksi.ambiltgl');
//     Route::get('/admin/laporantrx/detail/{id}', 'admin\TransaksiController@lapdetail')->name('admin.transaksi.lapdetail');
//     Route::get('/admin/laporan_stok_keluar', 'admin\KurangBahanController@indextgl');
//     Route::get('/admin/laporan_stok_keluar/tanggal:{tgl}', 'admin\KurangBahanController@indextglid');
//     Route::get('/admin/laporan_stok_masuk', 'admin\BeliBahanController@indextgl');
//     Route::get('/admin/laporan_stok_masuk/tanggal:{tgl}', 'admin\BeliBahanController@indextglid');
//     Route::get('/admin/laporan_keuangan2', 'admin\TransaksiController@uang')->name('admin.transaksi.uang');
//     Route::get('/admin/laporan_keuangan', 'admin\TransaksiController@uang2')->name('admin.transaksi.uang2');
//     Route::get('/admin/grafik-customer', 'admin\PelangganController@grafik')->name('admin.grafik-cus');
//     Route::get('/admin/product-laris', 'admin\ProductController@prdlaris')->name('admin.prodlaris');



//     //pelanggan
//     Route::get('/admin/pelanggan', 'admin\PelangganController@index')->name('admin.pelanggan');


//     //bahan
//     Route::get('/admin/bahan', 'admin\BahanController@index');
//     Route::get('/admin/createbahan', 'admin\BahanController@create');
//     Route::post('/admin/storebahan', 'admin\BahanController@store');
//     Route::get('/admin/editbahan/{id}', 'admin\BahanController@edit');
//     Route::patch('/admin/updatebahan/{id}', 'admin\BahanController@update');
//     Route::get('/admin/desbahan:{id}', 'admin\BahanController@destroy');
//     Route::get('/admin/delbahan/{id}', 'admin\BahanController@delete')->name('bhn.delete');
//     Route::get('/admin/bahan/cetak', 'admin\BahanController@cetak')->name('admin.bahan.cetak');

//     //stok masuk
//     Route::get('/admin/stok_masuk', 'admin\BeliBahanController@index');
//     Route::get('/admin/input_stok_masuk', 'admin\BeliBahanController@create');
//     Route::get('/admin/createbeli_bahan/{id_bahan}', 'admin\BeliBahanController@createid');
//     Route::post('/admin/storebeli_bahan', 'admin\BeliBahanController@store');
//     Route::post('/admin/storebeli_bahan/{id_bahan}', 'admin\BeliBahanController@storeid');
//     Route::get('/admin/editbeli_bahan/{id}', 'admin\BeliBahanController@edit');
//     Route::patch('/admin/updatebeli_bahan/{id}', 'admin\BeliBahanController@update');
//     Route::get('/admin/desbeli_bahan:{id}', 'admin\BeliBahanController@destroy');
//     Route::get('/admin/delbeli_bahan/{id}', 'admin\BeliBahanController@delete')->name('blbhn.delete');

//     //stok keluar
//     Route::get('/admin/kurang_bahan', 'admin\KurangBahanController@index');
//     Route::get('/admin/createkurang_bahan', 'admin\KurangBahanController@create');
//     Route::get('/admin/createkurang_bahan/{id_bahan}', 'admin\KurangBahanController@createid');
//     Route::post('/admin/storekurang_bahan', 'admin\KurangBahanController@store');
//     Route::post('/admin/storekurang_bahan/{id_bahan}', 'admin\KurangBahanController@storeid');
//     Route::get('/admin/editkurang_bahan/{id}', 'admin\KurangBahanController@edit');
//     Route::patch('/admin/updatekurang_bahan/{id}', 'admin\KurangBahanController@update');
//     Route::get('/admin/deskurang_bahan:{id}', 'admin\KurangBahanController@destroy');
//     Route::get('/admin/delkurang_bahan/{id}', 'admin\KurangBahanController@delete')->name('krgbhn.delete');


//     //tips
//     Route::get('/admin/tips', 'admin\TipsController@index');
//     Route::get('/admin/createtips', 'admin\TipsController@create');
//     Route::post('/admin/storetips', 'admin\TipsController@store');
//     Route::get('/admin/edittips/{id}', 'admin\TipsController@edit');
//     Route::patch('/admin/updatetips/{id}', 'admin\TipsController@update');
//     Route::get('/admin/destips:{id}', 'admin\TipsController@destroy');
//     Route::get('/admin/deltips/{id}', 'admin\TipsController@delete')->name('tips.delete');

//     Route::get('/edit_profile/{id}', 'UserController@edit');
//     Route::patch('/update_profil/{id}', 'UserController@update');
// });
















Route::group(['middleware' => ['auth', 'checkRole:customer']], function () {

    Route::post('/keranjang/simpan', 'user\KeranjangController@simpan')->name('user.keranjang.simpan');
    Route::post('/keranjang/save', 'user\KeranjangController@save')->name('user.keranjang.save');
    Route::get('/keranjang', 'user\KeranjangController@index')->name('user.keranjang');
    Route::post('/keranjang/update', 'user\KeranjangController@update')->name('user.keranjang.update');
    Route::get('/keranjang/delete/{id}', 'user\KeranjangController@delete')->name('user.keranjang.delete');
    Route::get('/alamat', 'user\AlamatController@index')->name('user.alamat');
    Route::get('/getcity/{id}', 'user\AlamatController@getCity')->name('user.alamat.getCity');
    Route::get('/getsubdistrict/{id}', 'user\AlamatController@getSubdistrict')->name('user.alamat.getSubdistrict');
    Route::post('/alamat/simpan', 'user\AlamatController@simpan')->name('user.alamat.simpan');
    Route::post('/alamat/update/{id}', 'user\AlamatController@update')->name('user.alamat.update');
    Route::get('/alamat/ubah/{id}', 'user\AlamatController@ubah')->name('user.alamat.ubah');
    Route::get('/checkout', 'user\CheckoutController@index')->name('user.checkout');
    Route::post('/order/simpan', 'user\OrderController@simpan')->name('user.order.simpan');
    Route::get('/order/sukses', 'user\OrderController@sukses')->name('user.order.sukses');
    Route::get('/akun/dashboard', 'UserController@usdash')->name('user.usdash');
    // Route::get('/akun/detail', 'UserController@detail')->name('user.detail');
    Route::get('/akundetail/{id}', 'UserController@editcus');
    Route::patch('/updatecus/{id}', 'UserController@updatecus');
    Route::get('/order', 'user\OrderController@index')->name('user.order');
    Route::get('/order/bayar', 'user\OrderController@bayar')->name('user.order.bayar');
    Route::get('/order/proses', 'user\OrderController@proses')->name('user.order.proses');
    Route::get('/order/riwayat', 'user\OrderController@riwayat')->name('user.order.riwayat');
    Route::get('/order/detail/{id}', 'user\OrderController@detail')->name('user.order.detail');
    Route::get('/order/pesananditerima/{id}', 'user\OrderController@pesananditerima')->name('user.order.pesananditerima');
    Route::get('/order/pesanandibatalkan/{id}', 'user\OrderController@pesanandibatalkan')->name('user.order.pesanandibatalkan');
    Route::get('/order/pembayaran/{id}', 'user\OrderController@pembayaran')->name('user.order.pembayaran');
    Route::post('/order/kirimbukti/{id}', 'user\OrderController@kirimbukti')->name('user.order.kirimbukti');
    Route::get('/user/cetak/{id}', 'user\OrderController@cetak')->name('user.cetak');

    Route::get('/cari', 'user\ProdukController@loadData')->name('cari');
});







Route::get('/edit_profile/{id}', 'UserController@edit');
Route::patch('/update_profil/{id}', 'UserController@update');






Route::get('/ongkir', 'OngkirController@index');
Route::get('/ongkir/province/{id}/cities', 'OngkirController@getCities');





Route::group(['middleware' => ['auth', 'checkRole:owner']], function () {

    Route::get('/dashboard_owner', 'DashboardController@indexown');
    Route::get('/admin_owner', 'DashboardController@indexown');
    Route::get('/admin/dataadmin', 'UserController@index')->name('admin.dataadmin');
    Route::get('/admin/tambahadmin', 'UserController@tambah')->name('admin.tambahadmin');
    Route::post('/admin/tambahadmin/store', 'UserController@store')->name('admin.tambahadmin.store');
    Route::get('/admin/deladmin/{id}', 'UserController@delete')->name('admin.delete');

    //laporan
    Route::get('/admin/laporantrx', 'admin\TransaksiController@lapselesai')->name('admin.transaksi.lapselesai');
    Route::get('/admin/laporantrx/tanggal:{tgl}', 'admin\TransaksiController@ambiltgl')->name('admin.transaksi.ambiltgl');
    Route::get('/admin/laporantrx/detail/{id}', 'admin\TransaksiController@lapdetail')->name('admin.transaksi.lapdetail');
    Route::get('/admin/laporan_stok_keluar', 'admin\KurangBahanController@indextgl');
    Route::get('/admin/laporan_stok_keluar/tanggal:{tgl}', 'admin\KurangBahanController@indextglid');
    Route::get('/admin/laporan_stok_masuk', 'admin\BeliBahanController@indextgl');
    Route::get('/admin/laporan_stok_masuk/tanggal:{tgl}', 'admin\BeliBahanController@indextglid');
    Route::get('/admin/laporan_keuangan2', 'admin\TransaksiController@uang')->name('admin.transaksi.uang');
    Route::get('/admin/laporan_keuangan', 'admin\TransaksiController@uang2')->name('admin.transaksi.uang2');
    Route::get('/admin/grafik-customer', 'admin\PelangganController@grafik')->name('admin.grafik-cus');
    Route::get('/admin/product-laris', 'admin\ProductController@prdlaris')->name('admin.prodlaris');
    Route::get('/admin/product-rekom', 'admin\ProductController@prdrekom')->name('admin.prodrekom');
});






Route::group(['middleware' => ['auth', 'checkRole:admincus']], function () {
    Route::get('/admin_admincust', 'DashboardController@indexcust');
    Route::get('/dashboard_admincust', 'DashboardController@indexcust');
    Route::get('/admin/pelanggan/cetak', 'admin\PelangganController@cetak')->name('admin.pelanggan.cetak');
    Route::get('/admin/pelanggan', 'admin\PelangganController@index')->name('admin.pelanggan');
});




Route::group(['middleware' => ['auth', 'checkRole:adminkat']], function () {
    Route::get('/admin_adminkat', 'DashboardController@indexkat');
    Route::get('/dashboard_adminkat', 'DashboardController@indexkat');




    Route::get('/admin/categories', 'admin\CategoriesController@index')->name('admin.categories');
    Route::get('/admin/categories/tambah', 'admin\CategoriesController@tambah')->name('admin.categories.tambah');
    Route::post('/admin/categories/store', 'admin\CategoriesController@store')->name('admin.categories.store');
    Route::post('/admin/categories/update/{id}', 'admin\CategoriesController@update')->name('admin.categories.update');
    Route::get('/admin/categories/edit/{id}', 'admin\CategoriesController@edit')->name('admin.categories.edit');
    Route::get('/admin/categories/delete/{id}', 'admin\CategoriesController@delete')->name('admin.categories.delete');


    Route::get('/admin/tips', 'admin\TipsController@index');
    Route::get('/admin/createtips', 'admin\TipsController@create');
    Route::post('/admin/storetips', 'admin\TipsController@store');
    Route::get('/admin/edittips/{id}', 'admin\TipsController@edit');
    Route::patch('/admin/updatetips/{id}', 'admin\TipsController@update');
    Route::get('/admin/destips:{id}', 'admin\TipsController@destroy');
    Route::get('/admin/deltips/{id}', 'admin\TipsController@delete')->name('tips.delete');


    //produk
    Route::get('/admin/product', 'admin\ProductController@index')->name('admin.product');
    Route::get('/admin/product/tambah', 'admin\ProductController@tambah')->name('admin.product.tambah');
    Route::post('/admin/product/store', 'admin\ProductController@store')->name('admin.product.store');
    Route::get('/admin/product/edit/{id}', 'admin\ProductController@edit')->name('admin.product.edit');
    Route::get('/admin/product/detail/{id}', 'admin\ProductController@detail')->name('admin.product.detail');
    Route::get('/admin/product/stok/{id}', 'admin\ProductController@stok')->name('admin.product.stok');
    Route::get('/admin/product/delete/{id}', 'admin\ProductController@delete')->name('admin.product.delete');
    Route::post('/admin/product/update/{id}', 'admin\ProductController@update')->name('admin.product.update');
    Route::post('/admin/product/updatestok/{id}', 'admin\ProductController@updatestok')->name('admin.product.updatestok');
    Route::get('/admin/product/cetak', 'admin\ProductController@cetak')->name('admin.product.cetak');
});



Route::group(['middleware' => ['auth', 'checkRole:admintrx']], function () {

    Route::get('/admin_admintrx', 'DashboardController@indextrx');
    Route::get('/dashboard_admintrx', 'DashboardController@indextrx');
    Route::get('/admin/rekening', 'admin\RekeningController@index')->name('admin.rekening');
    Route::get('/admin/rekening/edit/{id}', 'admin\RekeningController@edit')->name('admin.rekening.edit');
    Route::get('/admin/rekening/tambah', 'admin\RekeningController@tambah')->name('admin.rekening.tambah');
    Route::post('/admin/rekening/store', 'admin\RekeningController@store')->name('admin.rekening.store');
    Route::get('/admin/rekening/delete/{id}', 'admin\RekeningController@delete')->name('admin.rekening.delete');
    Route::post('/admin/rekening/update/{id}', 'admin\RekeningController@update')->name('admin.rekening.update');


    Route::get('/admin/transaksi', 'admin\TransaksiController@index')->name('admin.transaksi');
    Route::get('/admin/transaksi/perludicek', 'admin\TransaksiController@perludicek')->name('admin.transaksi.perludicek');
    Route::get('/admin/transaksi/perludikirim', 'admin\TransaksiController@perludikirim')->name('admin.transaksi.perludikirim');
    Route::get('/admin/transaksi/dikirim', 'admin\TransaksiController@dikirim')->name('admin.transaksi.dikirim');
    Route::get('/admin/transaksi/detail/{id}', 'admin\TransaksiController@detail')->name('admin.transaksi.detail');
    Route::get('/admin/transaksi/konfirmasi/{id}', 'admin\TransaksiController@konfirmasi')->name('admin.transaksi.konfirmasi');
    Route::get('/admin/transaksi/tidakvalid/{id}', 'admin\TransaksiController@tidakvalid')->name('admin.transaksi.tidakvalid');
    Route::post('/admin/transaksi/inputresi/{id}', 'admin\TransaksiController@inputresi')->name('admin.transaksi.inputresi');
    Route::get('/admin/transaksi/selesai', 'admin\TransaksiController@selesai')->name('admin.transaksi.selesai');
    Route::get('/admin/transaksi/gagal', 'admin\TransaksiController@gagal')->name('admin.transaksi.gagal');
    Route::get('/admin/transaksi/dibatalkan', 'admin\TransaksiController@dibatalkan')->name('admin.transaksi.dibatalkan');
});




Route::group(['middleware' => ['auth', 'checkRole:adminstok']], function () {

    Route::get('/admin_adminstok', 'DashboardController@indexstok');
    Route::get('/dashboard_adminstok', 'DashboardController@indexstok');


    //bahan
    Route::get('/admin/bahan', 'admin\BahanController@index');
    Route::get('/admin/createbahan', 'admin\BahanController@create');
    Route::post('/admin/storebahan', 'admin\BahanController@store');
    Route::get('/admin/editbahan/{id}', 'admin\BahanController@edit');
    Route::patch('/admin/updatebahan/{id}', 'admin\BahanController@update');
    Route::get('/admin/desbahan:{id}', 'admin\BahanController@destroy');
    Route::get('/admin/delbahan/{id}', 'admin\BahanController@delete')->name('bhn.delete');
    Route::get('/admin/bahan/cetak', 'admin\BahanController@cetak')->name('admin.bahan.cetak');

    //stok masuk
    Route::get('/admin/stok_masuk', 'admin\BeliBahanController@index');
    Route::get('/admin/input_stok_masuk', 'admin\BeliBahanController@create');
    Route::get('/admin/createbeli_bahan/{id_bahan}', 'admin\BeliBahanController@createid');
    Route::post('/admin/storebeli_bahan', 'admin\BeliBahanController@store');
    Route::post('/admin/cartbeli', 'admin\BeliBahanController@cartbeli');
    Route::get('/admin/delcart/{id}', 'admin\BeliBahanController@delcartbeli')->name('cartbl.delete');
    Route::get('/admin/delcart', 'admin\BeliBahanController@delcart')->name('crt.delete');
    Route::post('/admin/storebeli_bahan/{id_bahan}', 'admin\BeliBahanController@storeid');
    Route::get('/admin/editbeli_bahan/{id}', 'admin\BeliBahanController@edit');
    Route::patch('/admin/updatebeli_bahan/{id}', 'admin\BeliBahanController@update');
    Route::get('/admin/desbeli_bahan:{id}', 'admin\BeliBahanController@destroy');
    Route::get('/admin/delbeli_bahan/{id}', 'admin\BeliBahanController@delete')->name('blbhn.delete');

    //stok keluar
    Route::get('/admin/kurang_bahan', 'admin\KurangBahanController@index');
    Route::get('/admin/createkurang_bahan', 'admin\KurangBahanController@create');
    Route::get('/admin/createkurang_bahan/{id_bahan}', 'admin\KurangBahanController@createid');
    Route::post('/admin/storekurang_bahan', 'admin\KurangBahanController@store');
    Route::post('/admin/cartkrg', 'admin\KurangBahanController@cartkrg')->name('cartkurang');
    Route::get('/admin/delcartkrg/{id}', 'admin\KurangBahanController@delcartkrg')->name('cartkrg.delete');
    Route::get('/admin/delcartkrg', 'admin\KurangBahanController@delkrg')->name('ckrg.delete');
    Route::post('/admin/storekurang_bahan/{id_bahan}', 'admin\KurangBahanController@storeid');
    Route::get('/admin/editkurang_bahan/{id}', 'admin\KurangBahanController@edit');
    Route::patch('/admin/updatekurang_bahan/{id}', 'admin\KurangBahanController@update');
    Route::get('/admin/deskurang_bahan:{id}', 'admin\KurangBahanController@destroy');
    Route::get('/admin/delkurang_bahan/{id}', 'admin\KurangBahanController@delete')->name('krgbhn.delete');
});
