<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'role' => 'customer',
        'jk' => $faker->randomElement($array = array('laki-laki','perempuan')),
        'tgl_lahir' => $faker->dateTimeBetween($startDate ='-21 years', $endDate = '-17 years',$timezone = null ),
        // 'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'password' => '$10$Or9jZYlLy.fDgCN6TYP8turbfCFH2djA56x2yOc25leOx3QV8psI2', // 12345678
        'remember_token' => Str::random(10),
    ];
});
