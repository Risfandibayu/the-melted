<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text,
        'image' => 'imageproduct/WuazzL4eWTZLfrlfHYo5HeMkgbUAHnv9lLu6tJN9.jpg',
        'price' => $faker->numberBetween(12000,25000),
        'weigth' => $faker->randomElement($array = array(120,250,125,200)),
        'tag'=>'enak',
        'categories_id' => $faker->randomElement($array = array(12,25,13,26)),
        'stok' => $faker->numberBetween(0,25),

    ];
});
