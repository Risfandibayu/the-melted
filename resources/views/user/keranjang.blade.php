@extends('user.app')

@section('bc','Cart')
@section('content')
@include('user.bc')
@if(count($keranjangs) > 0)
<div class="c-content-box c-size-lg">
    
    <div class="container">
        <?php $subtotal = 0;
        $no = 0;
        foreach ($keranjangs as $k) : 
        $no++ ?>



        @if ($k->qty > $k->stok && $k->stok > 0)
        <div class="alert alert-warning alert-dismissible" role="alert">
            Jumlah produk <strong>{{$k->nama_produk}}</strong> melebihi stok yang tersedia saat ini. Mohon klik update keranjang untuk menyesuaikan! 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        @elseif ($k->stok == 0)
        <div class="alert alert-warning alert-dismissible" role="alert">
            {{-- Mohon maaf. Stok produk <strong>{{$k->nama_produk}}</strong> kosong untuk saat ini. Mohon klik update keranjang untuk menyesuaikan!  --}}
            Sorry Melters :( <strong>{{$k->nama_produk}}</strong> product stock is empty for now. Please click update cart to customize!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        @endif
        <?php endforeach; ?>
        @if (session('eror'))
            <div class="alert alert-{{session('alert-type')}} alert-dismissible" role="alert">
                {{session('eror')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        @endif
        <form class="col-md-12" method="post" action="{{ route('user.keranjang.update') }}">
            @csrf
            <div class="c-shop-cart-page-1">
                <div class="row c-cart-table-title">
                    <div class="col-md-2 c-cart-image">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Image</h3>
                    </div>
                    <div class="col-md-5 c-cart-desc">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Product Name</h3>
                    </div>
                    <div class="col-md-1 c-cart-qty">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Qty</h3>
                    </div>
                    <div class="col-md-2 c-cart-price">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Unit Price</h3>
                    </div>
                    <div class="col-md-1 c-cart-total">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Total</h3>
                    </div>
                    <div class="col-md-1 c-cart-remove"></div>
                </div>
                <!-- BEGIN: SHOPPING CART ITEM ROW -->
                <?php $subtotal = 0;
                $no = 0;
                foreach ($keranjangs as $keranjang) : 
                    $no++ ?>
                    <div class="row c-cart-table-row">
                        <h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first">Item {{$no}}</h2>
                        <div class="col-md-2 col-sm-3 col-xs-5 c-cart-image">
                            <img src="{{ asset('storage/'.$keranjang->image) }}" />
                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-7 c-cart-desc">
                            <h3><a href="{{ route('user.produk.detail',['id' =>  $keranjang->prdid]) }}" class="c-font-bold c-theme-link c-font-22 c-font-dark">{{ $keranjang->nama_produk }}</a></h3>
                        </div>
                        <div class="col-md-1 col-sm-3 col-xs-6 c-cart-qty">
                            <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">QTY</p>
                            <div class="c-input-group c-spinner">
                                <input type="hidden" name="id[]" value="{{ $keranjang->id }}">
                                <input type="text" name="qty[]" class="form-control c-item-{{$no}}" id="val" value="{{ $keranjang->qty }}" >
                                <div class="c-input-group-btn-vertical">
                                    <button class="btn btn-default" type="button" data_input="c-item-{{$no}}" data-maximum="{{$keranjang->stok}}"><i class="fa fa-caret-up"></i></button>
                                    <button class="btn btn-default" type="button" data_input="c-item-{{$no}}" min="1"><i class="fa fa-caret-down"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6 c-cart-price">
                            <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Unit Price</p>
                            <p class="c-cart-price c-font-bold">Rp{{ number_format($keranjang->price,2,',','.') }} </p>
                        </div>
                        <div class="col-md-1 col-sm-3 col-xs-6 c-cart-total">
                            <?php
                            $total = $keranjang->price * $keranjang->qty;
                            $subtotal = $subtotal + $total;
                            ?>
                            <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Total</p>
                            <p class="c-cart-price c-font-bold">Rp{{ number_format($total,2,',','.') }}</p>
                        </div>
                        <div class="col-md-1 col-sm-12 c-cart-remove text-right">
                            <a href="{{ route('user.keranjang.delete',['id' => $keranjang->id]) }}" class="c-theme-link c-cart-remove-desktop">×</a>
                            <a href="{{ route('user.keranjang.delete',['id' => $keranjang->id]) }}" class="c-cart-remove-mobile btn c-btn c-btn-md c-btn-square c-btn-red c-btn-border-1x c-font-uppercase">Remove item from Cart</a>
                        </div>
                    </div>
                <?php endforeach; ?>

                <!-- END: SUBTOTAL ITEM ROW -->
                <!-- BEGIN: SUBTOTAL ITEM ROW -->
                <div class="row">
                    <div class="c-cart-subtotal-row">
                        <div class="col-md-2 col-md-offset-9 col-sm-6 col-xs-6 c-cart-subtotal-border">
                            <h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2">Grand Total</h3>
                        </div>
                        <div class="col-md-1 col-sm-6 col-xs-6 c-cart-subtotal-border">
                            <h3 class="c-font-bold c-font-16">Rp{{ number_format($subtotal,0,',','.') }}</h3>
                        </div>
                    </div>
                </div>
                <!-- END: SUBTOTAL ITEM ROW -->
                <div class="c-cart-buttons">
                    <div class="row">
                        <div class="col-lg-3 text-center">
                            <a href="/produk" class="btn c-btn btn-lg c-btn-red c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-l">Continue Shopping</a>
                        </div>
                        @if($cekalamat > 0)
                        <div class="col-lg-5"></div>
                        <div class="col-lg-4 text-center">

                            @if ($cekstok > 0 || $cekqty > 0)
                            <button type="submit" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Update Cart</button>
                            
                            @else
                            <button type="submit" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Update Cart</button>
                            <a href="{{ route('user.checkout') }}" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Checkout</a>
                            @endif
                        </div>
                    </div>
                    <small class="c-cart-float-r text-center ">Click the <strong>Update Cart</strong> button when adding qty to cart</small>
                    {{-- <small class="c-cart-float-r text-center ">Klik tombol <strong>Update Kerajang</strong> ketika menambah qty pada keranjang</small> --}}
                    @else
                    <div class="col-lg-4"></div>
                    <div class="col-lg-5 text-center">
                        <button type="submit" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Update Cart</button>
                        <a href="{{ route('user.alamat') }}" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Set Address</a>
                    </div>
                </div>
                <small class="c-cart-float-r text-center ">Set your address to checkout</small>
                {{-- <small class="c-cart-float-r text-center ">Atur alamat anda untuk melakukan checkout</small> --}}
                @endif
            </div>
    </div>
    </form>
</div>
</div><!-- END: CONTENT/SHOPS/SHOP-CART-1 -->
</div>

@else
<!-- BEGIN: CONTENT/SHOPS/SHOP-CART-EMPTY -->
<div class="c-content-box c-size-lg">
    <div class="container">
        <div class="c-shop-cart-page-1 c-center">
            <i class="fa fa-frown-o c-font-dark c-font-50 c-font-thin "></i>
            <h2 class="c-font-thin c-center">Your Shopping Cart is Empty</h2>
            <a href="/produk" class="btn c-btn btn-lg c-btn-dark c-btn-square c-font-white c-font-bold c-font-uppercase">Continue Shopping</a>
        </div>
    </div>
</div><!-- END: CONTENT/SHOPS/SHOP-CART-EMPTY -->
</div>


@endif
@endsection
@section('style')
<link href="{{asset('shop')}}/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('shop')}}/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
<!-- <link href="{{asset('shop')}}/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css" /> -->
<!-- END: BASE PLUGINS -->
<!-- BEGIN: PAGE STYLES -->
<!-- <link href="{{asset('shop')}}/plugins/ilightbox/css/ilightbox.css" rel="stylesheet" type="text/css" /> -->
@endsection
@section('js')
<!-- <script src="{{asset('shop')}}/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script> -->
<!-- <script src="{{asset('shop')}}/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script> -->
<script src="{{asset('shop')}}/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<!-- <script src="{{asset('shop')}}/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script> -->
<script src="{{asset('shop')}}/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<!-- <script src="{{asset('shop')}}/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script> -->
<!-- <script src="{{asset('shop')}}/plugins/typed/typed.min.js" type="text/javascript"></script> -->
<!-- <script src="{{asset('shop')}}/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script> -->
<!-- <script src="{{asset('shop')}}/plugins/js-cookie/js.cookie.js" type="text/javascript"></script> -->
<!-- <script src="{{asset('shop')}}/plugins/zoom-master/jquery.zoom.min.js" type="text/javascript"></script> -->
@endsection