


@extends('user.app')

@section('content')
@include('user.bc')
@include('user.side')
<div class="c-layout-sidebar-content ">
                <!-- BEGIN: PAGE CONTENT -->
                <!-- BEGIN: CONTENT/SHOPS/SHOP-MY-ADDRESSES-1 -->
    <div class="c-content-title-1">
        <h3 class="c-font-uppercase c-font-bold">My Addresses</h3>
        <div class="c-line-left"></div>
    </div>
    <div class="row c-margin-t-25">
        <div class="col-md-12 col-sm-12 col-xs-12 c-margin-b-20">
            <ul class="list-unstyled">
                <li>{{Auth::user()->name}}</li>
                <li>{{ $alamat[0]->detail }}</li>
                <li>Kecamatan {{ $alamat[0]->kec }}, {{ $alamat[0]->kota }}</li>
                <li>{{ $alamat[0]->prov }}</li>
            </ul>
            <a href="{{ route('user.alamat.ubah',['id' => $alamat[0]->id] ) }}" class="btn c-theme-btn btn-xs"><i class="fa fa-edit"></i> Edit</a>
        </div>
        
    
    </div><!-- END: CONTENT/SHOPS/SHOP-MY-ADDRESSES-1 -->
                <!-- END: PAGE CONTENT -->
                </div>
		</div>
	</div>
@endsection