<!DOCTYPE html>
<!-- 
Theme: JANGO - Ultimate Multipurpose HTML Theme Built With Twitter Bootstrap 3.3.7
Version: 2.0.1
Author: Themehats
Site: http://www.themehats.com
Purchase: http://themeforest.net/item/jango-responsive-multipurpose-html5-template/11987314?ref=themehats
Contact: support@themehats.com
Follow: http://www.twitter.com/themehats
-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>The Melted</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta http-equiv="Content-type" content="text/html; charset=utf-8">
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <!-- <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'> -->
  <!-- <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@700&display=swap" rel="stylesheet"> -->
  <link href="https://fonts.googleapis.com/css2?family=Yusei+Magic&display=swap" rel="stylesheet">
  <link href="{{asset('shop')}}/plugins/socicon/socicon.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/animate/animate.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN: BASE PLUGINS  -->
  <link href="{{asset('shop')}}/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/revo-slider/css/layers.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/revo-slider/css/navigation.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/owl-carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/jquery-ui.css" rel="stylesheet" type="text/css" />
  {{-- <link href="http://code.jquery.com/ui/1.11.4/themes/black-tie/jquery-ui.css"> --}}
  
  <!-- END: BASE PLUGINS -->
  <!-- BEGIN: PAGE STYLES -->
  <!-- <link href="{{asset('shop')}}/plugins/ilightbox/css/ilightbox.css" rel="stylesheet" type="text/css" /> -->
  <!-- END: PAGE STYLES -->
  <!-- BEGIN THEME STYLES -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" /> -->

  @yield('style')
  <link href="{{asset('shop')}}/demos/index/css/plugins.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/demos/index/css/components.css" id="style_components" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/demos/index/css/themes/red2.css" rel="stylesheet" id="style_theme" type="text/css" />
  <link href="{{asset('shop')}}/demos/index/css/custom.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/demos/index/css/toastr.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/select2.css" rel="stylesheet" />
  <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/socicon@3.0.5/css/socicon.min.css"> -->
  <!-- END THEME STYLES -->
  <link rel="stylesheet" href="{{asset('assets/venobox/venobox.css')}}" type="text/css" media="screen" />
  <link rel="shortcut icon" href="{{asset('assets/media/logos/faviconnn.ico')}}" />
  <!-- <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" /> -->
  <style>
    .c-wa {
    color: inherit;
}
.c-wa:hover{
  color: white;
}
  </style>
</head>

<body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">

  <!-- BEGIN: LAYOUT/HEADERS/HEADER-1 -->
  <!-- BEGIN: HEADER -->
  <header class="c-layout-header c-layout-header-4 c-layout-header-default-mobile" data-minimize-offset="80">
    <div class="c-topbar c-topbar-light">
      <div class="container">
        <!-- BEGIN: INLINE NAV -->
        <nav class="c-top-menu c-pull-left">
          <ul class="c-icons c-theme-ul">
            <li><a href="https://wa.me/62895366439266" target="_blank"><i class="socicon-whatsapp mr-5"></i></a></li>
            <li><a href="https://www.instagram.com/themeltedd/" target="_blank"><i class="socicon-instagram mr-5"></i></a></li>
            <!-- <li><a href="#"><i class="fa fa-whatsapp"></i></a></li> -->
            <!-- <li><a href="#"><i class="fa fa-instagram"></i></a></li> -->
            <!-- <li><a href="#"><i class="icon-social-dribbble"></i></a></li> -->
            <li class="hide"><span>Phone: +99890345677</span></li>
          </ul>
        </nav>
        <!-- END: INLINE NAV -->
        <!-- BEGIN: INLINE NAV -->
        <nav class="c-top-menu c-pull-right">
          <ul class="c-links c-theme-ul">
            <li><a onclick="$('html, body').animate({ scrollTop: $(document).height() - $(window).height() }, 1000);">Help</a></li>
            <li class="c-divider">|</li>
            <li><a onclick="$('html, body').animate({ scrollTop: $(document).height() - $(window).height() }, 1000);">Contact</a></li>
          </ul>
          <ul class="c-ext c-theme-ul">
            <li class="c-lang dropdown c-last">
              <a href="#">ID</a>
              <ul class="dropdown-menu pull-right" role="menu">
                <li class="active"><a href="#">Indonesia</a></li>
              </ul>
            </li>
            <li class="c-search hide">
              <!-- BEGIN: QUICK SEARCH -->
              <form action="#">
                <input type="text" name="query" placeholder="search..." value="" class="form-control" autocomplete="off">
                <i class="fa fa-search"></i>
              </form>
              <!-- END: QUICK SEARCH -->
            </li>
          </ul>
        </nav>
        <!-- END: INLINE NAV -->
      </div>
    </div>
    <div class="c-navbar">
      <div class="container">
        <!-- BEGIN: BRAND -->
        <div class="c-navbar-wrapper clearfix">
          <div class="c-brand c-pull-left">
            <a href="/" class="c-logo">
              <img style="margin-top: -20px;margin-bottom: 0px;" src="{{asset('shop')}}/base/img/layout/logos/logo.png" alt="The Melted" class="c-desktop-logo">
              <img src="{{asset('shop')}}/base/img/layout/logos/logo.png" alt="The Melted" class="c-desktop-logo-inverse">
              <img style="width: 70px;" src="{{asset('shop')}}/base/img/layout/logos/logo.png" alt="The Melted" class="c-mobile-logo">
            </a>
            <button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
              <span class="c-line"></span>
              <span class="c-line"></span>
              <span class="c-line"></span>
            </button>
            <button class="c-topbar-toggler" type="button">
              <i class="fa fa-ellipsis-v"></i>
            </button>
            <button class="c-search-toggler" type="button">
              <i class="fa fa-search"></i>
            </button>
            @if(Auth::user() == NULL)

            @else
            <?php
            $user_id = \Auth::user()->id;
            
            $total_keranjang = \DB::table('keranjang')
              ->select(DB::raw('sum(qty) as jumlah'))
              ->where('user_id', $user_id)
              ->first();
            ?>
            @if($total_keranjang == null )

            <button class="c-cart-toggler" type="button">
              <i class="icon-handbag"></i> <span class="c-cart-number c-theme-bg">0</span>
            </button>
            @else
            <button class="c-cart-toggler" type="button">
              <i class="icon-handbag"></i> <span class="c-cart-number c-theme-bg">{{$total_keranjang->jumlah}}</span>
            </button>
            @endif
            @endif
          </div>
          <!-- END: BRAND -->
          <!-- BEGIN: QUICK SEARCH -->
          <form class="c-quick-search" action="{{ route('user.produk.cari') }}" method="get">
            @csrf
            {{-- <input type="text" name="cari" id="cari" placeholder="Type to search..." class="form-control cari" autocomplete="off"> --}}
            {{-- <input type="text" id='employee_search' name="cari"> --}}
            <input id="search" name="cari" type="text" placeholder="Enter Keywords" class="form-control"/>
            
            {{-- <select class="cari form-control" style="width:500px;" name="cari"></select> --}}
            <span class="c-theme-link">&times;</span>
          </form>
          <!-- END: QUICK SEARCH -->
          <!-- BEGIN: HOR NAV -->
          <!-- BEGIN: LAYOUT/HEADERS/MEGA-MENU -->
          <!-- BEGIN: MEGA MENU -->
          <!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
          <nav class="c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold">
            <ul class="nav navbar-nav c-theme-nav">
              <li class="{{ '/' == request()->path() ? 'c-active' : ''}}">
                <a href="/" class="c-link">Home</a>
              </li>
              <li class="{{ 'produk' == request()->path() ? 'c-active' : ''}}">
                <a href="/produk" class="c-link">Products</a>
              </li>
              <li class="{{ 'tips' == request()->path() ? 'c-active' : ''}}">
                <a href="/tips" class="c-link">Tips</a>
              </li>
              <li class="{{ 'aboutus' == request()->path() ? 'c-active' : ''}}">
                <a href="/aboutus" class="c-link">About us</a>
              </li>











              @if (Route::has('login'))
              @auth

              <li class="c-search-toggler-wrapper">
                <a href="#" class="c-btn-icon c-search-toggler"><i class="fa fa-search"></i></a>
              </li>


              <?php
              $user_id = \Auth::user()->id;
              $total_keranjang = \DB::table('keranjang')
                ->select(DB::raw('sum(qty) as jumlah'))
                ->where('user_id', $user_id)
                ->first();
              ?>

              @if($total_keranjang->jumlah == null)

              <li class="c-cart-toggler-wrapper">
                <a class="c-btn-icon c-cart-toggler"><i class="icon-handbag c-cart-icon"></i> <span class="c-cart-number c-theme-bg">0</span></a>
              </li>
              @else

              <li class="c-cart-toggler-wrapper">
                <a class="c-btn-icon c-cart-toggler"><i class="icon-handbag c-cart-icon"></i> <span class="c-cart-number c-theme-bg">{{ $total_keranjang->jumlah }}</span></a>
              </li>
              @endif

              <?php
              $user_id = \Auth::user()->id;
              $total_order = \DB::table('order')
                ->select(DB::raw('count(id) as jumlah'))
                ->where('user_id', $user_id)
                // ->where('status_order_id', '!=', 5)
                ->where('status_order_id', '!=', 6)
                ->where('status_order_id', '=', 1)
                ->first();
              ?>
              <li class="c-cart-toggler-wrapper">
                <!-- <a href="{{ route('user.order.bayar') }}" class="c-btn-icon"><i class="icon-notebook c-cart-icon"></i> <span class="c-cart-number c-theme-bg">{{ $total_order->jumlah }}</span></a> -->
              </li>




              <li>
                <a href="/akun/dashboard" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold"><i class="icon-user"></i> {{Auth::user()->name}}</a>
              </li>

              @else

              <li class="c-search-toggler-wrapper">
                <a href="#" class="c-btn-icon c-search-toggler"><i class="fa fa-search"></i></a>
              </li>
              <li class="c-cart-toggler-wrapper">
                <a href="/keranjang" class="c-btn-icon c-cart-toggler"><i class="icon-handbag c-cart-icon"></i></a>
              </li>

              <li>
                <!-- <a href="#" data-toggle="modal" data-target="#login-form" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold"><i class="icon-user"></i> Sign In | Register</a> -->
                <a href="{{ route('login') }}" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold"><i class="icon-user"></i> Sign In | Register</a>
              </li>




              @endauth
              @endif












            </ul>
          </nav>
          <!-- END: MEGA MENU -->
          <!-- END: LAYOUT/HEADERS/MEGA-MENU -->
          <!-- END: HOR NAV -->
        </div>
        <!-- BEGIN: LAYOUT/HEADERS/QUICK-CART -->
        <!-- BEGIN: CART MENU -->

        <?php
        if (Auth::user() == NULL) {
          $jmlbrg = DB::table('keranjang')
            ->select(DB::raw('sum(qty) as prod'))
            ->first();

          $total = DB::table('keranjang')
            ->join('products', 'products.id', '=', 'keranjang.products_id')
            ->select(DB::raw('sum(products.price * keranjang.qty) as tot'))
            ->groupby('keranjang.user_id')
            ->first();

          $ker = DB::table('keranjang')
            ->join('products', 'products.id', '=', 'keranjang.products_id')
            ->select('products.*', 'keranjang.qty as qty', 'keranjang.*')

            ->get();
        } else {



          $id_user = Auth::user()->id;
          $jmlbrg = DB::table('keranjang')
            ->select(DB::raw('sum(qty) as prod'))
            ->where('user_id', $id_user)
            ->first();

          $total = DB::table('keranjang')
            ->join('products', 'products.id', '=', 'keranjang.products_id')
            ->select(DB::raw('sum(products.price * keranjang.qty) as tot'))
            ->where('keranjang.user_id', '=', $id_user)
            ->groupby('keranjang.user_id')
            ->first();

          $ker = DB::table('keranjang')
            ->join('products', 'products.id', '=', 'keranjang.products_id')
            ->select('products.*', 'products.id as prdid', db::raw('sum(keranjang.qty) as qt'), 'keranjang.*')
            ->where('keranjang.user_id', '=', $id_user)
            ->groupby('keranjang.products_id')
            ->get();

        ?>

          <div class="c-cart-menu" >
            <div class="c-cart-menu-title">
              <p class="c-cart-menu-float-l c-font-sbold">{{$jmlbrg->prod}} item(s)</p>
              @if(count($ker) > 0)
              <p class="c-cart-menu-float-r c-theme-font c-font-sbold">Rp{{number_format($total->tot,'0',',','.')}}</p>
              @else
              @endif
            </div>

            @if(count($ker) == 0 || count($ker) == NULL)
            <div class="c-cart-menu-footer">
              <i class="fa fa-frown-o c-font-dark c-font-50 c-font-thin "></i>
              <h2 class="c-font-thin c-center">Your Shopping Cart is Empty</h2>
              <a href="/produk" class="btn c-btn btn-lg c-btn-dark c-btn-square c-font-white c-font-bold c-font-uppercase">Continue Shopping</a>
            </div>
            @else
            <ul class="c-cart-menu-items">
              @foreach($ker as $ker)
              <li>
                <div class="c-cart-menu-close">
                  <a href="{{ route('user.keranjang.delete',['id' => $ker->id]) }}" class="c-theme-link">×</a>
                </div>
                <img src="{{ asset('storage/'.$ker->image) }}" style="height: 100%;" />
                <div class="c-cart-menu-content">
                  <p>{{$ker->qt}} x <span class="c-item-price c-theme-font">Rp{{number_format($ker->price,0,',','.')}}</span></p>
                  <a href="{{ route('user.produk.detail',['id' =>  $ker->prdid]) }}" class="c-item-name c-font-sbold">{{$ker->name}}</a>
                </div>
              </li>
              @endforeach
            </ul>

            <div class="c-cart-menu-footer">
              <?php
              $cekalamat = DB::table('alamat')->where('user_id', $id_user)->get();
              ?>
              @if(count($cekalamat) == 0)
              <div class="row c-margin-b-15">
                <a href="/keranjang" class="btn btn-md c-btn c-btn-square c-btn-grey-3 c-font-white c-font-bold c-center c-font-uppercase">View Cart</a>

                <a href="{{ route('user.alamat') }}" class=" btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Set Address</a>
              </div>
              <small>Atur Alamat untuk melakukan checkout</small>
              @else
              <a href="/keranjang" class="btn btn-md c-btn c-btn-square c-btn-grey-3 c-font-white c-font-bold c-center c-font-uppercase">View Cart</a>


              <?php
              $cekstok = db::table('keranjang')
              ->select('*')
              ->join('products','products.id','=','keranjang.products_id')
              ->whereRaw('keranjang.qty > products.stok')
              ->where('keranjang.user_id',$id_user)
              ->count();

              $cekqty = db::table('keranjang')
              ->select('*')
              ->join('products','products.id','=','keranjang.products_id')
              ->where('products.stok','=',0)
              ->where('keranjang.user_id',$id_user)
              ->count();
              ?>
                @if ($cekstok > 0 || $cekqty > 0)

                @else
                <a href="/checkout" class="btn btn-md c-btn c-btn-square c-theme-btn c-font-white c-font-bold c-center c-font-uppercase">Checkout</a>
                @endif
              @endif
            </div>
          </div>
          @endif

        <?php
        }
        ?>
        <!-- END: CART MENU -->


        <!-- END: LAYOUT/HEADERS/QUICK-CART -->
      </div>
    </div>
  </header>

  <!-- END: HEADER -->
  <!-- END: LAYOUT/HEADERS/HEADER-1 -->

  <!-- BEGIN: CONTENT/USER/FORGET-PASSWORD-FORM -->
  <div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content c-square">
        <div class="modal-header c-no-border">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
          <p>To recover your password please fill in your email address</p>
          <form>
            <div class="form-group">
              <label for="forget-email" class="hide">Email</label>
              <input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email">
            </div>
            <div class="form-group">
              <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
              <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
            </div>
          </form>
        </div>
        <div class="modal-footer c-no-border">
          <span class="c-text-account">Don't Have An Account Yet ?</span>
          <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
        </div>
      </div>
    </div>
  </div><!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
  <!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
  <div class="modal fade c-content-login-form" id="signup-form" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content c-square">
        <div class="modal-header c-no-border">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <h3 class="c-font-24 c-font-sbold">Create An Account</h3>
          <p>Please fill in below form to create an account with us</p>
          <form>
            <div class="form-group">
              <label for="signup-email" class="hide">Email</label>
              <input type="email" class="form-control input-lg c-square" id="signup-email" placeholder="Email">
            </div>
            <div class="form-group">
              <label for="signup-username" class="hide">Username</label>
              <input type="email" class="form-control input-lg c-square" id="signup-username" placeholder="Username">
            </div>
            <div class="form-group">
              <label for="signup-fullname" class="hide">Fullname</label>
              <input type="email" class="form-control input-lg c-square" id="signup-fullname" placeholder="Fullname">
            </div>
            <div class="form-group">
              <label for="signup-country" class="hide">Country</label>
              <select class="form-control input-lg c-square" id="signup-country">
                <option value="1">Country</option>
              </select>
            </div>
            <div class="form-group">
              <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Signup</button>
              <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div><!-- END: CONTENT/USER/SIGNUP-FORM -->
  <!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
  <div class="modal fade c-content-login-form" id="login-form" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content c-square">
        <div class="modal-header c-no-border">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
          <p>Let's make today a great day!</p>
          <form>
            <div class="form-group">
              <label for="login-email" class="hide">Email</label>
              <input type="email" class="form-control input-lg c-square" id="login-email" placeholder="Email">
            </div>
            <div class="form-group">
              <label for="login-password" class="hide">Password</label>
              <input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
            </div>
            <div class="form-group">
              <div class="c-checkbox">
                <input type="checkbox" id="login-rememberme" class="c-check">
                <label for="login-rememberme" class="c-font-thin c-font-17">
                  <span></span>
                  <span class="check"></span>
                  <span class="box"></span>
                  Remember Me
                </label>
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
              <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
            </div>
            <div class="clearfix">
              <div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
                <span>or signup with</span>
              </div>
              <ul class="c-content-list-adjusted">
                <li>
                  <a class="btn btn-block c-btn-square btn-social btn-twitter">
                    <i class="fa fa-twitter"></i>
                    Twitter
                  </a>
                </li>
                <li>
                  <a class="btn btn-block c-btn-square btn-social btn-facebook">
                    <i class="fa fa-facebook"></i>
                    Facebook
                  </a>
                </li>
                <li>
                  <a class="btn btn-block c-btn-square btn-social btn-google">
                    <i class="fa fa-google"></i>
                    Google
                  </a>
                </li>
              </ul>
            </div>
          </form>
        </div>
        <div class="modal-footer c-no-border">
          <span class="c-text-account">Don't Have An Account Yet ?</span>
          <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
        </div>
      </div>
    </div>
  </div><!-- END: CONTENT/USER/LOGIN-FORM -->

  <!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
  <nav class="c-layout-quick-sidebar">
    <div class="c-header">
      <button type="button" class="c-link c-close">
        <i class="icon-login"></i>
      </button>
    </div>
    <div class="c-content">
      <li class="c-cart-toggler-wrapper">
        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();" class="c-btn-icon"><i class="icon-logout c-cart-icon"></i> Log out</a>
      </li>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
      </form>
    </div>
  </nav><!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->

  @yield('content')

  <!-- BEGIN: LAYOUT/FOOTERS/FOOTER-5 -->
  <!-- BEGIN: LAYOUT/FOOTERS/FOOTER-1 -->
  <a name="footer" id="footer"></a>
  <footer class="c-layout-footer c-layout-footer-1">
    <div class="c-prefooter">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="c-logo">
              <a href="#">
                <img src="{{asset('assets/media/logos/logu.png')}}" width="200px" alt="Jango">
                <!-- <img src="{{asset('shop')}}/base/img/layout/logos/logo-1.png" alt="Jango"> -->
              </a>
            </div>
            <p class="c-about">
              You support a small business and this package so happy to see you.
              We hope you enjoy your purchase.
              Enjoy your cake and have a nice day
            </p>


          </div>
          <div class="col-md-2">
            <div class="c-feedback">
              <h3>Quick Links</h3>
              <div class="c-links c-contact">
                <ul class=" c-nav">

                  <li><a href="/akun/dashboard">My Account</a></li>
                  <hr width="150px" style="border-top: 1px solid #c0c9d3;margin-top: 5px;
margin-bottom: 5px;">
                  <li><a href="/">Home</a></li>
                  <!-- <li><a href="#">About</a></li>
                <li><a href="#">Portfolio</a></li> -->
                  <li><a href="/produk">Products</a></li>
                  <li><a href="/tips">Tips</a></li>
                  <li><a href="/aboutus">About Us</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="c-feedback">
              <h3>Find Us</h3>

              <p class="c-contact c-links">

                <a href="https://goo.gl/maps/PAS85kneRtcP77kk9" target="_blank" class="c-link c-wa"><i class="icon-pointer" style="margin-right: 10px;color:#32c5d2;"></i>Jl. Imam Bonjol No.46, RT.04/RW.04<br>
                <i class="" style="margin-right: 28px;"></i>Ngadirejo, Kec. Kota Kediri<br>
                <i class="" style="margin-right: 28px;"></i>Kota Kediri, Jawa Timur <br>
                <i class="" style="margin-right: 28px;"></i>64122<br></a>
                <a href="tel:+62895366439266" class="c-link c-wa"><i class="icon-call-end" style="margin-right: 10px;color:#32c5d2;"></i>+62 895-3664-39266<br></a>
                <a href="https://wa.me/62895366439266" target="_blank" class="c-link c-wa"><i class="socicon-whatsapp" style="margin-right: 10px;color:#32c5d2;"></i>+62 895-3664-39266<br></a>
                <a href="https://www.instagram.com/themeltedd/" target="_blank" class="c-link c-wa"><i class="socicon-instagram" style="margin-right: 10px;color:#32c5d2;"></i>@themeltedd</a>
              </p>

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="c-postfooter">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <p class="c-copyright c-font-oswald c-font-14">
              Copyright &copy; The Melted Inc.
            </p>
          </div>
          <div class="col-md-6 col-sm-6">
            <ul class="c-socials">
              <li><a href="https://wa.me/62895366439266" target="_blank"><i class="socicon-whatsapp"></i></a></li>
              <li><a href="https://www.instagram.com/themeltedd/"  target="_blank"><i class="socicon-instagram"></i></a></li>
              <!-- <li><a href="#"><i class="icon-social-facebook"></i></a></li>
              <li><a href="#"><i class="icon-social-youtube"></i></a></li>
              <li><a href="#"><i class="icon-social-dribbble"></i></a></li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer><!-- END: LAYOUT/FOOTERS/FOOTER-1 -->

  <!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
  <div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
  </div><!-- END: LAYOUT/FOOTERS/GO2TOP -->

  <!-- BEGIN: LAYOUT/BASE/BOTTOM -->
  <!-- BEGIN: CORE PLUGINS -->
  <!--[if lt IE 9]>
	<script src="{{asset('shop')}}/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
  <script src="{{asset('shop')}}/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/jquery-migrate.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/jquery.easing.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/reveal-animate/wow.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/demos/default/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript"></script>

  <!-- END: CORE PLUGINS -->

  <!-- BEGIN: LAYOUT PLUGINS -->

  <!-- END: LAYOUT PLUGINS -->
  <!-- BEGIN: THEME SCRIPTS -->
  <script src="{{asset('shop')}}/base/js/components.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/base/js/components-shop.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/base/js/app.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/base/js/toastr.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/typed/typed.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
  @yield('js')
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script> -->
  @if(Session::has('message'))
  <script>
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": true,
      "progressBar": true,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };

    var type = "{{Session::get('alert-type','success')}}"
    switch (type) {
      case 'addcart':
        toastr.success("{{Session::get('message')}}<br> <a href='/keranjang' class='btn text-center btn-md c-btn c-btn-square c-btn-black c-font-white c-font-bold c-center c-font-uppercase'>View Cart</a>").css('box-sizing', '');
        // Swal.fire("Berhasil","{{Session::get('message')}}","success");
        // Swal.fire("Good job!", "You clicked the button!", "success");
        break;
      case 'fview':
        toastr.error("{{Session::get('message')}}<br> <a href='/keranjang' class='btn text-center btn-md c-btn c-btn-square c-btn-black c-font-white c-font-bold c-center c-font-uppercase'>View Cart</a>").css('box-sizing', '');
        // Swal.fire("Berhasil","{{Session::get('message')}}","success");
        // Swal.fire("Good job!", "You clicked the button!", "success");
        break;
      case 'fail':
        toastr.error("{{Session::get('message')}}");
        // Swal.fire("Berhasil","{{Session::get('message')}}","success");
        // Swal.fire("Good job!", "You clicked the button!", "success");
        break;
      case 'delcart':
        toastr.info("{{Session::get('message')}}<br> <a href='/keranjang' class='btn btn-md text-center c-btn c-btn-square c-btn-black  c-font-white c-font-bold c-center c-font-uppercase'>View Cart</a>").css('box-sizing', '');
        // Swal.fire("Berhasil","{{Session::get('message')}}","success");
        // Swal.fire("Good job!", "You clicked the button!", "success");
        break;
      case 'con':
        toastr.error("{{Session::get('message')}}");
        // Swal.fire("Berhasil","{{Session::get('message')}}","success");
        // Swal.fire("Good job!", "You clicked the button!", "success");
        break;
      case 'no':
        toastr.error("{{Session::get('message')}}");
        // Swal.fire("Berhasil","{{Session::get('message')}}","success");
        // Swal.fire("Good job!", "You clicked the button!", "success");
        break;
      case 'conkos':
        toastr.error("{{Session::get('message')}}");
        // Swal.fire("Berhasil","{{Session::get('message')}}","success");
        // Swal.fire("Good job!", "You clicked the button!", "success");
        break;
      case 'upprof':
        toastr.success("{{Session::get('message')}}");
        // Swal.fire("Berhasil","{{Session::get('message')}}","success");
        // Swal.fire("Good job!", "You clicked the button!", "success");
        break;
      case 'ordsuk':
        toastr.success("{{Session::get('message')}}");
        // Swal.fire("Berhasil","{{Session::get('message')}}","success");
        // Swal.fire("Good job!", "You clicked the button!", "success");
        break;
      case 'bisa':
        toastr.success("{{Session::get('message')}}");
        // Swal.fire("Berhasil","{{Session::get('message')}}","success");
        // Swal.fire("Good job!", "You clicked the button!", "success");
        break;
      case 'warn':
        toastr.warning("{{Session::get('message')}}");
        // Swal.fire("Berhasil","{{Session::get('message')}}","success");
        // Swal.fire("Good job!", "You clicked the button!", "success");
        break;

    }
  </script>
  @endif





  <script>
    $(document).ready(function() {
      App.init(); // init core    
    });
  </script>
  <!-- END: THEME SCRIPTS -->

  <!-- BEGIN: PAGE SCRIPTS -->
  <script>
    $(document).ready(function() {

      var slider = $('.c-layout-revo-slider .tp-banner');
      var cont = $('.c-layout-revo-slider .tp-banner-container');
      var height = (App.getViewPort().width < App.getBreakpoint('md') ? 400 : 620);

      var api = slider.show().revolution({
        sliderType: "standard",
        sliderLayout: "fullwidth",
        delay: 15000,
        autoHeight: 'off',
        gridheight: 500,

        navigation: {
          keyboardNavigation: "off",
          keyboard_direction: "horizontal",
          mouseScrollNavigation: "off",
          onHoverStop: "on",
          arrows: {
            style: "circle",
            enable: true,
            hide_onmobile: false,
            hide_onleave: false,
            tmp: '',
            left: {
              h_align: "left",
              v_align: "center",
              h_offset: 30,
              v_offset: 0
            },
            right: {
              h_align: "right",
              v_align: "center",
              h_offset: 30,
              v_offset: 0
            }
          },
          touch: {
            touchenabled: "on",
            swipe_threshold: 75,
            swipe_min_touches: 1,
            swipe_direction: "horizontal",
            drag_block_vertical: false
          },
        },
        viewPort: {
          enable: true,
          outof: "pause",
          visible_area: "80%"
        },

        shadow: 0,

        spinner: "spinner2",

        disableProgressBar: "on",

        fullScreenOffsetContainer: '.tp-banner-container',

        hideThumbsOnMobile: "on",
        hideNavDelayOnMobile: 1500,
        hideBulletsOnMobile: "on",
        hideArrowsOnMobile: "on",
        hideThumbsUnderResolution: 0,

      });
    }); //ready	
  </script>
  <!-- END: PAGE SCRIPTS -->
  <!-- END: LAYOUT/BASE/BOTTOM -->



  {{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> --}}

<script type="text/javascript">
  $('.cari').select2({
    placeholder: 'Cari...',
    ajax: {
      url: '/cari',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.name,
              id: item.id
            }
          })
        };
      },
      cache: true
    }
  });

</script>
<script type="text/javascript">

  // CSRF Token
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $(document).ready(function(){

    $( "#employee_search" ).autocomplete({
      source: function( request, response ) {
        // Fetch data
        $.ajax({
          url:"{{route('cari')}}",
          type: 'post',
          dataType: "json",
          data: {
             _token: CSRF_TOKEN,
             search: request.term
          },
          success: function( data ) {
             response( data );
          }
        });
      },
      select: function (event, ui) {
         // Set selection
         $('#employee_search').val(ui.item.label); // display the selected text
         $('#employeeid').val(ui.item.value); // save selected id to input
         return false;
      }
    });

  });
  </script>
<script src="{{asset('shop')}}/select2.js"></script>
<script src="{{asset('shop')}}/jquery-ui.js"></script>
<script>
  $( "#search" ).autocomplete({
      source: "livesearch",
      minLength: 0,
      select:function(event,ui) { 
        $('#search').val(ui.item.link);
      }
  });
  
  </script>
  
</body>

</html>