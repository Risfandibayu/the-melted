@extends('user.app')

@section('bc','Products')
@section('content')
<!-- BEGIN: PAGE CONTAINER -->

    <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-3 -->
    @include('user.bc')
    <div class="container">
        <div class="c-layout-sidebar-menu c-theme ">
            <!-- BEGIN: LAYOUT/SIDEBARS/SIDEBAR-MENU-1 -->
            <div class="c-sidebar-menu-toggler">
                <h3 class="c-title c-font-uppercase c-font-bold">Navigation</h3>
                <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                    <span class="c-line"></span>
                    <span class="c-line"></span>
                    <span class="c-line"></span>
                </a>
            </div>
            <div class="c-content-ver-nav">
                <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
                    <h3 class="c-font-bold c-font-uppercase">Categories</h3>
                    <div class="c-line-left c-theme-bg"></div>
                </div>
                <ul class="c-menu c-arrow-dot1 c-theme">
                    @foreach($categories as $categori)
                    <li><a href="{{ route('user.kategori',['id' => $categori->id]) }}">{{ $categori->name }} ({{ $categori->jumlah }})</a></li>
                    @endforeach
                </ul>
            </div>


        </div>
        <div class="c-layout-sidebar-content ">
            <!-- BEGIN: PAGE CONTENT -->
            
            <div class="c-margin-t-20"></div>

            <!-- BEGIN: CONTENT/SHOPS/SHOP-2-7 -->
            <div class="c-bs-grid-small-space">
                @foreach($produks->chunk(4) as $produk)
                <div class="row">
                    @foreach($produk as $produk)
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                @if($produk->stok == 0)
                                <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Out of Stock</div>
                                @else
                                @endif
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url({{ asset('storage/'.$produk->image) }});"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-18 c-font-slim">{{ $produk->name }}</p>
                                <p class="c-price c-font-16 c-font-slim">Rp{{ number_format($produk->price,0,',','.') }} &nbsp;
                                    <span class="c-font-16 c-font-line-through c-font-red">Rp{{ number_format(($produk->price + 2000),0,',','.') }}</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                @if($produk->stok == 0)
                                <div class="btn-group c-border-left c-border-top" style="width: 203%" role="group">
                                    <a  href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Detail</a>
                                </div>
                                <div class="btn-group c-border-top" role="group">
                                @else
                                <div class="btn-group c-border-top" role="group">
                                    <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Detail</a>
                                </div>
                                <div class="btn-group c-border-left c-border-top" role="group">
                                    @endif
                                    <form action="{{ route('user.keranjang.save') }}" method="post">
                                        <input type="hidden" type="text" name="qty" value="1">
                                        <input type="hidden" value="{{ $produk->stok }}" id="sisastok">
                                        @csrf
                                        @if(Route::has('login'))
                                        @auth
                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                        @endauth
                                        @endif
                                        <input type="hidden" name="products_id" value="{{ $produk->id }}">
                                @if($produk->stok == 0)
                                
                                @else

                                <button type="submit" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" href="#">Cart</button>
                                @endif
                                        <!-- <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a> -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach

            </div><!-- END: CONTENT/SHOPS/SHOP-2-7 -->

            <div class="c-margin-t-20"></div>
<!-- 
            <ul class="c-content-pagination c-square c-theme pull-right">
					<li class="c-prev"><a href="#"><i class="fa fa-angle-left"></i></a></li>
					<li><a href="#">1</a></li>
					<li class="c-active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li class="c-next"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                </ul> -->
            <!-- END: PAGE CONTENT -->
            <ul class="c-content-pagination c-square c-theme pull-right">
                {{ $produks->links() }}
            </ul> <!-- END: PAGE CONTENT -->
        </div>
    </div>
</div>
@endsection
@section('style')

@endsection
@section('js')

@endsection
