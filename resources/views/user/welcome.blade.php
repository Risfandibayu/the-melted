@extends('user.app')
@section('content')
<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page">
    <!-- BEGIN: PAGE CONTENT -->
    <!-- BEGIN: LAYOUT/SLIDERS/REVO-SLIDER-4 -->
    <section class="c-layout-revo-slider c-layout-revo-slider-4" dir="ltr">
        <div class="tp-banner-container c-theme">
            <div class="tp-banner rev_slider" data-version="5.0">
                <ul>
                    <!--BEGIN: SLIDE #1 -->
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                        <img alt="" src="{{asset('shop')}}/base/img/content/s/cook1.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                        <div class="tp-caption customin customout" data-x="center" data-y="center" data-hoffset="" data-voffset="-50" data-speed="500" data-start="1000" data-transform_idle="o:1;" data-transform_in="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;" data-transform_out="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
                            <h3 class="c-main-title-circle c-font-48 c-font-bold c-font-center c-font-uppercase c-font-white c-block">
                                MAKE YOUR MOOD<br>BE BETTER
                            </h3>
                        </div>
                        <div class="tp-caption lft" data-x="center" data-y="center" data-voffset="110" data-speed="900" data-start="2000" data-transform_idle="o:1;" data-transform_in="x:100;y:100;rX:120;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;" data-transform_out="x:100;y:100;rX:120;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;">
                            <a href="/produk" class="c-action-btn btn btn-lg c-btn-square c-theme-btn c-btn-bold c-btn-uppercase">Shop Now</a>
                        </div>
                    </li>
                    <!--END -->

                    <!--BEGIN: SLIDE #2 -->
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                        <img alt="" src="{{asset('shop')}}/base/img/content/s/slider-3.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                        <div class="tp-caption customin customout" data-x="center" data-y="center" data-hoffset="" data-voffset="-50" data-speed="500" data-start="1000" data-transform_idle="o:1;" data-transform_in="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;" data-transform_out="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:600;e:Back.easeInOut;" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
                            <h3 class="c-main-title-circle c-font-48 c-font-bold c-font-center c-font-uppercase c-font-white c-block">
                                EVERY DAY IS HAPPYDAY
                            </h3>
                        </div>
                        <div class="tp-caption lft" data-x="center" data-y="center" data-voffset="110" data-speed="900" data-start="2000" data-transform_idle="o:1;" data-transform_in="x:100;y:100;rX:120;scaleX:0.75;scaleY:0.75;o:0;s:900;e:Back.easeInOut;" data-transform_out="x:100;y:100;rX:120;scaleX:0.75;scaleY:0.75;o:0;s:900;e:Back.easeInOut;">
                            <a href="/produk" class="c-action-btn btn btn-lg c-btn-square c-theme-btn c-btn-bold c-btn-uppercase">Shop Now</a>
                        </div>
                    </li>
                    <!--END -->

                </ul>
            </div>
        </div>
    </section><!-- END: LAYOUT/SLIDERS/REVO-SLIDER-4 -->


    @if(!empty(Auth::user()))

    <?php
    // $rekom = DB::table(DB::raw('products a, log_customers b'))
    //     ->select('a.*')
    //     ->where('b.id_customer', \Auth::user()->id)
    //     ->where('a.name', 'like', DB::raw("CONCAT('%',b.log,'%')"))
    //     ->orwhere('b.id_customer', \Auth::user()->id)
    //     ->where('a.tag', 'like', DB::raw("CONCAT('%',b.log,'%')"))
    //     ->groupBy('a.name')
    //     ->orderBy(db::raw('max(b.created_at)'), 'DESC')
    //     ->orderBy(db::raw('count(a.name)'), 'DESC')
    //     ->get()
    if (\Auth::user()->jk == NULL && \Auth::user()->tgl_lahir == NULL) {
        $us = \Auth::user()->id;
        $rekom = DB::select("SELECT a.* FROM products a, `order` b, detail_order c, users d
        WHERE b.id = c.order_id
        and d.id = b.user_id
        and c.product_id = a.id
        and d.role = 'customer'
        and a.stok != 0
        GROUP BY a.name
        ORDER BY count(a.name) desc");
    }elseif (\Auth::user()->jk == NULL) {
        $us = \Auth::user()->id;
        $rekom = DB::select("SELECT a.* FROM products a, `order` b, detail_order c, users d
        WHERE b.id = c.order_id
        and d.id = b.user_id
        and c.product_id = a.id
        and a.stok != 0
        and d.role = 'customer'
        and (DATEDIFF((SELECT tgl_lahir from users WHERE id = $us),d.tgl_lahir) <= 1095)
        GROUP BY a.name
        ORDER BY count(a.name) desc");
    }elseif (\Auth::user()->tgl_lahir == NULL) {
        $us = \Auth::user()->id;
        $rekom = DB::select("SELECT a.* FROM products a, `order` b, detail_order c, users d
        WHERE b.id = c.order_id
        and d.id = b.user_id
        and c.product_id = a.id
        and a.stok != 0
        and d.role = 'customer'
        and d.jk = (SELECT jk from users WHERE id = $us)
        GROUP BY a.name
        ORDER BY count(a.name) desc");
    }else{
        $us = \Auth::user()->id;
        $rekom = DB::select("SELECT a.* FROM products a, `order` b, detail_order c, users d
        WHERE b.id = c.order_id
        and d.id = b.user_id
        and c.product_id = a.id
        and a.stok != 0
        and d.role = 'customer'
        and d.jk = (SELECT jk from users WHERE id = $us)
        and (DATEDIFF((SELECT tgl_lahir from users WHERE id = $us),d.tgl_lahir) <= 1095)
        GROUP BY a.name
        ORDER BY count(a.name) desc");
    }
    ?>
    <!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->
        @if(!empty(count($rekom)))
        <div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space">
            <div class="container">
                <div class="c-content-title-4">
                    <h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-white">Product Recomendation</span></h3>
                </div>
                <div class="row">
                    <div data-slider="owl">
                        <div class="owl-carousel owl-theme c-theme owl-small-space c-owl-nav-center" data-rtl="false" data-items="4" data-slide-speed="8000">





                            @foreach($rekom as $produk)
                            <div class="item">
                                <div class="c-content-product-2 c-bg-white c-border">
                                    <div class="c-content-overlay">
                                        @if($produk->stok == 0)
                                <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Out of Stock</div>
                                @else
                                @endif
                                        <div class="c-overlay-wrapper" >
                                            <div class="c-overlay-content" >
                                                <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                            </div>
                                        </div>
                                        <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url({{ asset('storage/'.$produk->image) }});"></div>
                                    </div>
                                    <div class="c-info">
                                        <p class="c-title c-font-18 c-font-slim">{{ $produk->name }}</p>
                                        <p class="c-price c-font-16 c-font-slim">Rp {{ number_format($produk->price,0,',','.') }} &nbsp;
                                            <span class="c-font-16 c-font-line-through c-font-red">Rp {{ number_format(($produk->price + 2000),0,',','.') }}</span>
                                        </p>
                                    </div>
                                    <div class="btn-group btn-group-justified" role="group">
                                        @if($produk->stok == 0)
                                        <div class="btn-group c-border-left c-border-top" style="width: 203%" role="group">
                                            <a  href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Detail</a>
                                        </div>
                                        <div class="btn-group c-border-top" role="group">
                                        @else
                                        <div class="btn-group c-border-top" role="group">
                                            <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Detail</a>
                                        </div>
                                        <div class="btn-group c-border-left c-border-top" role="group">
                                            @endif
                                            <form action="{{ route('user.keranjang.save') }}" method="post">
                                                <input type="hidden" type="text" name="qty" value="1">
                                                <input type="hidden" value="{{ $produk->stok }}" id="sisastok">
                                                @csrf
                                                @if(Route::has('login'))
                                                @auth
                                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                                @endauth
                                                @endif
                                                <input type="hidden" name="products_id" value="{{ $produk->id }}">
                                        @if($produk->stok == 0)
                                        
                                        @else

                                        <button type="submit" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" href="#">Cart</button>
                                        @endif
                                                <!-- <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a> -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div><!-- END: CONTENT/SHOPS/SHOP-2-2 -->
        @else
        @endif
    @else
    @endif
    <div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space">
        <div class="container">
            <div class="c-content-title-4">
                <h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-white">Most Popular</span></h3>
            </div>
            <div class="row">
                <div data-slider="owl">
                    <div class="owl-carousel owl-theme c-theme owl-small-space c-owl-nav-center" data-rtl="false" data-items="4" data-slide-speed="8000">
                        @foreach($produks as $produk)
                        <div class="item">
                            <div class="c-content-product-2 c-bg-white c-border">
                                <div class="c-content-overlay">
                                    @if($produk->stok == 0)
                                <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Out of Stock</div>
                                @else
                                @endif
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                        </div>
                                    </div>
                                    <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url({{ asset('storage/'.$produk->image) }});"></div>
                                </div>
                                <div class="c-info">
                                    <p class="c-title c-font-18 c-font-slim">{{ $produk->name }}</p>
                                    <p class="c-price c-font-16 c-font-slim">Rp {{ number_format($produk->price,0,',','.') }} &nbsp;
                                        <span class="c-font-16 c-font-line-through c-font-red">Rp {{ number_format(($produk->price + 2000),0,',','.') }}</span>
                                    </p>
                                </div>
                                <div class="btn-group btn-group-justified" role="group">
                                    @if($produk->stok == 0)
                                    <div class="btn-group c-border-left c-border-top" style="width: 203%" role="group">
                                        <a  href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Detail</a>
                                    </div>
                                    <div class="btn-group c-border-top" role="group">
                                    @else
                                    <div class="btn-group c-border-top" role="group">
                                        <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Detail</a>
                                    </div>
                                    <div class="btn-group c-border-left c-border-top" role="group">
                                        @endif
                                        <form action="{{ route('user.keranjang.save') }}" method="post">
                                            <input type="hidden" type="text" name="qty" value="1">
                                            <input type="hidden" value="{{ $produk->stok }}" id="sisastok">
                                            @csrf
                                            @if(Route::has('login'))
                                            @auth
                                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                            @endauth
                                            @endif
                                            <input type="hidden" name="products_id" value="{{ $produk->id }}">
                                    @if($produk->stok == 0)
                                    
                                    @else

                                    <button type="submit" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" href="#">Cart</button>
                                    @endif
                                            <!-- <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a> -->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/SHOPS/SHOP-2-2 -->


</div>
<!-- END: PAGE CONTAINER -->
@endsection
@section('style')

@endsection
@section('js')
<script src="{{asset('shop')}}/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/revo-slider/js/extensions/revolution.extension.video.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>

@endsection