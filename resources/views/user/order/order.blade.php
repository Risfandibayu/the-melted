

@extends('user.app')

@section('style')
<link href="{{asset('cust')}}/vendors/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="{{asset('cust')}}/vendors/nice-select/css/nice-select.css" rel="stylesheet">
@endsection
@section('content')
<!--================End Main Header Area =================-->
<section style="margin-top: 150px;" class="banner_area">
    <div class="container">
        <div class="banner_text">
            <h3>Produk</h3>
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="#">Produk</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->

<!--================Product Area =================-->
<section class="product_area p_100">
    <div class="container">
        <div class="row product_inner_row">
            <div class="col-lg-9">

                <div class="row product_item_inner">
                <table class="table table-bordered">
            <thead>
                <tr>
                <th class="product-thumbnail">Invoice</th>
                <th class="product-name">Total</th>
                <th class="product-price">Status</th>
                <th class="product-quantity" width="20%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order as $o)
                <tr>
                    <td>{{ $o->invoice }}</td>
                    <td>{{ $o->subtotal + $o->biaya_cod }}</td>
                    <td>{{ $o->name }}</td>
                    <td>
                    <a href="{{ route('user.order.pembayaran',['id' => $o->id]) }}" class="btn btn-success">Bayar</a>
                    <a href="{{ route('user.order.pesanandibatalkan',['id' => $o->id]) }}" onclick="return confirm('Yakin ingin membatalkan pesanan')" class="btn btn-danger">Batalkan</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            </table>

            <table class="table table-bordered">
            <thead>
                <tr>
                <th class="product-thumbnail">Invoice</th>
                <th class="product-name">Total</th>
                <th class="product-price">Status</th>
                <th class="product-quantity" width="20%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($dicek as $o)
                <tr>
                    <td>{{ $o->invoice }}</td>
                    <td>{{ $o->subtotal + $o->biaya_cod }}</td>
                    <td>
                        @if($o->name == 'Perlu Di Cek')
                        Dalam pengecekan pembayaran
                        @elseif($o->name == 'Telah Di Bayar')
                        Pesanan dalam proses
                        @else
                        {{ $o->name }}
                        @endif
                    </td>
                    <td>
                    <a href="{{ route('user.order.detail',['id' => $o->id]) }}" class="btn btn-success">Detail</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            </table>

            <table class="table table-bordered">
            <thead>
                <tr>
                <th class="product-thumbnail">Invoice</th>
                <th class="product-name">Total</th>
                <th class="product-price">Status</th>
                <th class="product-quantity" width="20%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($histori as $o)
                <tr>
                    <td>{{ $o->invoice }}</td>
                    <td>{{ $o->subtotal + $o->biaya_cod }}</td>
                    <td>{{ $o->name }}</td>
                    <td>
                    <a href="{{ route('user.order.detail',['id' => $o->id]) }}" class="btn btn-success">Detail</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            </table>

                </div>
            </div>
            <div class="col-lg-3">
                <div class="product_left_sidebar">
                    @include('user.aside')

                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Product Area =================-->
@endsection