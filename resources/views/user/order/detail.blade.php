@extends('user.app')
@section('js')
<script src="{{asset('shop')}}/base/js/sweetalert.js"></script>
<script>
    $(".batalbtn").click(function(e) {
        id = e.target.dataset.id;
        swal({
            title: "Apakah anda yakin?",
            text: "Pesanan akan di batalkan",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, Hapus",
            buttons: ["Cancel", "Ya, Batalkan"],
        }).then((result) => {
            if (result) {
                swal(
                    "Berhasil!",
                    "Pesanan di batalkan",
                    "success"
                );
                $(`#batal${id}`).submit();
            } else {

            }
        });
    });
    $(".terimabtn").click(function(e) {
        id = e.target.dataset.id;
        swal({
            title: "Apakah anda yakin sudah menerima pesanan?",
            text: "Pesanan akan di konfirmasi penerimaan",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, Hapus",
            buttons: ["Cancel", "Ya, Di terima"],
        }).then((result) => {
            if (result) {
                swal(
                    "Berhasil!",
                    "Pesanan Selesai",
                    "success"
                );
                $(`#terima${id}`).submit();
            } else {

            }
        });
    });
</script>
@endsection


@section('bc','Order Detail')
@section('content')
@include('user.bc')
<!-- BEGIN: PAGE CONTENT -->
<?php
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
?>
<div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
    <div class="container">
        <div class="c-shop-order-complete-1 c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Order Detail</h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            

            @if($order->status == 'Perlu Di Cek')
            <div class="" style="background-color: black">
                <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                    <i class="fa fa-circle-o-notch fa-spin"></i> Order In Progress.
                </p>
            </div>
            @elseif($order->status == 'Telah Di Bayar')
            <div class="" style="background-color: black">
                <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                    <i class="fa fa-circle-o-notch fa-spin"></i> Order In Progress.
                </p>
            </div>
            @elseif($order->status == 'Belum Bayar')
            <div class="" style="background-color: #ddb412;">
                <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                    <i class="fa fa-credit-card"></i> Orders Must Pay.
                </p>
            </div>
            @elseif($order->status == 'Pesanan Dalam Pengiriman')
            <div class="" style="background-color: black">
                <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                    <i class="fa fa-truck"></i> Orders In Delivery.
                </p>
            </div>
            @elseif($order->status == 'Pesanan Di Batalkan')
            <div class="c-theme-bg">
                <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                    <i class="fa fa-close"></i> Order Canceled.
                </p>
            </div>
            @elseif($order->status == 'Pesanan Gagal')
            <div class="c-theme-bg">
                <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                    <i class="fa  fa-ban"></i> Order Fail.
                </p>
            </div>
            @elseif($order->status == 'Pesanan Telah Sampai')
            <div class="" style="background-color: #2daa2d">
                <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                    <i class="fa fa-check"></i> Order Received.
                </p>
            </div>
            @elseif($order->status == 'Bukti Bayar Tidak Valid')
            <div class="" style="background-color: #ddb412;">
                <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                    <i class="fa fa-exclamation"></i> Proof Of Payment Invalid. Please Re-upload Proof Of Payment
                </p>
            </div>
            @endif


            <!-- BEGIN: ORDER SUMMARY -->
            <div class="row c-center">
                <ul class="c-list-inline list-inline">
                    <li>
                        <h3>Order Invoice</h3>
                        <p>#{{ $order->invoice }}</p>
                    </li>
                    <li>
                        <h3>Date Order</h3>
                        <p>{{ tgl_indo($order->tgl) }}</p>
                    </li>
                    <!-- <li>
                        <h3>Status</h3>
                        <p>@if($order->status == 'Perlu Di Cek')
                            Dalam pengecekan pembayaran
                            @elseif($order->status == 'Telah Di Bayar')
                            Pesanan dalam proses
                            @else
                            {{ $order->status }}
                            @endif
                        </p>
                    </li> -->
                    <li>
                        <h3>Total Payable</h3>
                        <p>Rp{{ number_format($order->subtotal + $order->biaya_cod,0,',','.') }}</p>
                    </li>
                    <li>
                        <h3>Payment Method</h3>
                        <p>@if($order->metode_pembayaran == 'trf')
                            Bank Transfer 
                            @else
                            COD
                            @endif</p>
                    </li>
                    @if ($order->status == 'Pesanan Dalam Pengiriman')
                    <li>
                        <h3>Receipt Number</h3>
                        <p>{{ $order->no_resi}}</p>
                    </li>

                    <li>
                        <h3>Delivery Service</h3>
                        <p>JNE</p>
                    </li>
                    @endif
                    
                </ul>
            </div>
            <!-- END: ORDER SUMMARY -->
            <!-- BEGIN: BANK DETAILS -->

            <!-- END: BANK DETAILS -->
            <!-- BEGIN: ORDER DETAILS -->
            <div class="c-order-details">
                <div class="c-border-bottom hidden-sm hidden-xs">
                    <div class="row">
                        <div class="col-md-3">
                            <h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Product</h3>
                        </div>
                        <div class="col-md-5">
                            <h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Description</h3>
                        </div>
                        <div class="col-md-2">
                            <h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Unit Price</h3>
                        </div>
                        <div class="col-md-2">
                            <h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Total</h3>
                        </div>
                    </div>
                </div>
                <!-- BEGIN: PRODUCT ITEM ROW -->
                @foreach($detail as $o)


                <div class="c-border-bottom c-row-item">
                    <div class="row">
                        <div class="col-md-3 col-sm-12 c-image">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="{{ route('user.produk.detail',['id' =>  $o->prid]) }}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-top-center c-overlay-object" data-height="height">
                                    <img width="100%" class="img-responsive" src="{{ asset('storage/'.$o->image) }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-8">
                            <ul class="c-list list-unstyled">
                                <li class="c-margin-b-25"><a href="{{ route('user.produk.detail',['id' =>  $o->prid]) }}" class="c-font-bold c-font-22 c-theme-link">{{ $o->nama_produk }}</a></li>
                                <li>Quantity: x {{ $o->qt }}</li>
                            </ul>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Unit Price</p>
                            <p class="c-font-sbold c-font-uppercase c-font-18">Rp{{number_format($o->price,0,',','.') }}</p>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Total</p>
                            <p class="c-font-sbold c-font-18">Rp{{ number_format($o->qt * $o->price,0,',','.') }}</p>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- END: PRODUCT ITEM ROW -->
                <!-- BEGIN: PRODUCT ITEM ROW -->
                <!-- END: PRODUCT ITEM ROW -->
                <div class="c-row-item c-row-total c-right">
                    <ul class="c-list list-unstyled">
                        <li>
                            <h3 class="c-font-regular c-font-22">Subtotal : &nbsp;
                                <span class="c-font-dark c-font-bold c-font-22">Rp{{ number_format($o->subtotal - $o->ongkir,0,',','.')}}</span>
                            </h3>
                        </li>
                        <li>
                            <h3 class="c-font-regular c-font-22">Ongkir : &nbsp;
                                <span class="c-font-dark c-font-bold c-font-22">Rp{{number_format($o->ongkir,0,',','.')}}</span>
                            </h3>
                        </li>
                        <li>
                            <h3 class="c-font-regular c-font-22">Grand Total : &nbsp;
                                <span class="c-font-dark c-font-bold c-font-22">Rp{{number_format($o->subtotal,0,',','.')}}</span>
                            </h3>
                        </li>
                        <hr>
                        <li>
                            <div class="row">
                            <a class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r" href="{{ route('user.cetak',['id' => $order->id]) }}" class="btn pink_more kec">Cetak Nota</a>
                            @if($order->status_order_id == 4)
                            <a data-id="{{$order->id}}" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r terimabtn">Pesanan Di Terima</a>
                            <a href="{{ url()->previous() }}" class="btn c-btn btn-lg btn-primary c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r" id="back">Back</a><br>
                            <small>Jika pesanan belum datang harap jangan tekan tombol pesanan di terima</small>
                            <form action="{{ route('user.order.pesananditerima',['id' => $order->id]) }}}" id="terima{{$order->id}}" method="GET">
                                @csrf
                                @method('post')
                            </form>
                            @elseif($order->status_order_id == 1 || $order->status_order_id == 8)
                            <a class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r" href="{{ route('user.order.pembayaran',['id' => $order->id]) }}" class="btn pink_more kec">Pay</a>
                            <a data-id="{{$order->id}}" id="kec3" id="batal" class="btn c-btn btn-lg c-btn-black c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r batalbtn">
                                Cancel Order
                            </a>
                            <a href="{{ url()->previous() }}" class="btn c-btn btn-lg btn-primary c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r " id="back">Back</a>
                            <form action="{{ route('user.order.pesanandibatalkan',['id' => $order->id]) }}" id="batal{{$order->id}}" method="GET">
                                @csrf
                                @method('post')
                            </form>

                            @else
                            <a href="{{ url()->previous() }}" class="btn btn-primary btn c-btn btn-lg c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r" id="back">Back</a>
                            @endif
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END: ORDER DETAILS -->
            <!-- BEGIN: CUSTOMER DETAILS -->

            <!-- END: CUSTOMER DETAILS -->
        </div>
    </div>
</div>
<!-- END: PAGE CONTENT -->
</div>
@endsection