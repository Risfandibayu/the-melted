@extends('user.app')

@section('bc','New Order')
@section('js')
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script src="{{asset('shop')}}/base/js/sweetalert.js"></script>
<script>
    $(".batalbtn").click(function(e) {
        id = e.target.dataset.id;
        swal({
            title: "Apakah anda yakin?",
            text: "Pesanan akan di batalkan",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, Hapus",
            buttons: ["Cancel", "Ya, Batalkan"],
        }).then((result) => {
            if (result) {
                swal(
                    "Berhasil!",
                    "Pesanan di batalkan",
                    "success"
                );
                $(`#batal${id}`).submit();
            } else {

            }
        });
    });
</script>
@endsection
@section('content')
@include('user.bc')
@include('user.side')
<?php
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
?>
<div class="c-layout-sidebar-content ">
    <!-- BEGIN: PAGE CONTENT -->
    <!-- BEGIN: CONTENT/SHOPS/SHOP-ORDER-HISTORY -->
    @if(count($order) == 0)
    <div class="c-content-title-1">
        <h3 class="c-font-uppercase c-font-bold">New Order</h3>
        <div class="c-shop-cart-page-1 c-center">
                <i class="fa fa-frown-o c-font-dark c-font-50 c-font-thin "></i>
                <h2 class="c-font-thin c-center">No orders have been made yet.</h2>
                <a href="/produk" class="btn c-btn btn-lg c-btn-dark c-btn-square c-font-white c-font-bold c-font-uppercase">Continue Shopping</a>
            </div>
    </div>
    @else
    <div class="c-content-title-1">
        <h3 class="c-font-uppercase c-font-bold">New Order</h3>
        {{-- <p><small>Lakukan pembayaran maksimal selang <strong>2 hari</strong> dari tanggal pemesanan</small></p> --}}
        <p><small>Please make payment a maximum of <strong>2 days</strong> from the time of order</small></p>
        <div class="c-line-left"></div>
    </div>
    




    @foreach($order as $o)
    @if($o->item == 0)
    <div class="row c-margin-b-40">
        <div class="c-content-product-2 c-bg-white">
            <div class="col-md-4 text-center">
                <div class="c-content-overlay">
                    <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url({{ asset('storage/'.$o->prdim) }});"></div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="c-info-list text-right">

                    <h3 class="c-title c-font-bold c-font-22 c-font-dark">
                        <div class="row c-margin-b-10">
                            <div class="col-sm-5 col-md-5 col-xs-5 text-left">
                                <p class="c-font-18 c-font-thin c-margin-b-5">Order #: {{$o->inv}}</p>
                                <p class="c-order-date c-font-14 c-font-thin c-theme-font">{{tgl_indo($o->tgl)}}</p>
                            </div>
                            <div class="col-sm-7 col-md-7 col-xs-7 text-right">
                                <p class="c-font-20 bg-primary mr-2">
                                {{-- @if ($o->status == 'Belum Bayar')
                                    To Pay
                                @else
                                    Invalid proof of payment
                                @endif --}}
                                {{$o->status}}
                                
                                </p>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-7 col-sm-7 col-xs-7 text-left">

                                <a class="c-theme-link" href="{{ route('user.produk.detail',['id' =>  $o->prdid]) }}">{{$o->prd}}</a>
                            </div>
                            <div class="col-lg-5 col-md-5 col-xs-5 text-right">
                                <span class="c-font-thin c-font-20"> x {{$o->prdqty}}</span>
                                <p class="c-price c-font-26 c-font-thin">Rp{{number_format($o->tot,0,',','.')}}</p>
                            </div>
                        </div>
                    </h3>



                    <a href="{{ route('user.order.detail',['id' => $o->id]) }}" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">View Detail</a>
                    <a href="{{ route('user.order.pembayaran',['id' => $o->id]) }}" class="btn c-btn btn-lg btn-primary c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Pay</a>
                    <a data-id="{{$o->id}}" class="batalbtn btn c-btn btn-lg c-btn-black c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Cancel</a>

                    <form action="{{ route('user.order.pesanandibatalkan',['id' => $o->id]) }}" id="batal{{$o->id}}" method="GET">
                        @csrf
                        @method('post')
                    </form>
                </div>
            </div>
        </div>
    </div>
    <hr>
    @else
    <div class="row c-margin-b-40">
        <div class="c-content-product-2 c-bg-white">
            <div class="col-md-4 text-center">
                <div class="c-content-overlay">
                    <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url({{ asset('storage/'.$o->prdim) }});"></div>
                    <a href="{{ route('user.order.detail',['id' => $o->id]) }}" class="col-md-12 col-sm-12 col-xs-12" style="border: 1px solid;">And {{$o->item}} item(s) other</a>
                </div>
            </div>
            <div class="col-md-8">
                <div class="c-info-list text-right">

                    <h3 class="c-title c-font-bold c-font-22 c-font-dark">
                        <div class="row c-margin-b-10">
                            <div class="col-sm-5 col-md-5 col-xs-5 text-left">
                                <p class="c-font-18 c-font-thin c-margin-b-5">Order #: {{$o->inv}}</p>
                                <p class="c-order-date c-font-14 c-font-thin c-theme-font">{{tgl_indo($o->tgl)}}</p>
                            </div>
                            <div class="col-sm-7 col-md-7 col-xs-7 text-right">
                                <p class="c-font-20 bg-primary mr-2">
                                    {{-- @if ($o->status == 'Belum Bayar')
                                    To Pay
                                    @else
                                        Invalid proof of payment
                                    @endif     --}}
                                    {{$o->status}}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-7 col-sm-7 col-xs-7 text-left">

                                <a class="c-theme-link" href="{{ route('user.produk.detail',['id' =>  $o->prdid]) }}">{{$o->prd}}</a>
                            </div>
                            <div class="col-lg-5 col-md-5 col-xs-5 text-right">

                                <a href="{{ route('user.order.detail',['id' => $o->id]) }}" class="c-font-thin c-font-20"> And {{$o->item}} item(s) other</a>
                                <p class="c-price c-font-26 c-font-thin">Rp{{number_format($o->tot,0,',','.')}}</p>
                            </div>
                        </div>
                    </h3>



                    <a href="{{ route('user.order.detail',['id' => $o->id]) }}" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">View Detail</a>
                    <a href="{{ route('user.order.pembayaran',['id' => $o->id]) }}" class="btn c-btn btn-lg btn-primary c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Pay</a>
                    <a data-id="{{$o->id}}" class="batalbtn btn c-btn btn-lg c-btn-black c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Cancel</a>

                    <form action="{{ route('user.order.pesanandibatalkan',['id' => $o->id]) }}" id="batal{{$o->id}}" method="GET">
                        @csrf
                        @method('post')
                    </form>
                </div>
            </div>
        </div>
    </div>
    <hr>
    @endif
    @endforeach

    @endif




</div>
</div>
@endsection