<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Example 1</title>
  <link rel="stylesheet" href="assets/laporan/style.css" media="all" />
</head>

<body>

  <?php
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
?>

  
  <header class="clearfix">

    <div id="project">
      <div>The Melted </div>
      <div>Make Your Mood Be Better</div>
      <div>Instragram : @themeltedd</div>
      <div>Whatsapp : +62 895-3664-39266</div>
    </div>
    <div id="logo">
      <img src="assets/laporan/asdd.jpg">
    </div>
  </header>
  <h2>INVOICE</h2>
  <main>
    <!-- BEGIN: ORDER SUMMARY -->
    

    <table>
        <thead>
            <tr>
                <th>Order invoice</th>
                <th>Date Order</th>
                <th>Total Payable</th>
                <th>Payment Method</th>
            </tr>
            <tr>
                <td style="text-align: center">#{{ $order->invoice }}</td>
                <td style="text-align: center">{{ tgl_indo($order->tgl) }}</td>
                <td style="text-align: center">Rp {{ number_format($order->subtotal + $order->biaya_cod,0,',','.') }}</td>
                <td style="text-align: center">@if($order->metode_pembayaran == 'trf')
                    Bank Transfer 
                    @else
                    COD
                    @endif</td>
            </tr>
            <tr>
                <th colspan="2">Product</th>
                <th>Unit Price</th>
                <th>Total</th>
            </tr>
            @foreach($detail as $o)
            <tr>
                <td colspan="2">{{ $o->nama_produk }} 
                <br> Quantity: x {{ $o->qt }}</td>
                <td>Rp {{number_format($o->price,0,',','.') }}</td>
                <td>Rp {{ number_format($o->qt * $o->price,0,',','.') }}</td>
            </tr>
            @endforeach
            
            <tr>
                <td></td>
                <td></td>
                <td>Subtotal :</td>
                <td>Rp {{ number_format($o->subtotal - $o->ongkir,0,',','.')}}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>Ongkir :</td>
                <td>Rp {{number_format($o->ongkir,0,',','.')}}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>Grand Total :</td>
                <td>Rp {{number_format($o->subtotal,0,',','.')}}</td>
            </tr>
        </thead>
    </table>

    
</body>

</html>