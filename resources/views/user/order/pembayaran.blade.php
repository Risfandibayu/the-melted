@extends('user.app')

@section('bc','Checkout Success')
@section('style')
<link rel="stylesheet" href="{{asset('assets/venobox/venobox.css')}}" type="text/css" media="screen" />
@endsection
@section('js')
<script>
    $(document).ready(function() {
      $('.venobox').venobox({
        framewidth: '370px', // default: ''
        titleattr: 'data-title', // default: 'title'
        numeratio: true, // default: false
        infinigall: true, // default: false
        spinner:'three-bounce',
      });
    });
  </script>
@endsection
@section('content')
@include('user.bc')
<!-- BEGIN: PAGE CONTENT -->
<div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
    <div class="container">
        <div class="c-shop-order-complete-1 c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">upload proof of payment</h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <div class="row product_inner_row">
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <div class="row text-center c-margin-b-20">
                        @foreach($rekening as $key)
                        <div class="col-sm-3 col-xs-6" style="margin-bottom: 2px;">
                            <div class="card text-white bg-primary mb-3 " style="padding: 20px;height: 190px;">
                                <div class="card-header">{{ $key->bank_name }}</div>
                                <div class="card-body c-margin-t-30">
                                    <h2 class="c-margin-b-20" style="color: white;font-family: Arial, Helvetica, sans-serif;"> {{ $key->no_rekening }}</h2>
                                    <p class="card-text">A.n. {{ $key->atas_nama }}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row  mb-4">

                        <div class="col-md-12 text-center">
                            Transfer Sebesar <strong>Rp{{ number_format($order->subtotal,0,',','.') }}</strong> Ke Nomor Rekening Di Atas.
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <form action="{{ route('user.order.kirimbukti',['id' => $order->id ]) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-8 text-center">
                                            <label for="">Select file proof of payment</label>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-8">

                                            <input style="height: 44px;line-height: 2;" type="file" name="bukti_pembayaran" id="" class="form-control" required>
                                        </div>
                                        <div class="col-lg-4 text-center">

                                            <button type="submit" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r ">Submit</button>
                                            <a href="{{ url()->previous() }}" class="btn c-btn btn-lg btn-primary c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r " id="back">Back</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="col-lg-1"></div>
            </div>
        </div>
    </div>
</div>
<!-- END: PAGE CONTENT -->
</div>
@endsection