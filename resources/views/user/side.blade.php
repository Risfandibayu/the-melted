<div class="container">
    <div class="c-layout-sidebar-menu c-theme ">
        <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
        <div class="c-sidebar-menu-toggler">
            <h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
            <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                <span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
            </a>
        </div>

        <ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
            <li class="c-dropdown c-open">
                <a href="javascript:;" class="c-toggler">My Profile<span class="c-arrow"></span></a>
                <ul class="c-dropdown-menu">
                    <li class="{{ 'akun/dashboard' == request()->path() ? 'c-active' : ''}}">
                        <a href="/akun/dashboard">My Dashboard</a>
                    </li>
                    <li class="{{ 'akundetail/'.Auth::user()->id == request()->path() ? 'c-active' : ''}}">
                        <a href="/akundetail/{{Auth::user()->id}}">Edit Profile</a>
                    </li>
                    <li class="{{ 'order/bayar' == request()->path() ? 'c-active' : ''}}">
                        <a href="/order/bayar">New Order</a>
                    </li>
                    <li class="{{ 'order/proses' == request()->path() ? 'c-active' : ''}}">
                        <a href="/order/proses">Order in Progress</a>
                    </li>
                    <li class="{{ 'order/riwayat' == request()->path() ? 'c-active' : ''}}">
                        <a href="/order/riwayat">Order History</a>
                    </li>
                    <?php

                    use Illuminate\Support\Facades\Auth;

                    $alamat = DB::table('alamat')
                        ->select('*')
                        ->where('user_id', Auth::user()->id)
                        ->first();
                    ?>

                    @if($alamat == NULL)
                    <li class="{{ 'alamat' == request()->path() ? 'c-active' : ''}}">
                        <a href="/alamat">My Addresses</a>
                    </li>
                    @else
                    <li class="{{ 'alamat' == request()->path() || 'alamat/ubah/'.$alamat->id == request()->path() ? 'c-active' : ''}}">
                        <a href="/alamat">My Addresses</a>
                    </li>
                    @endif

                    <li class="">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">Log out</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </li>
        </ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
    </div>