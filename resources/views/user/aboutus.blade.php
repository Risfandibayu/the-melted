@extends('user.app')





@section('bc','About Us')
@section('content')
@include('user.bc')
<div class="c-content-box c-size-md c-bg-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 wow animated fadeInLeft">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-font-uppercase c-font-bold">About us</h3>
                    <div class="c-line-left c-theme-bg"></div>
                </div>
                <!-- End-->
                <p>The Melted was founded in 2020. The melted takes place in the city of Kediri. The initial goal was just for fun but unexpectedly the shop got bigger and was known by many people. the purpose of fad has now turned into making bread not only to be enjoyed alone, but so that many people can taste it.
                   </p>
                <ul class="c-content-list-1 c-theme  c-font-uppercase">
                    <li>Improve Taste Quality</li>
                    <li>Maintain High Quality Ingredients</li>
                    <li>Taste Innovation</li>
                </ul>
                <p>
                    You support a small business and this package so happy to see you.
                    We hope you enjoy your purchase.
                    Enjoy your cake and have a nice day
                    </p>
            </div>
            <div class="col-sm-6 wow animated fadeInRight">
                <div class="c-content-client-logos-1">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1">
                        <h3 class="c-font-uppercase c-font-bold">Our Address</h3>
                        <div class="c-line-left c-theme-bg"></div>
                    </div>
                    <!-- End-->
                    <div >
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d359.6171812162037!2d112.02647841813712!3d-7.813361275645677!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e78576506362609%3A0x3b20ac049fed093c!2sThe%20Melted!5e0!3m2!1sid!2sid!4v1614089296561!5m2!1sid!2sid" width="100%" height="300px" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/MISC/ABOUT-1 -->



<!-- END: PAGE CONTENT -->
</div>
@endsection
@section('style')
<link href="{{asset('shop')}}/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css" />
  <!-- END: BASE PLUGINS -->
  <!-- BEGIN: PAGE STYLES -->
  <!-- <link href="{{asset('shop')}}/plugins/ilightbox/css/ilightbox.css" rel="stylesheet" type="text/css" /> -->
@endsection
@section('js')
<script src="{{asset('shop')}}/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/typed/typed.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/zoom-master/jquery.zoom.min.js" type="text/javascript"></script>
@endsection