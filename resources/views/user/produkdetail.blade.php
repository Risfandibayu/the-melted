@extends('user.app')





@section('bc','Produk Details')
@section('content')
@include('user.bc')
<div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
    <div class="container">
        <div class="c-shop-product-details-2">
            <div class="row">
                <div class="col-md-6">
                    <div class="c-product-gallery">
                        <div class="c-product-gallery-content">
                            <div class="c-zoom">
                                <img src="{{ asset('storage/'.$produk->image) }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="c-product-meta">
                        <div class="c-content-title-1" style="width: 500px;">
                            <h3 class="c-font-uppercase c-font-bold">{{ $produk->name }}</h3>
                            <div class="c-line-left"></div>
                        </div>
                        
                        <div class="c-product-price">Rp{{ number_format($produk->price,0,',','.') }}</div>
                        <div class="c-product-short-desc">
                            <?php
                            $text = $produk->description;
                            $strip = strip_tags(htmlspecialchars_decode(stripcslashes($text)), '<a>');
                            echo substr($strip, 0, 150);
                            if (strlen(trim($produk->description)) > 10) echo "..."; ?>
                            <br>
                            <a style="cursor: pointer; " onclick="$('html, body').animate({ scrollTop: $(document).height() / 3 }, 1000);"><u>Read more</u></a>
                        </div>
                        <div class="c-product-short-desc">
                            Category : <strong>{{ $produk->nama_kategori}}</strong> <br>
                            Tags : <strong>{{ $produk->tag}}</strong> <br>
                            @if($produk->stok == 0)

                            @else
                            Stock : <strong>{{ $produk->stok}}</strong>
                            @endif
                        </div>
                        
                        @if($produk->stok == 0)
                        <strong class="c-font-red c-font-20"> Sorry, the product stock is empty <i class="fa fa-frown-o c-font-red c-font-bold "></i> </strong>
                        <br>
                        <br>
                        <strong class="c-font-green"> Stay tuned for the updates, melters <i class="fa fa-heart c-font-green c-font-bold "></i> <i class="fa fa-smile-o c-font-green c-font-bold "></i></strong>
                        <br>
                        or try our other products <i class="fa fa-hand-o-down "></i>
                        <br>
                        <br>
                        <a href="/produk" class="btn c-btn btn-lg c-btn-red c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-l">Other Products</a>
                        @else
                        <div class="c-product-add-cart c-margin-t-20">
                            <form action="{{ route('user.keranjang.simpan') }}" method="post">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="c-input-group c-spinner">
                                            <p class="c-product-meta-label c-product-margin-2 c-font-uppercase c-font-bold">QTY:</p>
                                            <input type="text" name="qty" class="form-control c-item-1" value="1">
                                            <div class="c-input-group-btn-vertical">


                                            @if(Auth::user() == NULL)
                                                <button class="btn btn-default" type="button" data-maximum={{$produk->stok}} data_input="c-item-1"><i class="fa fa-caret-up" ></i></button>
                                            @else
                                                @if($crt)
                                                    @if ($produk->stok - $crt->qty == 0 )
                                                    <button class="btn btn-default" type="button" data-maximum={{$produk->stok - $crt->qty}} data_input="c-item-1" disabled><i class="fa fa-caret-up" ></i></button>
                                                    @else
                                                    <button class="btn btn-default" type="button" data-maximum={{$produk->stok - $crt->qty}} data_input="c-item-1"><i class="fa fa-caret-up" ></i></button>
                                                    @endif
                                                @else
                                                <button class="btn btn-default" type="button" data-maximum={{$produk->stok}} data_input="c-item-1"><i class="fa fa-caret-up" ></i></button>
                                                @endif
                                            @endif


                                                <button class="btn btn-default" type="button" data_input="c-item-1"><i class="fa fa-caret-down"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="{{ $produk->stok }}" id="sisastok">
                                    @csrf
                                    @if(Route::has('login'))
                                    @auth
                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                    @endauth
                                    @endif
                                    <input type="hidden" name="products_id" value="{{ $produk->id }}">
                                    
                                    @if($produk->stok == 0)
                                        <div class="col-sm-12 col-xs-12 c-margin-t-20">
                                            
                                            <button type="submit" class="btn c-btn btn-lg c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase" disabled>Add to Cart</button>
                                            <br>
                                            <small style="font-size: 10px">!Stok produk kosong</small>
                                        </div>
                                    @elseif($crt)
                                        @if ($produk->stok - $crt->qty == 0)
                                            <div class="col-sm-12 col-xs-12 c-margin-t-20">
                                                
                                                <button type="submit" class="btn c-btn btn-lg c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase" disabled>Add to Cart</button>
                                                <br>
                                                <small style="font-size: 10px">!Anda sudah memasukan produk ini dengan maksimal stok yang tersedia</small>
                                            </div>
                                        @else
                                        <div class="col-sm-12 col-xs-12 c-margin-t-20">
                                            
                                            <button type="submit" class="btn c-btn btn-lg c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase" >Add to Cart</button>
                                        </div>
                                        @endif
                                        
                                    @else
                                        <div class="col-sm-12 col-xs-12 c-margin-t-20">
                                            
                                            <button type="submit" class="btn c-btn btn-lg c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase" >Add to Cart</button>
                                        </div>
                                    @endif
                                    
                                </div>
                            </form>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-PRODUCT-TAB-1 -->
<div id="des" class="c-content-box c-size-md c-no-padding">
    <div class="c-shop-product-tab-1" role="tabpanel">
        <div class="container">
            <ul class="nav nav-justified" role="tablist">
                <li role="presentation" class="active">
                    <a class="c-font-uppercase c-font-bold" href="#tab-1" role="tab" data-toggle="tab">Description</a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="tab-1">
                <div class="c-product-desc " style="padding: 0px">
                    <div class="container">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10"><p>
                            <?php
                            $str = $produk->description;
                            echo htmlspecialchars_decode($str);
                            ?></p>
                        </p></div>
                        <div class="col-lg-1"></div>
                        

                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/SHOPS/SHOP-PRODUCT-TAB-1 -->

@if(count($relate) > 0)
<!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space">
    <div class="container">
        <div class="c-content-title-4">
            <h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-white">Products Related</span></h3>
        </div>
        <div class="row">
            <div data-slider="owl">
                <div class="owl-carousel owl-theme c-theme owl-small-space c-owl-nav-center" data-rtl="false" data-items="4" data-slide-speed="8000">
                    @foreach($relate as $produk)
                    <div class="item">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                @if($produk->stok == 0)
                                <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Out of Stock</div>
                                @else
                                @endif
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url({{ asset('storage/'.$produk->image) }});"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-18 c-font-slim">{{ $produk->name }}</p>
                                <p class="c-price c-font-16 c-font-slim">Rp {{ number_format($produk->price,0,',','.') }} &nbsp;
                                    <span class="c-font-16 c-font-line-through c-font-red">Rp {{ number_format(($produk->price + 2000),0,',','.') }}</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                @if($produk->stok == 0)
                                <div class="btn-group c-border-left c-border-top" style="width: 203%" role="group">
                                    <a  href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Detail</a>
                                </div>
                                <div class="btn-group c-border-top" role="group">
                                @else
                                <div class="btn-group c-border-top" role="group">
                                    <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Detail</a>
                                </div>
                                <div class="btn-group c-border-left c-border-top" role="group">
                                    @endif
                                    <form action="{{ route('user.keranjang.save') }}" method="post">
                                        <input type="hidden" type="text" name="qty" value="1">
                                        <input type="hidden" value="{{ $produk->stok }}" id="sisastok">
                                        @csrf
                                        @if(Route::has('login'))
                                        @auth
                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                        @endauth
                                        @endif
                                        <input type="hidden" name="products_id" value="{{ $produk->id }}">
                                @if($produk->stok == 0)
                                
                                @else

                                <button type="submit" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" href="#">Cart</button>
                                @endif
                                        <!-- <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a> -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/SHOPS/SHOP-2-2 -->
@else

@endif

</div>
@endsection
@section('style')
<link href="{{asset('shop')}}/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css" />
  <!-- END: BASE PLUGINS -->
  <!-- BEGIN: PAGE STYLES -->
  <!-- <link href="{{asset('shop')}}/plugins/ilightbox/css/ilightbox.css" rel="stylesheet" type="text/css" /> -->
@endsection
@section('js')
<script src="{{asset('shop')}}/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/typed/typed.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/zoom-master/jquery.zoom.min.js" type="text/javascript"></script>
@endsection