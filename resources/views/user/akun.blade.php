@extends('user.app')

@section('style')
<link href="{{asset('shop')}}/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('shop')}}/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('shop')}}/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('shop')}}/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('js')
<script src="{{asset('shop')}}/plugins/moment.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{asset('shop')}}/demos/default/js/scripts/pages/datepicker.js" type="text/javascript"></script>
@endsection

@section('bc','Edit Profile')
@section('content')
@include('user.bc')
@include('user.side')
<div class="c-layout-sidebar-content ">
    <!-- BEGIN: PAGE CONTENT -->
    <div class="c-content-title-1">
        <h3 class="c-font-uppercase c-font-bold">Edit Profile</h3>
        <div class="c-line-left"></div>
    </div>
    <form class="c-shop-form-1" action="/updatecus/{{$user->id}}" id="formValidate" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <!-- BEGIN: ADDRESS FORM -->
        <div class="">
            <!-- BEGIN: BILLING ADDRESS -->

            <div class="row">
                <div class="col-md-12">
                    <!-- <label class="control-label">Name</label>
                    <input type="text" class="form-control c-square c-theme" placeholder="First Name"> -->
                    <label class="form-control-label" for="input-address">Nama</label>
                    <input id="name" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Masukkan Nama" type="text" value="{{ $user->name }}">
                    @if ($errors->has('name'))
                    <div class="invalid-feedback">{{$errors->first('name')}}</div>
                    @endif
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- <label class="control-label">Name</label>
                    <input type="text" class="form-control c-square c-theme" placeholder="First Name"> -->
                    <label class="form-control-label" for="input-address">Nomor Telepon</label>
                    <input id="no_telp" name="no_telp" class="form-control {{ $errors->has('no_telp') ? 'is-invalid':'' }}" placeholder="Masukkan Nomor Telepon" type="text" value="{{ $user->no_telp }}">
                    @if ($errors->has('no_telp'))
                    <div class="invalid-feedback">{{$errors->first('no_telp')}}</div>
                    @endif
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <label class="control-label">Gender</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="jk" value="laki-laki" <?php if ($user->jk == 'laki-laki') {
                                                                                echo 'checked';
                                                                            } ?>>
                            Laki-laki
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="jk" value="perempuan" <?php if ($user->jk == 'perempuan') {
                                                                                echo 'checked';
                                                                            } ?>>
                            Perempuan
                        </label>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-12">
                    <label class="control-label">Date of birth</label>
                    <div class="input-group input-medium date date-picker" id="tgl_lahir" data-date-format="dd/mm/yyyy" data-rtl="false">
                        <input type="text" class="form-control c-square c-theme">
                        <span class="input-group-btn">
                            <button class="btn default c-btn-square" type="button"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label">Date of birth</label>

                    <input id="tgl_lahir" type="date" name="tgl_lahir" class="form-control {{ $errors->has('tgl_lahir') ? 'is-invalid':'' }}" value="{{ $user->tgl_lahir }}" placeholder="Masukkan tanggal lahir" data-error=".errorTxt1" style="white-space: nowrap;line-height: 20px;">
                    <div class="errorTxt1"></div>
                    @if ($errors->has('tgl_lahir'))
                    <div class="invalid-feedback">{{$errors->first('tgl_lahir')}}</div>
                    @endif

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <label class="control-label">Email Address</label>
                    <input type="email" name="email" value="{{$user->email}}" class="form-control c-square c-theme" placeholder="Email Address">
                </div>

            </div>
            <div hidden class="form-group">
                <label for="kt_image_1">Foto Profil</label>
                <div class="foto">
                    <div class="image-input image-input-outline" id="kt_image_1">
                        <div class="image-input-wrapper" style="background-image: url( {{asset('storage/'.$user['ft_profil'])}} )"></div>

                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Ubah foto">
                            <i class="fa fa-pen icon-sm text-muted"></i>
                            <input type="file" name="ft_profil" accept=".png, .jpg, .jpeg" />
                            <input type="hidden" name="ft_profil_remove" />

                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Ubah foto produk">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
            </div>
            <!-- END: BILLING ADDRESS -->
            <!-- BEGIN: PASSWORD -->

            <div class="row">
                <div class="form-group col-md-12">
                    <label class="control-label">Change Password</label>
                    <input id="newpassword" type="password" name="newpassword" class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" placeholder="Masukkan password" type="text">
                    @if ($errors->has('password'))
                    <div class="invalid-feedback">{{$errors->first('password')}}</div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="control-label">Repeat Password</label>
                    <input id="newpassword_confirmation" type="password" name="newpassword_confirmation" class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" placeholder="Masukkan password konfirmasi" type="text">
                    @if ($errors->has('password'))
                    <div class="invalid-feedback">{{$errors->first('password')}}</div>
                    @endif
                    <p class="help-block">Jika Tidak ingin merubah password kosongi saja form password di atas</p>
                    <p class="help-block">Hint: The password should be at least six characters long. <br />
                        To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ).</p>
                </div>
            </div>
            <input type="text" hidden name="role" value="customer">
            <input type="text" hidden name="password" value="{{$user->password}}">
            <input type="text" hidden name="password_confirmation" value="{{$user->password}}">

            <!-- END: PASSWORD -->
            <div class="row c-margin-t-30">
                <div class="form-group col-md-12" role="group">
                    <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Save</button>
                    <!-- <button type="submit" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</button> -->
                </div>
            </div>
        </div>
    </form> <!-- END: PAGE CONTENT -->
</div>
</div>
</div>
<!-- END: ADDRESS FORM -->


@endsection