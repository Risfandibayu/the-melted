<div class="c-layout-page c-margin-b-20">
    <div class="c-layout-breadcrumbs-1 c-bgimage c-subtitle c-fonts-uppercase c-fonts-bold c-bg-img-center" style="background-image: url({{asset('shop')}}/base/img/content/s/bgb.jpg)">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-bold c-font-white c-font-20 c-font-slim">@yield('bc')</h3>
            </div>
            
        </div>
    </div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-3 -->