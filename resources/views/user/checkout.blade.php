@extends('user.app')
@section('content')
@include('user.bc')
<div class="c-content-box c-size-lg">
  <div class="container">
    <form class="c-shop-form-1" action="{{ route('user.order.simpan') }}" method="POST">
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 c-padding-20">
          <div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
            <h1 class="c-font-bold c-font-uppercase c-font-24">Your Order</h1>
            {{ csrf_field() }}
            <div class="row c-margin-b-15">
              <div class="col-md-6 c-font-20">
                <h2>Product</h2>
              </div>
              <div class="col-md-6 c-font-20 text-right">
                <h2>Total</h2>
              </div>
            </div>
            <div class="row c-border-bottom"></div>
            <?php $subtotal = 0; ?>
            @foreach($keranjangs as $keranjang)
            <?php
            $total = $keranjang->price * $keranjang->qt;
            $subtotal = $subtotal + $total;
            ?>
            <div class="row c-margin-b-15 c-margin-t-15">
              <div class="col-md-6 c-font-20"><a href="shop-product-details.html" class="c-theme-link">{{ $keranjang->nama_produk }} x {{ $keranjang->qt }}</a></div>
              <div class="col-md-6 c-font-20 text-right">
                <p class="">Rp{{ number_format($total,0,',','.') }}</p>
              </div>
            </div>
            @endforeach

            <div class="row c-margin-b-15 c-margin-t-15">
              <div class="col-md-6 c-font-20">Subtotal</div>
              <div class="col-md-6 c-font-20 text-right">
                <p class="">Rp<span class="c-subtotal">{{ number_format($subtotal,0,',','.') }}</span></p>
              </div>
            </div>
            <div class="row c-border-top c-margin-b-15"></div>
            <div class="row">
              <div class="col-md-3 c-font-20">Alamat Penerima</div>
              <div class="col-md-9 text-right">
                <p>{{ $alamat->detail }}, {{ $alamat->kec }}, {{ $alamat->kota }}, {{ $alamat->prov }}</p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 c-font-20">Ongkir</div>
              <div class="col-md-6 c-font-20 text-right">
                <p class="">Rp<span class="c-subtotal">{{ number_format($ongkir,0,',','.') }}</span></p>
              </div>
            </div>
            <div class="row c-border-bottom"></div>
            <div class="row c-margin-b-15 c-margin-t-15">
              <div class="col-md-6 c-font-20">
                <p class="c-font-30">Total</p>
              </div>
              <div class="col-md-6 c-font-20 text-right">
                <?php $alltotal = $subtotal + $ongkir; ?>
                <p class="c-font-bold c-font-30">Rp<span class="c-shipping-total">{{ number_format($alltotal,0,',','.') }}</span></p>
              </div>
            </div>
            <div class="row c-border-bottom"></div>


            <div class="row  c-margin-t-15">
              <div class="col-md-12">
                <label for="">Catatan</label>
                <textarea name="pesan" class="form-control"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label for="">Nomor telepon penerima *</label>
                <input type="text" id="no_hp" name="no_hp" id="" class="form-control {{ $errors->has('no_hp') ? 'is-invalid':'' }}" data-error=".errorTxt1">
                <div class="errorTxt1"></div>
                @if ($errors->has('no_hp'))
                <div class="invalid-feedback">{{$errors->first('no_hp')}}</div>
                @endif
              </div>
              <input type="hidden" name="invoice" value="{{ $invoice }}">
              <input type="hidden" name="subtotal" value="{{ $alltotal }}">
              <input type="hidden" name="ongkir" value="{{ $ongkir }}">
            </div>
            <div class="row">
              <div class="col-md-12">
                <label for="">Pilih Metode Pembayaran</label>
                <div class="c-radio-list">
                  <div class="c-radio">
                    <input type="radio" id="radio1" class="c-radio" value="trf" name="metode_pembayaran" checked="">
                    <label for="radio1" class="c-font-bold c-font-20">
                      <span class="inc"></span>
                      <span class="check"></span>
                      <span class="box"></span>
                      Bank Transfer
                    </label>
                  </div>
                  
                </div>
              </div>
            </div>
            
            <div class="row c-border-bottom"></div>
            <div class="row c-margin-t-20">
              <div class="form-group col-md-12" role="group">
                <button type="submit" value="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Order Now</button>
                <a href="/keranjang" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Back to Cart</a>
              </div>
            </div>
          </div>
          <div class="col-md-1"></div>

        </div>
        <!-- END: ORDER FORM -->
      </div>
    </form>
  </div>
</div>
<!-- END: PAGE CONTENT -->
</div>
@endsection