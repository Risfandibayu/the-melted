@extends('user.app')

@section('bc','Checkout Success')
@section('content')
@include('user.bc')
<!-- BEGIN: PAGE CONTENT -->
<div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
    <div class="container">
        <div class="c-shop-order-complete-1 c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Checkout Success</h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <div class="bg-primary" >
                <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                    <i class="fa fa-check"></i> Thank you. Your order has been received.
                </p>
            </div>
            <div class="text-center">
                <a href="{{ route('user.order.bayar') }}" id="oke" class="btn btn-sm c-theme-btn ">Menu Pembayaran</a>
            </div>
            <!-- BEGIN: ORDER SUMMARY -->
            <!-- END: CUSTOMER DETAILS -->
        </div>
    </div>
</div>
<!-- END: PAGE CONTENT -->
</div>
@endsection