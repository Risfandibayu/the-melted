
@extends('user.app')

@section('style')
<link href="{{asset('cust')}}/vendors/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="{{asset('cust')}}/vendors/nice-select/css/nice-select.css" rel="stylesheet">
<style>
    #tes {
        margin-bottom: .5rem;
        /* font-family: inherit; */
        font-family: "Playfair Display", serif;
        font-weight: 500;
        line-height: 1.2;
        color: black;
    }
</style>
@endsection
@section('js')
<script type="text/javascript">
    var toHtml = (tag, value) => {
        $(tag).html(value);
    }
    $(document).ready(function() {
        //  $('#province_id').select2();
        //  $('#cities_id').select2();
        $('#province_id').on('change', function() {
            var id = $('#province_id').val();
            var url = window.location.href;
            var urlNya = url.substring(0, url.lastIndexOf('/alamat/'));
            $.ajax({
                type: 'GET',
                url: urlNya + '/getcity/' + id,
                dataType: 'json',
                success: function(data) {
                    var op = '<option hidden value="">Pilih Kota</option>';
                    if (data.length > 0) {
                        var i = 0;
                        for (i = 0; i < data.length; i++) {
                            op += `<option value="${data[i].city_id}">${data[i].title}</option>`
                        }
                    }
                    toHtml('[name="cities_id"]', op);
                }
            })
        })
    });
</script>
<script type="text/javascript">
    var toHtml = (tag, value) => {
        $(tag).html(value);
    }
    $(document).ready(function() {
        //  $('#province_id').select2();
        //  $('#cities_id').select2();
        $('#cities_id').on('change', function() {
            var id = $('#cities_id').val();
            var url = window.location.href;
            var urlNya = url.substring(0, url.lastIndexOf('/alamat/'));
            $.ajax({
                type: 'GET',
                url: urlNya + '/getsubdistrict/' + id,
                dataType: 'json',
                success: function(data) {
                    var op = '<option hidden value="">Pilih Kecamatan</option>';
                    if (data.length > 0) {
                        var i = 0;
                        for (i = 0; i < data.length; i++) {
                            op += `<option value="${data[i].subdistrict_id}">${data[i].subdistrict_name}</option>`
                        }
                    }
                    toHtml('[name="subdistrict_id"]', op);
                }
            })
        })
    });
</script>
@endsection


@section('bc','Edit Profile')
@section('content')
@include('user.bc')
@include('user.side')
<div class="c-layout-sidebar-content ">
    <!-- BEGIN: PAGE CONTENT -->
    <div class="c-content-title-1">
        <h3 class="c-font-uppercase c-font-bold">Set Address</h3>
        <div class="c-line-left"></div>
    </div>
    <form action="{{ route('user.alamat.simpan') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <label for="">Pilih Provinsi</label>
                <select name="province_id" id="province_id" class="form-control">
                    <option value="" hidden>-- Pilih Provinsi --</option>
                    @foreach($province as $provinsi)
                    <option value="{{ $provinsi->province_id }}">{{ $provinsi->title }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="">Pilih Kota/Kabupaten</label>
                <select   name="cities_id" id="cities_id" class="form-control">

                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="">Pilih Kecamatan</label>
                <select name="subdistrict_id" id="subdistrict_id" class="form-control">
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="">Alamat Lengkap <small>(Kelurahan/Desa, Dusun, RT/RW, Nomor rumah, Nama jalan, dll)</small></label>
                <textarea placeholder="Masukan Alamat lengkap" name="detail" id="" cols="5" class="form-control" rows="2"></textarea>
                <!-- <input type="text" name="detail" id="" placeholde="Desa/Nama Jalan" class="form-control"> -->
            </div>
        </div>
        <div class="row c-margin-t-30">
            <div class="form-group col-md-12" role="group">
                <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Save</button>
                <!-- <button type="submit" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</button> -->
            </div>
        </div>
    </form>
</div>
</div>
</div>
<!-- END: ADDRESS FORM -->


@endsection