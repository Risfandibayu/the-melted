@extends('user.app')





@section('bc','Tips')
@section('content')
@include('user.bc')
<?php
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
?>
<div class="c-content-box c-size-md">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="c-content-blog-post-1-list">
                    @foreach($tips as $tip)
                    <div class="c-content-blog-post-1">
                        <div class="c-media">
                                    <div class="item">
                                        <div class="c-content-media-2" style="background-image: url({{ asset('storage/'.$tip->foto) }}); min-height: 460px;">
                                        </div>
                                    </div>
                        </div>

                        <div class="c-title c-font-bold c-font-uppercase">
                            <a href="{{ route('tipsdetail',['id' =>  $tip->id]) }}">{{$tip->judul}}</a>
                        </div>

                        <div class="c-desc">
                            <?php
                            $text = $tip->deskripsi;
                            $strip = strip_tags(htmlspecialchars_decode(stripcslashes($text)), '<a>');
                            echo substr($strip, 0, 150);
                            if (strlen(trim($tip->deskripsi)) > 150) echo ""; ?>
                            <a href="{{ route('tipsdetail',['id' =>  $tip->id]) }}">read more...</a>
                        </div>

                        <div class="c-panel">
                            <div class="c-author">By <a href="#"><span class="c-font-uppercase">{{$tip->name}}</span></a></div>
                            <div class="c-date">on <span class="c-font-uppercase">{{ tgl_indo($tip->tgl)}}</span></div>
                        </div>
                    </div>
                    @endforeach

                    <div class="c-pagination">
                        <ul class="c-content-pagination c-theme">
                            {{ $tips->links() }}
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- END: BLOG LISTING  -->

<!-- END: PAGE CONTENT -->
</div>
<!-- END: PAGE CONTAINER -->
@endsection
@section('style')
<link href="{{asset('shop')}}/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('shop')}}/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css" />
  <!-- END: BASE PLUGINS -->
  <!-- BEGIN: PAGE STYLES -->
  <!-- <link href="{{asset('shop')}}/plugins/ilightbox/css/ilightbox.css" rel="stylesheet" type="text/css" /> -->
@endsection
@section('js')
<script src="{{asset('shop')}}/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/typed/typed.min.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
  <script src="{{asset('shop')}}/plugins/zoom-master/jquery.zoom.min.js" type="text/javascript"></script>
@endsection