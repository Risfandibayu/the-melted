


@extends('user.app')

@section('bc','Product Category')
@section('content')
@include('user.bc')
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space">
    <div class="container">
        <div class="c-content-title-4">
            <h3 class="c-font-uppercase c-center c-font-bold c-line-strike" style="line-height: 1.2"><span class="c-bg-white">Products Category {{$categories->name}}</span></h3>
        </div>
        @foreach($produks->chunk(4) as $produk)
        <div class="row">
            @foreach($produk as $produk)
            <div class="col-md-3 col-sm-6 c-margin-b-20">
                <div class="c-content-product-2 c-bg-white c-border">
                    <div class="c-content-overlay">
                        <!-- <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div> -->
                        @if($produk->stok == 0)
                                <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Out of Stock</div>
                                @else
                                @endif
                        <div class="c-overlay-wrapper">
                            <div class="c-overlay-content">
                                <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                            </div>
                        </div>
                        <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url({{ asset('storage/'.$produk->image) }});"></div>
                    </div>
                    <div class="c-info">
                        <p class="c-title c-font-18 c-font-slim">{{ $produk->name }}</p>
                        <p class="c-price c-font-16 c-font-slim">Rp{{ number_format($produk->price,0,',','.') }} &nbsp;
                            <span class="c-font-16 c-font-line-through c-font-red">Rp{{ number_format(($produk->price + 2000),0,',','.') }}</span>
                        </p>
                    </div>
                    <div class="btn-group btn-group-justified" role="group">
                        @if($produk->stok == 0)
                        <div class="btn-group c-border-left c-border-top" style="width: 203%" role="group">
                            <a  href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Detail</a>
                        </div>
                        <div class="btn-group c-border-top" role="group">
                        @else
                        <div class="btn-group c-border-top" role="group">
                            <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Detail</a>
                        </div>
                        <div class="btn-group c-border-left c-border-top" role="group">
                            @endif
                            <form action="{{ route('user.keranjang.save') }}" method="post">
                                <input type="hidden" type="text" name="qty" value="1">
                                <input type="hidden" value="{{ $produk->stok }}" id="sisastok">
                                @csrf
                                @if(Route::has('login'))
                                @auth
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                @endauth
                                @endif
                                <input type="hidden" name="products_id" value="{{ $produk->id }}">
                        @if($produk->stok == 0)
                        
                        @else

                        <button type="submit" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" href="#">Cart</button>
                        @endif
                                <!-- <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
        <ul class="c-content-pagination c-square c-theme pull-right">
            {{ $produks->links() }}
        </ul>
    </div>
</div><!-- END: CONTENT/SHOPS/SHOP-2-1 -->
</div>
<!--================End Blog Main Area =================-->
@endsection