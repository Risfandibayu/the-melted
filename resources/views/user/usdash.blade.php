
@extends('user.app')

@section('bc','My Dashboard')
@section('style')
@endsection

@section('content')
<!--================End Main Header Area =================-->
@include('user.bc')
@include('user.side')
<!--================End Main Header Area =================-->

<!--================Product Area =================-->

<div class="c-layout-sidebar-content ">
    <!-- BEGIN: PAGE CONTENT -->
    <!-- BEGIN: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
    <div class="c-content-title-1">
        <h3 class="c-font-uppercase c-font-bold">My Dashboard</h3>
        <div class="c-line-left"></div>
        <p class="">
            Hello <a href="#" class="c-theme-link">{{Auth::user()->name}}</a> (not <a href="#" class="c-theme-link">{{Auth::user()->name}}</a>? <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();" class="c-theme-link">Sign out</a>). <br />
        </p>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 c-margin-b-20">
            <h3 class="c-font-uppercase c-font-bold">{{Auth::user()->name}}</h3>
            <p>Dari dashboard akun Anda, Anda dapat melihat <a href="/order/bayar">Pesanan belum bayar</a>, <a href="/order/proses">Pesanan dalam proses</a>, <a href="/order/riwayat">Riwayat pesanan anda</a>, <a href="/alamat">mengelola alamat anda</a>, dan <a href="/akundetail/{{Auth::user()->id}}">detail akun Anda</a> .</p>

        </div>
    </div><!-- END: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
    <!-- END: PAGE CONTENT -->
</div>
</div>
</div>
<!--================End Product Area =================-->
@endsection