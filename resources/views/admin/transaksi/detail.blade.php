@extends('admin.layout.master')

@section('title','Detail Pemesanan')
@section('judul','Detail Pemesanan')
@section('judulhead','Detail Pemesanan')
@section('halaman','Detail Pemesanan')

@section('style')
<link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css"> -->
<link rel="stylesheet" href="{{asset('assets/venobox/venobox.css')}}" type="text/css" media="screen" />
@endsection

@section('content')


<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-1">
        <!--begin::Page Heading-->
        <div class="d-flex align-items-baseline flex-wrap mr-5">
          <!--begin::Page Title-->
          <h5 class="text-dark font-weight-bold my-1 mr-5">Detail Pemesanan</h5>
          <!--end::Page Title-->
          <!--begin::Breadcrumb-->
          <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
            <li class="breadcrumb-item">
              <a href="/dashboard" class="text-muted">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
              <a href="#" class="text-muted">Detail Pemesanan</a>
            </li>

          </ul>
          <!--end::Breadcrumb-->
        </div>
        <!--end::Page Heading-->
      </div>
      <!--end::Info-->
      <div class="d-flex align-items-center">
        <div class="dropdown">
          <!--begin::Toggle-->
          <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
              <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
              <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
              </button>
            </div>
          </div>
          <!--end::Toggle-->
          <!--begin::Dropdown-->
          <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <!--begin:Header-->
            <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
              <h4 class="text-white font-weight-bold">Jam Digital</h4>
            </div>
            <!--end:Header-->
            <!--begin:Nav-->
            <div class="row row-paddingless">
              <!--begin:Item-->
              <div class="col-3 ml-4">
                <a class="btn-disabled d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                </a>
              </div>
              <div class="col-1">
                <a class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                </a>
              </div>

              <!--end:Item-->
              <!--begin:Item-->
              <div class="col-3">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                </a>
              </div>
              <div class="col-1">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                </a>
              </div>
              <!--end:Item-->
              <!--begin:Item-->
              <div disabled class="col-3">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                </a>
              </div>
              <!--end:Item-->
            </div>
            <!--end:Nav-->
          </div>
          <!--end::Dropdown-->
        </div>
        <div class="dropdown">
          <!--begin::Toggle-->
          <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
              <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
              <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
              </button>
            </div>
          </div>
          <!--end::Toggle-->
          <!--begin::Dropdown-->
          <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <!--begin:Header-->
            <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
              <h4 class="text-white font-weight-bold">Tanggal</h4>
            </div>
            <!--end:Header-->
            <!--begin:Nav-->
            <div class="row row-paddingless">
              <!--begin:Item-->
              <div class="col-12">
                <a class="d-block text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                </a>
              </div>
              <div class="col-6">
                <a class="d-block text-right pr-3 " style="padding-top: 1px;">

                  <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                </a>
              </div>
              <div class="row col-6">
                <div class="col-12">
                  <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">

                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                  </a>
                </div>
                <div class="col-12">
                  <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">

                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                  </a>
                </div>
              </div>
              <!--end:Item-->
            </div>
            <!--end:Nav-->
          </div>
          <!--end::Dropdown-->
        </div>

      </div>
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--begin::Notice-->

      <!--begin::Card-->
      <div class="card card-custom">
        <div class="card-header">
          <div class="card-title">
            <span class="card-icon">
              <i class="flaticon2-indent-dots text-primary"></i>
            </span>
            <h3 class="card-label">Detail Pesanan {{ $order->invoice }}</h3>
          </div>
          <div class="card-toolbar">
            <!--begin::Dropdown-->

            <!--end::Dropdown-->
            <!--begin::Button-->
            <a href="{{ url()->previous() }}" class="btn btn-light-primary font-weight-bolder btn-sm">
              <i class="flaticon2-back icon-nm"></i>Kembali
            </a>

            <!--end::Button-->
          </div>
        </div>
        <div class="card-body">
          <!--begin: Datatable-->
          <div class="row">
            <div class="col-md-7">
              <table>
                <tr>
                  <td>No Invoice</td>
                  <td>:</td>
                  <td class="p-2">{{ $order->invoice }}</td>
                </tr>
                <tr>
                  <td>Metode Pembayaran</td>
                  <td>:</td>
                  <td class="p-2">
                    
                      @if($order->metode_pembayaran == 'trf')
                      Transfer Bank
                      @else
                      Lainya
                      @endif
                      </td>
                </tr>
                @if($order->metode_pembayaran == 'cod')
                <tr>
                  <td>Biaya Cod</td>
                  <td>:</td>
                  <td class="p-2">{{ $order->biaya_cod }}</td>
                </tr>
                @endif
                <tr>
                  <td>Status Pesanan</td>
                  <td>:</td>
                  <td class="p-2">{{ $order->status }}</td>
                </tr>
                <tr>
                  <td>Total</td>
                  <td>:</td>
                  <td class="p-2">Rp. {{ number_format($order->subtotal,2,',','.') }} ( Sudah Termasuk Ongkir )</td>
                </tr>
                <tr>
                  <td>Biaya Ongkir</td>
                  <td>:</td>
                  <td class="p-2">Rp. {{ number_format($order->ongkir,2,',','.') }}</td>
                </tr>
                <tr>
                  <td>Kurir</td>
                  <td>:</td>
                  <td class="p-2">JNE Service OKE</td>
                </tr>
                @if($order->no_resi != null)
                <tr>
                  <td>No Resi</td>
                  <td>:</td>
                  <td class="p-2">{{ $order->no_resi }}</td>
                </tr>
                @endif
                <tr>
                  <td>No Hp</td>
                  <td>:</td>
                  <td class="p-2">{{ $order->no_hp }}</td>
                </tr>
                <tr>
                  <td>Catatan Pelanggan</td>
                  <td>:</td>
                  <td class="p-2">{{ $order->pesan }}</td>
                </tr>
                <tr>
                  <td>Alamat Pengiriman</td>
                  <td>:</td>
                  {{-- <td class="p-2" width="70%">{{ $order->detalamat }}, Kecamatan {{ $order->kec }}, {{ $order->namakota }}, {{ $order->namaprov }}</td> --}}
                  <td class="p-2" width="70%">{{ $order->alamat_pengiriman }}</td>
                </tr>
                @if($order->bukti_pembayaran != null)
                <tr>
                  <td height="1%">Bukti Pembayaran</td>
                  <td>:</td>
                  <td class="p-2" rowspan="2">
                    <!-- <img src="{{ asset('storage/'.$order->bukti_pembayaran) }}" alt="" srcset="" class="img-fluid" width="300"> -->
                    <a class="venobox" data-gall="gallery01" href="{{ asset('storage/'.$order->bukti_pembayaran) }}"><img  width="80" src="{{ asset('storage/'.$order->bukti_pembayaran) }}" alt="image alt" /></a>

                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  
                </tr>
                <tr>
                  <td></td>
                  <td> </td>
                  <td class="pl-2" style="margin-top: -10px;"><small class="text-muted">klik gambar untuk melihat bukti pembayaran ukuran penuh</small></td>
                </tr>
                @if($order->status_order_id == 2)
                <tr>
                  <td></td>
                  <td></td>
                  <td class="p-2">
                    <!-- <a href="{{ route('admin.transaksi.konfirmasi',['id' => $order->id]) }}" onclick="return confirm('Yakin ingin mengonfirmasi pesanan ini?')" class="btn btn-primary mt-1">
                      Konfirmasi Telah Bayar
                    </a> -->
                    <a data-id="{{$order->id}}" class="btn btn btn-primary postbtn">
                      <form action="{{ route('admin.transaksi.konfirmasi',['id' => $order->id]) }}" id="post{{$order->id}}" method="GET">
                        @csrf
                        @method('post')
                      </form>
                      Konfirmasi Telah Bayar
                    </a>
                    <a data-id="{{$order->id}}" class="btn btn btn-danger invalbtn">
                      <form action="{{ route('admin.transaksi.tidakvalid',['id' => $order->id]) }}" id="inval{{$order->id}}" method="GET">
                        @csrf
                        @method('post')
                      </form>
                      Tidak valid
                    </a>

                    <br>
                    <small>Klik tombol ini jika pembeli sudah terbukti melakukan pembayaran</small>
                  </td>
                </tr>
                @endif
                @endif
                @if($order->status_order_id == 3)
                <tr>
                  <td>No Resi</td>
                  <td>:</td>
                  <td class="pt-5">
                    <form action="{{ route('admin.transaksi.inputresi',['id' => $order->id]) }}" method="POST">
                      @csrf
                      <div class="form-group">
                        <div class="input-group">
                          <input type="text" class="form-control form-control-sm" placeholder="Input Resi" aria-label="Recipient's username" aria-describedby="basic-addon2" name="no_resi" required>
                          <div class="input-group-append">
                            <button type="submit" class="btn btn-sm btn-primary" type="button">Simpan</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </td>
                </tr>
                @endif
              </table>
            </div>
            <div class="col-md-5">
              <div class="table-responsive">
                <table class="table table-bordered table-hovered">
                  <thead class="bg-primary text-white">
                    <tr>
                      <th width="5%">No</th>
                      <th>Nama Produk</th>
                      <th>QTY</th>
                      <th>Total Harga</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 0; ?>
                    @foreach($detail as $dt)
                    <?php $no++; ?>
                    <tr>
                      <td align="center">{{$no}}</td>
                      <td>{{ $dt->nama_produk }}</td>
                      <td>{{ $dt->qty }}</td>
                      <td>{{ $dt->qty * $dt->price }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!--end: Datatable-->
        </div>
      </div>
      <!--end::Card-->
    </div>
    <!--end::Container-->
  </div>
  <!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js')
<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<!-- <script src="{{asset('assets/js/pages/crud/datatables/data-sources/html.js')}}"></script> -->
<!-- <script src="{{asset('assets/js/pages/features/miscellaneous/toastr.js')}}"></script> -->
<!-- <script src="{{asset('assets/js/pages/features/miscellaneous/sweetalert2.js')}}"></script> -->

<script>
  $(document).ready(function() {
    $('#tabel').DataTable({
      responsive: true,
    });
  });
</script>

<script type="text/javascript" src="{{asset('assets/venobox/venobox.min.js')}}"></script>
<script>
  $(document).ready(function() {
    $('.venobox').venobox({
      framewidth: '370px', // default: ''
      titleattr: 'data-title', // default: 'title'
      numeratio: true, // default: false
      infinigall: true, // default: false
      spinner:'three-bounce',
    });
  });
</script>
<script>
  function kon() {
    $(".postbtn").click(function(e) {

      id = e.target.dataset.id;
      Swal.fire({
        title: "Apakah anda yakin?",
        text: "Data yang sudah anda hapus tidak akan bisa kembali!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya, Hapus"
      }).then(function(result) {
        if (result.value) {

          Swal.fire(
            "Terhapus!",
            "Data telah terhapus.",
            "success"
          );
          $(`#post${id}`).submit();

        } else {

        }
      });
    });
  }
  $(".postbtn").click(function(e) {

    id = e.target.dataset.id;
    Swal.fire({
      title: "Apakah pembayaran sudah valid?",
      text: "Anda akan mengkonfirmasi pembayaran ini",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Ya, Valid"
    }).then(function(result) {
      if (result.value) {

        Swal.fire(
          "Pembayaran Ter-Konfirmasi!",
          "Data telah Tersimpan.",
          "success"
        );
        $(`#post${id}`).submit();

      } else {

      }
    });
  });
  $(".invalbtn").click(function(e) {

    id = e.target.dataset.id;
    Swal.fire({
      title: "Apakah pembayaran tidak valid?",
      text: "Anda akan menggagalkan pembayaran ini",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Ya, Tidak Valid"
    }).then(function(result) {
      if (result.value) {

        Swal.fire(
          "Pembayaran Invalid!",
          "Data telah Tersimpan.",
          "success"
        );
        $(`#inval${id}`).submit();

      } else {

      }
    });
  });
</script>

@if(Session::has('message'))
<script>
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

  var type = "{{Session::get('alert-type','success')}}"
  switch (type) {
    case 'success':
      toastr.success("{{Session::get('message')}}");
      // Swal.fire("Berhasil","{{Session::get('message')}}","success");
      // Swal.fire("Good job!", "You clicked the button!", "success");
      break;

  }
</script>
@endif

@endsection