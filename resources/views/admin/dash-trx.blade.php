@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('judul', 'Dashboard')
@section('judulhead', 'Dashboard')
@section('halaman', 'Dashboard')
@section('content')

<?php
function pecahtgl($timestamp)
{
    $pecahkan = preg_split('/( |,|-|:)/', $timestamp);
    $hari = array(
        7 => 'Minggu',
        1 => 'Senin',
        2 => 'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu',
    );
    $bulan = array(
        1 =>   'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => ' Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    );
    return $hari[(int)$pecahkan[0]] . ',' . $pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4] . ' ' . $bulan[(int)$pecahkan[5]] . ' ' . $pecahkan[6] . ' ' . $pecahkan[7] . ' ' . $pecahkan[8] . ' ' . $pecahkan[1];
}

?>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <!-- <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">#XRS-45670</span>
                <a href="#" class="btn btn-light-warning font-weight-bolder btn-sm">Add New</a> -->
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
                            <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Jam Digital</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-3 ml-4">
                                <a class="btn-disabled d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div disabled class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                                </a>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
                            <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Tanggal</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-12">
                                <a class="d-block text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="d-block text-right pr-3 " style="padding-top: 1px;">
                                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                                </a>
                            </div>
                            <div class="row col-6">
                                <div class="col-12">
                                    <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">
                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">
                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                                    </a>
                                </div>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Dashboard-->
            <!--begin::Row-->
            <div class="row">
                <div class="col-xl-4">
                    <!--begin::Stats Widget 15-->
                    <div href="#" class="card card-custom bg-success card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-white svg-icon-2x">
                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $trxmasuk->id }}</div>
                            <div class="font-weight-bold text-inverse-success font-size-sm">Pesanan Masuk Hari ini</div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 15-->
                </div>
                <div class="col-xl-4">
                    <!--begin::Stats Widget 15-->
                    <div href="#" class="card card-custom bg-warning card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-white svg-icon-2x">
                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $transcek->id }}</div>
                            <div class="font-weight-bold text-inverse-success font-size-sm">Pesanan Perlu pengecekan</div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 15-->
                </div>
                <div class="col-xl-4">
                    <!--begin::Stats Widget 15-->
                    <div href="#" class="card card-custom bg-warning card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-white svg-icon-2x">
                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $transkirim->id }}</div>
                            <div class="font-weight-bold text-inverse-success font-size-sm">Pesanan Perlu Di kirim</div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 15-->
                </div>

            </div>
            <div class="row">
               
                <div class="col-xl-6 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Pesanan Belum Melakukan Pembayaran</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th> Invoice </th>
                                            <!-- <th> Pemesan </th> -->
                                            <th> Subtotal </th>
                                            <th> Status Pesanan </th>
                                            <th width="17%"> Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orderblm as $order)
                                        <tr>
                                            <td>{{ $order->invoice }}</td>
                                            <!-- <td>{{ $order->nama_pemesan }}</td> -->
                                            <td>Rp {{ number_format($order->subtotal + $order->biaya_cod,0,',','.')}}</td>
                                            <td>{{ $order->name }}</td>
                                            <td>

                                                @if($order->name == 'Belum Bayar' || $order->name == 'Barang Di Kirim' || $order->name == 'Barang Telah Sampai')
                                                <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-primary"><i class="flaticon2-information font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Lihat Detail</small></a>
                                                @elseif($order->name == 'Perlu Di Cek')
                                                <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-warning"><i class="flaticon2-exclamation font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Cek Pembayaran</small></a>
                                                @elseif($order->name == 'Telah Di Bayar')
                                                <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-warning"><i class="flaticon2-pen font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Input Nomor Resi</small></a>
                                                @endif


                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Pesanan Perlu Pengecekan & Perlu Di Kirim</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th> Invoice </th>
                                            <!-- <th> Pemesan </th> -->
                                            <th> Subtotal </th>
                                            <th> Status Pesanan </th>
                                            <th width="17%"> Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orderperlu as $order)
                                        <tr>
                                            <td>{{ $order->invoice }}</td>
                                            <!-- <td>{{ $order->nama_pemesan }}</td> -->
                                            <td>Rp {{ number_format($order->subtotal + $order->biaya_cod,0,',','.')}}</td>
                                            <td>{{ $order->name }}</td>
                                            <td>

                                                @if($order->name == 'Belum Bayar' || $order->name == 'Barang Di Kirim' || $order->name == 'Barang Telah Sampai')
                                                <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-primary"><i class="flaticon2-information font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Lihat Detail</small></a>
                                                @elseif($order->name == 'Perlu Di Cek')
                                                <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-warning"><i class="flaticon2-exclamation font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Cek Pembayaran</small></a>
                                                @elseif($order->name == 'Telah Di Bayar')
                                                <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-warning"><i class="flaticon2-pen font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Input Nomor Resi</small></a>
                                                @endif


                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Dashboard-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection
@section('js')
<!-- <script src="assets/js/pages/features/charts/apexcharts.js"></script> -->
@if(Session::has('message'))
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var type = "{{Session::get('alert-type','success')}}"
    switch (type) {
        case 'success':
            toastr.success("{{Session::get('message')}}");
            // Swal.fire("Berhasil","{{Session::get('message')}}","success");
            // Swal.fire("Good job!", "You clicked the button!", "success");
            break;

    }
</script>
@endif
@endsection