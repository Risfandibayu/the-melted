


@extends('admin.layout.master')
@section('style')
<link href="{{asset('assets/extra-libs/prism/prism.css')}}" rel="stylesheet">
@endsection
@section('title','Edit Kategori')
@section('content')






<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-1">
        <!--begin::Page Heading-->
        <div class="d-flex align-items-baseline flex-wrap mr-5">
          <!--begin::Page Title-->
          <h5 class="text-dark font-weight-bold my-1 mr-5">Kategori</h5>
          <!--end::Page Title-->
          <!--begin::Breadcrumb-->
          <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
            <li class="breadcrumb-item">
              <a href="/dashboard" class="text-muted">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
              <a href="/kategori" class="text-muted">Kategori</a>
            </li>
            <li class="breadcrumb-item">
              <a href="#" class="text-muted">Edit Kategori</a>
            </li>

          </ul>
          <!--end::Breadcrumb-->
        </div>
        <!--end::Page Heading-->
      </div>
      <!--end::Info-->
      <div class="d-flex align-items-center">
        <div class="dropdown">
          <!--begin::Toggle-->
          <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
              <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
              <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
              </button>
            </div>
          </div>
          <!--end::Toggle-->
          <!--begin::Dropdown-->
          <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <!--begin:Header-->
            <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
              <h4 class="text-white font-weight-bold">Jam Digital</h4>
            </div>
            <!--end:Header-->
            <!--begin:Nav-->
            <div class="row row-paddingless">
              <!--begin:Item-->
              <div class="col-3 ml-4">
                <a class="btn-disabled d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                </a>
              </div>
              <div class="col-1">
                <a class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                </a>
              </div>

              <!--end:Item-->
              <!--begin:Item-->
              <div class="col-3">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                </a>
              </div>
              <div class="col-1">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                </a>
              </div>
              <!--end:Item-->
              <!--begin:Item-->
              <div disabled class="col-3">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                </a>
              </div>
              <!--end:Item-->
            </div>
            <!--end:Nav-->
          </div>
          <!--end::Dropdown-->
        </div>
        <div class="dropdown">
          <!--begin::Toggle-->
          <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
              <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
              <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
              </button>
            </div>
          </div>
          <!--end::Toggle-->
          <!--begin::Dropdown-->
          <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <!--begin:Header-->
            <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
              <h4 class="text-white font-weight-bold">Tanggal</h4>
            </div>
            <!--end:Header-->
            <!--begin:Nav-->
            <div class="row row-paddingless">
              <!--begin:Item-->
              <div class="col-12">
                <a class="d-block text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                </a>
              </div>
              <div class="col-6">
                <a class="d-block text-right pr-3 " style="padding-top: 1px;">

                  <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                </a>
              </div>
              <div class="row col-6">
                <div class="col-12">
                  <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">

                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                  </a>
                </div>
                <div class="col-12">
                  <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">

                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                  </a>
                </div>
              </div>
              <!--end:Item-->
            </div>
            <!--end:Nav-->
          </div>
          <!--end::Dropdown-->
        </div>

      </div>
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--begin::Notice-->

      <!--begin::Card-->
      <div class="card card-custom">
        <div class="card-header">
          <h3 class="card-title">
            Edit Kategori
          </h3>

        </div>
        <!--begin::Form-->

        <form action="{{ route('admin.pengaturan.updatealamat',['id' => $id ]) }}" id="formValidate" method="POST">
          @csrf
          <div class="card-body">

            <div class="form-group">
              <label class="form-control-label" for="input-address">Provinsi</label>
              <select required name="province_id" id="province_id" class="form-control{{ $errors->has('name') ? 'is-invalid':'' }}">
                @foreach($provinces as $province)
                <option value="{{ $province->province_id }}">{{ $province->title }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-grup">
              <label class="form-control-label" for="input-address">Kota/Kabupaten</label>
              <select name="cities_id" id="cities_id" class="form-control" required>
              </select>
            </div>
            <div class="form-group mt-3">
              <label class="form-control-label" for="input-address">Detail Alamat</label>
              <input type="text" class="form-control" name="detail" required>
            </div>

          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-primary mr-2">Simpan</button>
            <a href="{{ url('/pengaturan/alamat') }}" class="btn btn-secondary">
              Kembali
            </a>
          </div>
        </form>
        <!--end::Form-->
      </div>
      <!--end::Card-->
    </div>
    <!--end::Container-->
  </div>
  <!--end::Entry-->
</div>
<!--end::Content-->
@endsection



@section('js')
<script src="{{asset('assets/extra-libs/prism/prism.js')}}"></script>
<script src="{{asset('dist/js/pages/forms/jquery.validate.min.js')}}"></script>
<script src="assets/js/pages/crud/file-upload/image-input.js"></script>

<script>
  FormValidation.formValidation(
    document.getElementById('formValidate'), {
      fields: {
        name: {
          validators: {
            notEmpty: {
              message: 'Silahkan masukan kategori'
            }
          }
        },
        id_kategori: {
          validators: {
            notEmpty: {
              message: 'Silahkan pilih kategori'
            }
          }
        },
        harga: {
          validators: {
            notEmpty: {
              message: 'Silahkan masukan harga'
            }
          }
        },
      },

      plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        // Bootstrap Framework Integration
        bootstrap: new FormValidation.plugins.Bootstrap(),
        // Validate fields when clicking the Submit button
        submitButton: new FormValidation.plugins.SubmitButton(),
        // Submit the form when all fields are valid
        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
      }
    }
  );
</script>

<script>
  var avatar1 = new KTImageInput('kt_image_1');
</script>
<script type="text/javascript">
  var toHtml = (tag, value) => {
    $(tag).html(value);
  }
  $(document).ready(function() {
    //  $('#province_id').select2();
    //  $('#cities_id').select2();
    $('#province_id').on('change', function() {
      var id = $('#province_id').val();
      var url = window.location.href;
      var urlNya = url.substring(0, url.lastIndexOf('/ubahalamat/'));
      $.ajax({
        type: 'GET',
        url: urlNya + '/alamat/getcity/' + id,
        dataType: 'json',
        success: function(data) {
          var op = '<option value="">Pilih Kota</option>';
          if (data.length > 0) {
            var i = 0;
            for (i = 0; i < data.length; i++) {
              op += `<option value="${data[i].city_id}">${data[i].title}</option>`
            }
          }
          toHtml('[name="cities_id"]', op);
        }
      })
    })
  });
</script>
@endsection