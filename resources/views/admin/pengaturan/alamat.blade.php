@extends('admin.layout.master')

@section('title','Pengaturan')
@section('judul','Pengaturan')
@section('judulhead','Pengaturan')
@section('halaman','Pengaturan')

@section('style')
<link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css"> -->
@endsection

@section('content')

<?php
function tgl_indo($tanggal)
{
  $bulan = array(
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);

  // variabel pecahkan 0 = tanggal
  // variabel pecahkan 1 = bulan
  // variabel pecahkan 2 = tahun

  return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
?>

<?php
function pecahtgl($timestamp)
{
  $pecahkan = preg_split('/( |,|-|:)/', $timestamp);
  $hari = array(
    7 => 'Minggu',
    1 => 'Senin',
    2 => 'Selasa',
    3 => 'Rabu',
    4 => 'Kamis',
    5 => 'Jumat',
    6 => 'Sabtu',
  );
  $bulan = array(
    1 =>   'Januari',
    2 => 'Februari',
    3 => 'Maret',
    4 => 'April',
    5 => 'Mei',
    6 => ' Juni',
    7 => 'Juli',
    8 => 'Agustus',
    9 => 'September',
    10 => 'Oktober',
    11 => 'November',
    12 => 'Desember'
  );
  return $hari[(int)$pecahkan[0]] . ',' . $pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4] . ' ' . $bulan[(int)$pecahkan[5]] . ' ' . $pecahkan[6] . ' ' . $pecahkan[7] . ' ' . $pecahkan[8] . ' ' . $pecahkan[1];
}

?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-1">
        <!--begin::Page Heading-->
        <div class="d-flex align-items-baseline flex-wrap mr-5">
          <!--begin::Page Title-->
          <h5 class="text-dark font-weight-bold my-1 mr-5">Pengaturan</h5>
          <!--end::Page Title-->
          <!--begin::Breadcrumb-->
          <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
            <li class="breadcrumb-item">
              <a href="/dashboard" class="text-muted">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
              <a href="#" class="text-muted">Pengaturan</a>
            </li>

          </ul>
          <!--end::Breadcrumb-->
        </div>
        <!--end::Page Heading-->
      </div>
      <!--end::Info-->
      <div class="d-flex align-items-center">
        <div class="dropdown">
          <!--begin::Toggle-->
          <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
              <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
              <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
              </button>
            </div>
          </div>
          <!--end::Toggle-->
          <!--begin::Dropdown-->
          <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <!--begin:Header-->
            <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
              <h4 class="text-white font-weight-bold">Jam Digital</h4>
            </div>
            <!--end:Header-->
            <!--begin:Nav-->
            <div class="row row-paddingless">
              <!--begin:Item-->
              <div class="col-3 ml-4">
                <a class="btn-disabled d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                </a>
              </div>
              <div class="col-1">
                <a class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                </a>
              </div>

              <!--end:Item-->
              <!--begin:Item-->
              <div class="col-3">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                </a>
              </div>
              <div class="col-1">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                </a>
              </div>
              <!--end:Item-->
              <!--begin:Item-->
              <div disabled class="col-3">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                </a>
              </div>
              <!--end:Item-->
            </div>
            <!--end:Nav-->
          </div>
          <!--end::Dropdown-->
        </div>
        <div class="dropdown">
          <!--begin::Toggle-->
          <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
              <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
              <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
              </button>
            </div>
          </div>
          <!--end::Toggle-->
          <!--begin::Dropdown-->
          <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <!--begin:Header-->
            <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
              <h4 class="text-white font-weight-bold">Tanggal</h4>
            </div>
            <!--end:Header-->
            <!--begin:Nav-->
            <div class="row row-paddingless">
              <!--begin:Item-->
              <div class="col-12">
                <a class="d-block text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                </a>
              </div>
              <div class="col-6">
                <a class="d-block text-right pr-3 " style="padding-top: 1px;">

                  <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                </a>
              </div>
              <div class="row col-6">
                <div class="col-12">
                  <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">

                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                  </a>
                </div>
                <div class="col-12">
                  <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">

                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                  </a>
                </div>
              </div>
              <!--end:Item-->
            </div>
            <!--end:Nav-->
          </div>
          <!--end::Dropdown-->
        </div>

      </div>
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--begin::Notice-->

      <!--begin::Card-->
      <div class="card card-custom">
        <div class="card-header">
          <div class="card-title">
            <span class="card-icon">
              <i class="flaticon2-pin text-primary"></i>
            </span>
            <h3 class="card-label">Alamat Toko</h3>
          </div>
          <div class="card-toolbar">

          </div>
        </div>
        <div class="card-body">
          <!--begin: Datatable-->
          @if($cekalamat < 1) <div class="row">
            <div class="col-md-12">
              <form action="{{ route('admin.pengaturan.simpanalamat') }}" method="POST">
                @csrf
                <div class="form-group">
                  <label>Provinsi</label>
                  <select required name="province_id" id="province_id" class="form-control">
                    @foreach($provinces as $province)
                    <option value="{{ $province->province_id }}">{{ $province->title }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-grup">
                  <label for="">Kota/Kabupaten</label>
                  <select name="cities_id" id="cities_id" class="form-control" required>
                  </select>
                </div>
                <div class="form-group mt-3">
                  <label>Detail Alamat</label>
                  <input type="text" class="form-control" name="detail" required>
                </div>
                <div class="text-right">
                  <button type="submit" class="btn btn-success text-right">Simpan</button>
                </div>
              </form>
            </div>
        </div>
        @endif
        <div class="col-md-12">
          <table style="font-size: 15px;">
            <tr>
              <th>Alamat Sekarang</th>
              <th>:</th>
              <td>{{ $alamat->detail }}, {{ $alamat->kota }}, {{ $alamat->prov }}</td>
            </tr>
          </table>
          <a href="{{ route('admin.pengaturan.ubahalamat',['id' =>  $alamat->id]) }}">Klik untuk mengubah alamat toko</a>
        </div>
        <!--end: Datatable-->
      </div>

    </div>
    <!--end::Card-->

  </div>
  <!--end::Container-->
</div>
<!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js')
<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<!-- <script src="{{asset('assets/js/pages/crud/datatables/data-sources/html.js')}}"></script> -->
<!-- <script src="{{asset('assets/js/pages/features/miscellaneous/toastr.js')}}"></script> -->
<!-- <script src="{{asset('assets/js/pages/features/miscellaneous/sweetalert2.js')}}"></script> -->

<script>
  $(document).ready(function() {
    $('#tabel').DataTable({
      responsive: true,
    });
  });
</script>


<script>
  function del() {
    $(".deletebtn").click(function(e) {

      id = e.target.dataset.id;
      Swal.fire({
        title: "Apakah anda yakin?",
        text: "Data yang sudah anda hapus tidak akan bisa kembali!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya, Hapus"
      }).then(function(result) {
        if (result.value) {

          Swal.fire(
            "Terhapus!",
            "Data telah terhapus.",
            "success"
          );
          $(`#delete${id}`).submit();

        } else {

        }
      });
    });
  }
</script>

@if(Session::has('message'))
<script>
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

  var type = "{{Session::get('alert-type','success')}}"
  switch (type) {
    case 'success':
      toastr.success("{{Session::get('message')}}");
      // Swal.fire("Berhasil","{{Session::get('message')}}","success");
      // Swal.fire("Good job!", "You clicked the button!", "success");
      break;

  }
</script>
<script type="text/javascript">
  var toHtml = (tag, value) => {
    $(tag).html(value);
  }
  $(document).ready(function() {
    //  $('#province_id').select2();
    //  $('#cities_id').select2();
    $('#province_id').on('change', function() {
      var id = $('#province_id').val();
      var url = window.location.href;
      $.ajax({
        type: 'GET',
        url: url + '/getcity/' + id,
        dataType: 'json',
        success: function(data) {
          var op = '<option value="">Pilih Kota</option>';
          if (data.length > 0) {
            var i = 0;
            for (i = 0; i < data.length; i++) {
              op += `<option value="${data[i].city_id}">${data[i].title}</option>`
            }
          }
          toHtml('[name="cities_id"]', op);
        }
      })
    })
  });
</script>
@endif

@endsection