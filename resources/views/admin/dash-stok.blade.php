@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('judul', 'Dashboard')
@section('judulhead', 'Dashboard')
@section('halaman', 'Dashboard')
@section('content')

<?php
function pecahtgl($timestamp)
{
    $pecahkan = preg_split('/( |,|-|:)/', $timestamp);
    $hari = array(
        7 => 'Minggu',
        1 => 'Senin',
        2 => 'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu',
    );
    $bulan = array(
        1 =>   'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => ' Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    );
    return $hari[(int)$pecahkan[0]] . ',' . $pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4] . ' ' . $bulan[(int)$pecahkan[5]] . ' ' . $pecahkan[6] . ' ' . $pecahkan[7] . ' ' . $pecahkan[8] . ' ' . $pecahkan[1];
}

?>
    <!--begin::Subheader-->
    <!--begin::Info-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
            </div>
                <!--begin::Page Title-->
                <!--end::Page Title-->
                <!--begin::Actions-->
                <!-- <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">#XRS-45670</span>
                <a href="#" class="btn btn-light-warning font-weight-bolder btn-sm">Add New</a> -->
                <!--end::Actions-->
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
                            <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Jam Digital</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-3 ml-4">
                                <a class="btn-disabled d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div disabled class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                                </a>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
                            <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Tanggal</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-12">
                                <a class="d-block text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="d-block text-right pr-3 " style="padding-top: 1px;">
                                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                                </a>
                            </div>
                            <div class="row col-6">
                                <div class="col-12">
                                    <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">
                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">
                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                                    </a>
                                </div>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Dashboard-->
            <!--begin::Row-->
            <div class="row">

                <div class="col-xl-4">
                    <!--begin::Stats Widget 15-->
                    <div href="#" class="card card-custom bg-danger card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-white svg-icon-2x">
                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Code\Warning-1-circle.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                                        <rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />
                                        <rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $bahmin->id }}</div>
                            <div class="font-weight-bold text-inverse-success font-size-sm">Bahan dengan stok minim</div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 15-->
                </div>
                <div class="col-xl-4">
                    <!--begin::Stats Widget 15-->
                    <div href="#" class="card card-custom bg-success card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-2x svg-icon-white">
                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Files\Import.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 7.000000) rotate(-180.000000) translate(-12.000000, -7.000000) " x="11" y="1" width="2" height="12" rx="1" />
                                        <path d="M17,8 C16.4477153,8 16,7.55228475 16,7 C16,6.44771525 16.4477153,6 17,6 L18,6 C20.209139,6 22,7.790861 22,10 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,9.99305689 C2,7.7839179 3.790861,5.99305689 6,5.99305689 L7.00000482,5.99305689 C7.55228957,5.99305689 8.00000482,6.44077214 8.00000482,6.99305689 C8.00000482,7.54534164 7.55228957,7.99305689 7.00000482,7.99305689 L6,7.99305689 C4.8954305,7.99305689 4,8.88848739 4,9.99305689 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,10 C20,8.8954305 19.1045695,8 18,8 L17,8 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                        <path d="M14.2928932,10.2928932 C14.6834175,9.90236893 15.3165825,9.90236893 15.7071068,10.2928932 C16.0976311,10.6834175 16.0976311,11.3165825 15.7071068,11.7071068 L12.7071068,14.7071068 C12.3165825,15.0976311 11.6834175,15.0976311 11.2928932,14.7071068 L8.29289322,11.7071068 C7.90236893,11.3165825 7.90236893,10.6834175 8.29289322,10.2928932 C8.68341751,9.90236893 9.31658249,9.90236893 9.70710678,10.2928932 L12,12.5857864 L14.2928932,10.2928932 Z" fill="#000000" fill-rule="nonzero" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $stokmasuk->id }}</div>
                            <div class="font-weight-bold text-inverse-success font-size-sm">Input Stok Bahan Masuk Hari ini</div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 15-->
                </div>
                <div class="col-xl-4">
                    <!--begin::Stats Widget 15-->
                    <div href="#" class="card card-custom bg-dark card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-white svg-icon-2x">

                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Files\Group-folders.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M4.5,21 L21.5,21 C22.3284271,21 23,20.3284271 23,19.5 L23,8.5 C23,7.67157288 22.3284271,7 21.5,7 L11,7 L8.43933983,4.43933983 C8.15803526,4.15803526 7.77650439,4 7.37867966,4 L4.5,4 C3.67157288,4 3,4.67157288 3,5.5 L3,19.5 C3,20.3284271 3.67157288,21 4.5,21 Z" fill="#000000" opacity="0.3" />
                                        <path d="M2.5,19 L19.5,19 C20.3284271,19 21,18.3284271 21,17.5 L21,6.5 C21,5.67157288 20.3284271,5 19.5,5 L9,5 L6.43933983,2.43933983 C6.15803526,2.15803526 5.77650439,2 5.37867966,2 L2.5,2 C1.67157288,2 1,2.67157288 1,3.5 L1,17.5 C1,18.3284271 1.67157288,19 2.5,19 Z" fill="#000000" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $bahantot->id }}</div>
                            <div class="font-weight-bold text-inverse-success font-size-sm">Jumlah Data Bahan</div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 15-->
                </div>

            </div>
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Daftar Bahan Dengan Stok Minim</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width: 2%;">#</th>
                                            <th style="width: 20%;">Bahan</th>
                                            <th style="width: 12%;">Stok</th>
                                            <th>Tanggal Terakhir Input Stok</th>
                                            <th>Tanggal Terakhir Update Stok</th>
                                            <!-- <th style="width: 20%;">Opsi stok</th> -->
                                            <!-- <th>Aksi</th> -->

                                            <th style="width: 14%;">Opsi stok</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 0; ?>
                                        @foreach ($stok_minim as $bahan)
                                        <?php $no++; ?>
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $bahan->nama_bahan}} </td>
                                            <td>
                                                @if($bahan->stok === null || $bahan->stok === 0)
                                                <span class="text-danger">*</span> <em> Stok kosong </em>
                                                @else
                                                <strong>{{$bahan->stok}}</strong> <small>({{ $bahan->satuan}})</small>
                                                @endif

                                            </td>
                                            <td>
                                                @if($bahan->tanggal_beli === null || $bahan->tanggal_beli === 0)
                                                <span class="text-danger">*</span><small> <em> Belum ada stok masuk </em></small>
                                                @else
                                                {{ pecahtgl(date('N, d - m - Y', strtotime($bahan->tanggal_beli)))}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($bahan->tanggal_kurang === null || $bahan->tanggal_kurang === 0)
                                                <span class="text-danger">*</span><small><em> Update stok belum di lakukan</em></small>
                                                @else

                                                {{ pecahtgl(date('N, d - m - Y', strtotime($bahan->tanggal_kurang)))}}
                                                @endif
                                            </td>


                                            <td>
                                                {{-- <a href="/admin/createbeli_bahan/{{$bahan->id}}" class="py-2 btn btn-sm  btn-light-success"> <i class="flaticon-download-1 icon-nm"></i> <small class="font-weight-bolder"> Input Stok</small></a> --}}
                                                <button type="button" data-toggle="modal" data-target="#inputstok{{$bahan->id}}" class="py-2 btn btn-sm  btn-light-success"> <i class="flaticon-download-1 icon-nm"></i> <small class="font-weight-bolder"> Input Stok</small></button> 
                                    
                                                <div class="modal fade" id="inputstok{{$bahan->id}}" tabindex="-1" role="dialog" aria-labelledby="inputmodalabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="editmodalabel">Input Stok {{$bahan->nama_bahan}} ( Stok saat ini : {{$bahan->stok}} )</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <i aria-hidden="true" class="ki ki-close"></i>
                                                                </button>
                                                            </div>
                                                            <form action="/admin/storebeli_bahan/{{$bahan->id}}" class="formValidate" id="formValidate" method="POST" enctype="multipart/form-data">
                                                                @csrf
                                                                @method('post')
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="input-address">Tanggal</label>
                                                                    <div class="input-group date">
                                                                        <input id="tanggal_beli" data-date-end-date="0d"  type="text" data-tanggal_beli="tanggal_beli" autocomplete="off" name="tanggal_beli" class="form-control tglbeli {{ $errors->has('tanggal_beli') ? 'is-invalid':'' }}" placeholder="Masukkan tanggal" data-error=".errorTxt1">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text">
                                                                                <i class="la la-calendar-check-o"></i>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="errorTxt1"></div>
                                                                    @if ($errors->has('tanggal_beli'))
                                                                    <div class="invalid-feedback">{{$errors->first('tanggal_beli')}}</div>
                                                                    @endif
                                                                </div>
                                                                <!-- <div class="form-group">
                                                                    <label class="form-control-label" for="input-address">Tanggal Pembelian</label>
                                                                    <div class="">
                                                                        <div class="input-group date">
                                                                            
                                                                            <input id="kt_datepicker_2_modal" data-date-format="dd/mm/yyyy" name="tanggal_beli" class="form-control {{ $errors->has('tanggal_beli') ? 'is-invalid':'' }}" readonly="readonly" placeholder="Masukkan kategori" data-error=".errorTxt1">
                                                                            <div class="input-group-append">
                                                                                <span class="input-group-text">
                                                                                    <i class="la la-calendar-check-o"></i>
                                                                                </span>
                                                                            </div>
                                                                            @if ($errors->has('tanggal_beli'))
                                                                            <div class="invalid-feedback">{{$errors->first('tanggal_beli')}}</div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div> -->
                                                                <div hidden class="form-group">
                                                                    <label for="id_bahan">Bahan</label><span class="text-danger"> *</span>
                                                                    <select class="form-control" name="id_bahan" id="id_bahan">
                                                                        <option value="" hidden>-- Pilih Bahan --</option>
                                                                        @foreach ($bahans as $bhn)
                                                                        <option value="{{ $bahan->id }}" {{ $bhn->id == $bahan->id ? 'selected':'' }}>{{ ucfirst($bhn->nama_bahan) }} <small>({{ ucfirst($bhn->satuan) }})</small></option>
                                                                        
                                                                        @endforeach
                                                                    </select>
                                                                    
                                                                    @if (('id_bahan') === 0)
                                                                    <div class="invalid-feedback">{{$errors->first('id_bahan')}}</div>
                                                                    @endif
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="input-address">Jumlah</label><span class="text-danger"> *</span>
                                                                    <input id="qty" name="qty" class="form-control {{ $errors->has('qty') ? 'is-invalid':'' }}" placeholder="Masukkan jumlah stok masuk" type="number" value="{{ old('qty') }}" data-error=".errorTxt1">
                                                                    <div class="errorTxt1"></div>
                                                                    
                                                                    @if ($errors->has('qty'))
                                                                    <!-- <div class="alert invalid-feedback">{{$errors->first('qty')}}
                                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div> -->
                                                                    <div class="invalid-feedback">{{$errors->first('qty')}}</div>
                                                                    @endif
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="input-address">Total</label><span class="text-danger"> *</span>
                                                                    <input id="total" name="total" class="form-control {{ $errors->has('total') ? 'is-invalid':'' }}" placeholder="Masukkan total pengeluaran" type="number" value="{{ old('total') }}" data-error=".errorTxt1">
                                                                    <div class="errorTxt1"></div>
                                                                    @if ($errors->has('total'))
                                                                    <!-- <div class="alert invalid-feedback">{{$errors->first('total')}}
                                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div> -->
                                                                    <div class="invalid-feedback">{{$errors->first('total')}}</div>
                                                                    @endif
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="input-address">Keterangan</label>
                                                                    <input id="keterangan" name="keterangan" class="form-control {{ $errors->has('keterangan') ? 'is-invalid':'' }}" placeholder="Masukkan keterangan" type="text" value="{{ old('keterangan') }}" data-error=".errorTxt1">
                                                                    <div class="errorTxt1"></div>
                                                                    @if ($errors->has('keterangan'))
                                                                    <!-- <div class="alert invalid-feedback">{{$errors->first('keterangan')}}
                                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div> -->
                                                                    <div class="invalid-feedback">{{$errors->first('keterangan')}}</div>
                                                                    @endif
                                                                </div>
                                                
                                                                    
                                                                    
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                    </div>
                                                </div>
            

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Dashboard-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection
@section('js')
<!-- <script src="assets/js/pages/features/charts/apexcharts.js"></script> -->
@if(Session::has('message'))
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var type = "{{Session::get('alert-type','success')}}"
    switch (type) {
        case 'success':
            toastr.success("{{Session::get('message')}}");
            // Swal.fire("Berhasil","{{Session::get('message')}}","success");
            // Swal.fire("Good job!", "You clicked the button!", "success");
            break;

    }
</script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.tglbeli').datepicker({
            format: "yyyy/mm/dd",
            todayHighlight: true,
            autoclose: true,
            todayBtn: "linked",
            clearBtn: true,
        });
    });
</script>
<!-- <script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js"></script> -->
<script>
    $('.tglbeli').data('tanggal_beli').maxDate(0)
</script>
<script>
    var elements = document.getElementsByClassName("tglbeli");
    var today = moment().format('YYYY/MM/DD');
    for(var i=0; i<elements.length; i++) {
    
    elements[i].value = today;;
    }
    // document.write(names);

    // var today = moment().format('YYYY/MM/DD');
    // document.querySelectorAll("tanggal_beli").value = today;
    
    </script>
@endsection