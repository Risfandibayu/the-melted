@extends('admin.layout.master')

@section('style')
<link href="{{asset('assets/extra-libs/prism/prism.css')}}" rel="stylesheet">
@endsection
@section('title','Tambah Data Tips')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Tips</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="/dashboard" class="text-muted">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/kategori" class="text-muted">Tips</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Tambah Data Tips</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
                            <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Jam Digital</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-3 ml-4">
                                <a class="btn-disabled d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div disabled class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                                </a>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
                            <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Tanggal</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-12">
                                <a class="d-block text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="d-block text-right pr-3 " style="padding-top: 1px;">
                                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                                </a>
                            </div>
                            <div class="row col-6">
                                <div class="col-12">
                                    <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">
                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">
                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                                    </a>
                                </div>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
            </div>
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header">
                    <h3 class="card-title">
                        Tambah Data Tips
                    </h3>
                </div>
                <!--begin::Form-->
                <form action="/admin/storetips" class="formValidate" id="formValidate" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-control-label" for="input-address">Judul Tips</label><span class="text-danger"> *</span>
                            <input id="judul" name="judul" class="form-control {{ $errors->has('judul') ? 'is-invalid':'' }}" placeholder="Masukkan nama produk" type="text" value="{{ old('judul') }}" data-error=".errorTxt1">
                            <div class="errorTxt1"></div>
                            @if ($errors->has('judul'))
                            <div class="invalid-feedback">{{$errors->first('judul')}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Artikel</label>
                            <textarea class="tox-target" id="deskripsi" name="deskripsi" rows="3"></textarea>
                            <!-- <div id="deskripsi" name="deskripsi" style="height: 325px">
                                Compose a message
                            </div> -->
                            @if ($errors->has('deskripsi'))
                            <div class="invalid-feedback">{{$errors->first('deskripsi')}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Foto Artikel</label>
                            <div></div>
                            <div class="custom-file">
                                <!-- <input type="file" class="custom-file-input" id="customFile" /> -->
                                <input id="foto" name="foto" class="custom-file-input {{ $errors->has('foto') ? 'is-invalid':'' }}" placeholder="Upload foto kategori" type="file" value="{{ old('foto') }}">
                                <label class="custom-file-label" for="customFile">Pilih file</label>
                            </div>
                        </div>
                        <div hidden class="form-group">
                            <label class="form-control-label" for="input-address">ID User</label><span class="text-danger"> *</span>
                            <input id="id_user" name="id_user" class="form-control {{ $errors->has('id_user') ? 'is-invalid':'' }}" placeholder="Masukkan nama produk" type="text" value="{{ Auth::user()->id}}" data-error=".errorTxt1">
                            <div class="errorTxt1"></div>
                            @if ($errors->has('id_user'))
                            <div class="invalid-feedback">{{$errors->first('id_user')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        <a href="{{ url('/admin/tips') }}" class="btn btn-secondary">
                            Kembali
                        </a>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js')
<script src="{{asset('assets/extra-libs/prism/prism.js')}}"></script>
<script src="{{asset('dist/js/pages/forms/jquery.validate.min.js')}}"></script>

<script>
    var KTQuilDemos = function() {
        // Private functions
        var demo2 = function() {
            var Delta = Quill.import('delta');
            var quill = new Quill('#kt_quil_2', {
                modules: {
                    toolbar: true
                },
                placeholder: 'Type your text here...',
                theme: 'snow'
            });
            // Store accumulated changes
            var change = new Delta();
            quill.on('text-change', function(delta) {
                change = change.compose(delta);
            });
            // Save periodically
            setInterval(function() {
                if (change.length() > 0) {
                    console.log('Saving changes', change);
                    /*
                    Send partial changes
                    $.post('/your-endpoint', {
                    partial: JSON.stringify(change)
                    });
                    Send entire document
                    $.post('/your-endpoint', {
                    doc: JSON.stringify(quill.getContents())
                    });
                    */
                    change = new Delta();
                }
            }, 5 * 1000);
            // Check for unsaved data
        }
        return {
            // public functions
            init: function() {
                demo2();
            }
        };
    }();
    jQuery(document).ready(function() {
        KTQuilDemos.init();
    });
</script>
<script src="{{asset('assets/js/pages/crud/forms/editors/quill.js')}}"></script>
<script>
    var form = document.querySelector('form');
    form.onsubmit = function() {
        // Populate hidden form on submit
        var about = document.querySelector('input[name=deskripsi]');
        about.value = JSON.stringify(quill.getContents());

        console.log("Submitted", $(form).serialize(), $(form).serializeArray());

        // No back end to actually submit to!
        return false;
    };
</script>


<script>
    FormValidation.formValidation(
        document.getElementById('formValidate'), {
            fields: {
                judul: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan nama produk'
                        }
                    }
                },
                fotos: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan harga'
                        }
                    }
                },
            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                // Bootstrap Framework Integration
                bootstrap: new FormValidation.plugins.Bootstrap(),
                // Validate fields when clicking the Submit button
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            }
        }
    );
</script>
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>

<script>
        tinymce.init({
            selector: '#deskripsi',
            menubar: true,
            toolbar: ['styleselect fontselect fontsizeselect | undo redo | cut copy paste | link image | blockquote subscript superscript | advlist | autolink | lists charmap | print preview ',
            'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent '], 
            plugins : 'advlist autolink link image lists charmap print preview code'
        });       
</script>
<!-- <script src="{{asset('assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js')}}"></script>
<script>
    ClassicEditor
            .create( document.querySelector( '#deskripsi' ) )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );
</script> -->
@endsection