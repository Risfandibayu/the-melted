@extends('admin.layout.master')

@section('style')
<link href="{{asset('assets/extra-libs/prism/prism.css')}}" rel="stylesheet">
@endsection
@section('title','Edit Profile')
@section('content')
<!-- Edit prop -->
<?php
// Level
function lewel($level)
{
    if ($level == 1) {
        $level = "Owner";
        echo $level;
    } elseif ($level == 2) {
        $level = "Admin Customer";
        echo $level;
    } elseif ($level == 3) {
        $level = "Admin Katalog";
        echo $level;
    } elseif ($level == 4) {
        $level = "Admin Transaksi";
        echo $level;
    } elseif ($level == 5) {
        $level = "Admin Stok Bahan";
        echo $level;
    } elseif ($level == 6) {
        $level = "Customer";
        echo $level;
    }
}

// Jenis Kelamin
function jk($jk)
{
    if ($jk == 1) {
        $jk = "Laki-Laki";
        echo $jk;
    } elseif ($jk == 2) {
        $jk = "Perempuan";
        echo $jk;
    }
}

// status 
function sts($sts)
{
    if ($sts == 1) {
        $sts = "Meminta";
        echo $sts;
    } elseif ($sts == 2) {
        $sts = "Diterima";
        echo $sts;
    } elseif ($sts == 3) {
        $sts = "Lupa Password";
        echo $sts;
    } elseif ($sts == 4) {
        $sts = "Aktif Order";
        echo $sts;
    } elseif ($sts == 5) {
        $sts = "Jarang Order";
        echo $sts;
    }
}
?>


<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">User</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="/dashboard" class="text-muted">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/kategori" class="text-muted">User</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Edit User</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
                            <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Jam Digital</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-3 ml-4">
                                <a class="btn-disabled d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div disabled class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                                </a>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
                            <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Tanggal</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-12">
                                <a class="d-block text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="d-block text-right pr-3 " style="padding-top: 1px;">
                                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                                </a>
                            </div>
                            <div class="row col-6">
                                <div class="col-12">
                                    <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">
                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">
                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                                    </a>
                                </div>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
            </div>
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header">
                    <h3 class="card-title">
                        Edit User
                    </h3>
                </div>
                <!--begin::Form-->
                <form action="/update_profil/{{$user->id}}" id="formValidate" method="POST" enctype="multipart/form-data">
                    @method('patch')
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="kt_image_1">Foto Profil</label>
                            <div class="foto">
                                <div class="image-input image-input-outline" id="kt_image_1">
                                    <div class="image-input-wrapper" style="background-image: url( {{asset('storage/'.$user['ft_profil'])}} )"></div>

                                    <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Ubah foto">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" name="ft_profil" accept=".png, .jpg, .jpeg" />
                                        <input type="hidden" name="ft_profil_remove" />

                                    </label>

                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Ubah foto produk">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="input-address">Nama</label>
                            <input id="name" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Masukkan Nama " type="text" value="{{ $user->name }}">
                            @if ($errors->has('name'))
                            <div class="invalid-feedback">{{$errors->first('name')}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="input-address">Nomor Telepon</label>
                            <input id="no_telp" name="no_telp" class="form-control {{ $errors->has('no_telp') ? 'is-invalid':'' }}" placeholder="Masukkan Nomor Telepon" type="text" value="{{ $user->no_telp }}">
                            @if ($errors->has('no_telp'))
                            <div class="invalid-feedback">{{$errors->first('no_telp')}}</div>
                            @endif
                        </div>
                        <div hidden class="form-group">
                            <label for="role">Level</label><span class="text-danger"> *</span>
                            <select class="form-control" name="role" id="role">
                                <option value="{{$user->role}}" hidden>{{$user->role}}</option>
                                <option value="owner">Owner</option>
                                <option value="admincus">Admin Customer</option>
                                <option value="adminkat">Admin Katalog</option>
                                <option value="admintrx">Admin Transaksi</option>
                                <option value="adminstok">Admin Stok Bahan</option>
                                <option value="customer">Customer</option>
                                <option value="admin">Admin</option>

                            </select>
                            @if (('role') === 0)
                            <div class="invalid-feedback">{{$errors->first('role')}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="jk">Jenis Kelamin</label><span class="text-danger"> *</span>
                            <select class="form-control" name="jk" id="jk">
                                <option value="" hidden>-- Pilih Jenis Kelamin --</option>
                                <!-- <option value="{{$user->jk}}" hidden>{{jk($user->jk)}}</option> -->
                                <option hidden <?php if($user->jk == 'laki-laki' || 'perempuan'){ echo 'selected';} ?> value="{{$user->jk}}">{{$user->jk}}</option>
                                <option value="laki-laki">Laki-laki</option>
                                <option value="perempuan">Perempuan</option>

                            </select>
                            @if (('jk') === 0)
                            <div class="invalid-feedback">{{$errors->first('jk')}}</div>
                            @endif
                        </div>
                        
                        <div hidden class="form-group">
                            <label class="form-control-label" for="input-address">Tanggal Lahir</label>
                            <div class="input-group date">
                                <input id="tgl_lahir" data-date-end-date="0d" type="text" data-tgl_lahir="tgl_lahir" autocomplete="off" name="tgl_lahir" class="form-control {{ $errors->has('tgl_lahir') ? 'is-invalid':'' }}" value="{{ $user->tgl_lahir }}" placeholder="Masukkan tanggal lahir" data-error=".errorTxt1">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="errorTxt1"></div>
                            @if ($errors->has('tgl_lahir'))
                            <div class="invalid-feedback">{{$errors->first('tgl_lahir')}}</div>
                            @endif
                        </div>
                        
                        <div hidden class="form-group">
                            <label class="form-control-label" for="input-address">Email</label>
                            <input id="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" placeholder="Masukkan kategori" type="text" value="{{ $user->email }}">
                            @if ($errors->has('email'))
                            <div class="invalid-feedback">{{$errors->first('email')}}</div>
                            @endif
                        </div>
                        <div hidden class="form-group">
                            <label class="form-control-label" for="input-address">Username</label>
                            <input id="username" name="username" class="form-control {{ $errors->has('username') ? 'is-invalid':'' }}" placeholder="Masukkan kategori" type="text" value="{{ $user->username }}">
                            @if ($errors->has('username'))
                            <div class="invalid-feedback">{{$errors->first('username')}}</div>
                            @endif
                        </div>
                        <div hidden class="form-group">
                            <label class="form-control-label" for="input-address">Password</label>
                            <input id="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" placeholder="Masukkan kategori" type="text" value="{{ $user->password }}">
                            @if ($errors->has('password'))
                            <div class="invalid-feedback">{{$errors->first('password')}}</div>
                            @endif
                        </div>
                        <div hidden class="form-group">
                            <label class="form-control-label" for="input-address">Password</label>
                            <input id="password" name="password_confirmation" class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" placeholder="Masukkan kategori" type="text" value="{{ $user->password }}">
                            @if ($errors->has('password'))
                            <div class="invalid-feedback">{{$errors->first('password')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        <a href="{{ url()->previous() }}" class="btn btn-secondary">
                            Kembali
                        </a>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js')
<script src="{{asset('assets/extra-libs/prism/prism.js')}}"></script>
<script src="{{asset('dist/js/pages/forms/jquery.validate.min.js')}}"></script>
<script src="assets/js/pages/crud/file-upload/image-input.js"></script>
<!-- Validasi -->
<script>
    FormValidation.formValidation(
        document.getElementById('formValidate'), {
            fields: {
                nama_kategori: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan kategori'
                        }
                    }
                },
                id_kategori: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan pilih kategori'
                        }
                    }
                },
                harga: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan harga'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                // Bootstrap Framework Integration
                bootstrap: new FormValidation.plugins.Bootstrap(),
                // Validate fields when clicking the Submit button
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            }
        }
    );
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tgl_lahir').datepicker({
            format: "yyyy/mm/dd",
            todayHighlight: true,
            autoclose: true,
            todayBtn: "linked",
            clearBtn: true,
        });
    });
</script>

<!-- Edit foto -->
<script>
    var avatar1 = new KTImageInput('kt_image_1');
</script>
@endsection