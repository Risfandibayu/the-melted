@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('judul', 'Dashboard')
@section('judulhead', 'Dashboard')
@section('halaman', 'Dashboard')
@section('content')

<?php
function pecahtgl($timestamp)
{
  $pecahkan = preg_split('/( |,|-|:)/', $timestamp);
  $hari = array(
    7 => 'Minggu',
    1 => 'Senin',
    2 => 'Selasa',
    3 => 'Rabu',
    4 => 'Kamis',
    5 => 'Jumat',
    6 => 'Sabtu',
  );
  $bulan = array(
    1 =>   'Januari',
    2 => 'Februari',
    3 => 'Maret',
    4 => 'April',
    5 => 'Mei',
    6 => ' Juni',
    7 => 'Juli',
    8 => 'Agustus',
    9 => 'September',
    10 => 'Oktober',
    11 => 'November',
    12 => 'Desember'
  );
  return $hari[(int)$pecahkan[0]] . ',' . $pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4] . ' ' . $bulan[(int)$pecahkan[5]] . ' ' . $pecahkan[6] . ' ' . $pecahkan[7] . ' ' . $pecahkan[8] . ' ' . $pecahkan[1];
}

?>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-2">
        <!--begin::Page Title-->
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
        <!--end::Page Title-->
        <!--begin::Actions-->
        <!-- <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">#XRS-45670</span>
                <a href="#" class="btn btn-light-warning font-weight-bolder btn-sm">Add New</a> -->
        <!--end::Actions-->
      </div>
      <!--end::Info-->
      <!--begin::Toolbar-->
      <div class="d-flex align-items-center">
        <div class="dropdown">
          <!--begin::Toggle-->
          <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
              <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
              <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
              </button>
            </div>
          </div>
          <!--end::Toggle-->
          <!--begin::Dropdown-->
          <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <!--begin:Header-->
            <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
              <h4 class="text-white font-weight-bold">Jam Digital</h4>
            </div>
            <!--end:Header-->
            <!--begin:Nav-->
            <div class="row row-paddingless">
              <!--begin:Item-->
              <div class="col-3 ml-4">
                <a class="btn-disabled d-block py-10 text-center  border-bottom">
                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                </a>
              </div>
              <div class="col-1">
                <a class="d-block py-10 text-center  border-bottom">
                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                </a>
              </div>
              <!--end:Item-->
              <!--begin:Item-->
              <div class="col-3">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                </a>
              </div>
              <div class="col-1">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                </a>
              </div>
              <!--end:Item-->
              <!--begin:Item-->
              <div disabled class="col-3">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                </a>
              </div>
              <!--end:Item-->
            </div>
            <!--end:Nav-->
          </div>
          <!--end::Dropdown-->
        </div>
        <div class="dropdown">
          <!--begin::Toggle-->
          <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
              <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
              <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
              </button>
            </div>
          </div>
          <!--end::Toggle-->
          <!--begin::Dropdown-->
          <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <!--begin:Header-->
            <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
              <h4 class="text-white font-weight-bold">Tanggal</h4>
            </div>
            <!--end:Header-->
            <!--begin:Nav-->
            <div class="row row-paddingless">
              <!--begin:Item-->
              <div class="col-12">
                <a class="d-block text-center  border-bottom">
                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                </a>
              </div>
              <div class="col-6">
                <a class="d-block text-right pr-3 " style="padding-top: 1px;">
                  <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                </a>
              </div>
              <div class="row col-6">
                <div class="col-12">
                  <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">
                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                  </a>
                </div>
                <div class="col-12">
                  <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">
                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                  </a>
                </div>
              </div>
              <!--end:Item-->
            </div>
            <!--end:Nav-->
          </div>
          <!--end::Dropdown-->
        </div>
      </div>
      <!--end::Toolbar-->
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--begin::Dashboard-->
      <!--begin::Row-->
      <div class="row">
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $trxmasuk->id }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Transaksi Masuk Hari ini</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $stokmasuk->id }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Stok Masuk Hari ini</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 13-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-danger card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Shopping\Wallet.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24" />
                    <circle fill="#000000" opacity="0.3" cx="20.5" cy="12.5" r="1.5" />
                    <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 6.500000) rotate(-15.000000) translate(-12.000000, -6.500000) " x="3" y="3" width="18" height="7" rx="1" />
                    <path d="M22,9.33681558 C21.5453723,9.12084552 21.0367986,9 20.5,9 C18.5670034,9 17,10.5670034 17,12.5 C17,14.4329966 18.5670034,16 20.5,16 C21.0367986,16 21.5453723,15.8791545 22,15.6631844 L22,18 C22,19.1045695 21.1045695,20 20,20 L4,20 C2.8954305,20 2,19.1045695 2,18 L2,6 C2,4.8954305 2.8954305,4 4,4 L20,4 C21.1045695,4 22,4.8954305 22,6 L22,9.33681558 Z" fill="#000000" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-danger font-weight-bolder font-size-h5 mb-2 mt-5">Rp. {{ number_format($pendapatanhariini->penghasilan,0,',','.') }}</div>
              <!-- <div class="text-inverse-danger font-weight-bolder font-size-h5 mb-2 mt-5">Shopping Cart</div> -->
              <div class="font-weight-bold text-inverse-danger font-size-sm">pendapatan hari ini</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 13-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 13-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-danger card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Shopping\Wallet.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24" />
                    <circle fill="#000000" opacity="0.3" cx="20.5" cy="12.5" r="1.5" />
                    <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 6.500000) rotate(-15.000000) translate(-12.000000, -6.500000) " x="3" y="3" width="18" height="7" rx="1" />
                    <path d="M22,9.33681558 C21.5453723,9.12084552 21.0367986,9 20.5,9 C18.5670034,9 17,10.5670034 17,12.5 C17,14.4329966 18.5670034,16 20.5,16 C21.0367986,16 21.5453723,15.8791545 22,15.6631844 L22,18 C22,19.1045695 21.1045695,20 20,20 L4,20 C2.8954305,20 2,19.1045695 2,18 L2,6 C2,4.8954305 2.8954305,4 4,4 L20,4 C21.1045695,4 22,4.8954305 22,6 L22,9.33681558 Z" fill="#000000" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-danger font-weight-bolder font-size-h5 mb-2 mt-5">Rp. {{ number_format($pendapatan7->penghasilan,0,',','.') }}</div>
              <!-- <div class="text-inverse-danger font-weight-bolder font-size-h5 mb-2 mt-5">Shopping Cart</div> -->
              <div class="font-weight-bold text-inverse-danger font-size-sm">pendapatan 7 hari terakhir</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 13-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">Rp {{ number_format($pengeluaranhariini->pengeluaran,0,',','.') }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Pengeluaran hari ini</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">Rp {{ number_format($pengeluaran7->pengeluaran,0,',','.') }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Pengeluaran 7 Hari terakir</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $promin->id }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Produk dengan stok minim</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $bahmin->id }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Bahan dengan stok minim</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $bahantot->id }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Jumlah Data Bahan</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $produktot->id }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Jumlah Data Produk</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $tipstot->id }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Jumlah Data Tips</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $pelanggan->id }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Jumlah Data Customer</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $pelanggantoday->id }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Customer daftar hari ini</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $transcek->id }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Order Perlu pengecekan</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>
        <div class="col-xl-3">
          <!--begin::Stats Widget 15-->
          <a href="#" class="card card-custom bg-primary bg-hover-state-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
              <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $transkirim->id }}</div>
              <div class="font-weight-bold text-inverse-success font-size-sm">Order Perlu Di kirim</div>
            </div>
            <!--end::Body-->
          </a>
          <!--end::Stats Widget 15-->
        </div>















      </div>
      <div class="row">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">10 Transaksi Terbaru</h4>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th> Invoice </th>
                      <th> Pemesan </th>
                      <th> Subtotal </th>
                      <th> Status Pesanan </th>
                      <th width="17%"> Aksi </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($order_baru as $order)
                    <tr>
                      <td>{{ $order->invoice }}</td>
                      <td>{{ $order->nama_pemesan }}</td>
                      <td>Rp {{ number_format($order->subtotal + $order->biaya_cod,0,',','.')}}</td>
                      <td>{{ $order->name }}</td>
                      <td>

                        @if($order->name == 'Belum Bayar' || $order->name == 'Barang Di Kirim' || $order->name == 'Barang Telah Sampai')
                        <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-primary"><i class="flaticon2-information font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Lihat Detail</small></a>
                        @elseif($order->name == 'Perlu Di Cek')
                        <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-warning"><i class="flaticon2-exclamation font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Cek Pembayaran</small></a>
                        @elseif($order->name == 'Telah Di Bayar')
                        <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-warning"><i class="flaticon2-pen font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Input Nomor Resi</small></a>
                        @endif


                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-10">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Transaksi pada 7 hari terakir</h4>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th> Invoice </th>
                      <th> Pemesan </th>
                      <th> Subtotal </th>
                      <th> Status Pesanan </th>
                      <th width="17%"> Aksi </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($order7 as $order)
                    <tr>
                      <td>{{ $order->invoice }}</td>
                      <td>{{ $order->nama_pemesan }}</td>
                      <td>Rp {{ number_format($order->subtotal + $order->biaya_cod,0,',','.')}}</td>
                      <td>{{ $order->name }}</td>
                      <td>

                        @if($order->name == 'Belum Bayar' || $order->name == 'Barang Di Kirim' || $order->name == 'Barang Telah Sampai')
                        <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-primary"><i class="flaticon2-information font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Lihat Detail</small></a>
                        @elseif($order->name == 'Perlu Di Cek')
                        <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-warning"><i class="flaticon2-exclamation font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Cek Pembayaran</small></a>
                        @elseif($order->name == 'Telah Di Bayar')
                        <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-warning"><i class="flaticon2-pen font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Input Nomor Resi</small></a>
                        @endif


                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-10">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Stok masuk pada 7 hari terakir</h4>
              <div class="table-responsive">
                <table class="table table-hover table-checkable display" id="tabel" style="margin-top: 13px !important">
                  <thead>
                    <tr>
                      <th style="width: 2%;">No</th>
                      <th>Tanggal</th>
                      <th>Bahan</th>
                      <th>Jumlah</th>
                      <th>Total</th>
                      <th>Keterangan</th>
                      <!-- <th>Aksi</th> -->
                      <th style="width: 12%;">Actions</th>

                    </tr>
                  </thead>
                  <tbody>


                    <?php $no = 0; ?>
                    @foreach ($beli7 as $beli_bahan)
                    <!-- <?php $no++; ?> -->

                    <tr>
                      <td>{{ $no }}</td>
                      
                      <td>{{ pecahtgl(date('N, d - m - Y', strtotime($beli_bahan->tanggal_beli)))}}</td>
                      <td>{{ $beli_bahan->nama_bahan}} </td>
                      <td><strong>{{ $beli_bahan->qty}}</strong> <small>({{$beli_bahan->satuan}})</small></td>
                      <td>Rp {{ number_format($beli_bahan->total,0,',','.')}}</td>
                      <!-- <td>{{ $beli_bahan->keterangan}}</td> -->
                      <td>
                        @if($beli_bahan->keterangan === null || $beli_bahan->keterangan === 0)
                        <span class="text-danger">*</span> <small><em> Keterangan kosong </em></small>
                        @else
                        {{$beli_bahan->keterangan}}
                        @endif
                      </td>
                      <td nowarp="nowarp">

                        <a href="/admin/editbeli_bahan/{{$beli_bahan->id}}" class="btn btn-sm btn-clean btn-icon" title="Edit details">
                          <span class="svg-icon svg-icon-md">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" \ transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) " />
                                <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1" />
                              </g>
                            </svg>
                          </span>
                        </a>
                        <!-- <a href="/desbahan:{{$beli_bahan->id}}" onclick="return confirm('Apakah anda yakin')" id="delete" class="delete btn btn-sm btn-clean btn-icon" title="Delete">
                                        <span class="svg-icon svg-icon-md">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero" />
                                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                    </a> -->
                        <a href="javascript:del();" data-id="{{$beli_bahan->id}}" class="btn btn-sm btn-clean btn-icon deletebtn">
                          <form action="{{route('blbhn.delete',$beli_bahan->id)}}" id="delete{{$beli_bahan->id}}" method="GET">
                            @csrf
                            @method('delete')
                          </form>
                          <span class="svg-icon svg-icon-md" data-id="{{$beli_bahan->id}}" id="delete{{$beli_bahan->id}}">
                            <svg xmlns="http://www.w3.org/2000/svg" data-id="{{$beli_bahan->id}}" id="delete{{$beli_bahan->id}}" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                              <g stroke="none" data-id="{{$beli_bahan->id}}" id="delete{{$beli_bahan->id}}" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect data-id="{{$beli_bahan->id}}" id="delete{{$beli_bahan->id}}" x="0" y="0" width="24" height="24" />
                                <path data-id="{{$beli_bahan->id}}" id="delete{{$beli_bahan->id}}" d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero" />
                                <path data-id="{{$beli_bahan->id}}" id="delete{{$beli_bahan->id}}" d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3" />
                              </g>
                            </svg>
                          </span>
                        </a>


                      </td>





                    </tr>
                    @endforeach

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-10">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Transaksi perlu pengecekan dan perlu di kirim</h4>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th> Invoice </th>
                      <th> Pemesan </th>
                      <th> Subtotal </th>
                      <th> Status Pesanan </th>
                      <th width="17%"> Aksi </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($orderperlu as $order)
                    <tr>
                      <td>{{ $order->invoice }}</td>
                      <td>{{ $order->nama_pemesan }}</td>
                      <td>Rp {{ number_format($order->subtotal + $order->biaya_cod,0,',','.')}}</td>
                      <td>{{ $order->name }}</td>
                      <td>

                        @if($order->name == 'Belum Bayar' || $order->name == 'Barang Di Kirim' || $order->name == 'Barang Telah Sampai')
                        <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-primary"><i class="flaticon2-information font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Lihat Detail</small></a>
                        @elseif($order->name == 'Perlu Di Cek')
                        <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-warning"><i class="flaticon2-exclamation font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Cek Pembayaran</small></a>
                        @elseif($order->name == 'Telah Di Bayar')
                        <a href="{{ route('admin.transaksi.detail',['id'=>$order->id]) }}" class="py-2 btn font-weight-bolder btn-light-warning"><i class="flaticon2-pen font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Input Nomor Resi</small></a>
                        @endif


                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-10">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Daftar Cusomer yang daftar pada hari ini</h4>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th style="width: 2%;">No</th>
                      <th>Nama Customer</th>
                      <th>Nomor Telepon</th>
                      <th>Jenis Kelamin</th>
                      <!-- <th>Tanggal Lahir</th> -->
                      <th>Usia</th>
                      <th>Email</th>
                      <th>Alamat</th>
                      <!-- <th>Aksi</th> -->
                      <!-- <th style="width: 12%;">Actions</th> -->

                    </tr>
                  </thead>
                  <tbody>


                    <?php $no = 0; ?>
                    @foreach ($peldaftoday as $pel)
                    <?php $no++; ?>
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $pel->name}}</td>
                      <td>{{ $pel->no_telp}}</td>
                      <td>{{ $pel->jk}}</td>
                      <!-- <td>{{ $pel->tgl_lahir}}</td> -->
                      <td>{{ $pel->usia}}</td>
                      <td>{{ $pel->email }}</td>
                      <td>
                        @if($pel->detail == NULL || $pel->kota == NULL || $pel->prov == NULL)
                        <small><em>Customer belum mengisi alamat</em></small>
                        @else
                        {{ $pel->detail }}, {{ $pel->kota }}, {{ $pel->prov }}
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-10">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Daftar Bahan Dengan Stok Minim</h4>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th style="width: 2%;">#</th>
                      <th style="width: 8%;">Bahan</th>
                      <th style="width: 12%;">Stok</th>
                      <th>Tanggal Terakhir Input Stok</th>
                      <th>Tanggal Terakhir Update Stok</th>
                      <!-- <th style="width: 20%;">Opsi stok</th> -->
                      <!-- <th>Aksi</th> -->

                      <th style="width: 14%;">Opsi stok</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 0; ?>
                    @foreach ($stok_minim as $bahan)
                    <?php $no++; ?>
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $bahan->nama_bahan}} </td>
                      <td>
                        @if($bahan->stok === null || $bahan->stok === 0)
                        <span class="text-danger">*</span> <em> Stok kosong </em>
                        @else
                        <strong>{{$bahan->stok}}</strong> <small>({{ $bahan->satuan}})</small>
                        @endif

                      </td>
                      <td>
                        @if($bahan->tanggal_beli === null || $bahan->tanggal_beli === 0)
                        <span class="text-danger">*</span><small> <em> Belum ada stok masuk </em></small>
                        @else
                        {{ pecahtgl(date('N, d - m - Y', strtotime($bahan->tanggal_beli)))}}
                        @endif
                      </td>
                      <td>
                        @if($bahan->tanggal_kurang === null || $bahan->tanggal_kurang === 0)
                        <span class="text-danger">*</span><small><em> Update stok belum di lakukan</em></small>
                        @else

                        {{ pecahtgl(date('N, d - m - Y', strtotime($bahan->tanggal_kurang)))}}
                        @endif
                      </td>


                      <td>
                        <a href="/admin/createbeli_bahan/{{$bahan->id}}" class="py-2 btn btn-sm  btn-light-success"> <i class="flaticon-download-1 icon-nm"></i> <small class="font-weight-bolder"> Input Stok</small></a>


                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row mt-10">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Daftar Produk Dengan Stok Minim</h4>
              <div class="table-responsive">
                <table class="table table-hover table-checkable" id="tabel" style="margin-top: 13px !important">
                  <thead>
                    <tr>
                      <th style="width: 2%;">No</th>
                      <!-- <th>Foto</th> -->
                      <th>Nama Produk</th>
                      <!-- <th>Tag</th> -->
                      <th>Kategori</th>
                      <!-- <th>Berat</th> -->
                      <th>Harga</th>
                      <th>Stok</th>
                      <!-- <th>Deskripsi</th> -->
                      <td width="14%">Opsi Stok</td>
                      <!-- <th>Aksi</th> -->
                    </tr>
                  </thead>
                  <tbody>


                    <?php $no = 0; ?>
                    @foreach ($pro_stok as $product)
                    <?php $no++; ?>
                    <tr>
                      <td style="width: 2%;">{{ $no }}</td>
                      <!-- <td>
                                    @if($product->image)
                                    <img src="{{ asset ('storage/'.$product->image)}}" height="90px" alt="">
                                    @else
                                    <span class="text-danger">*</span> <small><em> Belum ada foto</em></small>
                                    @endif

                                </td> -->
                      <td>{{ $product->name}}</td>
                      <!-- <td>{{ $product->tag}}</td> -->
                      <td>
                        {{ $product->nama_kategori}}
                      </td>
                      <!-- <td>{{ $product->weigth}}gr</td> -->
                      <td>Rp {{ number_format($product->price,0,',','.')}}</td>
                      <td>
                        @if($product->stok === null || $product->stok === 0)
                        <span class="text-danger">*</span> <small><em> Stok kosong </em></small>
                        @else
                        <strong>{{$product->stok}}</strong>
                        @endif

                      </td>
                      <!-- <td>
                                    @if($product->description === null)
                                    <span class="text-danger">*</span><small> <em> Belum ada description </em></small>
                                    @else
                                    <?php
                                    $text = $product->description;
                                    $strip = strip_tags(htmlspecialchars_decode(stripcslashes($text)), '<a>');
                                    echo substr($strip, 0, 50);
                                    if (strlen(trim($product->description)) > 50) echo "..."; ?>
                                    @endif
                                </td> -->

                      <td>
                        <a href="{{ route('admin.product.stok',['id'=>$product->id]) }}" class="py-2 btn font-weight-bolder btn-light-success"><i class="flaticon-download-1 icon-nm"></i><small class="font-weight-bolder ">Input Stok</small></a>
                      </td>

                    </tr>
                    @endforeach

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>


      <!--end::Row-->
      <!--begin::Row-->

      <!--end::Row-->
      <!--end::Dashboard-->
    </div>
    <!--end::Container-->
  </div>
  <!--end::Entry-->
</div>
@endsection
@section('js')
<!-- <script src="assets/js/pages/features/charts/apexcharts.js"></script> -->

@endsection