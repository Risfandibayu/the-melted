@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('judul', 'Dashboard')
@section('judulhead', 'Dashboard')
@section('halaman', 'Dashboard')
@section('content')

<?php
function pecahtgl($timestamp)
{
    $pecahkan = preg_split('/( |,|-|:)/', $timestamp);
    $hari = array(
        7 => 'Minggu',
        1 => 'Senin',
        2 => 'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu',
    );
    $bulan = array(
        1 =>   'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => ' Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    );
    return $hari[(int)$pecahkan[0]] . ',' . $pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4] . ' ' . $bulan[(int)$pecahkan[5]] . ' ' . $pecahkan[6] . ' ' . $pecahkan[7] . ' ' . $pecahkan[8] . ' ' . $pecahkan[1];
}

?>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <!-- <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">#XRS-45670</span>
                <a href="#" class="btn btn-light-warning font-weight-bolder btn-sm">Add New</a> -->
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
                            <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Jam Digital</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-3 ml-4">
                                <a class="btn-disabled d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div disabled class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                                </a>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
                            <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Tanggal</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-12">
                                <a class="d-block text-center  border-bottom">
                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="d-block text-right pr-3 " style="padding-top: 1px;">
                                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                                </a>
                            </div>
                            <div class="row col-6">
                                <div class="col-12">
                                    <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">
                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">
                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                                    </a>
                                </div>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Dashboard-->
            <!--begin::Row-->
            <div class="row">
                <div class="col-xl-4">
                    <!--begin::Stats Widget 15-->
                    <a href="#" class="card card-custom bg-danger card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-white svg-icon-2x">
                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Code\Warning-1-circle.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                                        <rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />
                                        <rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $promin->id }}</div>
                            <div class="font-weight-bold text-inverse-success font-size-sm">Produk dengan stok minim</div>
                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 15-->
                </div>
                <div class="col-xl-4">
                    <!--begin::Stats Widget 15-->
                    <a href="#" class="card card-custom bg-dark card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-white svg-icon-2x">
                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Files\Group-folders.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M4.5,21 L21.5,21 C22.3284271,21 23,20.3284271 23,19.5 L23,8.5 C23,7.67157288 22.3284271,7 21.5,7 L11,7 L8.43933983,4.43933983 C8.15803526,4.15803526 7.77650439,4 7.37867966,4 L4.5,4 C3.67157288,4 3,4.67157288 3,5.5 L3,19.5 C3,20.3284271 3.67157288,21 4.5,21 Z" fill="#000000" opacity="0.3" />
                                        <path d="M2.5,19 L19.5,19 C20.3284271,19 21,18.3284271 21,17.5 L21,6.5 C21,5.67157288 20.3284271,5 19.5,5 L9,5 L6.43933983,2.43933983 C6.15803526,2.15803526 5.77650439,2 5.37867966,2 L2.5,2 C1.67157288,2 1,2.67157288 1,3.5 L1,17.5 C1,18.3284271 1.67157288,19 2.5,19 Z" fill="#000000" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <div class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $produktot->id }}</div>
                            <div class="font-weight-bold text-inverse-success font-size-sm">Jumlah Data Produk</div>
                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 15-->
                </div>
                <div class="col-xl-4">
                    <!--begin::Stats Widget 15-->
                    <a href="#" class="card card-custom bg-dark card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-white svg-icon-2x">
                                
                                    <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Files\Group-folders.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M4.5,21 L21.5,21 C22.3284271,21 23,20.3284271 23,19.5 L23,8.5 C23,7.67157288 22.3284271,7 21.5,7 L11,7 L8.43933983,4.43933983 C8.15803526,4.15803526 7.77650439,4 7.37867966,4 L4.5,4 C3.67157288,4 3,4.67157288 3,5.5 L3,19.5 C3,20.3284271 3.67157288,21 4.5,21 Z" fill="#000000" opacity="0.3" />
                                            <path d="M2.5,19 L19.5,19 C20.3284271,19 21,18.3284271 21,17.5 L21,6.5 C21,5.67157288 20.3284271,5 19.5,5 L9,5 L6.43933983,2.43933983 C6.15803526,2.15803526 5.77650439,2 5.37867966,2 L2.5,2 C1.67157288,2 1,2.67157288 1,3.5 L1,17.5 C1,18.3284271 1.67157288,19 2.5,19 Z" fill="#000000" />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                                <div class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $tipstot->id }}</div>
                                <div class="font-weight-bold text-inverse-success font-size-sm">Jumlah Data Tips</div>
                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 15-->
                </div>
            </div>
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Daftar Produk Dengan Stok Minim</h4>
                            <div class="table-responsive">
                                <table class="table table-hover table-checkable" id="tabel" style="margin-top: 13px !important">
                                    <thead>
                                        <tr>
                                            <th style="width: 2%;">No</th>
                                            <!-- <th>Foto</th> -->
                                            <th>Nama Produk</th>
                                            <!-- <th>Tag</th> -->
                                            <th>Kategori</th>
                                            <!-- <th>Berat</th> -->
                                            <th>Harga</th>
                                            <th>Stok</th>
                                            <!-- <th>Deskripsi</th> -->
                                            <td width="14%">Opsi Stok</td>
                                            <!-- <th>Aksi</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>


                                        <?php $no = 0; ?>
                                        @foreach ($pro_stok as $product)
                                        <?php $no++; ?>
                                        <tr>
                                            <td style="width: 2%;">{{ $no }}</td>
                                            <!-- <td>
                                    @if($product->image)
                                    <img src="{{ asset ('storage/'.$product->image)}}" height="90px" alt="">
                                    @else
                                    <span class="text-danger">*</span> <small><em> Belum ada foto</em></small>
                                    @endif

                                </td> -->
                                            <td>{{ $product->name}}</td>
                                            <!-- <td>{{ $product->tag}}</td> -->
                                            <td>
                                                {{ $product->nama_kategori}}
                                            </td>
                                            <!-- <td>{{ $product->weigth}}gr</td> -->
                                            <td>Rp {{ number_format($product->price,0,',','.')}}</td>
                                            <td>
                                                @if($product->stok === null || $product->stok === 0)
                                                <span class="text-danger">*</span> <small><em> Stok kosong </em></small>
                                                @else
                                                <strong>{{$product->stok}}</strong>
                                                @endif

                                            </td>
                                            <!-- <td>
                                    @if($product->description === null)
                                    <span class="text-danger">*</span><small> <em> Belum ada description </em></small>
                                    @else
                                    <?php
                                    $text = $product->description;
                                    $strip = strip_tags(htmlspecialchars_decode(stripcslashes($text)), '<a>');
                                    echo substr($strip, 0, 50);
                                    if (strlen(trim($product->description)) > 50) echo "..."; ?>
                                    @endif
                                </td> -->

                                            <td>
                                                {{-- <a href="{{ route('admin.product.stok',['id'=>$product->id]) }}" class="py-2 btn font-weight-bolder btn-light-success"><i class="flaticon-download-1 icon-nm"></i><small class="font-weight-bolder ">Input Stok</small></a> --}}

                                                <button type="button" data-toggle="modal" data-target="#inputstok{{$product->id}}" class="py-2 btn btn-sm  btn-light-success"> <i class="flaticon-download-1 icon-nm"></i> <small class="font-weight-bolder"> Update Stok</small></button> 
                                    
                                    <div class="modal fade" id="inputstok{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="inputmodalabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodalabel">Update Stok Produk {{ $product->name }} ( Stok saat ini : {{$product->stok}} )</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <i aria-hidden="true" class="ki ki-close"></i>
                                                    </button>
                                                </div>
                                                <form  action="{{ route('admin.product.updatestok',['id' => $product->id]) }}" class="formValidate" id="formValidate" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    @method('post')
                                                <div class="modal-body">
                                                    <div hidden class="form-group">
                                                        <label class="form-control-label" for="input-address">Nama Produk</label><span class="text-danger"> *</span>
                                                        <input id="name" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Masukkan nama produk" type="text" value="{{ $product->name }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                                                        @if ($errors->has('name'))
                                                        <div class="invalid-feedback">{{$errors->first('name')}}</div>
                                                        @endif
                                                    </div>
                                                    <div hidden class="form-group">
                                                        <label for="id_kategori">Kategori</label><span class="text-danger"> *</span>
                                                        <select class="form-control" name="categories_id" id="categories_id">
                                                            @foreach ($categories as $categorie)
                                                            <option value="{{ $categorie->id }}" <?php if ($product->categories_id == $categorie->id) {
                                                                                                        echo 'selected';
                                                                                                    } ?>>{{ $categorie->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        @if (('categories_id') === 0)
                                                        <div class="invalid-feedback">{{$errors->first('categories_id')}}</div>
                                                        @endif
                                                    </div>
                                                    <div hidden class="form-group">
                                                        <label class="form-control-label" for="input-address">Harga</label><span class="text-danger"> *</span>
                                                        <input id="weigth" name="weigth" class="form-control {{ $errors->has('weigth') ? 'is-invalid':'' }}" placeholder="Masukkan weigth" type="number" value="{{ $product->weigth }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                                                        @if ($errors->has('weigth'))
                                                        <div class="invalid-feedback">{{$errors->first('weigth')}}</div>
                                                        @endif
                                                    </div>
                                                    <div hidden class="form-group">
                                                        <label class="form-control-label" for="input-address">Harga</label><span class="text-danger"> *</span>
                                                        <input id="price" name="price" class="form-control {{ $errors->has('price') ? 'is-invalid':'' }}" placeholder="Masukkan price" type="number" value="{{ $product->price }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                                                        @if ($errors->has('price'))
                                                        <div class="invalid-feedback">{{$errors->first('price')}}</div>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-address">Stok</label>
                                                        <input id="stok" name="stok" class="form-control {{ $errors->has('stok') ? 'is-invalid':'' }}" placeholder="Masukkan stok" type="number" value="{{ $product->stok }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                            
                                                    </div>
                                                    <div hidden class="form-group">
                                                        <label for="deskripsi">Deskripsi</label>
                                                        <textarea class="tox-target" name="description" id="deskripsi" rows="3">{{ $product->description}}</textarea>
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <label>Foto Produk</label>
                                                        <div></div>
                                                        <div class="custom-file">
                                                            <input id="foto" name="foto" class="custom-file-input {{ $errors->has('foto') ? 'is-invalid':'' }}" placeholder="Upload foto kategori" type="file" value="{{ old('foto') }}">
                                                            <label class="custom-file-label" for="customFile">Pilih file</label>
                                                        </div>
                                                    </div> -->
                                                    <div hidden class="form-group">
                                                        <label for="kt_image_1">Foto Produk</label>
                                                        <div class="foto">
                                                            <div class="image-input image-input-outline" id="kt_image_1">
                                                                <div class="image-input-wrapper" style="background-image: url( {{asset('storage/'.$product->image)}} )"></div>
                            
                                                                <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Ubah foto">
                                                                    <i class="fa fa-pen icon-sm text-muted"></i>
                                                                    <input type="file" name="image" accept=".png, .jpg, .jpeg" />
                                                                    <input type="hidden" name="image_remove" />
                            
                                                                </label>
                            
                                                                <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Ubah foto produk">
                                                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div hidden class="form-group row">
                                                        <label class="form-control-label col-lg-12 col-md-12 col-sm-12" for="input-address">Tag</label>
                                                        <!-- <input id="kt_tagify_5" name="tag" data-role="tagsinput" class="form-control tagify {{ $errors->has('tag') ? 'is-invalid':'' }}" value="{{ old('tag') }}" > -->
                                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <input id="kt_tagify_5" class="form-control tagify {{ $errors->has('tag') ? 'is-invalid':'' }}" name='tag' placeholder="Masukan Tag" value="{{ $product->tag }}" />
                                                        </div>
                                                        @if ($errors->has('tag'))
                                                        <div class="invalid-feedback">{{$errors->first('tag')}}</div>
                                                        @endif
                                                    </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                                                        </div>
                                                    </div>
                                                </form>
                                        </div>
                                    </div>
                                            </td>

                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Dashboard-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection
@section('js')
<!-- <script src="assets/js/pages/features/charts/apexcharts.js"></script> -->
@if(Session::has('message'))
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var type = "{{Session::get('alert-type','success')}}"
    switch (type) {
        case 'success':
            toastr.success("{{Session::get('message')}}");
            // Swal.fire("Berhasil","{{Session::get('message')}}","success");
            // Swal.fire("Good job!", "You clicked the button!", "success");
            break;

    }
</script>
@endif
@endsection