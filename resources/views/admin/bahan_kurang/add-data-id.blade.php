@extends('admin.layout.master')

@section('style')
<link href="{{asset('assets/extra-libs/prism/prism.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/js/jquery-ui.css')}}">

@endsection
@section('title','Update Stok')
@section('content')


<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5" Beli>Bahan</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="/admin/dashboard" class="text-muted">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/admin/bahan" class="text-muted" Beli>Bahan</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Update Stok</a>
                        </li>

                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
                            <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Jam Digital</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-3 ml-4">
                                <a class="btn-disabled d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>

                            <!--end:Item-->
                            <!--begin:Item-->
                            <div class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div disabled class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                                </a>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
                            <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Tanggal</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-12">
                                <a class="d-block text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="d-block text-right pr-3 " style="padding-top: 1px;">

                                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                                </a>
                            </div>
                            <div class="row col-6">
                                <div class="col-12">
                                    <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                                    </a>
                                </div>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>

            </div>
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Notice-->

            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header">
                    <h3 class="card-title">
                        Update Stok {{$bahan->nama_bahan}} ( Stok saat ini :{{$bahan->stok}})
                    </h3>

                </div>
                <!--begin::Form-->
                <form action="/admin/storekurang_bahan/{{$bahan->id}}" class="formValidate" id="formValidate" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="card-body">

                        <div class="form-group">
                            <label class="form-control-label" for="input-address">Tanggal</label>
                            <div class="input-group date">
                                <input id="tanggal_kurang" autocomplete="off" data-date-end-date="0d" value="" type="text" data-tanggal_kurang="tanggal_kurang" name="tanggal_kurang" class="form-control {{ $errors->has('tanggal_kurang') ? 'is-invalid':'' }}" placeholder="Masukkan tanggal" data-error=".errorTxt1">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="errorTxt1"></div>
                            @if ($errors->has('tanggal_kurang'))
                            <div class="invalid-feedback">{{$errors->first('tanggal_kurang')}}</div>
                            @endif
                        </div>
                        <!-- <div class="form-group">
                            <label class="form-control-label" for="input-address">Tanggal Pembelian</label>
                            <div class="">
                                <div class="input-group date">
                                    
                                    <input id="kt_datepicker_2_modal" data-date-format="dd/mm/yyyy" name="tanggal_beli" class="form-control {{ $errors->has('tanggal_beli') ? 'is-invalid':'' }}" readonly="readonly" placeholder="Masukkan kategori" data-error=".errorTxt1">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                    @if ($errors->has('tanggal_beli'))
                                    <div class="invalid-feedback">{{$errors->first('tanggal_beli')}}</div>
                                    @endif
                                </div>
                            </div>
                        </div> -->
                        
                        <div class="form-group " hidden>
                            <label for="id_bahan">Bahan</label><span class="text-danger"> *</span>
                            <select class="form-control" name="id_bahan" id="id_bahan">
                                <option value="" hidden>-- Pilih Kategori --</option>
                                @foreach ($bahans as $bhn)
                                <option value="{{ $bahan->id }}" {{ $bhn->id == $bahan->id ? 'selected':'' }}>{{ ucfirst($bhn->nama_bahan) }} <small>({{ ucfirst($bhn->satuan) }})</small></option>
                                
                                @endforeach
                            </select>
                            
                            @if (('id_bahan') === 0)
                            <div class="invalid-feedback">{{$errors->first('id_bahan')}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="input-address">Stok tersedia</label>
                            <input id="jumlah" name="jumlah" class="form-control {{ $errors->has('jumlah') ? 'is-invalid':'' }}" placeholder="Masukkan jumlah bahan" type="number" value="{{ old('jumlah') }}" data-error=".errorTxt1">
                            <div class="errorTxt1"></div>
                            
                            @if ($errors->has('jumlah'))
                            <!-- <div class="alert invalid-feedback">{{$errors->first('jumlah')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div> -->
                            <div class="invalid-feedback">{{$errors->first('jumlah')}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="input-address">Keterangan</label>
                            <input id="keterangan" name="keterangan" class="form-control {{ $errors->has('keterangan') ? 'is-invalid':'' }}" placeholder="Masukkan keterangan" type="text" value="{{ old('keterangan') }}" data-error=".errorTxt1">
                            <div class="errorTxt1"></div>
                            @if ($errors->has('keterangan'))
                            <!-- <div class="alert invalid-feedback">{{$errors->first('keterangan')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div> -->
                            <div class="invalid-feedback">{{$errors->first('keterangan')}}</div>
                            @endif
                        </div>
                        <!-- <div class="form-group">
                            <label>Gambar</label>
                            <div></div>
                            <div class="custom-file">
                                
                                <input id="gambar" name="gambar" class="custom-file-input {{ $errors->has('gambar') ? 'is-invalid':'' }}" placeholder="Upload foto kategori" type="file" value="{{ old('gambar') }}">
                                <label class="custom-file-label" for="customFile">Pilih file</label>
                            </div>
                        </div> -->

                    </div>
                    <div class="card-footer">
                        <!-- <a class="btn subbtn btn-primary  mr-2">Simpan</a> -->
                        <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        <a href="{{ url()->previous() }}" class="btn btn-secondary">
                            Kembali
                        </a>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js')
<script src="{{asset('assets/extra-libs/prism/prism.js')}}"></script>
<script src="{{asset('dist/js/pages/forms/jquery.validate.min.js')}}"></script>

<script>
    FormValidation.formValidation(
        document.getElementById('formValidate'), {
            fields: {
                tanggal_kurang: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan tanggal pembelian'
                        }
                    }
                },
                id_bahan: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan bahan'
                        }
                    }
                },
                jumlah: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan jumlah bahan'
                        }
                    }
                },
                total: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan satuan'
                        }
                    }
                },
                satuan: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan satuan'
                        }
                    }
                },

            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                // Bootstrap Framework Integration
                bootstrap: new FormValidation.plugins.Bootstrap(),
                // Validate fields when clicking the Submit button
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            }
        }
    );
</script>

<script>
    $(".subbtn").click(function(e) {
        id = e.target.dataset.id;
        Swal.fire({
            title: "Apakah data sudah benar?",
            text: "Data yang sudah masuk tidak dapat di edit!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, benar"
        }).then(function(result) {
            if (result.value) {
                document.getElementById('formValidate').submit();
            } else {}
        });
    });
</script>

<!-- <script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.js')}}"></script> -->

<script type="text/javascript">
    $(document).ready(function() {
        $('#tanggal_kurang').datepicker({
            format: "yyyy/mm/dd",
            todayHighlight: true,
            autoclose: true,
            todayBtn: "linked",
            clearBtn: true,
        });
    });
</script>
<!-- <script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js"></script> -->
<script>
    $('#tanggal_kurang').data('tanggal_kurang').maxDate(0)
</script>

<script>
    var today = moment().format('YYYY/MM/DD');
    document.getElementById("tanggal_kurang").value = today;
    
    </script>

@if(Session::has('message'))
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    var type = "{{Session::get('alert-type','success')}}"
    switch (type) {
        case 'success':
            toastr.success("{{Session::get('message')}}");
            // Swal.fire("Berhasil","{{Session::get('message')}}","success");
            // Swal.fire("Good job!", "You clicked the button!", "success");
            break;
        case 'danger':
            toastr.error("{{Session::get('message')}}");
            // Swal.fire("Berhasil","{{Session::get('message')}}","success");
            // Swal.fire("Good job!", "You clicked the button!", "success");
            break;

    }
</script>
@endif



@endsection