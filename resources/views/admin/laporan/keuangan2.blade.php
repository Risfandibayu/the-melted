@extends('admin.layout.master')

@section('title','Laporan Keuangan')
@section('judul','Laporan Keuangan')
@section('judulhead','Laporan Keuangan')
@section('halaman','Laporan Keuangan')

@section('style')
<link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css"> -->
{{-- <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" /> --}}
<link rel="stylesheet" type="text/css" href="{{asset('assets/daterangepicker.css')}}" />
<style>
.daterangepicker .ranges li {
    font-size: 13px;
    padding: 7px 12px;
    cursor: pointer;
}
.daterangepicker .calendar-table th, .daterangepicker .calendar-table td {
    white-space: nowrap;
    text-align: center;
    vertical-align: middle;
    min-width: 32px;
    width: 35px;
    height: 24px;
    line-height: 33px;
    font-size: 13px;
    border-radius: 4px;
    border: 1px solid transparent;
    white-space: nowrap;
    cursor: pointer;
}
</style>
@endsection

@section('content')

<?php
function pecahtgl($timestamp)
{
    $pecahkan = preg_split('/( |,|-|:)/', $timestamp);
    $hari = array(
        7 => 'Minggu',
        1 => 'Senin',
        2 => 'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu',
    );
    $bulan = array(
        1 =>   'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => ' Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    );
    return $hari[(int)$pecahkan[0]] . ',' . $pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4] . ' ' . $bulan[(int)$pecahkan[5]] . ' ' . $pecahkan[6] . ' ' . $pecahkan[7] . ' ' . $pecahkan[8] . ' ' . $pecahkan[1];
}


?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Laporan Keuangan</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="/dashboard" class="text-muted">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Laporan Keuangan</a>
                        </li>

                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
                            <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Jam Digital</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-3 ml-4">
                                <a class="btn-disabled d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>

                            <!--end:Item-->
                            <!--begin:Item-->
                            <div class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div disabled class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                                </a>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
                            <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Tanggal</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-12">
                                <a class="d-block text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="d-block text-right pr-3 " style="padding-top: 1px;">

                                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                                </a>
                            </div>
                            <div class="row col-6">
                                <div class="col-12">
                                    <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                                    </a>
                                </div>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>

            </div>
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Notice-->

            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="flaticon2-google-drive-file text-primary"></i>
                        </span>
                        <h3 class="card-label">Laporan Keuangan</h3>
                    </div>
                    <div class="card-toolbar col-lg-6 mr-n15">
                        <!--begin::Dropdown-->
                        <div class="row col-lg-12">
                            <div class='input-group col-lg-10' id='kt_daterangepicker_6'>
                                <input type='text' class="form-control" readonly="readonly" placeholder="Pilih range tanggal" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                            <button name="refresh" id="refresh" class="ml-n3 col-lg-2 btn btn-default">Reset</button>
                        </div>
                        <!--end::Dropdown-->
                        <!--begin::Button-->

                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div hidden class="row form-group input-daterange">
                        <div class="col-md-3">
                            <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" readonly />
                        </div>
                        <div class="col-md-3">
                            <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" readonly />
                        </div>
                        <div class="col-md-6">
                            <button type="button" name="filter" id="filter" class="btn m-auto btn-light-primary"><small>Filter</small></button>
                            <button type="button" name="filter" id="today" class="btn m-auto btn-light-primary"><small>Today</small></button>
                            <button type="button" name="filter" id="yesterday" class="btn m-auto btn-light-primary"><small>Yesterday</small></button>
                            <button type="button" name="filter" id="lastweek" class="btn m-auto btn-light-primary"><small>Last Week</small></button>
                            <button type="button" name="filter" id="thisweek" class="btn m-auto btn-light-primary"><small>this Week</small></button>
                            <button type="button" name="filter" id="last30" class="btn m-auto btn-light-primary"><small>Last 30 Days</small></button>
                            <button type="button" name="filter" id="thismonth" class="btn m-auto btn-light-primary"><small>This Month</small></button>
                            <button type="button" name="filter" id="lastmonth" class="btn m-auto btn-light-primary"><small>last Month</small></button>
                            <button type="button" name="filter" id="alltime" class="btn m-auto btn-light-primary"><small>All the time</small></button>
                            <button type="button" name="refresh" id="refresh" class="btn m-auto btn-default"><small>Refresh</small></button>
                        </div>
                    </div>
                    <table class="table table-hover table-checkable display nowrap" id="tabel" style="margin-top: 13px !important">

                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tanggal</th>
                                <th style="width: 20%;">Uang Masuk</th>
                                <th style="width: 20%;">Uang Keluar</th>
                                <th style="width: 20%;">Laba Rugi</th>
                                <!-- <th style="width: 20%;">Laba rugi</th -->
                                <!-- <th>Status Pesanan</th> -->
                                <!-- <th width="15%">Aksi</th> -->
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="2" class="font-weight-bolder font-size-sm" style="text-align:right">Total:</th>
                                <th class="font-weight-bolder font-size-sm" id="totumas"></th>
                                <th class="font-weight-bolder font-size-sm" id="totukel"></th>
                                <th class="font-weight-bolder font-size-h6-sm" id="total"></th>
                            </tr>
                            
                        </tfoot>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js')
<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<!-- <script src="assets/plugins/global/plugins.bundle.js"></script>
<script src="assets/plugins/custom/prismjs/prismjs.bundle.js"></script> -->
<script src="assets/js/scripts.bundle.js"></script>
<!-- <script src="{{asset('assets/js/pages/crud/datatables/data-sources/html.js')}}"></script> -->
<!-- <script src="{{asset('assets/js/pages/features/miscellaneous/toastr.js')}}"></script> -->
<!-- <script src="{{asset('assets/js/pages/features/miscellaneous/sweetalert2.js')}}"></script> -->
<!-- <script>
    $(document).ready(function() {
        $('#tabel').DataTable({
            dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            responsive: true,
            fixedHeader: {
                header: true,
                footer: true
            },
            border: {
                footer: true,
            },
            "footerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,Rp .<small><em>Tidak ada uang masuk</em></small><small><em>Tidak ada uang keluar</em></small>]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                function formatNumber(num) {
                    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                }

                // Total over all pages

                total2 = api
                    .column(2)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                pageTotal2 = api
                    .column(2, {
                        page: 'current'
                    })
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(2).footer()).html(
                    // 'Rp ' + formatNumber(pageTotal2)
                    'Rp ' + formatNumber(total2)
                );
                total3 = api
                    .column(3)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                pageTotal3 = api
                    .column(3, {
                        page: 'current'
                    })
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(3).footer()).html(
                    // 'Rp ' + formatNumber(pageTotal3)
                    'Rp ' + formatNumber(total3)
                );


                if (pageTotal3 > pageTotal2 && total3 > total2) {
                    total4 = total3 - total2;
                    pageTotal4 = pageTotal3 - pageTotal2;
                    $(api.column(4).footer()).html(
                        'Rp ' + formatNumber(pageTotal4) + '( Total : ' + formatNumber(total4) + ')'
                    );
                    totall = document.querySelector('#total');
                    totall.classList.add('text-danger');
                    totall.classList.add('font-weight-boldest');
                    totall.classList.add('font-size-h5-xl');
                } else if (pageTotal2 > pageTotal3 && total2 > total3) {
                    total4 = total2 - total3;
                    pageTotal4 = pageTotal2 - pageTotal3;
                    $(api.column(4).footer()).html(
                        // 'Rp ' + formatNumber(pageTotal4) + '( Total : '+ formatNumber(total4)+')'
                        'Rp ' + formatNumber(total4)
                    );
                    totall = document.querySelector('#total');
                    totall.classList.remove('text-danger');
                    totall.classList.add('text-primary');
                    totall.classList.add('font-weight-boldest');
                    totall.classList.add('font-size-h5-xl');
                } else {
                    pageTotal4 = 0;
                    total4 = 0;
                    $(api.column(4).footer()).html(
                        // 'Rp ' + formatNumber(pageTotal4) + '( Total : '+ formatNumber(total4)+')'
                        'Rp ' + formatNumber(total4)
                    );
                    totall = document.querySelector('#total');
                    totall.classList.add('text-primary');
                    totall.classList.add('font-weight-boldest');
                    totall.classList.add('font-size-h5-xl');
                }
            }
        });
    });
</script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js" type="text/javascript"></script> -->
<!-- <script src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js" type="text/javascript"></script> -->
<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script> -->

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script> -->
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('assets/js/datetime-moment.js')}}"></script>

<script>
    $(document).ready(function() {
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        load_data();
        var $no = 0;
        $no++;


        function load_data(from_date = '', to_date = '') {
            $.fn.dataTable.ext.errMode = 'throw';

            $.fn.dataTable.moment('HH:mm MMM D, YY');
            $.fn.dataTable.moment('dddd, MMMM Do, YYYY');
            $.fn.dataTable.moment('dd-mm-yyyy');
            $('#tabel').DataTable({
                paging: false,
                "fnDrawCallback": function(oSettings) {
                    function getLastWeek() {
                        var today = new Date();
                        var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
                        return lastWeek;
                    }

                    var lastWeek = getLastWeek();
                    var lastWeekMonth = lastWeek.getMonth() + 1;
                    var mon = lastWeek.getMonth();
                    var lastWeekDay = lastWeek.getDate();
                    var lastWeekYear = lastWeek.getFullYear();

                    // var lastWeekDisplay = lastWeekMonth + "/" + lastWeekDay + "/" + lastWeekYear;
                    var lastWeekDisplay = lastWeekYear + "-" + lastWeekMonth + "-" + lastWeekDay;

                    var today = new Date();
                    var dd = String(today.getDate()).padStart(2, '0');
                    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var m = today.getMonth(); //January is 0!
                    var yyyy = today.getFullYear();
                    // today = mm + '/' + dd + '/' + yyyy;
                    today = yyyy + '-' + mm + '-' + dd;
                    // today = '2021-01-26';

                    var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                    var bulann = bulan[m];
                    var bul = bulan[mon];

                    fri = lastWeekDay + ' ' + bul + ' ' + lastWeekYear
                    tod = dd + ' ' + bulann + ' ' + yyyy

                    function toDate(dateStr) {
                        var parts = dateStr.split("-")
                        return (parts[2] + '-' + parts[1] + '-' + parts[0])
                    }
                    fr = toDate(lastWeekDisplay)
                    t = toDate(today)
                    // var from_date = lastWeekDisplay;
                    // var to_date = today;
                    // var from = $('#from_date').val(first);
                    // var to = $('#to_date').val(last);
                    if (from_date == '' && to_date == '') {
                        $('#tabel').DataTable().destroy();
                        from_date = lastWeekDisplay;
                        to_date = today;
                        load_data(from_date, to_date);
                        $('#from_date').val(lastWeekDisplay);
                        $('#to_date').val(today);
                        $("div.toolbar").html('Keuangan 7 hari terakhir');
                        $("div.ket").html('(Tanggal ' + fri + ' s/d hari ini)');
                        $('#kt_daterangepicker_6 .form-control').val('7 Hari Terakhir');
                    }
                },
                "sDom": `
                <'row'<'toolbar font-weight-bolder font-size-h4 mt-1 col-md-8'><'col-md-4'f>>
                <'row'<'ket font-size-h6-xxl mt-n4 mb-n5 col-md-8'><'col-md-4 mb-n5'>>
                <'row'<'col-sm-12'tr>><'row'<'col-sm-6'><'col-sm-6'p>>`,
                //     "sDom": `<'row'>
                // <'row'<'col-md-5'l><'toolbar col-md-3'><'col-md-4'frtip>>`,
                //     dom: `<'row'<'col-sm-12'tr>>
                // <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-3'l><'col-sm-12 col-md-4'p>>`,
                fixedHeader: {
                    header: true,
                    footer: true
                },
                columnDefs: [{
                    target: 1, //index of column
                    type: 'datetime',
                    def: function() {
                        return new Date();
                    },
                    format: 'dd-mm-yyyy',
                }],
                columnDefs: [{
                    target: 2, //index of column
                    "render": function(data, type, row) {
                        return data + ' (' + row[3] + ')';
                    },
                }],
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.transaksi.uang2") }}',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                "fnCreatedRow": function(row, data, index) {
                    $('td', row).eq(0).html(index + 1);
                },
                "footerCallback": function(row, data, start, end, display) {
                    totumas = document.querySelector('#totumas');
                    totumas.classList.add('font-weight-bolder');
                    totumas.classList.add('font-size-sm');
                    totumas = document.querySelector('#totukel');
                    totumas.classList.add('font-weight-bolder');
                    totumas.classList.add('font-size-sm');
                    var api = this.api(),
                        data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function(i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,Rp .<small><em>Tidak ada uang masuk</em></small><small><em>Tidak ada uang keluar</em></small>]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };

                    function formatNumber(num) {
                        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                    }

                    // Total over all pages

                    total2 = api
                        .column(2)
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal2 = api
                        .column(2, {
                            page: 'current'
                        })
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(2).footer()).html(
                        'Rp ' + formatNumber(total2)
                        // 'Rp ' + formatNumber(pageTotal2)
                        // 'Rp ' + formatNumber(total2) + '<br> Rp ' + formatNumber(pageTotal2)

                    );
                    
                    
                    total3 = api
                        .column(3)
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal3 = api
                        .column(3, {
                            page: 'current'
                        })
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(3).footer()).html(
                        'Rp ' + formatNumber(total3)
                        // 'Rp ' + formatNumber(pageTotal3)
                        // 'Rp ' + formatNumber(total3) + '<br>Rp ' + formatNumber(pageTotal3)
                    );

                    if (pageTotal3 > pageTotal2 || total3 > total2) {
                        total4 = total3 - total2;
                        pageTotal4 = pageTotal3 - pageTotal2;
                        $(api.column(4).footer()).html(
                            'Rp ' + formatNumber(total4)
                            // 'Rp ' + formatNumber(total4) + '<br>Rp ' + formatNumber(pageTotal4)
                        );
                        totall = document.querySelector('#total');
                        totall.classList.add('text-danger');
                        totall.classList.add('font-weight-bolder');
                        totall.classList.add('font-size-h5-xl');
                    } else if (pageTotal2 > pageTotal3 || total2 > total3) {
                        total4 = total2 - total3;
                        pageTotal4 = pageTotal2 - pageTotal3;
                        $(api.column(4).footer()).html(
                            // 'Rp ' + formatNumber(pageTotal4) + '( Total : '+ formatNumber(total4)+')'
                            'Rp ' + formatNumber(total4)
                            // 'Rp ' + formatNumber(total4) + '<br>Rp ' + formatNumber(pageTotal4)
                        );
                        totall = document.querySelector('#total');
                        totall.classList.remove('text-danger');
                        totall.classList.add('text-primary');
                        totall.classList.add('font-weight-bolder');
                        totall.classList.add('font-size-h5-xl');
                    } else {
                        pageTotal4 = 0;
                        total4 = 0;
                        $(api.column(4).footer()).html(
                            // 'Rp ' + formatNumber(pageTotal4) + '( Total : '+ formatNumber(total4)+')'
                            'Rp ' + formatNumber(total4)
                            // 'Rp ' + formatNumber(total4) + '<br>Rp ' + formatNumber(pageTotal4)
                        );
                        totall = document.querySelector('#total');
                        totall.classList.add('text-primary');
                        totall.classList.add('font-weight-bolder');
                        totall.classList.add('font-size-h5-xl');
                    }
                },
                columns: [{
                        data: 'tanggalno',
                        name: 'tanggalno',
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'totalorder',
                        name: 'totalorder',

                    },
                    {
                        data: 'totalbeli',
                        name: 'totalbeli',
                    },
                    {
                        data: 'labarugi',
                        name: 'labarugi',
                    },
                ],

            });
        }

        $('#filter').click(function() {

            var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

            function toDate(dateStr) {
                var parts = dateStr.split("-")
                return (parts[2] + ' ' + bulan[parts[1] - 1] + ' ' + parts[0])
            }
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            fr = toDate(from_date);
            t = toDate(to_date);



            if (from_date != '' && to_date != '') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan');
                $("div.ket").html('(Tanggal ' + fr + ' s/d ' + t + ')');
            } else {
                alert('Both Date is required');
            }
        });
        $('#thismonth').click(function() {

            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            function convert(str) {
                var date = new Date(str),
                    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                    day = ("0" + date.getDate()).slice(-2);
                return [date.getFullYear(), mnth, day].join("-");
            }
            var first = convert(firstDay)
            var last = convert(lastDay)

            var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

            function toDate(dateStr) {
                var parts = dateStr.split("-")
                return (parts[2] + ' ' + bulan[parts[1] - 1] + ' ' + parts[0])
            }
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            fr = toDate(first);
            t = toDate(last);

            var from_date = first;
            var to_date = last;
            var from = $('#from_date').val(first);
            var to = $('#to_date').val(last);
            if (from_date != '' && to_date != '') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan Bulan Ini');
                $("div.ket").html('(Tanggal ' + fr + ' s/d ' + t + ')');
            } else {
                alert('Both Date is required');
            }
        });

        $('#thisweek').click(function() {

            function convert(str) {
                var date = new Date(str),
                    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                    day = ("0" + date.getDate()).slice(-2);
                return [date.getFullYear(), mnth, day].join("-");
            }

            var curr = new Date; // get current date
            var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
            var last = first + 6; // last day is the first day + 6

            var firstday = new Date(curr.setDate(first)).toUTCString();
            var lastday = new Date(curr.setDate(last)).toUTCString();

            var first = convert(firstday)
            var last = convert(lastday)

            var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

            function toDate(dateStr) {
                var parts = dateStr.split("-")
                return (parts[2] + ' ' + bulan[parts[1] - 1] + ' ' + parts[0])
            }
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            fr = toDate(first);
            t = toDate(last);

            var from_date = first;
            var to_date = last;
            var from = $('#from_date').val(first);
            var to = $('#to_date').val(last);
            if (from_date != '' && to_date != '') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan Minggu ini');
                $("div.ket").html('(Tanggal ' + fr + ' s/d ' + t + ')');
            } else {
                alert('Both Date is required');
            }
        });


        $('#lastmonth').click(function() {

            var date = new Date();
            var firstDay = new Date(date.getFullYear(), (date.getMonth() - 1), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);

            function convert(str) {
                var date = new Date(str),
                    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                    day = ("0" + date.getDate()).slice(-2);
                return [date.getFullYear(), mnth, day].join("-");
            }
            var first = convert(firstDay)
            var last = convert(lastDay)

            var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

            function toDate(dateStr) {
                var parts = dateStr.split("-")
                return (parts[2] + ' ' + bulan[parts[1] - 1] + ' ' + parts[0])
            }
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            fr = toDate(first);
            t = toDate(last);

            var from_date = first;
            var to_date = last;
            var from = $('#from_date').val(first);
            var to = $('#to_date').val(last);
            if (from_date != '' && to_date != '') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan Bulan lalu');
                $("div.ket").html('(Tanggal ' + fr + ' s/d ' + t + ')');
            } else {
                alert('Both Date is required');
            }
        });
        $('#last30').click(function() {

            // var date = new Date();
            // var firstDay = new Date(date.getFullYear(), (date.getMonth() - 1), 1);
            // var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
            let now = new Date()
            var firstt = now.toUTCString()
            let last30days = new Date(now.setDate(now.getDate() - 30))
            var lastt = last30days.toUTCString()

            function convert(str) {
                var date = new Date(str),
                    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                    day = ("0" + date.getDate()).slice(-2);
                return [date.getFullYear(), mnth, day].join("-");
            }
            var first = convert(lastt)
            var last = convert(firstt)

            var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

            function toDate(dateStr) {
                var parts = dateStr.split("-")
                return (parts[2] + ' ' + bulan[parts[1] - 1] + ' ' + parts[0])
            }
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            fr = toDate(first);
            t = toDate(last);

            var from_date = first;
            var to_date = last;
            var from = $('#from_date').val(first);
            var to = $('#to_date').val(last);
            if (from_date != '' && to_date != '') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan 30 hari terakhir');
                $("div.ket").html('(Tanggal ' + fr + ' s/d ' + t + ')');
            } else {
                alert('Both Date is required');
            }
        });
        $('#today').click(function() {


            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            // today = mm + '/' + dd + '/' + yyyy;
            today = yyyy + '-' + mm + '-' + dd;


            var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

            function toDate(dateStr) {
                var parts = dateStr.split("-")
                return (parts[2] + ' ' + bulan[parts[1] - 1] + ' ' + parts[0])
            }
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            fr = toDate(today);
            t = toDate(today);

            // today = '2021-01-26';
            var from = $('#from_date').val(today);
            var to = $('#to_date').val(today);
            var from_date = today;
            var to_date = today;
            if (from_date != '' && to_date != '') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan Hari ini');
                $("div.ket").html('(Tanggal ' + fr + ')');
            } else {
                alert('Both Date is required');
            }
        });
        $('#yesterday').click(function() {


            var today = new Date();
            var dd = String(today.getDate() - 1).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            // today = mm + '/' + dd + '/' + yyyy;
            var yesterday = yyyy + '-' + mm + '-' + dd;
            // yesterday = '2021-01-26';
            var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

            function toDate(dateStr) {
                var parts = dateStr.split("-")
                return (parts[2] + ' ' + bulan[parts[1] - 1] + ' ' + parts[0])
            }
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            fr = toDate(yesterday);
            t = toDate(yesterday);

            var from = $('#from_date').val(yesterday);
            var to = $('#to_date').val(yesterday);
            var from_date = yesterday;
            var to_date = yesterday;
            if (from_date != '' && to_date != '') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan Hari Kemarin');
                $("div.ket").html('(Tanggal ' + fr + ')');
            } else {
                alert('Both Date is required');
            }
        });

        $('#lastweek').click(function() {

            function getLastWeek() {
                var today = new Date();
                var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
                return lastWeek;
            }

            var lastWeek = getLastWeek();
            var lastWeekMonth = lastWeek.getMonth() + 1;
            var mon = lastWeek.getMonth();
            var lastWeekDay = lastWeek.getDate();
            var lastWeekYear = lastWeek.getFullYear();

            // var lastWeekDisplay = lastWeekMonth + "/" + lastWeekDay + "/" + lastWeekYear;
            var lastWeekDisplay = lastWeekYear + "-" + lastWeekMonth + "-" + lastWeekDay;

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var m = today.getMonth(); //January is 0!
            var yyyy = today.getFullYear();
            // today = mm + '/' + dd + '/' + yyyy;
            today = yyyy + '-' + mm + '-' + dd;
            // today = '2021-01-26';

            var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
            var bulann = bulan[m];
            var bul = bulan[mon];

            fri = lastWeekDay + ' ' + bul + ' ' + lastWeekYear
            tod = dd + ' ' + bulann + ' ' + yyyy

            function toDate(dateStr) {
                var parts = dateStr.split("-")
                return (parts[2] + '-' + parts[1] + '-' + parts[0])
            }
            fr = toDate(lastWeekDisplay)
            t = toDate(today)
            var from_date = lastWeekDisplay;
            var to_date = today;
            var from = $('#from_date').val(lastWeekDisplay);
            var to = $('#to_date').val(today);
            if (from_date != '' && to_date != '') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan 7 hari terakhir');
                $("div.ket").html('(Tanggal ' + fri + ' s/d hari ini)');
            } else {
                alert('Both Date is required');
            }
        });



        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#tabel').DataTable().destroy();
            // $('#kt_daterangepicker_6 .form-control').val('');
            $('#kt_daterangepicker_6 .form-control').val('7 Hari Terakhir');
            $('*[data-range-key="30 Hari Terakhir"]').removeClass();
            $('*[data-range-key="7 Hari Terakhir"]').addClass('active');
            load_data();
        });
        $('#alltime').click(function() {
            $('#tabel').DataTable().destroy();
            from_date = '0000-00-00';
            to_date = '9999-12-30';
            $('#from_date').val('Semua waktu');
            $('#to_date').val('Semua Waktu');
            load_data(from_date, to_date);
            $("div.toolbar").html('Keuangan semua waktu');
        });

    });
</script>

<script>
    function del() {
        $(".deletebtn").click(function(e) {

            id = e.target.dataset.id;
            Swal.fire({
                title: "Apakah anda yakin?",
                text: "Data yang sudah anda hapus tidak akan bisa kembali!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya, Hapus"
            }).then(function(result) {
                if (result.value) {

                    Swal.fire(
                        "Terhapus!",
                        "Data telah terhapus.",
                        "success"
                    );
                    $(`#delete${id}`).submit();

                } else {

                }
            });
        });
    }
</script>

@if(Session::has('message'))
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var type = "{{Session::get('alert-type','success')}}"
    switch (type) {
        case 'success':
            toastr.success("{{Session::get('message')}}");
            // Swal.fire("Berhasil","{{Session::get('message')}}","success");
            // Swal.fire("Good job!", "You clicked the button!", "success");
            break;

    }
</script>
@endif

<script>
    var start = moment().subtract(29, 'days');
    var end = moment();


    $('#kt_daterangepicker_6').daterangepicker({

        buttonClasses: ' btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        opens: 'left',
        startDate: start,
        endDate: end,
        "locale": {
            "separator": " s/d ",
            "applyLabel": "Lihat",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "format": 'DD/MM/yyyy',
            "toLabel": "To",
            "customRangeLabel": "Custom Range",
            "weekLabel": "W",
            "daysOfWeek": [
                "Min",
                "Sen",
                "Sel",
                "Rab",
                "Kam",
                "Jum",
                "Sab"
            ],
            "monthNames": [
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember"
            ],
            "firstDay": 0
        },
        ranges: {
            'Hari Ini': [moment(), moment()],
            'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
            'Minggu Ini': [moment().subtract(6, 'days'), moment()],
            'Minggu Lalu': [moment().startOf('week').subtract(1, 'week'), moment().endOf('week').subtract(1, 'week')],
            '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
            'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
            'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Semua Waktu': [moment().year(), moment()],
        }

    }, function(start, end, label) {
        // $('#kt_daterangepicker_6 .form-control').val(label+start.format('YYYY-MM-DD') + ' s/d ' + end.format('YYYY-MM-DD'));
        // if (label == 'Custom Range') {
        //     $('#kt_daterangepicker_6 .form-control').val(fr + ' s/d ' + t);
        // } else {
        //     $('#kt_daterangepicker_6 .form-control').val(label);
        // }

        
        function load_data(from_date = '', to_date = '') {
            $.fn.dataTable.ext.errMode = 'throw';

            $.fn.dataTable.moment('HH:mm MMM D, YY');
            $.fn.dataTable.moment('dddd, MMMM Do, YYYY');
            $.fn.dataTable.moment('dd/mm/yyyy');
            $('#tabel').DataTable({
                paging: false,
                "fnDrawCallback": function(oSettings) {
                    function getLastWeek() {
                        var today = new Date();
                        var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
                        return lastWeek;
                    }

                    var lastWeek = getLastWeek();
                    var lastWeekMonth = lastWeek.getMonth() + 1;
                    var mon = lastWeek.getMonth();
                    var lastWeekDay = lastWeek.getDate();
                    var lastWeekYear = lastWeek.getFullYear();

                    // var lastWeekDisplay = lastWeekMonth + "/" + lastWeekDay + "/" + lastWeekYear;
                    var lastWeekDisplay = lastWeekYear + "-" + lastWeekMonth + "-" + lastWeekDay;

                    var today = new Date();
                    var dd = String(today.getDate()).padStart(2, '0');
                    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var m = today.getMonth(); //January is 0!
                    var yyyy = today.getFullYear();
                    // today = mm + '/' + dd + '/' + yyyy;
                    today = yyyy + '-' + mm + '-' + dd;
                    // today = '2021-01-26';

                    var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                    var bulann = bulan[m];
                    var bul = bulan[mon];

                    fri = lastWeekDay + ' ' + bul + ' ' + lastWeekYear
                    tod = dd + ' ' + bulann + ' ' + yyyy

                    function toDate(dateStr) {
                        var parts = dateStr.split("-")
                        return (parts[2] + '-' + parts[1] + '-' + parts[0])
                    }
                    fr = toDate(lastWeekDisplay)
                    t = toDate(today)
                    // var from_date = lastWeekDisplay;
                    // var to_date = today;
                    // var from = $('#from_date').val(first);
                    // var to = $('#to_date').val(last);
                    if (from_date == '' && to_date == '') {
                        $('#tabel').DataTable().destroy();
                        from_date = lastWeekDisplay;
                        to_date = today;
                        load_data(from_date, to_date);
                        $('#from_date').val(lastWeekDisplay);
                        $('#to_date').val(today);
                        $("div.toolbar").html('Keuangan 7 hari terakhir');
                        $("div.ket").html('(Tanggal ' + fri + ' s/d hari ini)');
                        $('*[data-range-key="30 Hari Terakhir"]').removeClass();
                        $('*[data-range-key="7 Hari Terakhir"]').addClass('active');
                    }
                },
                "sDom": `
                <'row'<'toolbar font-weight-bolder font-size-h4 mt-1 col-md-8'><'col-md-4'f>>
                <'row'<'ket font-size-h6-xxl mt-n4 mb-n5 col-md-8'><'col-md-4 mb-n5'>>
                <'row'<'col-sm-12'tr>><'row'<'col-sm-6'><'col-sm-6'p>>`,
                //     "sDom": `<'row'>
                // <'row'<'col-md-5'l><'toolbar col-md-3'><'col-md-4'frtip>>`,
                //     dom: `<'row'<'col-sm-12'tr>>
                // <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-3'l><'col-sm-12 col-md-4'p>>`,
                fixedHeader: {
                    header: true,
                    footer: true
                },
                columnDefs: [{
                    target: 1, //index of column
                    type: 'datetime',
                    def: function() {
                        return new Date();
                    },
                    format: 'dd-mm-yyyy',
                }],
                columnDefs: [{
                    target: 2, //index of column
                    "render": function(data, type, row) {
                        return data + ' (' + row[3] + ')';
                    },
                }],
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.transaksi.uang2") }}',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                "fnCreatedRow": function(row, data, index) {
                    $('td', row).eq(0).html(index + 1);
                },
                "footerCallback": function(row, data, start, end, display) {
                    totumas = document.querySelector('#totumas');
                    totumas.classList.add('font-weight-bolder');
                    totumas.classList.add('font-size-sm');
                    totumas = document.querySelector('#totukel');
                    totumas.classList.add('font-weight-bolder');
                    totumas.classList.add('font-size-sm');
                    var api = this.api(),
                        data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function(i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,Rp .<small><em>Tidak ada uang masuk</em></small><small><em>Tidak ada uang keluar</em></small>]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };

                    function formatNumber(num) {
                        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                    }

                    // Total over all pages

                    total2 = api
                        .column(2)
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal2 = api
                        .column(2, {
                            page: 'current'
                        })
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(2).footer()).html(
                        // 'Rp ' + formatNumber(pageTotal2)
                        'Rp ' + formatNumber(total2)
                        // 'Rp ' + formatNumber(total2) + '<br> Rp ' + formatNumber(pageTotal2)

                    );

                    total3 = api
                        .column(3)
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal3 = api
                        .column(3, {
                            page: 'current'
                        })
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(3).footer()).html(
                        // 'Rp ' + formatNumber(pageTotal3)
                        'Rp ' + formatNumber(total3)
                        // 'Rp ' + formatNumber(total3) + '<br> Rp ' + formatNumber(pageTotal3)
                    );

                    if (pageTotal3 > pageTotal2 && total3 > total2) {
                        total4 = total3 - total2;
                        pageTotal4 = pageTotal3 - pageTotal2;
                        $(api.column(4).footer()).html(
                            'Rp ' + formatNumber(total4)
                            // 'Rp ' + formatNumber(total4) + '<br> Rp ' + formatNumber(pageTotal4)
                        );
                        totall = document.querySelector('#total');
                        totall.classList.add('text-danger');
                        totall.classList.add('font-weight-bolder');
                        totall.classList.add('font-size-h5-xl');
                    } else if (pageTotal2 > pageTotal3 && total2 > total3) {
                        total4 = total2 - total3;
                        pageTotal4 = pageTotal2 - pageTotal3;
                        $(api.column(4).footer()).html(
                            // 'Rp ' + formatNumber(pageTotal4) + '( Total : '+ formatNumber(total4)+')'
                            'Rp ' + formatNumber(total4)
                            // 'Rp ' + formatNumber(total4) + '<br> Rp ' + formatNumber(pageTotal4)
                        );
                        totall = document.querySelector('#total');
                        totall.classList.remove('text-danger');
                        totall.classList.add('text-primary');
                        totall.classList.add('font-weight-bolder');
                        totall.classList.add('font-size-h5-xl');
                    } else {
                        pageTotal4 = 0;
                        total4 = 0;
                        $(api.column(4).footer()).html(
                            // 'Rp ' + formatNumber(pageTotal4) + '( Total : '+ formatNumber(total4)+')'
                            'Rp ' + formatNumber(total4)
                            // 'Rp ' + formatNumber(total4) + '<br> Rp ' + formatNumber(pageTotal4)
                        );
                        totall = document.querySelector('#total');
                        totall.classList.add('text-primary');
                        totall.classList.add('font-weight-bolder');
                        totall.classList.add('font-size-h5-xl');
                    }
                },
                columns: [{
                        data: 'tanggalno',
                        name: 'tanggalno'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'totalorder',
                        name: 'totalorder',

                    },
                    {
                        data: 'totalbeli',
                        name: 'totalbeli'
                    },
                    {
                        data: 'labarugi',
                        name: 'labarugi',

                    },
                ],

            });
        }

        function convert(str) {
            var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
            return [date.getFullYear(), mnth, day].join("-");
        }
        from_date = convert(start);
        to_date = convert(end);
        $('#from_date').val(from_date);
        $('#to_date').val(to_date);
        var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        function toDate(dateStr) {
            var parts = dateStr.split("-")
            return (parts[2] + ' ' + bulan[parts[1] - 1] + ' ' + parts[0])
        }
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        fr = toDate(from_date);
        t = toDate(to_date);

        if (label == 'Custom Range') {
            $('#kt_daterangepicker_6 .form-control').val(fr + ' s/d ' + t);
        } else {
            $('#kt_daterangepicker_6 .form-control').val(label);
        }

        // load_data(from_date, to_date);
        if (from_date != '' && to_date != '') {

            if (label == 'Hari Ini') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan hari ini');
                $("div.ket").html('(Tanggal ' + fr + ')');
            } else if (label == 'Kemarin') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan kemarin');
                $("div.ket").html('(Tanggal ' + fr + ')');
            } else if (label == '7 Hari Terakhir') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan 7 hari terakhir');
                $("div.ket").html('(Tanggal ' + fr + ' s/d hari ini)');
            } else if (label == 'Minggu Ini') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan minggu ini');
                $("div.ket").html('(Tanggal ' + fr + ' s/d ' + t + ')');
            } else if (label == 'Minggu Lalu') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan minggu lalu');
                $("div.ket").html('(Tanggal ' + fr + ' s/d ' + t + ')');
            } else if (label == '30 Hari Terakhir') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan 30 hari terakhir');
                $("div.ket").html('(Tanggal ' + fr + ' s/d hari ini)');
            } else if (label == 'Bulan Ini') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan bulan ini');
                $("div.ket").html('(Tanggal ' + fr + ' s/d ' + t + ')');
            } else if (label == 'Bulan Lalu') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan bulan lalu');
                $("div.ket").html('(Tanggal ' + fr + ' s/d ' + t + ')');
            } else if (label == 'Semua Waktu') {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan Keseluruhan');
                // $("div.ket").html('(Tanggal ' + fr + ')');
            } else {
                $('#tabel').DataTable().destroy();
                load_data(from_date, to_date);
                $("div.toolbar").html('Keuangan range tanggal');
                $("div.ket").html('(Tanggal ' + fr + ' s/d ' + t + ')');
            }
        } else {
            alert('Both Date is required');
        }
    });

    $('*[data-range-key="30 Hari Terakhir"]').removeClass();
    $('*[data-range-key="7 Hari Terakhir"]').addClass('active');
</script>
@endsection