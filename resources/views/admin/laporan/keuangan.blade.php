@extends('admin.layout.master')

@section('title','Laporan Keuangan')
@section('judul','Laporan Keuangan')
@section('judulhead','Laporan Keuangan')
@section('halaman','Laporan Keuangan')

@section('style')
<link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css"> -->
@endsection

@section('content')

<?php
function pecahtgl($timestamp)
{
    $pecahkan = preg_split('/( |,|-|:)/', $timestamp);
    $hari = array(
        7 => 'Minggu',
        1 => 'Senin',
        2 => 'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu',
    );
    $bulan = array(
        1 =>   'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => ' Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    );
    return $hari[(int)$pecahkan[0]] . ',' . $pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4] . ' ' . $bulan[(int)$pecahkan[5]] . ' ' . $pecahkan[6] . ' ' . $pecahkan[7] . ' ' . $pecahkan[8] . ' ' . $pecahkan[1];
}


?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Laporan Keuangan</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="/dashboard" class="text-muted">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Laporan Keuangan</a>
                        </li>

                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
                            <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Jam Digital</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-3 ml-4">
                                <a class="btn-disabled d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>

                            <!--end:Item-->
                            <!--begin:Item-->
                            <div class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div disabled class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                                </a>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
                            <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Tanggal</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-12">
                                <a class="d-block text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="d-block text-right pr-3 " style="padding-top: 1px;">

                                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                                </a>
                            </div>
                            <div class="row col-6">
                                <div class="col-12">
                                    <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                                    </a>
                                </div>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>

            </div>
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Notice-->

            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="flaticon2-google-drive-file text-primary"></i>
                        </span>
                        <h3 class="card-label">Laporan Keuangan</h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Dropdown-->

                        <!--end::Dropdown-->
                        <!--begin::Button-->

                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table table-hover table-checkable display nowrap" id="tabel" style="margin-top: 13px !important">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>Tanggal</th>
                                <th style="width: 20%;">Uang Masuk</th>
                                <th style="width: 20%;">Uang Keluar</th>
                                <th style="width: 20%;">Laba rugi</th>
                                <!-- <th>Status Pesanan</th> -->
                                <!-- <th width="15%">Aksi</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 0;
                            $Pemasukan = 0;
                            $Pengeluaran = 0;
                            $lababersih = 0;

                            ?>
                            @foreach($uang as $uang)
                            <?php
                            $no++;
                            $Pemasukan += $uang->totalorder;
                            $Pengeluaran += $uang->totalbeli;
                            $lababersih += ($uang->totalorder - $uang->totalbeli);
                            $laba = ($uang->totalorder - $uang->totalbeli);
                            $rugi = ($uang->totalbeli - $uang->totalorder);

                            ?>
                            <tr>
                                <td align="center">{{$no}}</td>
                                <!-- <td>{{ $uang->tanggal }}</td> -->
                                <!-- <td>{{ pecahtgl(date('N, d - m - Y', strtotime($uang->tanggal)))}}</td> -->
                                <td>{{date('d/m/Y', strtotime($uang->tanggal))}}</td>
                                <!-- <td>{{ $uang->tanggal_beli }}</td> -->
                                <!-- <td>{{ $uang->tanggalorder }}</td> -->

                                <!-- {{$uang->totalorder}} -->
                                @if($uang->totalorder == NULL)
                                <td>
                                    <small><em>Tidak ada uang masuk</em></small>
                                </td>
                                @else
                                <Strong>
                                    <td class="font-weight-bolder font-size-sm">
                                        Rp {{ number_format($uang->totalorder,0,',','.') }}
                                    </td>
                                </Strong>
                                @endif
                                <!-- {{$uang->totalbeli}} -->
                                @if($uang->totalbeli == NULL)
                                <td>
                                    <small><em>Tidak ada uang keluar</em></small>
                                </td>
                                @else
                                <strong>
                                    <td class="font-weight-bolder font-size-sm">
                                        Rp {{ number_format($uang->totalbeli,0,',','.') }}
                                    </td>
                                </strong>
                                @endif
                                <!-- <td>{{ $uang->totalorder }}</td> -->
                                <!-- <td>{{ $uang->totalorder -$uang->totalbeli }}</td> -->
                                <!-- Rp {{ number_format($uang->totalorder-$uang->totalbeli,0,',','.')}} -->
                                <!-- {{$uang->totalbeli-$uang->totalorder}} -->

                                @if( $uang->totalorder - $uang->totalbeli < 0) <span class="text-danger ">
                                    <strong>
                                        <td class="text-danger font-weight-bolder font-size-sm">
                                            Rp {{ number_format($rugi,0,',','.') }}
                                        </td>
                                    </strong>
                                    </span>
                                    @else
                                    <span>
                                        <strong>
                                            <td class="text-success font-weight-bolder font-size-h6-xl">
                                                Rp {{ number_format($laba,0,',','.') }}
                                            </td>
                                        </strong>
                                    </span>
                                    @endif




                            </tr>
                            @endforeach
                        </tbody>
                        <!-- <tfoot style="font-weight: bold; border-top: 1px double; border-collapse: separate; border-spacing: 3px;">
                            <tr>
                                <td colspan="2" style="text-align: right"><strong>Total</strong></td>
                                <td><strong>Rp. {{ number_format($Pemasukan,0,',','.') }}</strong></td>
                                <td><strong>Rp. {{ number_format($Pengeluaran,0,',','.') }}</strong></td>
                                <td>
                                    <span class="text-primary font-weight-boldest font-size-h5">
                                        <strong>
                                            Rp. {{ number_format($lababersih,0,',','.') }}
                                        </strong>
                                    </span>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot> -->
                        <tfoot>
                            <tr>
                                <th colspan="2" class="font-weight-bolder font-size-sm" style="text-align:right">Total:</th>
                                <th class="font-weight-bolder font-size-sm"></th>
                                <th class="font-weight-bolder font-size-sm"></th>
                                <th class="font-weight-bolder font-size-h6-sm" id="total"></th>
                            </tr>
                        </tfoot>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js')
<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<!-- <script src="{{asset('assets/js/pages/crud/datatables/data-sources/html.js')}}"></script> -->
<!-- <script src="{{asset('assets/js/pages/features/miscellaneous/toastr.js')}}"></script> -->
<!-- <script src="{{asset('assets/js/pages/features/miscellaneous/sweetalert2.js')}}"></script> -->
<script>
    $(document).ready(function() {
        $('#tabel').DataTable({
            // dom: `<'row'<'col-sm-12'tr>>
			// <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            responsive: true,
            fixedHeader: {
                header: true,
                footer: true
            },
            border: {
                footer: true,
            },
            "footerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,Rp .<small><em>Tidak ada uang masuk</em></small><small><em>Tidak ada uang keluar</em></small>]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                function formatNumber(num) {
                    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                }

                // Total over all pages

                total2 = api
                    .column(2)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                pageTotal2 = api
                    .column(2, {
                        page: 'current'
                    })
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(2).footer()).html(
                    // 'Rp ' + formatNumber(pageTotal2),
                    // 'Rp ' + formatNumber(total2)
                    'Rp ' + formatNumber(pageTotal2) + '( Total : ' + formatNumber(total2) + ')'
                );
                $('tr:eq(1) th:eq(2)', api.column(2).footer()).html(
                    formatNumber(total2)
                );
                total3 = api
                    .column(3)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                pageTotal3 = api
                    .column(3, {
                        page: 'current'
                    })
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(3).footer()).html(
                    // 'Rp ' + formatNumber(pageTotal3),
                    // 'Rp ' + formatNumber(total3)
                    'Rp ' + formatNumber(pageTotal3) + '( Total : ' + formatNumber(total3) + ')'
                );


                if (pageTotal3 > pageTotal2 || total3 > total2) {
                    total4 = total3 - total2;
                    pageTotal4 = pageTotal3 - pageTotal2;
                    $(api.column(4).footer()).html(
                        'Rp ' + formatNumber(pageTotal4) + '<br>( Total : ' + formatNumber(total4) + ')'
                    );
                    totall = document.querySelector('#total');
                    totall.classList.add('text-danger');
                    totall.classList.add('font-weight-boldest');
                    totall.classList.add('font-size-h5-xl');
                } else if (pageTotal2 > pageTotal3 || total2 > total3) {
                    total4 = total2 - total3;
                    pageTotal4 = pageTotal2 - pageTotal3;
                    $(api.column(4).footer()).html(
                        'Rp ' + formatNumber(pageTotal4) + '<br>( Total : '+ formatNumber(total4)+')'
                        // 'Rp ' + formatNumber(total4)
                    );
                    totall = document.querySelector('#total');
                    totall.classList.remove('text-danger');
                    totall.classList.add('text-primary');
                    totall.classList.add('font-weight-boldest');
                    totall.classList.add('font-size-h5-xl');
                } else {
                    pageTotal4 = 0;
                    total4 = 0;
                    $(api.column(4).footer()).html(
                        'Rp ' + formatNumber(pageTotal4) + '<br> ( Total : '+ formatNumber(total4)+')'
                        // 'Rp ' + formatNumber(total4)
                    );
                    totall = document.querySelector('#total');
                    totall.classList.add('text-primary');
                    totall.classList.add('font-weight-boldest');
                    totall.classList.add('font-size-h5-xl');
                }
            }
        });
    });
</script>


<script>
    function del() {
        $(".deletebtn").click(function(e) {

            id = e.target.dataset.id;
            Swal.fire({
                title: "Apakah anda yakin?",
                text: "Data yang sudah anda hapus tidak akan bisa kembali!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya, Hapus"
            }).then(function(result) {
                if (result.value) {

                    Swal.fire(
                        "Terhapus!",
                        "Data telah terhapus.",
                        "success"
                    );
                    $(`#delete${id}`).submit();

                } else {

                }
            });
        });
    }
</script>

@if(Session::has('message'))
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var type = "{{Session::get('alert-type','success')}}"
    switch (type) {
        case 'success':
            toastr.success("{{Session::get('message')}}");
            // Swal.fire("Berhasil","{{Session::get('message')}}","success");
            // Swal.fire("Good job!", "You clicked the button!", "success");
            break;

    }
</script>
@endif

<script>


</script>
@endsection