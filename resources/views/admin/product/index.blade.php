@extends('admin.layout.master')

@section('title','Produk')
@section('judul','Produk')
@section('judulhead','Produk')
@section('halaman','Produk')

@section('style')
<link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Produk</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="/admin" class="text-muted">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Produk</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
                            <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Jam Digital</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-3 ml-4">
                                <a class="btn-disabled d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>

                            <!--end:Item-->
                            <!--begin:Item-->
                            <div class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div disabled class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                                </a>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
                            <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Tanggal</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-12">
                                <a class="d-block text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="d-block text-right pr-3 " style="padding-top: 1px;">

                                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                                </a>
                            </div>
                            <div class="row col-6">
                                <div class="col-12">
                                    <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                                    </a>
                                </div>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>

            </div>
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Notice-->

            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="fas fa-shopping-bag text-primary"></i>
                        </span>
                        <h3 class="card-label">Produk</h3>

                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('admin.product.cetak') }}" class="btn btn-light-primary font-weight-bolder mr-2">
                            <span class="svg-icon svg-icon-md">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M16,17 L16,21 C16,21.5522847 15.5522847,22 15,22 L9,22 C8.44771525,22 8,21.5522847 8,21 L8,17 L5,17 C3.8954305,17 3,16.1045695 3,15 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,15 C21,16.1045695 20.1045695,17 19,17 L16,17 Z M17.5,11 C18.3284271,11 19,10.3284271 19,9.5 C19,8.67157288 18.3284271,8 17.5,8 C16.6715729,8 16,8.67157288 16,9.5 C16,10.3284271 16.6715729,11 17.5,11 Z M10,14 L10,20 L14,20 L14,14 L10,14 Z" fill="#000000" />
                                            <rect fill="#000000" opacity="0.3" x="8" y="2" width="8" height="2" rx="1" />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                                Cetak data
                        </a>
                        <a href="{{ route('admin.product.tambah') }}" class="btn btn-primary font-weight-bolder">
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <circle fill="#000000" cx="9" cy="15" r="6" />
                                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            Tambah Data
                        </a>

                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table table-hover table-checkable" id="tabel" style="margin-top: 13px !important">
                        <thead>
                            <tr>
                                <th style="width: 2%;">No</th>
                                <!-- <th>Foto</th> -->
                                <th>Nama Produk</th>
                                <!-- <th>Tag</th> -->
                                <th>Kategori</th>
                                <!-- <th>Berat</th> -->
                                <th>Harga</th>
                                <th>Stok</th>
                                <!-- <th>Deskripsi</th> -->
                                <td style="width: 25%;">Opsi</td>
                                <!-- <th>Aksi</th> -->
                                <th style="width: 10%;">Actions</th>

                            </tr>
                        </thead>
                        <tbody>


                            <?php $no = 0; ?>
                            @foreach ($products as $product)
                            <?php $no++; ?>
                            <tr>
                                <td style="width: 2%;">{{ $no }}</td>
                                <td>{{ $product->name}}</td>
                                <td>
                                    {{ $product->nama_kategori}}
                                </td>
                                <!-- <td>{{ $product->tag}}</td> -->
                                <!-- <td>{{ $product->weigth}}gr</td> -->
                                <td>Rp {{ number_format($product->price,0,',','.')}}</td>
                                <td>
                                    @if($product->stok === null || $product->stok === 0)
                                    <span class="text-danger">*</span> <small><em> Stok kosong </em></small>
                                    @else
                                    <strong>{{$product->stok}}</strong>
                                    @endif

                                </td>
                                <!-- <td>
                                    @if($product->description === null)
                                    <span class="text-danger">*</span><small> <em> Belum ada description </em></small>
                                    @else
                                    <?php
                                    $text = $product->description;
                                    $strip = strip_tags(htmlspecialchars_decode(stripcslashes($text)), '<a>');
                                    echo substr($strip, 0, 50);
                                    if (strlen(trim($product->description)) > 50) echo "..."; ?>
                                    @endif
                                </td> -->

                                <td>
                                    <!-- <a href="/updatestok/{{$product->id}}">Update Stok</a> -->
                                    <a href="{{ route('admin.product.detail',['id'=>$product->id]) }}" class="py-2 btn font-weight-bolder btn-light-primary"><i class="flaticon2-information font-weight-bolder  icon-nm"></i><small class="font-weight-bolder ">Lihat Detail</small></a>

                                    {{-- <a href="{{ route('admin.product.stok',['id'=>$product->id]) }}" class="py-2 btn font-weight-bolder btn-light-success"><i class="flaticon-download-1 icon-nm"></i><small class="font-weight-bolder ">Input Stok</small></a> --}}

                                    <button type="button" data-toggle="modal" data-target="#inputstok{{$product->id}}" class="py-2 btn btn-sm  btn-light-success"> <i class="flaticon-download-1 icon-nm"></i> <small class="font-weight-bolder"> Update Stok</small></button> 
                                    
                                    <div class="modal fade" id="inputstok{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="inputmodalabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodalabel">Update Stok Produk {{ $product->name }} ( Stok saat ini : {{$product->stok}} )</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <i aria-hidden="true" class="ki ki-close"></i>
                                                    </button>
                                                </div>
                                                <form  action="{{ route('admin.product.updatestok',['id' => $product->id]) }}" class="formValidate" id="formValidate" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    @method('post')
                                                <div class="modal-body">
                                                    <div hidden class="form-group">
                                                        <label class="form-control-label" for="input-address">Nama Produk</label><span class="text-danger"> *</span>
                                                        <input id="name" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Masukkan nama produk" type="text" value="{{ $product->name }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                                                        @if ($errors->has('name'))
                                                        <div class="invalid-feedback">{{$errors->first('name')}}</div>
                                                        @endif
                                                    </div>
                                                    <div hidden class="form-group">
                                                        <label for="id_kategori">Kategori</label><span class="text-danger"> *</span>
                                                        <select class="form-control" name="categories_id" id="categories_id">
                                                            @foreach ($categories as $categorie)
                                                            <option value="{{ $categorie->id }}" <?php if ($product->categories_id == $categorie->id) {
                                                                                                        echo 'selected';
                                                                                                    } ?>>{{ $categorie->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        @if (('categories_id') === 0)
                                                        <div class="invalid-feedback">{{$errors->first('categories_id')}}</div>
                                                        @endif
                                                    </div>
                                                    <div hidden class="form-group">
                                                        <label class="form-control-label" for="input-address">Harga</label><span class="text-danger"> *</span>
                                                        <input id="weigth" name="weigth" class="form-control {{ $errors->has('weigth') ? 'is-invalid':'' }}" placeholder="Masukkan weigth" type="number" value="{{ $product->weigth }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                                                        @if ($errors->has('weigth'))
                                                        <div class="invalid-feedback">{{$errors->first('weigth')}}</div>
                                                        @endif
                                                    </div>
                                                    <div hidden class="form-group">
                                                        <label class="form-control-label" for="input-address">Harga</label><span class="text-danger"> *</span>
                                                        <input id="price" name="price" class="form-control {{ $errors->has('price') ? 'is-invalid':'' }}" placeholder="Masukkan price" type="number" value="{{ $product->price }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                                                        @if ($errors->has('price'))
                                                        <div class="invalid-feedback">{{$errors->first('price')}}</div>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-address">Stok</label>
                                                        <input id="stok" name="stok" class="form-control {{ $errors->has('stok') ? 'is-invalid':'' }}" placeholder="Masukkan stok" type="number" value="{{ $product->stok }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                            
                                                    </div>
                                                    <div hidden class="form-group">
                                                        <label for="deskripsi">Deskripsi</label>
                                                        <textarea class="tox-target" name="description" id="deskripsi" rows="3">{{ $product->description}}</textarea>
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <label>Foto Produk</label>
                                                        <div></div>
                                                        <div class="custom-file">
                                                            <input id="foto" name="foto" class="custom-file-input {{ $errors->has('foto') ? 'is-invalid':'' }}" placeholder="Upload foto kategori" type="file" value="{{ old('foto') }}">
                                                            <label class="custom-file-label" for="customFile">Pilih file</label>
                                                        </div>
                                                    </div> -->
                                                    <div hidden class="form-group">
                                                        <label for="kt_image_1">Foto Produk</label>
                                                        <div class="foto">
                                                            <div class="image-input image-input-outline" id="kt_image_1">
                                                                <div class="image-input-wrapper" style="background-image: url( {{asset('storage/'.$product->image)}} )"></div>
                            
                                                                <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Ubah foto">
                                                                    <i class="fa fa-pen icon-sm text-muted"></i>
                                                                    <input type="file" name="image" accept=".png, .jpg, .jpeg" />
                                                                    <input type="hidden" name="image_remove" />
                            
                                                                </label>
                            
                                                                <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Ubah foto produk">
                                                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div hidden class="form-group row">
                                                        <label class="form-control-label col-lg-12 col-md-12 col-sm-12" for="input-address">Tag</label>
                                                        <!-- <input id="kt_tagify_5" name="tag" data-role="tagsinput" class="form-control tagify {{ $errors->has('tag') ? 'is-invalid':'' }}" value="{{ old('tag') }}" > -->
                                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <input id="kt_tagify_5" class="form-control tagify {{ $errors->has('tag') ? 'is-invalid':'' }}" name='tag' placeholder="Masukan Tag" value="{{ $product->tag }}" />
                                                        </div>
                                                        @if ($errors->has('tag'))
                                                        <div class="invalid-feedback">{{$errors->first('tag')}}</div>
                                                        @endif
                                                    </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                                                        </div>
                                                    </div>
                                                </form>
                                        </div>
                                    </div>
                                </td>
                                <td nowarp="nowarp">
                                    <!-- Edit button -->
                                    <a href="{{ route('admin.product.edit',['id'=>$product->id]) }}" class="btn btn-sm btn-clean btn-icon" title="Edit details">
                                        <span class="svg-icon svg-icon-md">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" \ transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) " />
                                                    <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1" />
                                                </g>
                                            </svg>
                                        </span>
                                    </a>
                                    <a href="javascript:del();" data-id="{{$product->id}}" class="btn btn-sm btn-clean btn-icon deletebtn">
                                        <form action="{{ route('admin.product.delete',['id'=>$product->id]) }}" id="delete{{$product->id}}" method="GET">
                                            @csrf
                                            @method('delete')
                                        </form>
                                        <span class="svg-icon svg-icon-md" data-id="{{$product->id}}" id="delete{{$product->id}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" data-id="{{$product->id}}" id="delete{{$product->id}}" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" data-id="{{$product->id}}" id="delete{{$product->id}}" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect data-id="{{$product->id}}" id="delete{{$product->id}}" x="0" y="0" width="24" height="24" />
                                                    <path data-id="{{$product->id}}" id="delete{{$product->id}}" d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero" />
                                                    <path data-id="{{$product->id}}" id="delete{{$product->id}}" d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                    </a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    <!--end: Datatable-->
                </div>

            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<!--end::Content-->

@endsection
@section('js')
<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<!-- <script src="assets/js/pages/features/miscellaneous/toastr.js"></script>
<script src="assets/js/pages/features/miscellaneous/sweetalert2.js"></script> -->

<script>
    $(document).ready(function() {
        $('#tabel').DataTable({
            responsive: true,
        });
    });
</script>


@if(Session::has('message'))
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var type = "{{Session::get('alert-type','success')}}"
    switch (type) {
        case 'success':
            toastr.success("{{Session::get('message')}}");
            // Swal.fire("Berhasil","{{Session::get('message')}}","success");
            // Swal.fire("Good job!", "You clicked the button!", "success");
            break;
        case 'berhasil':
            toastr.success("{{Session::get('message')}}");
            // Swal.fire("Berhasil","{{Session::get('message')}}","success");
            // Swal.fire("Good job!", "You clicked the button!", "success");
            break;

    }
</script>
@endif


<script>
    // $(".deletebtn").click(function(e) {
    //     id = e.target.dataset.id;
    //     Swal.fire({
    //         title: "Apakah anda yakin?",
    //         text: "Data yang sudah anda hapus tidak akan bisa kembali!",
    //         icon: "warning",
    //         showCancelButton: true,
    //         confirmButtonText: "Ya, Hapus"
    //     }).then(function(result) {
    //         if (result.value) {
    //             Swal.fire(
    //                 "Terhapus!",
    //                 "Data telah terhapus.",
    //                 "success"
    //             );
    //             $(`#delete${id}`).submit();
    //         } else {

    //         }
    //     });
    // });
    function del() {
        $(".deletebtn").click(function(e) {
            id = e.target.dataset.id;
            Swal.fire({
                title: "Apakah anda yakin?",
                text: "Data yang sudah anda hapus tidak akan bisa kembali!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya, Hapus"
            }).then(function(result) {
                if (result.value) {
                    Swal.fire(
                        "Terhapus!",
                        "Data telah terhapus.",
                        "success"
                    );
                    $(`#delete${id}`).submit();
                } else {

                }
            });
        });
    }
</script>
<!-- <script src="assets/js/pages/features/calendar/basic.js"></script> -->
<!-- <script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}"></script> -->

@endsection