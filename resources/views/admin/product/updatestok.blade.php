@extends('admin.layout.master')
@section('style')
<link href="{{asset('assets/extra-libs/prism/prism.css')}}" rel="stylesheet">
@endsection
@section('title','Update Stok')
@section('content')

<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Update Stok Produk</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="/admin" class="text-muted">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/admin/product" class="text-muted">Produk</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Update Stok Produk</a>
                        </li>

                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
                            <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Jam Digital</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-3 ml-4">
                                <a class="btn-disabled d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>

                            <!--end:Item-->
                            <!--begin:Item-->
                            <div class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div disabled class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                                </a>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
                            <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Tanggal</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-12">
                                <a class="d-block text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="d-block text-right pr-3 " style="padding-top: 1px;">

                                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                                </a>
                            </div>
                            <div class="row col-6">
                                <div class="col-12">
                                    <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                                    </a>
                                </div>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>

            </div>
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Notice-->

            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header">
                    <h3 class="card-title">
                        Update Stok Produk {{ $product->name }}
                    </h3>

                </div>
                <!--begin::Form-->
                <form action="{{ route('admin.product.updatestok',['id' => $product->id]) }}" method="POST" id="formValidate" enctype="multipart/form-data">

                    @csrf
                    <div class="card-body">
                        <div hidden class="form-group">
                            <label class="form-control-label" for="input-address">Nama Produk</label><span class="text-danger"> *</span>
                            <input id="name" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Masukkan nama produk" type="text" value="{{ $product['name'] }}" data-error=".errorTxt1">
                            <div class="errorTxt1"></div>
                            @if ($errors->has('name'))
                            <div class="invalid-feedback">{{$errors->first('name')}}</div>
                            @endif
                        </div>
                        <div hidden class="form-group">
                            <label for="id_kategori">Kategori</label><span class="text-danger"> *</span>
                            <select class="form-control" name="categories_id" id="categories_id">
                                @foreach ($categories as $categorie)
                                <option value="{{ $categorie->id }}" <?php if ($product->categories_id == $categorie->id) {
                                                                            echo 'selected';
                                                                        } ?>>{{ $categorie->name }}</option>
                                @endforeach
                            </select>
                            @if (('categories_id') === 0)
                            <div class="invalid-feedback">{{$errors->first('categories_id')}}</div>
                            @endif
                        </div>
                        <div hidden class="form-group">
                            <label class="form-control-label" for="input-address">Harga</label><span class="text-danger"> *</span>
                            <input id="weigth" name="weigth" class="form-control {{ $errors->has('weigth') ? 'is-invalid':'' }}" placeholder="Masukkan weigth" type="number" value="{{ $product['weigth'] }}" data-error=".errorTxt1">
                            <div class="errorTxt1"></div>
                            @if ($errors->has('weigth'))
                            <div class="invalid-feedback">{{$errors->first('weigth')}}</div>
                            @endif
                        </div>
                        <div hidden class="form-group">
                            <label class="form-control-label" for="input-address">Harga</label><span class="text-danger"> *</span>
                            <input id="price" name="price" class="form-control {{ $errors->has('price') ? 'is-invalid':'' }}" placeholder="Masukkan price" type="number" value="{{ $product['price'] }}" data-error=".errorTxt1">
                            <div class="errorTxt1"></div>
                            @if ($errors->has('price'))
                            <div class="invalid-feedback">{{$errors->first('price')}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="input-address">Stok</label>
                            <input id="stok" name="stok" class="form-control {{ $errors->has('stok') ? 'is-invalid':'' }}" placeholder="Masukkan stok" type="number" value="{{ $product['stok'] }}" data-error=".errorTxt1">
                            <div class="errorTxt1"></div>

                        </div>
                        <div hidden class="form-group">
                            <label for="deskripsi">Deskripsi</label>
                            <textarea class="tox-target" name="description" id="deskripsi" rows="3">{{ $product['description'] }}</textarea>
                        </div>
                        <!-- <div class="form-group">
                            <label>Foto Produk</label>
                            <div></div>
                            <div class="custom-file">
                                <input id="foto" name="foto" class="custom-file-input {{ $errors->has('foto') ? 'is-invalid':'' }}" placeholder="Upload foto kategori" type="file" value="{{ old('foto') }}">
                                <label class="custom-file-label" for="customFile">Pilih file</label>
                            </div>
                        </div> -->
                        <div hidden class="form-group">
                            <label for="kt_image_1">Foto Produk</label>
                            <div class="foto">
                                <div class="image-input image-input-outline" id="kt_image_1">
                                    <div class="image-input-wrapper" style="background-image: url( {{asset('storage/'.$product['image'])}} )"></div>

                                    <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Ubah foto">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" name="image" accept=".png, .jpg, .jpeg" />
                                        <input type="hidden" name="image_remove" />

                                    </label>

                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Ubah foto produk">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div hidden class="form-group row">
                            <label class="form-control-label col-lg-12 col-md-12 col-sm-12" for="input-address">Tag</label>
                            <!-- <input id="kt_tagify_5" name="tag" data-role="tagsinput" class="form-control tagify {{ $errors->has('tag') ? 'is-invalid':'' }}" value="{{ old('tag') }}" > -->
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <input id="kt_tagify_5" class="form-control tagify {{ $errors->has('tag') ? 'is-invalid':'' }}" name='tag' placeholder="Masukan Tag" value="{{ $product['tag'] }}" />
                            </div>
                            @if ($errors->has('tag'))
                            <div class="invalid-feedback">{{$errors->first('tag')}}</div>
                            @endif
                        </div>





                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        <a href="{{ url()->previous() }}" class="btn btn-secondary">
                            Kembali
                        </a>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection



@section('js')
<script src="{{asset('assets/extra-libs/prism/prism.js')}}"></script>
<script src="{{asset('dist/js/pages/forms/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/js/pages/crud/file-upload/image-input.js')}}"></script>
<script>
    FormValidation.formValidation(
        document.getElementById('formValidate'), {
            fields: {
                nama_produk: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan nama produk'
                        }
                    }
                },
                id_kategori: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan pilih kategori'
                        }
                    }
                },
                harga: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan harga'
                        }
                    }
                },
            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                // Bootstrap Framework Integration
                bootstrap: new FormValidation.plugins.Bootstrap(),
                // Validate fields when clicking the Submit button
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            }
        }
    );
</script>

<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>

<script>
    tinymce.init({
        selector: '#deskripsi',
        menubar: true,
        toolbar: ['styleselect fontselect fontsizeselect | undo redo | cut copy paste | link image | blockquote subscript superscript | advlist | autolink | lists charmap | print preview ',
            'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent '
        ],
        plugins: 'advlist autolink link image lists charmap print preview code'
    });
</script>

<script>
    var avatar1 = new KTImageInput('kt_image_1');
</script>
@endsection