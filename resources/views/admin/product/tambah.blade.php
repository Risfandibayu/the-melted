@extends('admin.layout.master')

@section('style')
<link href="{{asset('assets/extra-libs/prism/prism.css')}}" rel="stylesheet">


@endsection
@section('title','Tambah Data Produk')
@section('content')


<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-1">
        <!--begin::Page Heading-->
        <div class="d-flex align-items-baseline flex-wrap mr-5">
          <!--begin::Page Title-->
          <h5 class="text-dark font-weight-bold my-1 mr-5">Tambah Produk</h5>
          <!--end::Page Title-->
          <!--begin::Breadcrumb-->
          <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
            <li class="breadcrumb-item">
              <a href="/admin" class="text-muted">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
              <a href="/admin/product" class="text-muted">Produk</a>
            </li>
            <li class="breadcrumb-item">
              <a href="#" class="text-muted">Tambah Data Produk</a>
            </li>

          </ul>
          <!--end::Breadcrumb-->
        </div>
        <!--end::Page Heading-->
      </div>
      <!--end::Info-->
      <div class="d-flex align-items-center">
        <div class="dropdown">
          <!--begin::Toggle-->
          <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
              <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
              <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
              </button>
            </div>
          </div>
          <!--end::Toggle-->
          <!--begin::Dropdown-->
          <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <!--begin:Header-->
            <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
              <h4 class="text-white font-weight-bold">Jam Digital</h4>
            </div>
            <!--end:Header-->
            <!--begin:Nav-->
            <div class="row row-paddingless">
              <!--begin:Item-->
              <div class="col-3 ml-4">
                <a class="btn-disabled d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                </a>
              </div>
              <div class="col-1">
                <a class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                </a>
              </div>

              <!--end:Item-->
              <!--begin:Item-->
              <div class="col-3">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                </a>
              </div>
              <div class="col-1">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                </a>
              </div>
              <!--end:Item-->
              <!--begin:Item-->
              <div disabled class="col-3">
                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                </a>
              </div>
              <!--end:Item-->
            </div>
            <!--end:Nav-->
          </div>
          <!--end::Dropdown-->
        </div>
        <div class="dropdown">
          <!--begin::Toggle-->
          <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
              <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
              <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
              </button>
            </div>
          </div>
          <!--end::Toggle-->
          <!--begin::Dropdown-->
          <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <!--begin:Header-->
            <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
              <h4 class="text-white font-weight-bold">Tanggal</h4>
            </div>
            <!--end:Header-->
            <!--begin:Nav-->
            <div class="row row-paddingless">
              <!--begin:Item-->
              <div class="col-12">
                <a class="d-block text-center  border-bottom">

                  <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                </a>
              </div>
              <div class="col-6">
                <a class="d-block text-right pr-3 " style="padding-top: 1px;">

                  <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                </a>
              </div>
              <div class="row col-6">
                <div class="col-12">
                  <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">

                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                  </a>
                </div>
                <div class="col-12">
                  <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">

                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                  </a>
                </div>
              </div>
              <!--end:Item-->
            </div>
            <!--end:Nav-->
          </div>
          <!--end::Dropdown-->
        </div>

      </div>
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--begin::Notice-->

      <!--begin::Card-->
      <div class="card card-custom">
        <div class="card-header">
          <h3 class="card-title">
            Tambah Data Produk
          </h3>

        </div>
        <!--begin::Form-->
        <form action="{{ route('admin.product.store') }}" class="formValidate" id="formValidate" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="card-body">

            <div class="form-group">
              <label class="form-control-label" for="input-address">Nama Produk</label>
              <input id="name" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Masukkan nama produk" type="text" value="{{ old('name') }}" data-error=".errorTxt1">
              <div class="errorTxt1"></div>
              @if ($errors->has('name'))
              <div class="invalid-feedback">{{$errors->first('name')}}</div>
              @endif
            </div>

            <div class="form-group">
              <label for="categories_id">Kategori 1</label>
              <select class="form-control" name="categories_id" id="categories_id">
                <option value="" hidden>-- Pilih Kategori --</option>
                @foreach ($categories as $categorie)
                <option value="{{ $categorie->id }}">{{ ucfirst($categorie->name) }}</option>
                @endforeach
              </select>
              @if (('categories_id') === 0)
              <div class="invalid-feedback">{{$errors->first('categories_id')}}</div>
              @endif
            </div>

            <div class="form-group">
              <label class="form-control-label" for="input-address">Berat</label> <small> (Dalam satuan gram)</small>
              <input id="weigth" name="weigth" class="form-control {{ $errors->has('weigth') ? 'is-invalid':'' }}" placeholder="Masukkan berat produk" type="number" value="{{ old('weigth') }}" data-error=".errorTxt1">
              <div class="errorTxt1"></div>
              @if ($errors->has('weigth'))
              <div class="invalid-feedback">{{$errors->first('weigth')}}</div>
              @endif
            </div>
            <div class="form-group">
              <label class="form-control-label" for="input-address">Harga</label>
              <input id="price" name="price" class="form-control {{ $errors->has('price') ? 'is-invalid':'' }}" placeholder="Masukkan harga" type="number" value="{{ old('price') }}" data-error=".errorTxt1">
              <div class="errorTxt1"></div>
              @if ($errors->has('price'))
              <div class="invalid-feedback">{{$errors->first('price')}}</div>
              @endif
            </div>
            <div hidden class="form-group">
              <label class="form-control-label" for="input-address">Stok</label>
              <input id="stok" name="stok" class="form-control {{ $errors->has('stok') ? 'is-invalid':'' }}" placeholder="Masukkan stok" type="number" value="{{ old('stok') }}" data-error=".errorTxt1">
              <div class="errorTxt1"></div>

            </div>
            <div class="form-group">
              <label for="deskripsi">Deskripsi</label>
              <textarea class="tox-target" id="deskripsi" name="description" rows="3"></textarea>
            </div>
            <div class="form-group">
              <label>Foto Produk</label>
              <div></div>
              <div class="custom-file">
                <!-- <input type="file" class="custom-file-input" id="customFile" /> -->
                <input id="image" name="image" class="custom-file-input {{ $errors->has('image') ? 'is-invalid':'' }}" placeholder="Upload image kategori" type="file" value="{{ old('image') }}">
                <label class="custom-file-label" for="customFile">Pilih file</label>
                <small class="text-muted">Untuk Hasil yang lebih baik gunakan foto produk berukuran 8x10. Crop foto produk disini <a href="https://www.img2go.com/id/pangkas-gambar" target="_blank">www.img2go.com/id/pangkas-gambar</a> </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="form-control-label col-lg-12 col-md-12 col-sm-12" for="input-address">Tag</label>
              <!-- <input id="kt_tagify_5" name="tag" data-role="tagsinput" class="form-control tagify {{ $errors->has('tag') ? 'is-invalid':'' }}" value="{{ old('tag') }}" > -->
              <div class="col-lg-12 col-md-12 col-sm-12">
                <input id="kt_tagify_5" class="form-control tagify {{ $errors->has('tag') ? 'is-invalid':'' }}" name='tag' placeholder="Masukan Tag" type="text" />
              </div>
              @if ($errors->has('tag'))
              <div class="invalid-feedback">{{$errors->first('tag')}}</div>
              @endif
            </div>
          </div>

          <div class="card-footer">
            <button type="submit" class="btn btn-primary mr-2">Simpan</button>
            <a href="{{ url()->previous() }}" class="btn btn-secondary">
              Kembali
            </a>
          </div>
        </form>
        <!--end::Form-->
      </div>
      <!--end::Card-->
    </div>
    <!--end::Container-->
  </div>
  <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js')
<script src="{{asset('assets/extra-libs/prism/prism.js')}}"></script>
<script src="{{asset('dist/js/pages/forms/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/js/tagsinput/bootstrap-tagsinput.js')}}"></script>
<script src="{{asset('assets/js/tagsinput/bootstrap-tagsinput.min.js')}}"></script>


<script>
  var KTQuilDemos = function() {

    // Private functions
    var demo2 = function() {
      var Delta = Quill.import('delta');
      var quill = new Quill('#kt_quil_2', {
        modules: {
          toolbar: true
        },
        placeholder: 'Type your text here...',
        theme: 'snow'
      });

      // Store accumulated changes
      var change = new Delta();
      quill.on('text-change', function(delta) {
        change = change.compose(delta);
      });

      // Save periodically
      setInterval(function() {
        if (change.length() > 0) {
          console.log('Saving changes', change);
          /*
          Send partial changes
          $.post('/your-endpoint', {
          partial: JSON.stringify(change)
          });

          Send entire document
          $.post('/your-endpoint', {
          doc: JSON.stringify(quill.getContents())
          });
          */
          change = new Delta();
        }
      }, 5 * 1000);

      // Check for unsaved data

    }

    return {
      // public functions
      init: function() {
        demo2();
      }
    };
  }();

  jQuery(document).ready(function() {
    KTQuilDemos.init();
  });
</script>
<script src="{{asset('assets/js/pages/crud/forms/editors/quill.js')}}"></script>
<script>
  var form = document.querySelector('form');
  form.onsubmit = function() {
    // Populate hidden form on submit
    var about = document.querySelector('input[name=deskripsi]');
    about.value = JSON.stringify(quill.getContents());

    console.log("Submitted", $(form).serialize(), $(form).serializeArray());

    // No back end to actually submit to!
    return false;
  };
</script>


<script>
  FormValidation.formValidation(
    document.getElementById('formValidate'), {
      fields: {
        name: {
          validators: {
            notEmpty: {
              message: 'Silahkan masukan nama produk'
            }
          }
        },
        categories_id: {
          validators: {
            notEmpty: {
              message: 'Kategori 1 wajib diisi'
            }
          }
        },
        price: {
          validators: {
            notEmpty: {
              message: 'Silahkan masukan harga produk'
            }
          }
        },
        weigth: {
          validators: {
            notEmpty: {
              message: 'Silahkan masukan berat produk'
            }
          }
        },
        image: {
          validators: {
            notEmpty: {
              message: 'Silahkan upload foto produk'
            }
          }
        },
        tag: {
          validators: {
            notEmpty: {
              message: 'Silahkan masukan tag produk'
            }
          }
        },
      },

      plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        // Bootstrap Framework Integration
        bootstrap: new FormValidation.plugins.Bootstrap(),
        // Validate fields when clicking the Submit button
        submitButton: new FormValidation.plugins.SubmitButton(),
        // Submit the form when all fields are valid
        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
      }
    }
  );
</script>
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>

<script>
  tinymce.init({
    selector: '#deskripsi',
    menubar: true,
    toolbar: ['styleselect fontselect fontsizeselect | undo redo | cut copy paste | link image | blockquote subscript superscript | advlist | autolink | lists charmap | print preview ',
      'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent '
    ],
    plugins: 'advlist autolink link image lists charmap print preview code'
  });
</script>
<!-- <script src="{{asset('assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js')}}"></script>
<script>
    ClassicEditor
            .create( document.querySelector( '#deskripsi' ) )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );
</script> -->
<script src="{{asset('assets/js/pages/crud/forms/widgets/tagify.js')}}"></script>
<script>
  var toEl = document.getElementById('kt_tagify_5');
  var tagifyTo = new Tagify(toEl, {
    delimiters: ", ", // add new tags when a comma or a space character is entered
    placeholder: "Masukan tag",
    originalInputValueFormat: valuesArr => valuesArr.map(item => item.value).join(', '),
    maxTags: 10,
    blacklist: ["fuck", "shit", "pussy"],
    keepInvalidTags: true, // do not remove invalid tags (but keep them marked as invalid)
    whitelist: [{
      value: 'Chris Muller',
      email: 'chris.muller@wix.com',
      initials: '',
      initialsState: '',
      pic: './assets/media/users/100_11.jpg',
      class: 'tagify__tag--primary'
    }, {
      value: 'Nick Bold',
      email: 'nick.seo@gmail.com',
      initials: 'SS',
      initialsState: 'warning',
      pic: ''
    }, {
      value: 'Alon Silko',
      email: 'alon@keenthemes.com',
      initials: '',
      initialsState: '',
      pic: './assets/media/users/100_6.jpg'
    }, {
      value: 'Sam Seanic',
      email: 'sam.senic@loop.com',
      initials: '',
      initialsState: '',
      pic: './assets/media/users/100_8.jpg'
    }, {
      value: 'Sara Loran',
      email: 'sara.loran@tilda.com',
      initials: '',
      initialsState: '',
      pic: './assets/media/users/100_9.jpg'
    }, {
      value: 'Eric Davok',
      email: 'davok@mix.com',
      initials: '',
      initialsState: '',
      pic: './assets/media/users/100_13.jpg'
    }, {
      value: 'Sam Seanic',
      email: 'sam.senic@loop.com',
      initials: '',
      initialsState: '',
      pic: './assets/media/users/100_13.jpg'
    }, {
      value: 'Lina Nilson',
      email: 'lina.nilson@loop.com',
      initials: 'LN',
      initialsState: 'danger',
      pic: './assets/media/users/100_15.jpg'
    }],
    templates: {
      dropdownItem: function(tagData) {
        try {
          var html = '';

          html += '<div class="tagify__dropdown__item">';
          html += '   <div class="d-flex align-items-center">';
          html += '       <span class="symbol sumbol-' + (tagData.initialsState ? tagData.initialsState : '') + ' mr-2">';
          html += '           <span class="symbol-label" style="background-image: url(\'' + (tagData.pic ? tagData.pic : '') + '\')">' + (tagData.initials ? tagData.initials : '') + '</span>';
          html += '       </span>';
          html += '       <div class="d-flex flex-column">';
          html += '           <a href="#" class="text-dark-75 text-hover-primary font-weight-bold">' + (tagData.value ? tagData.value : '') + '</a>';
          html += '           <span class="text-muted font-weight-bold">' + (tagData.email ? tagData.email : '') + '</span>';
          html += '       </div>';
          html += '   </div>';
          html += '</div>';

          return html;
        } catch (err) {}
      }
    },
    transformTag: function(tagData) {
      tagData.class = 'tagify__tag tagify__tag--primary';
    },
    dropdown: {
      classname: "color-blue",
      enabled: 1,
      maxItems: 5
    },

  });
</script>
@endsection