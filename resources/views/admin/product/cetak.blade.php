
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example 1</title>
    <link rel="stylesheet" href="assets/laporan/style.css" media="all" />
  </head>
  <body>
    <header class="clearfix">
        
        <div id="project">
            <div>The Melted </div>
            <div>Make Your Mood Be Better</div>
            <div>Instragram : @themeltedd</div>
            <div>Whatsapp : +62 895-3664-39266</div>
        </div>
        <div id="logo">
          <img src="assets/laporan/asdd.jpg">
        </div>
    </header>
    <h2>Daftar Produk</h2>
    <main>
      <table>
        <thead>
            <tr>
                <th style="width: 20px;">NAMA PRODUK</th>
                <th style="width: 10px;">KATEGORI</th>
                <th style="width: 10px;">BERAT</th>
                <th style="width: 10px;">HARGA</th>
            </tr>
        </thead>
            <tbody>
            @foreach ($products as $pro)

			<tr>
				<td>{{$pro->name}}</td>
				<td>{{$pro->nama_kategori}}</td>
				<td>{{$pro->weigth}} gr</td>
				<td>Rp {{ number_format($pro->price,0,',','.')}}</td>
			</tr>
			@endforeach
        </tbody>
      </table>
  </body>
</html>