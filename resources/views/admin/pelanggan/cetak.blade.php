
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example 1</title>
    <link rel="stylesheet" href="assets/laporan/style.css" media="all" />
  </head>
  <body>
    <header class="clearfix">
        
        <div id="project">
            <div>The Melted </div>
            <div>Make Your Mood Be Better</div>
            <div>Instragram : @themeltedd</div>
            <div>Whatsapp : +62 895-3664-39266</div>
        </div>
        <div id="logo">
          <img src="assets/laporan/asdd.jpg">
        </div>
    </header>
    <h2>DAFTAR CUSTOMER</h2>
    <main>
      <table>
        <thead>
            <tr>
                <th style="width: 20px;">NAMA CUSTOMER</th>
                <th style="width: 10px;">NOMOR TELEPON</th>
                <th style="width: 10px;">JENIS KELAMIN</th>
                <th style="width: 10px;">USIA</th>
                <th style="width: 10px;">EMAIL</th>
                {{-- <th style="width: 10px;">ALAMAT</th> --}}
            </tr>
        </thead>
            <tbody>
            @foreach ($pelanggan as $pro)

              <tr>
                <td>{{$pro->name}}</td>
                <td>{{$pro->no_telp}}</td>
                <td>{{$pro->jk}}</td>
                <td>{{$pro->usia}}</td>
                <td>{{$pro->email}}</td>

              </tr>
              @endforeach
          </tbody>
      </table>
  </body>
</html>