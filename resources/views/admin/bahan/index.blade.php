@extends('admin.layout.master')

@section('title','Bahan')
@section('judul','Bahan')
@section('judulhead','Bahan')
@section('halaman','Bahan')

@section('style')
<link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css"> -->
@endsection

@section('content')


<?php
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
?>

<?php
function pecahtgl($timestamp)
{
    $pecahkan = preg_split('/( |,|-|:)/', $timestamp);
    $hari = array(
        7 => 'Minggu',
        1 => 'Senin',
        2 => 'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu',
    );
    $bulan = array(
        1 =>   'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => ' Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    );
    return $hari[(int)$pecahkan[0]] . ',' . $pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4] . ' ' . $bulan[(int)$pecahkan[5]] . ' ' . $pecahkan[6] . ' ' . $pecahkan[7] . ' ' . $pecahkan[8] . ' ' . $pecahkan[1];
}

?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Bahan</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="/dashboard" class="text-muted">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Bahan</a>
                        </li>

                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Jam :</span>
                            <span class="text-primary font-size-base font-weight-bolder"><span id="jam"></span></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Jam Digital</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-3 ml-4">
                                <a class="btn-disabled d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="jm"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>

                            <!--end:Item-->
                            <!--begin:Item-->
                            <div class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="menit"></span>
                                </a>
                            </div>
                            <div class="col-1">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;">:</span>
                                </a>
                            </div>
                            <!--end:Item-->
                            <!--begin:Item-->
                            <div disabled class="col-3">
                                <a btn-disabled class="d-block py-10 text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 61px;" id="dtk"></span>
                                </a>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-sm btn-light font-weight-bold btn-dropdown mr-1">
                            <span class="text-muted font-size-base font-weight-bold mr-2">Tanggal :</span>
                            <span class="text-primary font-size-base font-weight-bolder" id="tgl"></id=></span>
                            </button>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <!--begin:Header-->
                        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                            <h4 class="text-white font-weight-bold">Tanggal</h4>
                        </div>
                        <!--end:Header-->
                        <!--begin:Nav-->
                        <div class="row row-paddingless">
                            <!--begin:Item-->
                            <div class="col-12">
                                <a class="d-block text-center  border-bottom">

                                    <span class="d-block text-dark-75 font-weight-bold mt-2 mb-1" style="font-size: 70px;" id="hari"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="d-block text-right pr-3 " style="padding-top: 1px;">

                                    <span class="d-block text-dark-75 font-weight-bold" style="font-size: 80px;" id="tanggal"></span>
                                </a>
                            </div>
                            <div class="row col-6">
                                <div class="col-12">
                                    <a class="d-block pt-7 pl-3  border-bottom" style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="bulan"></span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a class="d-block pb-7 pl-3 " style="margin-right: -25px;">

                                        <span class="d-block text-dark-75 font-weight-bold" style="font-size: 25px;" id="tahun"></span>
                                    </a>
                                </div>
                            </div>
                            <!--end:Item-->
                        </div>
                        <!--end:Nav-->
                    </div>
                    <!--end::Dropdown-->
                </div>

            </div>
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Notice-->

            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="fas fa-file-signature text-primary"></i>
                        </span>
                        <h3 class="card-label">Bahan</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('admin.bahan.cetak') }}" class="btn btn-light-primary font-weight-bolder mr-2">
                            <span class="svg-icon svg-icon-md">

                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M16,17 L16,21 C16,21.5522847 15.5522847,22 15,22 L9,22 C8.44771525,22 8,21.5522847 8,21 L8,17 L5,17 C3.8954305,17 3,16.1045695 3,15 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,15 C21,16.1045695 20.1045695,17 19,17 L16,17 Z M17.5,11 C18.3284271,11 19,10.3284271 19,9.5 C19,8.67157288 18.3284271,8 17.5,8 C16.6715729,8 16,8.67157288 16,9.5 C16,10.3284271 16.6715729,11 17.5,11 Z M10,14 L10,20 L14,20 L14,14 L10,14 Z" fill="#000000" />
                                        <rect fill="#000000" opacity="0.3" x="8" y="2" width="8" height="2" rx="1" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            Cetak data
                        </a>
                        <a class="btn btn-success mr-2  font-weight-bolder" href="/admin/input_stok_masuk">

                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Communication\Incoming-box.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M22,17 L22,21 C22,22.1045695 21.1045695,23 20,23 L4,23 C2.8954305,23 2,22.1045695 2,21 L2,17 L6.27924078,17 L6.82339262,18.6324555 C7.09562072,19.4491398 7.8598984,20 8.72075922,20 L15.381966,20 C16.1395101,20 16.8320364,19.5719952 17.1708204,18.8944272 L18.118034,17 L22,17 Z" fill="#000000" />
                                        <path d="M2.5625,15 L5.92654389,9.01947752 C6.2807805,8.38972356 6.94714834,8 7.66969497,8 L16.330305,8 C17.0528517,8 17.7192195,8.38972356 18.0734561,9.01947752 L21.4375,15 L18.118034,15 C17.3604899,15 16.6679636,15.4280048 16.3291796,16.1055728 L15.381966,18 L8.72075922,18 L8.17660738,16.3675445 C7.90437928,15.5508602 7.1401016,15 6.27924078,15 L2.5625,15 Z" fill="#000000" opacity="0.3" />
                                        <path d="M11.1288761,0.733697713 L11.1288761,2.69017121 L9.12120481,2.69017121 C8.84506244,2.69017121 8.62120481,2.91402884 8.62120481,3.19017121 L8.62120481,4.21346991 C8.62120481,4.48961229 8.84506244,4.71346991 9.12120481,4.71346991 L11.1288761,4.71346991 L11.1288761,6.66994341 C11.1288761,6.94608579 11.3527337,7.16994341 11.6288761,7.16994341 C11.7471877,7.16994341 11.8616664,7.12798964 11.951961,7.05154023 L15.4576222,4.08341738 C15.6683723,3.90498251 15.6945689,3.58948575 15.5161341,3.37873564 C15.4982803,3.35764848 15.4787093,3.33807751 15.4576222,3.32022374 L11.951961,0.352100892 C11.7412109,0.173666017 11.4257142,0.199862688 11.2472793,0.410612793 C11.1708299,0.500907473 11.1288761,0.615386087 11.1288761,0.733697713 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.959697, 3.661508) rotate(-270.000000) translate(-11.959697, -3.661508) " />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>

                            Input stok masuk
                        </a>

                        <a class="btn btn-warning mr-2 font-weight-bolder" href="/admin/createkurang_bahan">
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Communication\Write.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) " />
                                        <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            Update stok
                        </a>

                        <a href="/admin/createbahan" class="btn btn-primary font-weight-bolder">
                            <!-- <i class="la la-plus"></i> -->
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <circle fill="#000000" cx="9" cy="15" r="6" />
                                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            Tambah Data

                        </a>

                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table table-hover table-checkable display" id="tabel" style="margin-top: 13px !important">
                        <thead>
                            <tr>
                                <th style="width: 1%;">#</th>
                                <th style="width: 13%;">Bahan</th>
                                <th style="width: 8%;">Stok</th>
                                <th>Tanggal Terakhir Input Stok</th>
                                <th>Tanggal Terakhir Update Stok</th>
                                <!-- <th style="width: 20%;">Opsi stok</th> -->
                                <!-- <th>Aksi</th> -->

                                <th style="width: 24%;">Opsi stok</th>
                                <th style="width: 12%;">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 0; ?>
                            @foreach ($bahan as $bahan)
                            <?php $no++; ?>
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{ $bahan->nama_bahan}} </td>
                                <td>
                                    @if($bahan->stok === null || $bahan->stok === 0)
                                    <span class="text-danger">*</span> <em> Stok kosong </em>
                                    @else
                                    <strong>{{$bahan->stok}}</strong> <small>({{ $bahan->satuan}})</small>
                                    @endif
                                </td>
                                <td>
                                    @if($bahan->tanggal_beli === null || $bahan->tanggal_beli === 0)
                                    <span class="text-danger">*</span><small> <em> Belum ada stok masuk </em></small>
                                    @else
                                    {{ pecahtgl(date('N, d - m - Y', strtotime($bahan->tanggal_beli)))}}
                                    @endif
                                </td>
                                <td>
                                    @if($bahan->tanggal_kurang === null || $bahan->tanggal_kurang === 0)
                                    <span class="text-danger">*</span><small><em> Update stok belum di lakukan</em></small>
                                    @else

                                    {{ pecahtgl(date('N, d - m - Y', strtotime($bahan->tanggal_kurang)))}}
                                    @endif
                                </td>


                                <td>




                                    {{-- <button type="button" data-toggle="modal" data-target="#inputstok{{$bahan->id}}" class="btn btn-sm btn-clean btn-icon" title="input stok">Input stok</button> --}}

                                    <button type="button" data-toggle="modal" data-target="#inputstok{{$bahan->id}}" class="py-2 btn btn-sm  btn-light-success"> <i class="flaticon-download-1 icon-nm"></i> <small class="font-weight-bolder"> Input Stok</small></button> 
                                    
                                    <div class="modal fade" id="inputstok{{$bahan->id}}" tabindex="-1" role="dialog" aria-labelledby="inputmodalabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodalabel">Input Stok {{$bahan->nama_bahan}} ( Stok saat ini : {{$bahan->stok}} )</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <i aria-hidden="true" class="ki ki-close"></i>
                                                    </button>
                                                </div>
                                                <form action="/admin/storebeli_bahan/{{$bahan->id}}" class="formValidate" id="formValidate" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    @method('post')
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-address">Tanggal</label>
                                                        <div class="input-group date">
                                                            <input id="tanggal_beli" data-date-end-date="0d"  type="text" data-tanggal_beli="tanggal_beli" autocomplete="off" name="tanggal_beli" class="form-control tglbeli {{ $errors->has('tanggal_beli') ? 'is-invalid':'' }}" placeholder="Masukkan tanggal" data-error=".errorTxt1">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="errorTxt1"></div>
                                                        @if ($errors->has('tanggal_beli'))
                                                        <div class="invalid-feedback">{{$errors->first('tanggal_beli')}}</div>
                                                        @endif
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <label class="form-control-label" for="input-address">Tanggal Pembelian</label>
                                                        <div class="">
                                                            <div class="input-group date">
                                                                
                                                                <input id="kt_datepicker_2_modal" data-date-format="dd/mm/yyyy" name="tanggal_beli" class="form-control {{ $errors->has('tanggal_beli') ? 'is-invalid':'' }}" readonly="readonly" placeholder="Masukkan kategori" data-error=".errorTxt1">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-check-o"></i>
                                                                    </span>
                                                                </div>
                                                                @if ($errors->has('tanggal_beli'))
                                                                <div class="invalid-feedback">{{$errors->first('tanggal_beli')}}</div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                    <div hidden class="form-group">
                                                        <label for="id_bahan">Bahan</label><span class="text-danger"> *</span>
                                                        <select class="form-control" name="id_bahan" id="id_bahan">
                                                            <option value="" hidden>-- Pilih Bahan --</option>
                                                            @foreach ($bahans as $bhn)
                                                            <option value="{{ $bahan->id }}" {{ $bhn->id == $bahan->id ? 'selected':'' }}>{{ ucfirst($bhn->nama_bahan) }} <small>({{ ucfirst($bhn->satuan) }})</small></option>
                                                            
                                                            @endforeach
                                                        </select>
                                                        
                                                        @if (('id_bahan') === 0)
                                                        <div class="invalid-feedback">{{$errors->first('id_bahan')}}</div>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-address">Jumlah</label><span class="text-danger"> *</span>
                                                        <input id="qty" name="qty" class="form-control {{ $errors->has('qty') ? 'is-invalid':'' }}" placeholder="Masukkan jumlah stok masuk" type="number" value="{{ old('qty') }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                                                        
                                                        @if ($errors->has('qty'))
                                                        <!-- <div class="alert invalid-feedback">{{$errors->first('qty')}}
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div> -->
                                                        <div class="invalid-feedback">{{$errors->first('qty')}}</div>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-address">Total</label><span class="text-danger"> *</span>
                                                        <input id="total" name="total" class="form-control {{ $errors->has('total') ? 'is-invalid':'' }}" placeholder="Masukkan total pengeluaran" type="number" value="{{ old('total') }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                                                        @if ($errors->has('total'))
                                                        <!-- <div class="alert invalid-feedback">{{$errors->first('total')}}
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div> -->
                                                        <div class="invalid-feedback">{{$errors->first('total')}}</div>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-address">Keterangan</label>
                                                        <input id="keterangan" name="keterangan" class="form-control {{ $errors->has('keterangan') ? 'is-invalid':'' }}" placeholder="Masukkan keterangan" type="text" value="{{ old('keterangan') }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                                                        @if ($errors->has('keterangan'))
                                                        <!-- <div class="alert invalid-feedback">{{$errors->first('keterangan')}}
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div> -->
                                                        <div class="invalid-feedback">{{$errors->first('keterangan')}}</div>
                                                        @endif
                                                    </div>
                                    
                                                        
                                                        
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                                                        </div>
                                                    </div>
                                                </form>
                                        </div>
                                    </div>



                                    {{-- <a href="/admin/createbeli_bahan/{{$bahan->id}}" class="py-2 btn btn-sm  btn-light-success"> <i class="flaticon-download-1 icon-nm"></i> <small class="font-weight-bolder"> Input Stok</small></a> --}}
                                    
                                    {{-- <a href="/admin/createkurang_bahan/{{$bahan->id}}" class="py-2 btn btn-sm btn-light-warning"><i class="flaticon2-writing icon-nm"></i><small class="font-weight-bolder">Update Stok</small></a> --}}


                                    <button type="button" data-toggle="modal" data-target="#updatestok{{$bahan->id}}" class="py-2 btn btn-sm  btn-light-warning"> <i class="flaticon2-writing icon-nm"></i> <small class="font-weight-bolder"> Update Stok</small></button> 
                                    
                                    <div class="modal fade" id="updatestok{{$bahan->id}}" tabindex="-1" role="dialog" aria-labelledby="inputmodalabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodalabel">Update Stok {{$bahan->nama_bahan}} ( Stok saat ini :{{$bahan->stok}})</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <i aria-hidden="true" class="ki ki-close"></i>
                                                    </button>
                                                </div>
                                                <form action="/admin/storekurang_bahan/{{$bahan->id}}" class="formValidateupdate" id="formValidateupdate" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    @method('post')
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-address">Tanggal</label>
                                                        <div class="input-group date">
                                                            <input id="tanggal_kurang" autocomplete="off" data-date-end-date="0d" value="" type="text" data-tanggal_kurang="tanggal_kurang" name="tanggal_kurang" class="form-control tglkurang {{ $errors->has('tanggal_kurang') ? 'is-invalid':'' }}" placeholder="Masukkan tanggal" data-error=".errorTxt1">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="errorTxt1"></div>
                                                        @if ($errors->has('tanggal_kurang'))
                                                        <div class="invalid-feedback">{{$errors->first('tanggal_kurang')}}</div>
                                                        @endif
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <label class="form-control-label" for="input-address">Tanggal Pembelian</label>
                                                        <div class="">
                                                            <div class="input-group date">
                                                                
                                                                <input id="kt_datepicker_2_modal" data-date-format="dd/mm/yyyy" name="tanggal_beli" class="form-control {{ $errors->has('tanggal_beli') ? 'is-invalid':'' }}" readonly="readonly" placeholder="Masukkan kategori" data-error=".errorTxt1">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-check-o"></i>
                                                                    </span>
                                                                </div>
                                                                @if ($errors->has('tanggal_beli'))
                                                                <div class="invalid-feedback">{{$errors->first('tanggal_beli')}}</div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                    
                                                    <div class="form-group " hidden>
                                                        <label for="id_bahan">Bahan</label><span class="text-danger"> *</span>
                                                        <select class="form-control" name="id_bahan" id="id_bahan">
                                                            <option value="" hidden>-- Pilih Kategori --</option>
                                                            @foreach ($bahans as $bhn)
                                                            <option value="{{ $bahan->id }}" {{ $bhn->id == $bahan->id ? 'selected':'' }}>{{ ucfirst($bhn->nama_bahan) }} <small>({{ ucfirst($bhn->satuan) }})</small></option>
                                                            
                                                            @endforeach
                                                        </select>
                                                        
                                                        @if (('id_bahan') === 0)
                                                        <div class="invalid-feedback">{{$errors->first('id_bahan')}}</div>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-address">Stok tersedia</label><span class="text-danger"> *</span>
                                                        <input id="jumlah" name="jumlah" class="form-control {{ $errors->has('jumlah') ? 'is-invalid':'' }}" placeholder="Masukkan jumlah bahan" type="number" value="{{ old('jumlah') }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                                                        
                                                        @if ($errors->has('jumlah'))
                                                        <!-- <div class="alert invalid-feedback">{{$errors->first('jumlah')}}
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div> -->
                                                        <div class="invalid-feedback">{{$errors->first('jumlah')}}</div>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-address">Keterangan</label>
                                                        <input id="keterangan" name="keterangan" class="form-control {{ $errors->has('keterangan') ? 'is-invalid':'' }}" placeholder="Masukkan keterangan" type="text" value="{{ old('keterangan') }}" data-error=".errorTxt1">
                                                        <div class="errorTxt1"></div>
                                                        @if ($errors->has('keterangan'))
                                                        <!-- <div class="alert invalid-feedback">{{$errors->first('keterangan')}}
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div> -->
                                                        <div class="invalid-feedback">{{$errors->first('keterangan')}}</div>
                                                        @endif
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <label>Gambar</label>
                                                        <div></div>
                                                        <div class="custom-file">
                                                            
                                                            <input id="gambar" name="gambar" class="custom-file-input {{ $errors->has('gambar') ? 'is-invalid':'' }}" placeholder="Upload foto kategori" type="file" value="{{ old('gambar') }}">
                                                            <label class="custom-file-label" for="customFile">Pilih file</label>
                                                        </div>
                                                    </div> -->
                                    
                                                        
                                                        
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                                                        </div>
                                                    </div>
                                                </form>
                                        </div>
                                    </div>


                                </td>

                                <td nowarp="nowarp">


                                    <a href="/admin/editbahan/{{$bahan->id}}" class="btn btn-sm btn-clean btn-icon" title="Edit details">
                                        <span class="svg-icon svg-icon-md">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" \ transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) " />
                                                    <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1" />
                                                </g>
                                            </svg>
                                        </span>
                                    </a>
                                    <!-- <a href="/desbahan:{{$bahan->id}}" onclick="return confirm('Apakah anda yakin')" id="delete" class="delete btn btn-sm btn-clean btn-icon" title="Delete">
                                        <span class="svg-icon svg-icon-md">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero" />
                                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                    </a> -->
                                    <a href="javascript:del();" data-id="{{$bahan->id}}" class="btn btn-sm btn-clean btn-icon deletebtn">
                                        <form action="{{route('bhn.delete',$bahan->id)}}" id="delete{{$bahan->id}}" method="GET">
                                            @csrf
                                            @method('delete')
                                        </form>
                                        <span class="svg-icon svg-icon-md" data-id="{{$bahan->id}}" id="delete{{$bahan->id}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" data-id="{{$bahan->id}}" id="delete{{$bahan->id}}" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" data-id="{{$bahan->id}}" id="delete{{$bahan->id}}" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect data-id="{{$bahan->id}}" id="delete{{$bahan->id}}" x="0" y="0" width="24" height="24" />
                                                    <path data-id="{{$bahan->id}}" id="delete{{$bahan->id}}" d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero" />
                                                    <path data-id="{{$bahan->id}}" id="delete{{$bahan->id}}" d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                    </a>


                                </td>





                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    <!--end: Datatable-->
                </div>

            </div>
            <!--end::Card-->

        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->





@endsection

@section('js')
<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<!-- <script src="{{asset('assets/js/pages/crud/datatables/data-sources/html.js')}}"></script> -->
<!-- <script src="{{asset('assets/js/pages/features/miscellaneous/toastr.js')}}"></script>
<script src="{{asset('assets/js/pages/features/miscellaneous/sweetalert2.js')}}"></script> -->

<script>
    $(document).ready(function() {
        $('#tabel').DataTable({
            responsive: true,
        });
    });
</script>


<script>
    function del() {
        $(".deletebtn").click(function(e) {

            id = e.target.dataset.id;
            Swal.fire({
                title: "Apakah anda yakin?",
                text: "Data yang sudah anda hapus tidak akan bisa kembali!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya, Hapus"
            }).then(function(result) {
                if (result.value) {

                    Swal.fire(
                        "Terhapus!",
                        "Data telah terhapus.",
                        "success"
                    );
                    $(`#delete${id}`).submit();

                } else {

                }
            });
        });
    }
</script>

@if(Session::has('message'))
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var type = "{{Session::get('alert-type','success')}}"
    switch (type) {
        case 'success':
            toastr.success("{{Session::get('message')}}");
            // Swal.fire("Berhasil","{{Session::get('message')}}","success");
            // Swal.fire("Good job!", "You clicked the button!", "success");
            break;

    }
</script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.tglbeli').datepicker({
            format: "yyyy/mm/dd",
            todayHighlight: true,
            autoclose: true,
            todayBtn: "linked",
            clearBtn: true,
        });
    });
    $(document).ready(function() {
        $('.tglkurang').datepicker({
            format: "yyyy/mm/dd",
            todayHighlight: true,
            autoclose: true,
            todayBtn: "linked",
            clearBtn: true,
        });
    });
</script>
<!-- <script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js"></script> -->
<script>
    $('.tglbeli').data('tanggal_beli').maxDate(0)
    $('.tglkurang').data('tanggal_beli').maxDate(0)
</script>
<script>
    var elements = document.getElementsByClassName("tglbeli");
    var today = moment().format('YYYY/MM/DD');
    for(var i=0; i<elements.length; i++) {
    
    elements[i].value = today;;
    }
    // document.write(names);

    // var today = moment().format('YYYY/MM/DD');
    // document.querySelectorAll("tanggal_beli").value = today;
    
    </script>
<script>
    var elements = document.getElementsByClassName("tglkurang");
    var today = moment().format('YYYY/MM/DD');
    for(var i=0; i<elements.length; i++) {
    
    elements[i].value = today;;
    }
    // document.write(names);

    // var today = moment().format('YYYY/MM/DD');
    // document.querySelectorAll("tanggal_beli").value = today;
    
    </script>
<script>
    FormValidation.formValidation(
        document.getElementById('formValidate'), {
            fields: {
                tanggal_beli: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan tanggal'
                        }
                    }
                },
                // id_bahan: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Silahkan masukan bahan'
                //         }
                //     }
                // },
                qty: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan jumlah stok masuk'
                        }
                    }
                },
                total: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan total'
                        }
                    }
                },
                satuan: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan masukan satuan'
                        }
                    }
                },

            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                // Bootstrap Framework Integration
                bootstrap: new FormValidation.plugins.Bootstrap(),
                // Validate fields when clicking the Submit button
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            }
        }
    );
</script>

@endsection