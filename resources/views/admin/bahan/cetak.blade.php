<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Example 1</title>
  <link rel="stylesheet" href="assets/laporan/style.css" media="all" />
</head>

<body>
  <?php
  function pecahtgl($timestamp)
  {
    $pecahkan = preg_split('/( |,|-|:)/', $timestamp);
    $hari = array(
      7 => 'Minggu',
      1 => 'Senin',
      2 => 'Selasa',
      3 => 'Rabu',
      4 => 'Kamis',
      5 => 'Jumat',
      6 => 'Sabtu',
    );
    $bulan = array(
      1 =>   'Januari',
      2 => 'Februari',
      3 => 'Maret',
      4 => 'April',
      5 => 'Mei',
      6 => ' Juni',
      7 => 'Juli',
      8 => 'Agustus',
      9 => 'September',
      10 => 'Oktober',
      11 => 'November',
      12 => 'Desember'
    );
    return $hari[(int)$pecahkan[0]] . ',' . $pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4] . ' ' . $bulan[(int)$pecahkan[5]] . ' ' . $pecahkan[6] . ' ' . $pecahkan[7] . ' ' . $pecahkan[8] . ' ' . $pecahkan[1];
  }

  ?>
  <header class="clearfix">

    <div id="project">
      <div>The Melted </div>
      <div>Make Your Mood Be Better</div>
      <div>Instragram : @themeltedd</div>
      <div>Whatsapp : +62 895-3664-39266</div>
    </div>
    <div id="logo">
      <img src="assets/laporan/asdd.jpg">
    </div>
  </header>
  <h2>Daftar Bahan Baku</h2>
  <main>
    <table>
      <thead>
        <tr>
          <th style="width: 2%;">#</th>
          <th style="width: 20%;">Bahan</th>
          <th style="width: 30%;">Stok</th>
          <th>Tanggal Terakhir Input Stok</th>
          <th>Tanggal Terakhir Update Stok</th>

        </tr>
      </thead>
      <tbody>
        <?php $no = 0; ?>
        @foreach ($bahan as $bahan)
        <?php $no++; ?>
        <tr>
          <td>{{ $no }}</td>
          <td style="text-align: center;">{{ $bahan->nama_bahan}} </td>
          <td>
            @if($bahan->stok === null || $bahan->stok === 0)
            <em> Stok kosong </em>
            @else
            <strong>{{$bahan->stok}}</strong> <small>({{ $bahan->satuan}})</small>
            @endif

          </td>
          <td>
            @if($bahan->tanggal_beli === null || $bahan->tanggal_beli === 0)
            <small> <em> Belum ada stok masuk </em></small>
            @else
            {{ pecahtgl(date('N, d - m - Y', strtotime($bahan->tanggal_beli)))}}
            @endif
          </td>
          <td>
            @if($bahan->tanggal_kurang === null || $bahan->tanggal_kurang === 0)
            <small><em> Update stok belum di lakukan</em></small>
            @else

            {{ pecahtgl(date('N, d - m - Y', strtotime($bahan->tanggal_kurang)))}}
            @endif
          </td>

          @endforeach

      </tbody>
    </table>
</body>

</html>