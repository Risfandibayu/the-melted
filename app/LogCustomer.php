<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogCustomer extends Model
{
    protected $table = 'log_customers';
    protected $fillable = ['id_customer','log','by_categories','created_at','updated_at'];
}
