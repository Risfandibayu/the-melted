<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bahan extends Model
{
    protected $table = 'bahans';
    protected $fillable = [
        'nama_bahan',
        'satuan',        
        'stok',        
        'stok_minim',        
    ];

    public function belibhn(){
        return $this->hasMany('App\beli_bahan','id_bahan','id');
    }
}
