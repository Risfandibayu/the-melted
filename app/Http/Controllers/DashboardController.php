<?php

namespace App\Http\Controllers;

use App\Categories;
use App\tips;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DashboardController extends Controller
{
    //

    public function index()
    {
        //ambil data data untuk ditampilkan di card pada dashboard
        $pendapatan = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where('status_order_id', 5)
            ->orWhere('status_order_id', 4)
            ->first();
        $pendapatantoday = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->where('status_order_id', 5)
            ->orwhere(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->Where('status_order_id', 4)
            ->first();

        $pendapatan7days = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->whereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->where('status_order_id', 5)
            ->orwhereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->Where('status_order_id', 4)
            ->first();

        $pengeluarantoday = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->where('tanggal_beli', '=', DB::raw('CURDATE()'))
            ->first();
        $pengeluaran7days = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->whereRaw('DATEDIFF(Now(),tanggal_beli) <= 7')
            ->first();


        $transaksi = DB::table('order')
            ->select(DB::raw('COUNT(id) as total_order'))
            ->first();
        $pelanggan = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->first();
        $pelanggantoday = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->where('created_at', '=', 'Curdate()')
            ->first();
        $peldaftoday = DB::table('users')
            ->leftjoin('alamat', 'alamat.user_id', '=', 'users.id')
            ->leftjoin('cities', 'cities.city_id', '=', 'alamat.cities_id')
            ->leftjoin('provinces', 'provinces.province_id', '=', 'cities.province_id')
            ->select('users.*', DB::raw('(Year(Curdate())- Year(users.tgl_lahir)) as usia'), 'alamat.detail', 'cities.title as kota', 'provinces.title as prov')
            ->where('users.role', '=', 'customer')
            ->where('users.created_at', '=', 'Curdate()')
            ->get();


        $order_terbaru  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->limit(10)
            ->get();
        $order7  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->whereRaw('DATEDIFF(Now(),order.created_at) <= 7')
            ->get();
        $orderperlu  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('status_order_id', '=', 2)
            ->orwhere('status_order_id', '=', 3)
            ->get();

        $bahan =  DB::table('bahans')
            ->select('bahans.id', 'bahans.nama_bahan', 'bahans.stok', 'bahans.satuan', DB::raw('max(beli_bahans.tanggal_beli) as tanggal_beli'), DB::raw('max(kurang_bahans.tanggal_kurang) as tanggal_kurang'))
            ->leftJoin('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->leftJoin('kurang_bahans', 'bahans.id', '=', 'kurang_bahans.id_bahan')
            // ->max('beli_bahans.tanggal_beli',$maxbeli)
            // ->max('kurang_bahans.tanggal_kurang',$maxkrg)
            ->groupBy('bahans.nama_bahan')
            ->where('bahans.stok', '<', 10)
            ->get();
        $bahmin =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->where('bahans.stok', '<', 10)
            ->first();
        $bahantot =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->first();

        $products = DB::table('products')
            ->leftJoin('categories as a', 'a.id', '=', 'products.categories_id')
            ->select('products.*', 'a.name as nama_kategori')
            ->where('products.stok', '<', 10)
            ->get();
        $prodmin = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->where('products.stok', '<', 10)
            ->first();
        $produktot = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->first();
        $tipstot = DB::table('tips')
            ->select(DB::raw('count(id) as id'))
            ->first();


        $prod = DB::table('products')
            ->Join('categories as a', 'a.id', '=', 'products.categories_id')
            ->Join('detail_order as b', 'b.product_id', '=', 'products.id')
            ->select('products.*', 'a.name as nama_kategori', DB::raw('sum(b.qty) as jmlterjual'), DB::raw('sum(products.price * qty) as pendapatan'))
            ->groupBy('products.id')
            ->orderBy(db::raw('sum(b.qty)'))
            ->get();




        $transmasuktoday = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->first();
        $transcek = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 2)
            ->first();
        $transkirim = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 3)
            ->first();
        $stokmasuktoday = DB::table('beli_bahans')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(tanggal_beli)'), '=', DB::raw('CURDATE()'))
            ->first();

        $beli7 = DB::table('bahans')
            ->join('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->whereRaw('DATEDIFF(Now(),beli_bahans.tanggal_beli) <= 7')
            ->orderBy('beli_bahans.tanggal_beli', 'DESC')
            ->get();


        $data = array(
            'pendapatan' => $pendapatan,
            'pendapatanhariini' => $pendapatantoday,
            'pendapatan7' => $pendapatan7days,
            'pengeluaranhariini' => $pengeluarantoday,
            'pengeluaran7' => $pengeluaran7days,
            'transaksi'  => $transaksi,
            'pelanggan'  => $pelanggan,
            'pelanggantoday'  => $pelanggantoday,
            'peldaftoday'  => $peldaftoday,
            'order_baru' => $order_terbaru,
            'orderperlu' => $orderperlu,
            'order7' => $order7,
            'stok_minim' => $bahan,
            'pro_stok' => $products,
            'pro_laris' => $prod,
            'trxmasuk' => $transmasuktoday,
            'stokmasuk' => $stokmasuktoday,
            'promin' => $prodmin,
            'bahmin' => $bahmin,
            'bahantot' => $bahantot,
            'produktot' => $produktot,
            'tipstot' => $tipstot,
            'transcek' => $transcek,
            'transkirim' => $transkirim,
            'beli7' => $beli7,
        );

        return view('admin/dashboard', $data);
    }

    public function indexown()
    {
        //ambil data data untuk ditampilkan di card pada dashboard
        $pendapatan = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where('status_order_id', 5)
            ->orWhere('status_order_id', 4)
            ->orWhere('status_order_id', 3)
            ->first();
        $pendapatantoday = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->where('status_order_id', 5)
            ->orwhere(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->Where('status_order_id', 4)
            ->orwhere(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->Where('status_order_id', 3)
            ->first();

        $pendapatan7days = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->whereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->where('status_order_id', 5)
            ->orwhereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->Where('status_order_id', 4)
            ->orwhereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->Where('status_order_id', 3)
            ->first();

        $pengeluarantoday = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->where('tanggal_beli', '=', DB::raw('CURDATE()'))
            ->first();
        $pengeluaran7days = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->whereRaw('DATEDIFF(Now(),tanggal_beli) <= 7')
            ->first();


        $transaksi = DB::table('order')
            ->select(DB::raw('COUNT(id) as total_order'))
            ->first();
        $pelanggan = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->first();
        $pelanggantoday = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->where('created_at', '=', 'Curdate()')
            ->first();
        $peldaftoday = DB::table('users')
            ->leftjoin('alamat', 'alamat.user_id', '=', 'users.id')
            ->leftjoin('cities', 'cities.city_id', '=', 'alamat.cities_id')
            ->leftjoin('provinces', 'provinces.province_id', '=', 'cities.province_id')
            ->select('users.*', DB::raw('(Year(Curdate())- Year(users.tgl_lahir)) as usia'), 'alamat.detail', 'cities.title as kota', 'provinces.title as prov')
            ->where('users.role', '=', 'customer')
            ->where('users.created_at', '=', 'Curdate()')
            ->get();


        $order_terbaru  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->limit(10)
            ->get();
        $order7  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', DB::raw('date(order.created_at) as tanggal'), 'status_order.name', 'users.name as nama_pemesan')
            ->whereRaw('DATEDIFF(Now(),order.created_at) <= 7')
            ->orderBy('order.created_at' , 'DESC')
            ->get();
        $orderperlu  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*',  'status_order.name', 'users.name as nama_pemesan')
            ->where('status_order_id', '=', 2)
            ->orwhere('status_order_id', '=', 3)
            ->get();
        $orderblm  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*',  'status_order.name', 'users.name as nama_pemesan')
            ->where('status_order_id', '=', 1)
            ->get();

        $bahan =  DB::table('bahans')
            ->select('bahans.id', 'bahans.nama_bahan', 'bahans.stok', 'bahans.satuan', DB::raw('max(beli_bahans.tanggal_beli) as tanggal_beli'), DB::raw('max(kurang_bahans.tanggal_kurang) as tanggal_kurang'))
            ->leftJoin('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->leftJoin('kurang_bahans', 'bahans.id', '=', 'kurang_bahans.id_bahan')
            // ->max('beli_bahans.tanggal_beli',$maxbeli)
            // ->max('kurang_bahans.tanggal_kurang',$maxkrg)
            ->groupBy('bahans.nama_bahan')
            ->where('bahans.stok', '<', 10)
            ->get();
        $bahmin =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->where('bahans.stok', '<', 10)
            ->first();
        $bahantot =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->first();

        $products = DB::table('products')
            ->leftJoin('categories as a', 'a.id', '=', 'products.categories_id')
            ->select('products.*', 'a.name as nama_kategori')
            ->where('products.stok', '<', 10)
            ->get();
        $prodmin = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->where('products.stok', '<', 10)
            ->first();
        $produktot = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->first();
        $tipstot = DB::table('tips')
            ->select(DB::raw('count(id) as id'))
            ->first();


        $prod = DB::table('products')
            ->Join('categories as a', 'a.id', '=', 'products.categories_id')
            ->Join('detail_order as b', 'b.product_id', '=', 'products.id')
            ->select('products.*', 'a.name as nama_kategori', DB::raw('sum(b.qty) as jmlterjual'), DB::raw('sum(products.price * qty) as pendapatan'))
            ->groupBy('products.id')
            ->orderBy(db::raw('sum(b.qty)'))
            ->get();




        $transmasuktoday = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->first();
        $transcek = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 2)
            ->first();
        $transkirim = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 3)
            ->first();
        $stokmasuktoday = DB::table('beli_bahans')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(tanggal_beli)'), '=', DB::raw('CURDATE()'))
            ->first();

        $beli7 = DB::table('bahans')
            ->select('bahans.*', db::raw('sum(beli_bahans.qty) as beli'), db::raw('sum(beli_bahans.total) as ttl'))
            ->join('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->whereRaw('DATEDIFF(Now(),beli_bahans.tanggal_beli) <= 7')
            ->groupBy('bahans.id')
            ->orderBy('beli_bahans.tanggal_beli', 'DESC')
            ->get();


        $data = array(
            'pendapatan' => $pendapatan,
            'pendapatanhariini' => $pendapatantoday,
            'pendapatan7' => $pendapatan7days,
            'pengeluaranhariini' => $pengeluarantoday,
            'pengeluaran7' => $pengeluaran7days,
            'transaksi'  => $transaksi,
            'pelanggan'  => $pelanggan,
            'pelanggantoday'  => $pelanggantoday,
            'peldaftoday'  => $peldaftoday,
            'order_baru' => $order_terbaru,
            'orderperlu' => $orderperlu,
            'orderblm' => $orderblm,
            'order7' => $order7,
            'stok_minim' => $bahan,
            'pro_stok' => $products,
            'pro_laris' => $prod,
            'trxmasuk' => $transmasuktoday,
            'stokmasuk' => $stokmasuktoday,
            'promin' => $prodmin,
            'bahmin' => $bahmin,
            'bahantot' => $bahantot,
            'produktot' => $produktot,
            'tipstot' => $tipstot,
            'transcek' => $transcek,
            'transkirim' => $transkirim,
            'beli7' => $beli7,
        );

        return view('admin.dash-owner', $data);
    }


    public function indexcust()
    {
        //ambil data data untuk ditampilkan di card pada dashboard
        $pendapatan = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where('status_order_id', 5)
            ->orWhere('status_order_id', 4)
            ->first();
        $pendapatantoday = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->where('status_order_id', 5)
            ->orwhere(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->Where('status_order_id', 4)
            ->first();

        $pendapatan7days = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->whereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->where('status_order_id', 5)
            ->orwhereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->Where('status_order_id', 4)
            ->first();

        $pengeluarantoday = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->where('tanggal_beli', '=', DB::raw('CURDATE()'))
            ->first();
        $pengeluaran7days = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->whereRaw('DATEDIFF(Now(),tanggal_beli) <= 7')
            ->first();


        $transaksi = DB::table('order')
            ->select(DB::raw('COUNT(id) as total_order'))
            ->first();
        $pelanggan = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->first();
        $pelangganlk = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->where('jk','=','laki-laki')
            ->first();
        $pelangganpr = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->where('jk','=','perempuan')
            ->first();
        
        $pelanggantoday = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->where('created_at', '=', 'Curdate()')
            ->first();
        $peldaftoday = DB::table('users')
            ->leftjoin('alamat', 'alamat.user_id', '=', 'users.id')
            ->leftjoin('cities', 'cities.city_id', '=', 'alamat.cities_id')
            ->leftjoin('provinces', 'provinces.province_id', '=', 'cities.province_id')
            ->select('users.*', DB::raw('(Year(Curdate())- Year(users.tgl_lahir)) as usia'), 'alamat.detail', 'cities.title as kota', 'provinces.title as prov')
            ->where('users.role', '=', 'customer')
            ->where(db::raw('date(users.created_at)'), '=', db::raw('date(Now())'))
            ->get();


        $order_terbaru  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->limit(10)
            ->get();
        $order7  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->whereRaw('DATEDIFF(Now(),order.created_at) <= 7')
            ->get();
        $orderperlu  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('status_order_id', '=', 2)
            ->orwhere('status_order_id', '=', 3)
            ->get();

        $bahan =  DB::table('bahans')
            ->select('bahans.id', 'bahans.nama_bahan', 'bahans.stok', 'bahans.satuan', DB::raw('max(beli_bahans.tanggal_beli) as tanggal_beli'), DB::raw('max(kurang_bahans.tanggal_kurang) as tanggal_kurang'))
            ->leftJoin('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->leftJoin('kurang_bahans', 'bahans.id', '=', 'kurang_bahans.id_bahan')
            // ->max('beli_bahans.tanggal_beli',$maxbeli)
            // ->max('kurang_bahans.tanggal_kurang',$maxkrg)
            ->groupBy('bahans.nama_bahan')
            ->where('bahans.stok', '<', 10)
            ->get();
        $bahmin =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->where('bahans.stok', '<', 10)
            ->first();
        $bahantot =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->first();

        $products = DB::table('products')
            ->leftJoin('categories as a', 'a.id', '=', 'products.categories_id')
            ->select('products.*', 'a.name as nama_kategori')
            ->where('products.stok', '<', 10)
            ->get();
        $prodmin = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->where('products.stok', '<', 10)
            ->first();
        $produktot = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->first();
        $tipstot = DB::table('tips')
            ->select(DB::raw('count(id) as id'))
            ->first();


        $prod = DB::table('products')
            ->Join('categories as a', 'a.id', '=', 'products.categories_id')
            ->Join('detail_order as b', 'b.product_id', '=', 'products.id')
            ->select('products.*', 'a.name as nama_kategori', DB::raw('sum(b.qty) as jmlterjual'), DB::raw('sum(products.price * qty) as pendapatan'))
            ->groupBy('products.id')
            ->orderBy(db::raw('sum(b.qty)'))
            ->get();




        $transmasuktoday = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->first();
        $transcek = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 2)
            ->first();
        $transkirim = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 3)
            ->first();
        $stokmasuktoday = DB::table('beli_bahans')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(tanggal_beli)'), '=', DB::raw('CURDATE()'))
            ->first();

        $beli7 = DB::table('bahans')
            ->join('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->whereRaw('DATEDIFF(Now(),beli_bahans.tanggal_beli) <= 7')
            ->orderBy('beli_bahans.tanggal_beli', 'DESC')
            ->get();



            $chartl1216 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','laki-laki')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['12','16'])
            ->first();
            $chartl1721 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','laki-laki')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['17','21'])
            ->first();
            $chartl2226 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','laki-laki')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['22','26'])
            ->first();
            $chartl2731 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','laki-laki')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['27','31'])
            ->first();
            $chartl3236 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','laki-laki')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['32','36'])
            ->first();
            $chartl3741 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','laki-laki')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['37','41'])
            ->first();
            $chartl4245 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','laki-laki')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['41','45'])
            ->first();
    
    
            $chartp1216 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','perempuan')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['12','16'])
            ->first();
            $chartp1721 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','perempuan')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['17','21'])
            ->first();
            $chartp2226 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','perempuan')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['22','26'])
            ->first();
            $chartp2731 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','perempuan')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['27','31'])
            ->first();
            $chartp3236 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','perempuan')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['32','36'])
            ->first();
            $chartp3741 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','perempuan')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['37','41'])
            ->first();
            $chartp4245 = DB::table('users')
            ->select(DB::raw('count(id) as tot'))
            ->where('role','customer')
            ->where('jk','perempuan')
            ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['41','45'])
            ->first();
        


        $data = array(
            'pendapatan' => $pendapatan,
            'pendapatanhariini' => $pendapatantoday,
            'pendapatan7' => $pendapatan7days,
            'pengeluaranhariini' => $pengeluarantoday,
            'pengeluaran7' => $pengeluaran7days,
            'transaksi'  => $transaksi,
            'pelanggan'  => $pelanggan,
            'pelangganlk'  => $pelangganlk,
            'pelangganpr'  => $pelangganpr,
            'pelanggantoday'  => $pelanggantoday,
            'peldaftoday'  => $peldaftoday,
            'order_baru' => $order_terbaru,
            'orderperlu' => $orderperlu,
            'order7' => $order7,
            'stok_minim' => $bahan,
            'pro_stok' => $products,
            'pro_laris' => $prod,
            'trxmasuk' => $transmasuktoday,
            'stokmasuk' => $stokmasuktoday,
            'promin' => $prodmin,
            'bahmin' => $bahmin,
            'bahantot' => $bahantot,
            'produktot' => $produktot,
            'tipstot' => $tipstot,
            'transcek' => $transcek,
            'transkirim' => $transkirim,
            'beli7' => $beli7,
            'chartl1216' => $chartl1216,
            'chartl1721' => $chartl1721,
            'chartl2226' => $chartl2226,
            'chartl2731' => $chartl2731,
            'chartl3236' => $chartl3236,
            'chartl3741' => $chartl3741,
            'chartl4245' => $chartl4245,
            'chartp1216' => $chartp1216,
            'chartp1721' => $chartp1721,
            'chartp2226' => $chartp2226,
            'chartp2731' => $chartp2731,
            'chartp3236' => $chartp3236,
            'chartp3741' => $chartp3741,
            'chartp4245' => $chartp4245,
        );

        return view('admin.dash-cust', $data);
    }
    public function indexkat()
    {
        //ambil data data untuk ditampilkan di card pada dashboard
        $pendapatan = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where('status_order_id', 5)
            ->orWhere('status_order_id', 4)
            ->first();
        $pendapatantoday = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->where('status_order_id', 5)
            ->orwhere(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->Where('status_order_id', 4)
            ->first();

        $pendapatan7days = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->whereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->where('status_order_id', 5)
            ->orwhereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->Where('status_order_id', 4)
            ->first();

        $pengeluarantoday = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->where('tanggal_beli', '=', DB::raw('CURDATE()'))
            ->first();
        $pengeluaran7days = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->whereRaw('DATEDIFF(Now(),tanggal_beli) <= 7')
            ->first();


        $transaksi = DB::table('order')
            ->select(DB::raw('COUNT(id) as total_order'))
            ->first();
        $pelanggan = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->first();
        $pelanggantoday = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->where('created_at', '=', 'Curdate()')
            ->first();
        $peldaftoday = DB::table('users')
            ->leftjoin('alamat', 'alamat.user_id', '=', 'users.id')
            ->leftjoin('cities', 'cities.city_id', '=', 'alamat.cities_id')
            ->leftjoin('provinces', 'provinces.province_id', '=', 'cities.province_id')
            ->select('users.*', DB::raw('(Year(Curdate())- Year(users.tgl_lahir)) as usia'), 'alamat.detail', 'cities.title as kota', 'provinces.title as prov')
            ->where('users.role', '=', 'customer')
            ->where('users.created_at', '=', 'Curdate()')
            ->get();


        $order_terbaru  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->limit(10)
            ->get();
        $order7  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->whereRaw('DATEDIFF(Now(),order.created_at) <= 7')
            ->get();
        $orderperlu  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('status_order_id', '=', 2)
            ->orwhere('status_order_id', '=', 3)
            ->get();

        $bahan =  DB::table('bahans')
            ->select('bahans.id', 'bahans.nama_bahan', 'bahans.stok', 'bahans.satuan', DB::raw('max(beli_bahans.tanggal_beli) as tanggal_beli'), DB::raw('max(kurang_bahans.tanggal_kurang) as tanggal_kurang'))
            ->leftJoin('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->leftJoin('kurang_bahans', 'bahans.id', '=', 'kurang_bahans.id_bahan')
            // ->max('beli_bahans.tanggal_beli',$maxbeli)
            // ->max('kurang_bahans.tanggal_kurang',$maxkrg)
            ->groupBy('bahans.nama_bahan')
            ->where('bahans.stok', '<', 10)
            ->get();
        $bahmin =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->where('bahans.stok', '<', 10)
            ->first();
        $bahantot =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->first();

        $products = DB::table('products')
            ->leftJoin('categories as a', 'a.id', '=', 'products.categories_id')
            ->select('products.*', 'a.name as nama_kategori')
            ->where('products.stok', '<', 10)
            ->get();
        $prodmin = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->where('products.stok', '<', 10)
            ->first();
        $produktot = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->first();
        $tipstot = DB::table('tips')
            ->select(DB::raw('count(id) as id'))
            ->first();


        $prod = DB::table('products')
            ->Join('categories as a', 'a.id', '=', 'products.categories_id')
            ->Join('detail_order as b', 'b.product_id', '=', 'products.id')
            ->select('products.*', 'a.name as nama_kategori', DB::raw('sum(b.qty) as jmlterjual'), DB::raw('sum(products.price * qty) as pendapatan'))
            ->groupBy('products.id')
            ->orderBy(db::raw('sum(b.qty)'))
            ->get();




        $transmasuktoday = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->first();
        $transcek = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 2)
            ->first();
        $transkirim = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 3)
            ->first();
        $stokmasuktoday = DB::table('beli_bahans')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(tanggal_beli)'), '=', DB::raw('CURDATE()'))
            ->first();

        $beli7 = DB::table('bahans')
            ->join('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->whereRaw('DATEDIFF(Now(),beli_bahans.tanggal_beli) <= 7')
            ->orderBy('beli_bahans.tanggal_beli', 'DESC')
            ->get();


        $data = array(
            'pendapatan' => $pendapatan,
            'pendapatanhariini' => $pendapatantoday,
            'pendapatan7' => $pendapatan7days,
            'pengeluaranhariini' => $pengeluarantoday,
            'pengeluaran7' => $pengeluaran7days,
            'transaksi'  => $transaksi,
            'pelanggan'  => $pelanggan,
            'pelanggantoday'  => $pelanggantoday,
            'peldaftoday'  => $peldaftoday,
            'categories' => Categories::all(),
            'order_baru' => $order_terbaru,
            'orderperlu' => $orderperlu,
            'order7' => $order7,
            'stok_minim' => $bahan,
            'pro_stok' => $products,
            'pro_laris' => $prod,
            'trxmasuk' => $transmasuktoday,
            'stokmasuk' => $stokmasuktoday,
            'promin' => $prodmin,
            'bahmin' => $bahmin,
            'bahantot' => $bahantot,
            'produktot' => $produktot,
            'tipstot' => $tipstot,
            'transcek' => $transcek,
            'transkirim' => $transkirim,
            'beli7' => $beli7,
        );

        return view('admin.dash-kat', $data);
    }
    public function indexstok()
    {
        //ambil data data untuk ditampilkan di card pada dashboard
        $pendapatan = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where('status_order_id', 5)
            ->orWhere('status_order_id', 4)
            ->first();
        $bahans = \App\bahan::orderBy('id', 'ASC')->get();
        $pendapatantoday = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->where('status_order_id', 5)
            ->orwhere(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->Where('status_order_id', 4)
            ->first();

        $pendapatan7days = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->whereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->where('status_order_id', 5)
            ->orwhereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->Where('status_order_id', 4)
            ->first();

        $pengeluarantoday = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->where('tanggal_beli', '=', DB::raw('CURDATE()'))
            ->first();
        $pengeluaran7days = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->whereRaw('DATEDIFF(Now(),tanggal_beli) <= 7')
            ->first();


        $transaksi = DB::table('order')
            ->select(DB::raw('COUNT(id) as total_order'))
            ->first();
        $pelanggan = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->first();
        $pelanggantoday = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->where('created_at', '=', 'Curdate()')
            ->first();
        $peldaftoday = DB::table('users')
            ->leftjoin('alamat', 'alamat.user_id', '=', 'users.id')
            ->leftjoin('cities', 'cities.city_id', '=', 'alamat.cities_id')
            ->leftjoin('provinces', 'provinces.province_id', '=', 'cities.province_id')
            ->select('users.*', DB::raw('(Year(Curdate())- Year(users.tgl_lahir)) as usia'), 'alamat.detail', 'cities.title as kota', 'provinces.title as prov')
            ->where('users.role', '=', 'customer')
            ->where('users.created_at', '=', 'Curdate()')
            ->get();


        $order_terbaru  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->limit(10)
            ->get();
        $order7  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->whereRaw('DATEDIFF(Now(),order.created_at) <= 7')
            ->get();
        $orderperlu  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('status_order_id', '=', 2)
            ->orwhere('status_order_id', '=', 3)
            ->get();

        $bahan =  DB::table('bahans')
            ->select('bahans.id', 'bahans.nama_bahan', 'bahans.stok', 'bahans.satuan', DB::raw('max(beli_bahans.tanggal_beli) as tanggal_beli'), DB::raw('max(kurang_bahans.tanggal_kurang) as tanggal_kurang'))
            ->leftJoin('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->leftJoin('kurang_bahans', 'bahans.id', '=', 'kurang_bahans.id_bahan')
            // ->max('beli_bahans.tanggal_beli',$maxbeli)
            // ->max('kurang_bahans.tanggal_kurang',$maxkrg)
            ->groupBy('bahans.nama_bahan')
            ->whereRaw('bahans.stok_minim > bahans.stok')
            // ->orwhere('bahans.stok','=',0)
            ->get();
        $bahmin =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->whereRaw('bahans.stok_minim > bahans.stok')
            // ->orwhere('bahans.stok','=',0)
            ->first();
        $bahantot =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->first();

        $products = DB::table('products')
            ->leftJoin('categories as a', 'a.id', '=', 'products.categories_id')
            ->select('products.*', 'a.name as nama_kategori')
            ->where('products.stok', '<', 10)
            ->get();
        $prodmin = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->where('products.stok', '<', 10)
            ->first();
        $produktot = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->first();
        $tipstot = DB::table('tips')
            ->select(DB::raw('count(id) as id'))
            ->first();


        $prod = DB::table('products')
            ->Join('categories as a', 'a.id', '=', 'products.categories_id')
            ->Join('detail_order as b', 'b.product_id', '=', 'products.id')
            ->select('products.*', 'a.name as nama_kategori', DB::raw('sum(b.qty) as jmlterjual'), DB::raw('sum(products.price * qty) as pendapatan'))
            ->groupBy('products.id')
            ->orderBy(db::raw('sum(b.qty)'))
            ->get();




        $transmasuktoday = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->first();
        $transcek = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 2)
            ->first();
        $transkirim = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 3)
            ->first();
        $stokmasuktoday = DB::table('beli_bahans')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(tanggal_beli)'), '=', DB::raw('CURDATE()'))
            ->first();

        $beli7 = DB::table('bahans')
            ->join('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->whereRaw('DATEDIFF(Now(),beli_bahans.tanggal_beli) <= 7')
            ->orderBy('beli_bahans.tanggal_beli', 'DESC')
            ->get();


        $data = array(
            'pendapatan' => $pendapatan,
            'pendapatanhariini' => $pendapatantoday,
            'pendapatan7' => $pendapatan7days,
            'pengeluaranhariini' => $pengeluarantoday,
            'pengeluaran7' => $pengeluaran7days,
            'transaksi'  => $transaksi,
            'pelanggan'  => $pelanggan,
            'bahans' => $bahans,
            'pelanggantoday'  => $pelanggantoday,
            'peldaftoday'  => $peldaftoday,
            'order_baru' => $order_terbaru,
            'orderperlu' => $orderperlu,
            'order7' => $order7,
            'stok_minim' => $bahan,
            'pro_stok' => $products,
            'pro_laris' => $prod,
            'trxmasuk' => $transmasuktoday,
            'stokmasuk' => $stokmasuktoday,
            'promin' => $prodmin,
            'bahmin' => $bahmin,
            'bahantot' => $bahantot,
            'produktot' => $produktot,
            'tipstot' => $tipstot,
            'transcek' => $transcek,
            'transkirim' => $transkirim,
            'beli7' => $beli7,
        );

        return view('admin.dash-stok', $data);
    }
    public function indextrx()
    {
        //ambil data data untuk ditampilkan di card pada dashboard
        $pendapatan = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where('status_order_id', 5)
            ->orWhere('status_order_id', 4)
            ->first();
        $pendapatantoday = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->where('status_order_id', 5)
            ->orwhere(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->Where('status_order_id', 4)
            ->first();

        $pendapatan7days = DB::table('order')
            ->select(DB::raw('SUM(subtotal) as penghasilan'))
            ->whereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->where('status_order_id', 5)
            ->orwhereRaw('DATEDIFF(Now(),created_at) <= 7')
            ->Where('status_order_id', 4)
            ->first();

        $pengeluarantoday = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->where('tanggal_beli', '=', DB::raw('CURDATE()'))
            ->first();
        $pengeluaran7days = DB::table('beli_bahans')
            ->select(DB::raw('SUM(total) as pengeluaran'))
            ->whereRaw('DATEDIFF(Now(),tanggal_beli) <= 7')
            ->first();


        $transaksi = DB::table('order')
            ->select(DB::raw('COUNT(id) as total_order'))
            ->first();
        $pelanggan = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->first();
        
        $pelanggantoday = DB::table('users')
            ->select(DB::raw('COUNT(id) as id'))
            ->where('role', '=', 'customer')
            ->where('created_at', '=', 'Curdate()')
            ->first();
        $peldaftoday = DB::table('users')
            ->leftjoin('alamat', 'alamat.user_id', '=', 'users.id')
            ->leftjoin('cities', 'cities.city_id', '=', 'alamat.cities_id')
            ->leftjoin('provinces', 'provinces.province_id', '=', 'cities.province_id')
            ->select('users.*', DB::raw('(Year(Curdate())- Year(users.tgl_lahir)) as usia'), 'alamat.detail', 'cities.title as kota', 'provinces.title as prov')
            ->where('users.role', '=', 'customer')
            ->where('users.created_at', '=', 'Curdate()')
            ->get();


        $order_terbaru  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->limit(10)
            ->get();
        $order7  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->whereRaw('DATEDIFF(Now(),order.created_at) <= 7')
            ->get();
        $orderperlu  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('status_order_id', '=', 2)
            ->orwhere('status_order_id', '=', 3)
            ->get();
        $orderblm  = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*',  'status_order.name', 'users.name as nama_pemesan')
            ->where('status_order_id', '=', 1)
            ->get();

        $bahan =  DB::table('bahans')
            ->select('bahans.id', 'bahans.nama_bahan', 'bahans.stok', 'bahans.satuan', DB::raw('max(beli_bahans.tanggal_beli) as tanggal_beli'), DB::raw('max(kurang_bahans.tanggal_kurang) as tanggal_kurang'))
            ->leftJoin('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->leftJoin('kurang_bahans', 'bahans.id', '=', 'kurang_bahans.id_bahan')
            // ->max('beli_bahans.tanggal_beli',$maxbeli)
            // ->max('kurang_bahans.tanggal_kurang',$maxkrg)
            ->groupBy('bahans.nama_bahan')
            ->where('bahans.stok', '<', 10)
            ->get();
        $bahmin =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->where('bahans.stok', '<', 10)
            ->first();
        $bahantot =  DB::table('bahans')
            ->select(DB::raw('count(id) as id'))
            ->first();

        $products = DB::table('products')
            ->leftJoin('categories as a', 'a.id', '=', 'products.categories_id')
            ->select('products.*', 'a.name as nama_kategori')
            ->where('products.stok', '<', 10)
            ->get();
        $prodmin = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->where('products.stok', '<', 10)
            ->first();
        $produktot = DB::table('products')
            ->select(DB::raw('count(id) as id'))
            ->first();
        $tipstot = DB::table('tips')
            ->select(DB::raw('count(id) as id'))
            ->first();


        $prod = DB::table('products')
            ->Join('categories as a', 'a.id', '=', 'products.categories_id')
            ->Join('detail_order as b', 'b.product_id', '=', 'products.id')
            ->select('products.*', 'a.name as nama_kategori', DB::raw('sum(b.qty) as jmlterjual'), DB::raw('sum(products.price * qty) as pendapatan'))
            ->groupBy('products.id')
            ->orderBy(db::raw('sum(b.qty)'))
            ->get();




        $transmasuktoday = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()'))
            ->first();
        $transcek = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 2)
            ->first();
        $transkirim = DB::table('order')
            ->select(DB::raw('count(id) as id'))
            ->where('status_order_id', '=', 3)
            ->first();
        $stokmasuktoday = DB::table('beli_bahans')
            ->select(DB::raw('count(id) as id'))
            ->where(DB::raw('DATE(tanggal_beli)'), '=', DB::raw('CURDATE()'))
            ->first();

        $beli7 = DB::table('bahans')
            ->join('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->whereRaw('DATEDIFF(Now(),beli_bahans.tanggal_beli) <= 7')
            ->orderBy('beli_bahans.tanggal_beli', 'DESC')
            ->get();


        $data = array(
            'pendapatan' => $pendapatan,
            'pendapatanhariini' => $pendapatantoday,
            'pendapatan7' => $pendapatan7days,
            'pengeluaranhariini' => $pengeluarantoday,
            'pengeluaran7' => $pengeluaran7days,
            'transaksi'  => $transaksi,
            'pelanggan'  => $pelanggan,
            
            'pelanggantoday'  => $pelanggantoday,
            'peldaftoday'  => $peldaftoday,
            'order_baru' => $order_terbaru,
            'orderperlu' => $orderperlu,
            'orderblm' => $orderblm,
            'order7' => $order7,
            'stok_minim' => $bahan,
            'pro_stok' => $products,
            'pro_laris' => $prod,
            'trxmasuk' => $transmasuktoday,
            'stokmasuk' => $stokmasuktoday,
            'promin' => $prodmin,
            'bahmin' => $bahmin,
            'bahantot' => $bahantot,
            'produktot' => $produktot,
            'tipstot' => $tipstot,
            'transcek' => $transcek,
            'transkirim' => $transkirim,
            'beli7' => $beli7,
        );

        return view('admin.dash-trx', $data);
    }

    public function about(){
        return view('user.aboutus');
    }
    
    public function tips(){


        

        $data = array(
            'tips' => DB::table('tips')
            ->join('users','tips.id_user','=','users.id')
            ->select('*', 'tips.id as id','users.name', DB::raw('date(tips.tanggal_upload) as tgl'))
            ->orderBy('tanggal_upload', 'DESC')
            ->paginate(3)
        );
        return view('user.tips',$data);
    }
    public function tip($id){


        

        $data = array(
            'tip' => DB::table('tips')
            ->join('users','tips.id_user','=','users.id')
            ->select('*','users.name', DB::raw('date(tips.tanggal_upload) as tgl'))
            ->where('tips.id',$id)
            ->first()
        );
        return view('user.tipsdetail',$data);
    }
}

