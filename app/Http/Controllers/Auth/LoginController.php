<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected function redirectTo()
    {
        if (auth()->user()->role == 'admin') {
            return '/admin';
        } else if (auth()->user()->role == 'owner') {
            return '/admin_owner';
        } else if (auth()->user()->role == 'admincus') {
            return '/admin_admincust';
        } else if (auth()->user()->role == 'adminkat') {
            return '/admin_adminkat';
        } else if (auth()->user()->role == 'admintrx') {
            return '/admin_admintrx';
        } else if (auth()->user()->role == 'adminstok') {
            return '/admin_adminstok';
        } else {
            return '/';
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
