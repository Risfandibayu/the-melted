<?php

namespace App\Http\Controllers\admin;

use App\bahan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Alert;
use App\beli_bahan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use PDF;

class BahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $bahan = \App\bahan::all();
        // return view('admin.bahan.index', ['bahan' => $bahan]);

        // $maxbeli = beli_bahan::max('beli_bahans.tanggal_beli');
        // $maxkrg = \App\kurang_bahan::max('kurang_bahans.tanggal_kurang');

        $bahan =  DB::table('bahans')
        ->select('bahans.*','bahans.id','bahans.nama_bahan','bahans.stok','bahans.satuan',DB::raw('max(beli_bahans.tanggal_beli) as tanggal_beli'),DB::raw('max(kurang_bahans.tanggal_kurang) as tanggal_kurang'))
        ->leftJoin('beli_bahans','bahans.id','=','beli_bahans.id_bahan')
        ->leftJoin('kurang_bahans','bahans.id','=','kurang_bahans.id_bahan')
        // ->max('beli_bahans.tanggal_beli',$maxbeli)
        // ->max('kurang_bahans.tanggal_kurang',$maxkrg)
        ->groupBy('bahans.nama_bahan')
        ->get();

        // DB::raw('max(beli_bahans.tanggal_beli) as tanggal_beli')
        // DB::raw('max(kurang_bahans.tanggal_kurang) as tanggal_kurang')
        
        // return view('admin.beli_bahan.index', ['beli_bahan' => $beli_bahan, 'bhn' => $bhn]);

        //tanggal
        // $beli_bahan =  DB::table('beli_bahans')
        // ->select('*', DB::raw('sum(beli_bahans.total) as total'))
        // ->join('bahans','beli_bahans.id_bahan','=','bahans.id')
        // ->groupBy('beli_bahans.tanggal_beli')
        // ->get();
        $bahans = \App\bahan::orderBy('id', 'ASC')->get();
        $blbhn = \App\beli_bahan::with('bahans');
        $krgbhn = \App\kurang_bahan::with('bahans');

        return view('admin.bahan.index', ['bahan' => $bahan,'bahans' => $bahans, 'blbhn' => $blbhn ,'krgbhn' => $krgbhn]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bahan.add-data');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function validator(array $data){
        return Validator::make($data,[
            'nama_bahan' => ['required'],
            'satuan' => ['required'],
            'stok_minim' => ['required'],
            
        ]);
    }
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();
        bahan::insert([
            'nama_bahan' => $request->get('nama_bahan'),
            'satuan' => $request->get('satuan'),
            'stok' => $request->get('stok'),
            'stok_minim' => $request->get('stok_minim'),
            
        ]);
        $notif = array(
            'message' => 'Data Berhasil Di Tambahkan',
            'alert-type' => 'success'
        );
        
        return redirect('/admin/bahan')->with($notif);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\bahan  $bahan
     * @return \Illuminate\Http\Response
     */
    public function show(bahan $bahan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\bahan  $bahan
     * @return \Illuminate\Http\Response
     */
    public function edit(bahan $bahan,$id)
    {
        $bahan = \App\bahan::where('id', $id)->first();
        return view('admin.bahan.edit-data', compact('bahan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\bahan  $bahan
     * @return \Illuminate\Http\Response
     */
    public function valid(array $data,$id){
        return Validator::make($data,[
            'nama_bahan' => ['required'],
            'satuan' => ['required'],
            'stok_minim' => ['required'],
            
        ]);
    }
    public function update(Request $request, bahan $bahan,$id)
    {
        //
        $this->valid($request->all(), $id)->validate();
        
        bahan::findOrFail($id)->update([
            'nama_bahan' => $request->get('nama_bahan'),
            'satuan' => $request->get('satuan'),
            'stok' => $request->get('stok'),
            'stok_minim' => $request->get('stok_minim'),
        ]);
        echo "success";
        $notif = array(
            'message' => 'Data Berhasil di Ubah',
            'alert-type' => 'success'
        );
        // return redirect('kategori')->with('sukses', 'Data Berhasil Diubah');
        return redirect('/admin/bahan')->with($notif);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\bahan  $bahan
     * @return \Illuminate\Http\Response
     */
    public function destroy(bahan $bahan)
    {
        //
    }
    public function delete($id)
    {
        DB::table('bahans')->where('id',$id)->delete();
        // DB::table('beli_bahans')->where('id_bahan',$id)->delete();
        return redirect()->back();
    }

    public function cetak()
    {
        // $bahan = \App\bahan::all();
        // return view('admin.bahan.index', ['bahan' => $bahan]);

        // $maxbeli = beli_bahan::max('beli_bahans.tanggal_beli');
        // $maxkrg = \App\kurang_bahan::max('kurang_bahans.tanggal_kurang');

        $bahan =  DB::table('bahans')
        ->select('bahans.id','bahans.nama_bahan','bahans.stok','bahans.satuan',DB::raw('max(beli_bahans.tanggal_beli) as tanggal_beli'),DB::raw('max(kurang_bahans.tanggal_kurang) as tanggal_kurang'))
        ->leftJoin('beli_bahans','bahans.id','=','beli_bahans.id_bahan')
        ->leftJoin('kurang_bahans','bahans.id','=','kurang_bahans.id_bahan')
        // ->max('beli_bahans.tanggal_beli',$maxbeli)
        // ->max('kurang_bahans.tanggal_kurang',$maxkrg)
        ->groupBy('bahans.nama_bahan')
        ->get();

        // DB::raw('max(beli_bahans.tanggal_beli) as tanggal_beli')
        // DB::raw('max(kurang_bahans.tanggal_kurang) as tanggal_kurang')
        
        // return view('admin.beli_bahan.index', ['beli_bahan' => $beli_bahan, 'bhn' => $bhn]);

        //tanggal
        // $beli_bahan =  DB::table('beli_bahans')
        // ->select('*', DB::raw('sum(beli_bahans.total) as total'))
        // ->join('bahans','beli_bahans.id_bahan','=','bahans.id')
        // ->groupBy('beli_bahans.tanggal_beli')
        // ->get();

        $blbhn = \App\beli_bahan::with('bahans');
        $krgbhn = \App\kurang_bahan::with('bahans');

        // return view('admin.bahan.index', ['bahan' => $bahan, 'blbhn' => $blbhn ,'krgbhn' => $krgbhn]);
        $pdf = PDF::loadview('admin.bahan.cetak', ['bahan' => $bahan, 'blbhn' => $blbhn ,'krgbhn' => $krgbhn]);
        // $pdf = PDF::loadview('admin.product.cetak',['products'=>$products]);
    	return $pdf->stream();
    }
}
