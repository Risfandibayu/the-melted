<?php

namespace App\Http\Controllers\admin;

use App\kurang_bahan;
use App\cart_kurang;
use App\Http\Controllers\Controller;
use App\bahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Alert;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;


class KurangBahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kurang_bahan =  DB::table('bahans')

            ->join('kurang_bahans', 'bahans.id', '=', 'kurang_bahans.id_bahan')
            ->groupBy('kurang_bahans.id_bahan','kurang_bahans.created_at')
            ->orderBy('kurang_bahans.created_at', 'DESC')
            ->get();

        // $beli_bahan = \App\beli_bahan::all();
        // return view('admin.beli_bahan.index', ['beli_bahan' => $beli_bahan]);
        $bhn = \App\kurang_bahan::with('bahans');
        return view('admin.bahan_kurang.index', ['kurang_bahan' => $kurang_bahan, 'bhn' => $bhn]);
    }
    public function indextgl(){
        $kurang_bahan =  DB::table('kurang_bahans')
        ->join('bahans','kurang_bahans.id_bahan','=','bahans.id')
        ->select('*',DB::raw("SUBSTRING_INDEX(GROUP_CONCAT(distinct(bahans.nama_bahan)),',',5) as namab"))
        ->groupBy('kurang_bahans.tanggal_kurang')
        ->orderBy('kurang_bahans.tanggal_kurang','DESC')
        ->get();
        $bhn = \App\kurang_bahan::with('bahans');
        return view('admin.laporan.stok-keluar', ['kurang_bahan' => $kurang_bahan, 'bhn' => $bhn]);
    }

    public function indextglid($tgl){
        $kurang_bahan =  DB::table('kurang_bahans')
        ->select('*',DB::raw('sum(kurang_bahans.jumlah) as jumlah'))
        ->join('bahans','kurang_bahans.id_bahan','=','bahans.id')
        ->where('kurang_bahans.tanggal_kurang',$tgl)
        ->groupBy('kurang_bahans.id_bahan')
        ->get();
        $bhn = \App\kurang_bahan::with('bahans');

        return view('admin.laporan.stok-keluar-detail', ['kurang_bahan' => $kurang_bahan, 'bhn' => $bhn]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $user = \Auth::user()->id;
        $bahan = \App\bahan::orderBy('nama_bahan', 'ASC')->get();
        $cartkrg = DB::table('cart_kurang')
            ->join('bahans', 'bahans.id', '=', 'cart_kurang.id_bahan')
            ->select('bahans.nama_bahan as nama','bahans.satuan as satuan', 'cart_kurang.*')
            ->where('id_user',$user)
            ->orderBy('cart_kurang.created_at', 'ASC')
            ->get();
        return view('admin.bahan_kurang.add-data', compact('bahan', 'cartkrg'));
    }


    public function createid(bahan $bahan, $id_bahan)
    {
        // $bel = \App\beli_bahan::where('id_bahan', $id_bahan)->first();
        // $bahan = \App\bahan::where('id', $id)->first();
        // $bahan = \App\bahan::orderBy('id', 'ASC')->first();
        $bahan = \App\bahan::where('id', $id_bahan)->first();
        // $beli_bahan = \App\beli_bahan::orderBy('id', 'ASC')->get();
        // return view('admin.beli_bahan.add-data-id', compact('bahan','beli_bahan'));
        // $beli_bahan = \App\beli_bahan::where('id', $id)->first();
        $bahans = \App\bahan::orderBy('id', 'ASC')->get();
        return view('admin.bahan_kurang.add-data-id', compact('bahan', 'bahans'));
    }
    public function delcartkrg($id){
        DB::table('cart_kurang')->where('id', $id)->delete();
        return redirect()->back();
    }
    public function delkrg(){
        $user = \Auth::user()->id;
        DB::table('cart_kurang')->where('id_user', $user)->delete();
        return redirect('/admin/bahan');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'tanggal_kurang' => ['required'],


        ]);
    }
    public function validatorr(array $data)
    {
        return Validator::make($data, [
            'id_bahan' => ['required'],
            'jumlah' => ['required'],


        ]);
    }
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();
        $usr = \Auth::user()->id;
        $bahan = DB::table('cart_kurang')->where('id_user', $usr)->get();
        // $stokbahan = \App\bahan::findOrFail($bahan->id_bahan);
        $current_date_time = Carbon::now()->toDateTimeString();
            if (count($bahan) == 0 || count($bahan) == null) {
                $notif = array(
                    'message' => 'Anda Belum memasukan List update stok',
                    'alert-type' => 'danger'
                );

                return redirect()->back()->with($notif);
            } else {
                foreach ($bahan as $bahn) {
                    $stokbahan = \App\bahan::findOrFail($bahn->id_bahan);
                    $beli_bahan = kurang_bahan::insert([
                        'tanggal_kurang' => $request->get('tanggal_kurang'),
                        'id_bahan' => $bahn->id_bahan,
                        'jumlah' => $stokbahan->stok - $bahn->jumlah,
                        'keterangan' => $request->get('keterangan'),
                        'created_at' => $current_date_time,
                        'updated_at' => $current_date_time,
                    ]);
                    $bhn = \App\bahan::findOrFail($bahn->id_bahan);
                    $bhn->stok = $bahn->jumlah;
                    $bhn->save();
                }
                $usr = \Auth::user()->id;
                DB::table('cart_kurang')->where('id_user', $usr)->delete();
                $notif = array(
                    'message' => 'update Stok Berhasil',
                    'alert-type' => 'success'
                );

                return redirect('/admin/bahan')->with($notif);
            }
    }
    public function cartkrg(Request $request){
        $this->validatorr($request->all())->validate();
        $stokbahan = \App\bahan::findOrFail($request->id_bahan);
        $current_date_time = Carbon::now()->toDateTimeString();
        $crt = DB::table('cart_kurang')
            ->select('*')
            ->where('id_bahan',$request->get('id_bahan'))
            ->first();
        if ($stokbahan->stok < $request->get('jumlah')) {
            $notif = array(
                'message' => 'Stok yang anda masukan melebihi data stok sebelumnya',
                'alert-type' => 'danger'
            );

            return redirect()->back()->with($notif);
        }elseif( $crt ){
            $notif = array(
                'message' => 'Bahan yang anda pilih sudah masuk ke dalam list',
                'alert-type' => 'danger'
            );

            return redirect()->back()->with($notif);
        }
        
        else {
            $cart_krg = cart_kurang::insert([
                'id_bahan' => $request->get('id_bahan'),
                'jumlah' => $request->get('jumlah'),
                'id_user' => \Auth::user()->id,
                'created_at' => $current_date_time,
                'updated_at' => $current_date_time,

            ]);
            return redirect()->back();
        }
    }


    public function validid(array $data, $id_bahan)
    {
        return Validator::make($data, [
            'tanggal_kurang' => ['required'],
            'id_bahan' => ['required'],
            'jumlah' => ['required'],
            

        ]);
    }

    public function storeid(Request $request, $id_bahan)
    {
        // $id_bahan = \App\bahan::where('id_bahan',$request->id)->first();
        $this->validid($request->all(), $id_bahan)->validate();
        $id = bahan::findOrfail($id_bahan);
        $current_date_time = Carbon::now()->toDateTimeString();
        $stokbahan = \App\bahan::findOrFail($request->id_bahan);

        if ($stokbahan->stok < $request->get('jumlah')) {
            $notif = array(
                'message' => 'Data yang anda masukan salah!',
                'alert-type' => 'danger'
            );

            return redirect()->back()->with($notif);
        } else {
            $kurang_bahan = kurang_bahan::insert([
                'tanggal_kurang' => $request->get('tanggal_kurang'),
                'id_bahan' => $id_bahan,
                'jumlah' => ($stokbahan->stok - $request->get('jumlah')),
                'keterangan' => $request->get('keterangan'),
                'created_at' => $current_date_time,
                'updated_at' => $current_date_time,
            ]);

            // beli_bahan::findOrFail($id)->insert([
            //     'tanggal_beli' => $request->get('tanggal_beli'),
            //     'id_bahan' => $request->get('id_bahan'),
            //     'qty' => $request->get('qty'),
            //     'total' => $request->get('total'),
            //     'keterangan' => $request->get('keterangan'),
            // ]);

            
            $bhn = \App\bahan::findOrFail($request->id_bahan);
            $bhn->stok = $request->jumlah;
            $bhn->save();

            $notif = array(
                'message' => 'Update Stok '.$bhn->nama_bahan.' Berhasil',
                'alert-type' => 'success'
            );

            return redirect('/admin/bahan')->with($notif);
        }
    }





    /**
     * Display the specified resource.
     *
     * @param  \App\kurang_bahan  $kurang_bahan
     * @return \Illuminate\Http\Response
     */
    public function show(kurang_bahan $kurang_bahan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\kurang_bahan  $kurang_bahan
     * @return \Illuminate\Http\Response
     */
    public function edit(kurang_bahan $kurang_bahan, $id)
    {
        $kurang_bahan = \App\kurang_bahan::where('id', $id)->first();
        $bahan = \App\bahan::orderBy('id', 'ASC')->get();

        return view('admin.bahan_kurang.edit-data', compact('bahan', 'kurang_bahan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\kurang_bahan  $kurang_bahan
     * @return \Illuminate\Http\Response
     */
    public function valid(array $data, $id)
    {
        return Validator::make($data, [
            'tanggal_kurang' => ['required'],
            'id_bahan' => ['required'],
            'jumlah' => ['required'],
        ]);
    }
    public function update(Request $request, kurang_bahan $kurang_bahan, $id)
    {
        $this->valid($request->all(), $id)->validate();
        $stokbahan = \App\bahan::findOrFail($request->id_bahan);
        $jmllama = \App\kurang_bahan::findOrFail($id);
        kurang_bahan::findOrFail($id)->update([
            'tanggal_kurang' => $request->get('tanggal_kurang'),
            'id_bahan' => $request->get('id_bahan'),
            'jumlah' => (($stokbahan->stok += $jmllama->jumlah) - $request->get('jumlah')),
            'keterangan' => $request->get('keterangan'),
        ]);

        $bhn = \App\bahan::findOrFail($request->id_bahan);
        $bhn->stok = $request->jumlah;
        $bhn->save();

        $notif = array(
            'message' => 'Data Berhasil di ubah',
            'alert-type' => 'success'
        );
        // return redirect('kategori')->with('sukses', 'Data Berhasil Diubah');
        return redirect('admin/kurang_bahan')->with($notif);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\kurang_bahan  $kurang_bahan
     * @return \Illuminate\Http\Response
     */
    public function destroy(kurang_bahan $kurang_bahan)
    {
        //
    }
    public function delete($id)
    {
        $kr = \App\kurang_bahan::findOrFail($id);

        $bhn = \App\bahan::findOrFail($kr->id_bahan);
        $bhn->stok = $bhn->stok +  $kr->jumlah;
        $bhn->save();

        DB::table('kurang_bahans')->where('id', $id)->delete();
        return redirect()->back();
    }
}
