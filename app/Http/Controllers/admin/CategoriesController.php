<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Categories;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    public function index()
    {

        //Ambil data kategori dari database
        $data = array(
            'categories' => Categories::all()
        );
        //menampilkan view
        return view('admin.categories.index', $data);
    }

    //function menampilkan view tambah data
    public function tambah()
    {
        return view('admin.categories.tambah');
    }

    public function store(Request $request)
    {
        //Simpan datab ke database    
        Categories::create([
            'name' => $request->name
        ]);

        //lalu reireact ke route admin.categories dengan mengirim flashdata(session) berhasil tambah data untuk manampilkan alert succes tambah data
        $notif = array(
            'message' => 'Data Berhasil Di Tambahkan',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.categories')->with($notif);
    }

    public function update($id, Request $request)
    {
        //ambil data sesuai id dari parameter
        $categorie = Categories::FindOrFail($id);
        //lalu ambil apa aja yang mau diupdate
        $categorie->name = $request->name;

        //lalu simpan perubahan
        $categorie->save();
        $notif = array(
            'message' => 'Data Berhasil Di Ubah',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.categories')->with($notif);
    }

    //function menampilkan form edit
    public function edit($id)
    {
        $data = array(
            'categorie' => $categorie = Categories::FindOrFail($id)
        );
        $notif = array(
            'message' => 'Data Berhasil Di Ubah',
            'alert-type' => 'success'
        );
        return view('admin.categories.edit', $data)->with($notif);
    }

    public function delete($id)
    {
        //hapus data sesuai id dari parameter
        $ctg = (DB::table('products')->where('categories_id', $id)->get());
        if (count($ctg) == 0) {
            Categories::destroy($id);

            $notif = array(
                'message' => 'Data Berhasil Di Hapus',
                'alert-type' => 'success'
            );
        } else {

            

            $notif = array(
                'message' => 'Kategori tidak berhasil di hapus karena terdapat produk dengan kategori terkait',
                'alert-type' => 'danger'
            );
        }

        return redirect()->route('admin.categories')->with($notif);
    }
}
