<?php

namespace App\Http\Controllers\admin;

use App\tips;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class TipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tips = \App\tips::all();
        return view('admin.tips.index', ['tips' => $tips]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tips.add-data');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function validator(array $data){
        return Validator::make($data,[
            'judul' => ['required'],
            'deskripsi' => ['required'],
            'gambar' => ['mimes:jpg,png,jpeg,gif'],
        ]);
    }
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();
        if ($request->file('foto')) {
            $foto = $request->file('foto')->store('tipsgambar', 'public');
        }else{
            $foto=null;
        }
        tips::insert([
            // 'tanggal_upload' => $request->get('tanggal_upload'),
            'judul' => $request->get('judul'),
            'deskripsi' => $request->get('deskripsi'),
            'id_user' => $request->get('id_user'),
            'foto' => $foto,
        ]);
        $notif = array(
            'message' => 'Data Berhasil Di Tambahkan',
            'alert-type' => 'success'
        );
        
        return redirect('/admin/tips')->with($notif);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tips  $tips
     * @return \Illuminate\Http\Response
     */
    public function show(tips $tips)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tips  $tips
     * @return \Illuminate\Http\Response
     */
    public function edit(tips $tips,$id)
    {
        //
        $tips = \App\tips::where('id', $id)->first();
        return view('admin.tips.edit-data', compact('tips'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tips  $tips
     * @return \Illuminate\Http\Response
     */
    public function valid(array $data,$id){
        return Validator::make($data,[
            'judul' => ['required'],
            'deskripsi' => ['required'],
            'gambar' => ['mimes:jpg,png,jpeg,gif'],
        ]);
    }
    public function update(Request $request, tips $tips,$id)
    {
        $this->valid($request->all(), $id)->validate();
        if ($request->file('foto')) {
            $up = $request->file('foto')->store('tipsgambar','public');
            $foto = tips::findOrFail($id);
            if ($foto->foto) {
                Storage::delete('public/'.$foto->foto);
                $foto->foto = $up;
            }else{
                $foto->foto = $up;
            }
            $foto->save();
        }
        tips::findOrFail($id)->update([
            'judul' => $request->get('judul'),
            'deskripsi' => $request->get('deskripsi'),
        ]);
        echo "success";
        $notif = array(
            'message' => 'Data Berhasil di ubah',
            'alert-type' => 'success'
        );
        // return redirect('kategori')->with('sukses', 'Data Berhasil Diubah');
        return redirect('/admin/tips')->with($notif);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tips  $tips
     * @return \Illuminate\Http\Response
     */
    public function destroy(tips $tips)
    {
        //
    }
    public function delete($id){
        DB::table('tips')->where('id',$id)->delete();
        return redirect()->back();
    }
}
