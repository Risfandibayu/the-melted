<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
class PelangganController extends Controller
{
    public function index()
    {
        //ambil data pelanggan yang di join dengan table alamat, city,dan province
        $data = array(
            'pelanggan' => DB::table('users')
                        ->leftjoin('alamat','alamat.user_id','=','users.id')
                        ->leftjoin('cities','cities.city_id','=','alamat.cities_id')
                        ->leftjoin('provinces','provinces.province_id','=','cities.province_id')
                        ->select('users.*', DB::raw('(Year(Curdate())- Year(users.tgl_lahir)) as usia'),'alamat.detail','cities.title as kota','provinces.title as prov')
                        ->where('users.role','=','customer')
                        ->get(),
            'jml' => DB::table('users')
                    ->select(DB::raw('COUNT(id) as id'))
                    ->where('role','=','customer')
                    ->first()
                    );
        

        return view('admin.pelanggan.index',$data);
    }

    public function cetak(){
        
            $pelanggan = DB::table('users')
                        ->select('users.*', DB::raw('(Year(Curdate())- Year(users.tgl_lahir)) as usia'))
                        ->where('users.role','=','customer')
                        ->get();
        $pdf = PDF::loadview('admin.pelanggan.cetak', ['pelanggan' => $pelanggan]);
        // $pdf = PDF::loadview('admin.product.cetak',['products'=>$products]);
    	return $pdf->stream();
    }

    public function grafik(){
        
        $chartl1216 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','laki-laki')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['12','16'])
        ->first();
        $chartl1721 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','laki-laki')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['17','21'])
        ->first();
        $chartl2226 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','laki-laki')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['22','26'])
        ->first();
        $chartl2731 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','laki-laki')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['27','31'])
        ->first();
        $chartl3236 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','laki-laki')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['32','36'])
        ->first();
        $chartl3741 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','laki-laki')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['37','41'])
        ->first();
        $chartl4245 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','laki-laki')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['41','45'])
        ->first();


        $chartp1216 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','perempuan')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['12','16'])
        ->first();
        $chartp1721 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','perempuan')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['17','21'])
        ->first();
        $chartp2226 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','perempuan')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['22','26'])
        ->first();
        $chartp2731 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','perempuan')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['27','31'])
        ->first();
        $chartp3236 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','perempuan')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['32','36'])
        ->first();
        $chartp3741 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','perempuan')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['37','41'])
        ->first();
        $chartp4245 = DB::table('users')
        ->select(DB::raw('count(id) as tot'))
        ->where('role','customer')
        ->where('jk','perempuan')
        ->whereBetween(DB::raw('((Year(CURDATE())) - (year(tgl_lahir)))'),['41','45'])
        ->first();
        
        $data = array(
            'chartl1216' => $chartl1216,
            'chartl1721' => $chartl1721,
            'chartl2226' => $chartl2226,
            'chartl2731' => $chartl2731,
            'chartl3236' => $chartl3236,
            'chartl3741' => $chartl3741,
            'chartl4245' => $chartl4245,
            'chartp1216' => $chartp1216,
            'chartp1721' => $chartp1721,
            'chartp2226' => $chartp2226,
            'chartp2731' => $chartp2731,
            'chartp3236' => $chartp3236,
            'chartp3741' => $chartp3741,
            'chartp4245' => $chartp4245,
        );

        return view('admin.laporan.grafik', $data);
    }
}
