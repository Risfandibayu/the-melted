<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;

class TransaksiController extends Controller
{
    public function index()
    {
        //ambil data order yang status nya 1 atau masih baru/belum melalukan pembayaran
        $order = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('order.status_order_id', 1)
            ->orwhere('order.status_order_id', 8)
            ->get();
        $data = array(
            'orderbaru' => $order
        );

        return view('admin.transaksi.index', $data);
    }

    public function detail($id)
    {
        //ambil data detail order sesuai id
        $detail_order = DB::table('detail_order')
            ->join('products', 'products.id', '=', 'detail_order.product_id')
            ->join('order', 'order.id', '=', 'detail_order.order_id')
            ->select('products.name as nama_produk', 'products.image', 'detail_order.*', 'products.price', 'order.*')
            ->where('detail_order.order_id', $id)
            ->get();
        $order = DB::table('order')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->join('alamat', 'alamat.user_id', '=', 'users.id')
            ->join('cities', 'cities.city_id', '=', 'alamat.cities_id')
            ->join('provinces', 'provinces.province_id', '=', 'cities.province_id')
            ->join('subdistricts','subdistricts.subdistrict_id','=','alamat.subdistrict_id')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->select('order.*', 'alamat.detail as detalamat', 'subdistricts.subdistrict_name as kec', 'cities.title as namakota', 'provinces.title as namaprov', 'users.name as nama_pelanggan', 'status_order.name as status')
            ->where('order.id', $id)
            ->first();
        $data = array(
            'detail' => $detail_order,
            'order'  => $order
        );
        return view('admin.transaksi.detail', $data);
    }
    public function lapdetail($id)
    {
        //ambil data detail order sesuai id
        $detail_order = DB::table('detail_order')
            ->join('products', 'products.id', '=', 'detail_order.product_id')
            ->join('order', 'order.id', '=', 'detail_order.order_id')
            ->select('products.name as nama_produk', 'products.image', 'detail_order.*', 'products.price', 'order.*')
            ->where('detail_order.order_id', $id)
            ->get();
        $order = DB::table('order')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->join('alamat', 'alamat.user_id', '=', 'users.id')
            ->join('cities', 'cities.city_id', '=', 'alamat.cities_id')
            ->join('provinces', 'provinces.province_id', '=', 'cities.province_id')
            ->join('subdistricts','subdistricts.subdistrict_id','=','alamat.subdistrict_id')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->select('order.*', 'alamat.detail as detalamat', 'cities.title as namakota', 'subdistricts.subdistrict_name as kec', 'provinces.title as namaprov', 'users.name as nama_pelanggan', 'status_order.name as status')
            ->where('order.id', $id)
            ->first();
        $data = array(
            'detail' => $detail_order,
            'order'  => $order
        );
        return view('admin.laporan.transaksi-detail', $data);
    }

    public function perludicek()
    {
        //ambil data order yang status nya 2 atau belum di cek / sudah bayar
        $order = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('order.status_order_id', 2)
            ->get();
        $data = array(
            'orderbaru' => $order
        );

        return view('admin.transaksi.perludicek', $data);
    }

    public function perludikirim()
    {
        //ambil data order yang status nya 3 sudah dicek dan perlu dikirim(input no resi)
        $order = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('order.status_order_id', 3)
            ->get();
        $data = array(
            'orderbaru' => $order
        );

        return view('admin.transaksi.perludikirim', $data);
    }

    public function selesai()
    {
        //ambil data order yang status nya 5 barang sudah diterima pelangan
        $order = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('order.status_order_id', 5)
            ->get();
        $data = array(
            'orderbaru' => $order
        );

        return view('admin.transaksi.selesai', $data);
    }
    public function gagal()
    {
        //ambil data order yang status nya 5 barang sudah diterima pelangan
        $order = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('order.status_order_id', 7)
            ->get();
        $data = array(
            'orderbaru' => $order
        );

        return view('admin.transaksi.gagal', $data);
    }


    public function uang()
    {
        //ambil data order yang status nya 5 barang sudah diterima pelangan
        // $order = DB::table('order')
        //     ->select('*',DB::raw("date('created_at') as tanggalorder"), DB::raw("(sum(subtotal)+sum(biaya_cod)) as totalorder"))
        //     ->where('status_order_id', 5)
        //     ->whereNull('beli_bahans.id')
        //     ->groupBy(DB::raw("date('created_at')"));
        // $bl = DB::table('beli_bahans')
        //     ->select('*',DB::raw("date('tanggal_beli') as tanggalbeli"), db::raw("sum('total') as totalbeli"))
        //     ->groupBy(DB::raw("date('tanggal_beli')"))
        //     ->unionAll($order);
        // $order = DB::select("SELECT (tanggal.dt) as tanggal , (beli_bahans.tanggal_beli), date(`order`.created_at) as tanggalorder ,
 
        // (SELECT sum(beli_bahans.total) from beli_bahans
        // WHERE beli_bahans.tanggal_beli=tanggal.dt) as totalbeli ,
        
        // (SELECT (sum(`order`.subtotal) + sum(`order`.biaya_cod)) from `order`
        // WHERE date(`order`.created_at)=tanggal.dt
        // ) as totalorder 
        
        // from tanggal
        // left JOIN beli_bahans on beli_bahans.tanggal_beli = tanggal.dt
        // left JOIN `order` on date(`order`.created_at) = tanggal.dt
        // WHERE beli_bahans.tanggal_beli is Not NULL or `order`.created_at is not null or `order`.status_order_id = 5 or `order`.status_order_id = 4
        // GROUP BY tanggal.dt
        // order by tanggal.dt desc
        // ");
        // $order = DB::select("SELECT (tanggal.dt) as tanggal , (beli_bahans.tanggal_beli), date(`order`.created_at) as tanggalorder ,
 
        // (SELECT sum(beli_bahans.total) from beli_bahans
        // WHERE beli_bahans.tanggal_beli=tanggal.dt) as totalbeli ,
        
        // (SELECT (sum(`order`.subtotal) + sum(`order`.biaya_cod)) from `order`
        // WHERE date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 5 or date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 4
        // ) as totalorder 
        
        // from tanggal
        // left JOIN beli_bahans on beli_bahans.tanggal_beli = tanggal.dt
        // left JOIN `order` on date(`order`.created_at) = tanggal.dt
        // WHERE beli_bahans.tanggal_beli is Not NULL or `order`.created_at is not null  AND `order`.status_order_id = 5 OR `order`.status_order_id = 4
        // GROUP BY tanggal.dt
        // order by tanggal.dt desc
        // ");
        $keuangan = DB::select("SELECT (tanggal.dt) as tanggal , (beli_bahans.tanggal_beli), date(`order`.created_at) as tanggalorder ,
        (SELECT sum(beli_bahans.total) from beli_bahans
        WHERE beli_bahans.tanggal_beli=tanggal.dt) as totalbeli ,
        (SELECT (sum(`order`.subtotal) + sum(`order`.biaya_cod)) from `order`
        WHERE (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 5) or (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 4)
        ) as totalorder 
        from tanggal
        left JOIN beli_bahans on beli_bahans.tanggal_beli = tanggal.dt
        left JOIN `order` on date(`order`.created_at) = tanggal.dt
        WHERE beli_bahans.tanggal_beli is Not NULL or (`order`.created_at is not null  AND `order`.status_order_id = 5) OR (`order`.created_at is not null  AND `order`.status_order_id = 4)
        GROUP BY tanggal.dt
        order by tanggal.dt desc
        ");
            
        $data = array(
            'uang' => $keuangan
        );

        return view('admin.laporan.keuangan', $data);
    }

    public function uang2(Request $request)
    {
        //ambil data order yang status nya 5 barang sudah diterima pelangan
        // $order = DB::table('order')
        //     ->select('*',DB::raw("date('created_at') as tanggalorder"), DB::raw("(sum(subtotal)+sum(biaya_cod)) as totalorder"))
        //     ->where('status_order_id', 5)
        //     ->whereNull('beli_bahans.id')
        //     ->groupBy(DB::raw("date('created_at')"));
        // $bl = DB::table('beli_bahans')
        //     ->select('*',DB::raw("date('tanggal_beli') as tanggalbeli"), db::raw("sum('total') as totalbeli"))
        //     ->groupBy(DB::raw("date('tanggal_beli')"))
        //     ->unionAll($order);
        // $order = DB::select("SELECT (tanggal.dt) as tanggal , (beli_bahans.tanggal_beli), date(`order`.created_at) as tanggalorder ,
 
        // (SELECT sum(beli_bahans.total) from beli_bahans
        // WHERE beli_bahans.tanggal_beli=tanggal.dt) as totalbeli ,
        
        // (SELECT (sum(`order`.subtotal) + sum(`order`.biaya_cod)) from `order`
        // WHERE date(`order`.created_at)=tanggal.dt
        // ) as totalorder 
        
        // from tanggal
        // left JOIN beli_bahans on beli_bahans.tanggal_beli = tanggal.dt
        // left JOIN `order` on date(`order`.created_at) = tanggal.dt
        // WHERE beli_bahans.tanggal_beli is Not NULL or `order`.created_at is not null or `order`.status_order_id = 5 or `order`.status_order_id = 4
        // GROUP BY tanggal.dt
        // order by tanggal.dt desc
        // ");
        // $order = DB::select("SELECT (tanggal.dt) as tanggal , (beli_bahans.tanggal_beli), date(`order`.created_at) as tanggalorder ,
 
        // (SELECT sum(beli_bahans.total) from beli_bahans
        // WHERE beli_bahans.tanggal_beli=tanggal.dt) as totalbeli ,
        
        // (SELECT (sum(`order`.subtotal) + sum(`order`.biaya_cod)) from `order`
        // WHERE date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 5 or date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 4
        // ) as totalorder 
        
        // from tanggal
        // left JOIN beli_bahans on beli_bahans.tanggal_beli = tanggal.dt
        // left JOIN `order` on date(`order`.created_at) = tanggal.dt
        // WHERE beli_bahans.tanggal_beli is Not NULL or `order`.created_at is not null  AND `order`.status_order_id = 5 OR `order`.status_order_id = 4
        // GROUP BY tanggal.dt
        // order by tanggal.dt desc
        // ");
        // $keuangan = DB::select("SELECT (tanggal.dt) as tanggal , (beli_bahans.tanggal_beli), date(`order`.created_at) as tanggalorder ,
        // (SELECT sum(beli_bahans.total) from beli_bahans
        // WHERE beli_bahans.tanggal_beli=tanggal.dt) as totalbeli ,
        // (SELECT (sum(`order`.subtotal) + sum(`order`.biaya_cod)) from `order`
        // WHERE (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 5) or (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 4)
        // ) as totalorder 
        // from tanggal
        // left JOIN beli_bahans on beli_bahans.tanggal_beli = tanggal.dt
        // left JOIN `order` on date(`order`.created_at) = tanggal.dt
        // WHERE beli_bahans.tanggal_beli is Not NULL or (`order`.created_at is not null  AND `order`.status_order_id = 5) OR (`order`.created_at is not null  AND `order`.status_order_id = 4)
        // GROUP BY tanggal.dt
        // order by tanggal.dt desc
        // ");
        // $keuangan = DB::select("SELECT (tanggal.dt) as tanggal , (beli_bahans.tanggal_beli), date(`order`.created_at) as tanggalorder ,
         
        //         (SELECT sum(beli_bahans.total) from beli_bahans
        //         WHERE beli_bahans.tanggal_beli=tanggal.dt) as totalbeli ,
                
        //         (SELECT (sum(`order`.subtotal) + sum(`order`.biaya_cod)) from `order`
        //         WHERE (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 5) or (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 4)
        //         ) as totalorder
                
        //         from tanggal
        //         left JOIN beli_bahans on beli_bahans.tanggal_beli = tanggal.dt
        //         left JOIN `order` on date(`order`.created_at) = tanggal.dt
        //         WHERE ((tanggal.dt BETWEEN $request->from_date and $request->to_date) and beli_bahans.tanggal_beli is Not NULL) or ((tanggal.dt BETWEEN $request->from_date and $request->to_date) and (`order`.created_at is not null  AND `order`.status_order_id = 5)) OR ((tanggal.dt BETWEEN $request->from_date and $request->to_date) and (`order`.created_at is not null  AND `order`.status_order_id = 4))
        //         GROUP BY tanggal.dt
        //         order by tanggal.dt desc   
        // ");

        
        function pecahtgl($timestamp)
        {
            $pecahkan = preg_split('/( |,|-|:)/',$timestamp);
            $hari = array(
                7 => 'Minggu',
                1 => 'Senin',
                2 =>'Selasa',
                3 => 'Rabu',
                4 => 'Kamis',
                5 => 'Jumat',
                6 => 'Sabtu',
            ); $bulan = array(
                1 =>   'Januari',
                2 => 'Februari',
                3 => 'Maret',
                4 => 'April',
                5 => 'Mei',
                6 =>' Juni',
                7 => 'Juli',
                8 => 'Agustus',
                9 => 'September',
                10 => 'Oktober',
                11 => 'November',
                12 => 'Desember'
            );
            return $hari[(int)$pecahkan[0]].','.$pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4]. ' ' . $bulan[(int)$pecahkan[5]]. ' ' . $pecahkan[6]. ' ' . $pecahkan[7]. ' ' . $pecahkan[8]. ' ' . $pecahkan[1];
        }
        







            if(request()->ajax())
            {
            if(!empty($request->from_date))
                {
                    $data = DB::select("SELECT DATE_FORMAT(tanggal.dt,'%d - %m - %Y') as tanggalno, DATE_FORMAT(tanggal.dt,'%d-%m-%Y') as tanggal ,
 
                    
                    Concat('Rp ',fRupiah(IFNULL((SELECT (sum(`order`.subtotal) + sum(`order`.biaya_cod)) from `order`
                    WHERE 
                    (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 5) 
                    or (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 4) or (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 3) 
                    )
                    
                    ,0))) as totalorder,
                    Concat('Rp ',fRupiah(IFNULL((SELECT sum(beli_bahans.total) from beli_bahans
                    WHERE beli_bahans.tanggal_beli=tanggal.dt),0))) as totalbeli ,
                            
                    Concat('Rp ',fRupiah((
                            IFNULL((SELECT (sum(`order`.subtotal) + sum(`order`.biaya_cod)) from `order`
                    WHERE (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 5) or (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 4) or (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 3)
                    ),0) - 
                            (IFNULL((SELECT sum(beli_bahans.total) from beli_bahans
                    WHERE beli_bahans.tanggal_beli=tanggal.dt),0)))))
                            as labarugi
                    from tanggal
                    left JOIN beli_bahans on beli_bahans.tanggal_beli = tanggal.dt
                    left JOIN `order` on date(`order`.created_at) = tanggal.dt
                    WHERE ((tanggal.dt BETWEEN '$request->from_date' and '$request->to_date') and beli_bahans.tanggal_beli is Not NULL) or ((tanggal.dt BETWEEN '$request->from_date' and '$request->to_date') and (`order`.created_at is not null  AND `order`.status_order_id = 5)) OR ((tanggal.dt BETWEEN '$request->from_date' and '$request->to_date') and (`order`.created_at is not null  AND `order`.status_order_id = 4)) OR ((tanggal.dt BETWEEN '$request->from_date' and '$request->to_date') and (`order`.created_at is not null  AND `order`.status_order_id = 3))
                    GROUP BY tanggal.dt
                    order by tanggal.dt desc
                    ");
                }
            else
                {
                    $data = DB::select("SELECT DATE_FORMAT(tanggal.dt,'%d-%m-%Y') as tanggalno,DATE_FORMAT(tanggal.dt,'%d-%m-%Y') as tanggal,
                    Concat('Rp ',fRupiah(IFNULL((SELECT (sum(`order`.subtotal) + sum(`order`.biaya_cod)) from `order`
                    WHERE (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 5) or (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 4) or (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 3)
                    ),0))) as totalorder ,
                    Concat('Rp ',fRupiah(IFNULL((SELECT sum(beli_bahans.total) from beli_bahans
                    WHERE beli_bahans.tanggal_beli=tanggal.dt),0))) as totalbeli ,
                    Concat('Rp ',fRupiah((
                            IFNULL((SELECT (sum(`order`.subtotal) + sum(`order`.biaya_cod)) from `order`
                    WHERE (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 5) or (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 4) or (date(`order`.created_at)=tanggal.dt AND `order`.status_order_id = 3)
                    ),0) - 
                            (IFNULL((SELECT sum(beli_bahans.total) from beli_bahans
                    WHERE beli_bahans.tanggal_beli=tanggal.dt),0)))))
                            as labarugi
                    from tanggal
                    left JOIN beli_bahans on beli_bahans.tanggal_beli = tanggal.dt
                    left JOIN `order` on date(`order`.created_at) = tanggal.dt
                    WHERE beli_bahans.tanggal_beli is Not NULL or (`order`.created_at is not null  AND `order`.status_order_id = 5) OR (`order`.created_at is not null  AND `order`.status_order_id = 4) OR (`order`.created_at is not null  AND `order`.status_order_id = 3)
                    GROUP BY tanggal.dt
                    order by tanggal.dt desc
                    ");
                }
            return datatables()->of($data)->make(true);
            }
            return view('admin.laporan.keuangan2');
            }
            
        // $data = array(
        //     'uang' => $keuangan
        // );

        // return view('admin.laporan.keuangan2', $data);


    public function lapselesai()
    {
        //ambil data order yang status nya 5 barang sudah diterima pelangan
        $order = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan', DB::raw("count(order.id) as totorder"), DB::raw("CONCAT(day(order.created_at),'-',MONTH(order.created_at),'-',YEAR(order.created_at)) as tanggal"), DB::raw('sum(order.subtotal + order.biaya_cod) as total'))
            ->groupBy('tanggal')
            ->orderBy('order.created_at', 'DESC')
            ->get();
        $data = array(
            'orderbaru' => $order
        );

        return view('admin.laporan.transaksi', $data);
    }

    public function ambiltgl($tgl)
    {
        $order = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan', DB::raw("CONCAT(day(order.created_at),'-',MONTH(order.created_at),'-',YEAR(order.created_at)) as tanggal"))
            ->where(DB::raw("CONCAT(day(order.created_at),'-',MONTH(order.created_at),'-',YEAR(order.created_at))"), $tgl)
            ->get();
        $data = array(
            'orderbaru' => $order
        );

        return view('admin.laporan.transaksi-tgl', $data);
    }

    public function dibatalkan()
    {
        //ambil data order yang status nya 6 dibatalkan pelanngan
        $order = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('order.status_order_id', 6)
            ->get();
        $data = array(
            'orderbaru' => $order
        );

        return view('admin.transaksi.dibatalkan', $data);
    }

    public function dikirim()
    {
        //ambil data order yang status nya 4 atau sedang dikirim
        $order = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->select('order.*', 'status_order.name', 'users.name as nama_pemesan')
            ->where('order.status_order_id', 4)
            ->get();
        $data = array(
            'orderbaru' => $order
        );

        return view('admin.transaksi.dikirim', $data);
    }

    public function konfirmasi($id)
    {
        //function ini untuk mengkonfirmasi bahwa pelanngan sudah melakukan pembayaran
        $order = Order::findOrFail($id);
        $order->status_order_id = 3;
        $order->save();

        $kurangistok = DB::table('detail_order')->where('order_id', $id)->get();
        foreach ($kurangistok as $kurang) {
            $ambilproduk = DB::table('products')->where('id', $kurang->product_id)->first();
            $ubahstok = $ambilproduk->stok - $kurang->qty;

            $update = DB::table('products')
                ->where('id', $kurang->product_id)
                ->update([
                    'stok' => $ubahstok
                ]);
        }
        $notif = array(
            'message' => 'Konfirmasi bukti pembayaran berhasil',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.transaksi.perludikirim')->with($notif);
    }

    public function tidakvalid($id){
        $order = Order::findOrFail($id);
        $order->status_order_id = 8;
        $order->save();
        $notif = array(
            'message' => 'Penolakan konfirmasi bukti pembayaran berhasil',
            'alert-type' => 'success'
        );
        return redirect('admin/transaksi')->with($notif);
    }

    public function inputresi($id, Request $request)
    {
        //funtion untuk menginput no resi pesanan
        $order = Order::findOrFail($id);
        $order->no_resi = $request->no_resi;
        $order->status_order_id = 4;
        $order->save();
        $notif = array(
            'message' => 'Input resi Berhasil',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.transaksi.perludikirim')->with($notif);
    }

    
}
