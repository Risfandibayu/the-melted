<?php

namespace App\Http\Controllers\admin;

use App\beli_bahan;
use App\cart_beli;
use App\Http\Controllers\Controller;
use App\bahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Alert;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;


class BeliBahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Detail
        $beli_bahan =  DB::table('bahans')
            ->select('*', db::raw('sum(beli_bahans.qty) as qty'), db::raw('sum(beli_bahans.total) as total'))
            ->join('beli_bahans', 'bahans.id', '=', 'beli_bahans.id_bahan')
            ->groupBy('beli_bahans.id_bahan', 'beli_bahans.created_at')
            ->orderBy('beli_bahans.created_at', 'DESC')
            ->get();
        $bhn = \App\beli_bahan::with('bahans');
        // return view('admin.beli_bahan.index', ['beli_bahan' => $beli_bahan, 'bhn' => $bhn]);

        //tanggal
        // $beli_bahan =  DB::table('beli_bahans')
        // ->select('*', DB::raw('sum(beli_bahans.total) as total'))
        // ->join('bahans','beli_bahans.id_bahan','=','bahans.id')
        // ->groupBy('beli_bahans.tanggal_beli')
        // ->get();

        $bhn = \App\beli_bahan::with('bahans');

        return view('admin.beli_bahan.index', ['beli_bahan' => $beli_bahan, 'bhn' => $bhn]);
    }



    public function indextgl()
    {
        $beli_bahan =  DB::table('beli_bahans')
            ->select('*', DB::raw('sum(beli_bahans.total) as total'), DB::raw("count(distinct(beli_bahans.id_bahan)) as totbahan"), DB::raw("SUBSTRING_INDEX(GROUP_CONCAT(distinct(bahans.nama_bahan)),',',5) as namab"))
            ->join('bahans', 'beli_bahans.id_bahan', '=', 'bahans.id')
            ->groupBy('beli_bahans.tanggal_beli')
            ->orderBy('beli_bahans.tanggal_beli', 'DESC')
            ->get();
        $bhn = \App\beli_bahan::with('bahans');
        return view('admin.laporan.stok-masuk', ['beli_bahan' => $beli_bahan, 'bhn' => $bhn]);
    }



    public function ambiltgl($tgl)
    {
        $beli_bahan = \App\beli_bahan::where('tanggal_beli', $tgl)->first();
        // $bahan = \App\bahan::where('id', $id)->first();
        // $bahan = \App\bahan::orderBy('id', 'ASC')->first();
        $bahan = \App\bahan::orderBy('id', 'ASC')->get();
        // $beli_bahan = \App\beli_bahan::orderBy('id', 'ASC')->get();
        // return view('admin.beli_bahan.add-data-id', compact('bahan','beli_bahan'));
        // $beli_bahan = \App\beli_bahan::where('id', $id)->first();
        // $bahan = \App\bahan::orderBy('id', 'ASC')->get();
        return view('admin.beli_bahan.add-data', compact('bahan', 'beli_bahan'));
    }

    public function indextglid($tgl)
    {
        $beli_bahan =  DB::table('beli_bahans')
            ->select('*', DB::raw('sum(beli_bahans.total) as total'), DB::raw('sum(beli_bahans.qty) as qty'))
            ->join('bahans', 'beli_bahans.id_bahan', '=', 'bahans.id')
            ->where('beli_bahans.tanggal_beli', $tgl)
            ->groupBy('beli_bahans.id_bahan')
            ->get();
        $bhn = \App\beli_bahan::with('bahans');

        return view('admin.laporan.stok-masuk-detail', ['beli_bahan' => $beli_bahan, 'bhn' => $bhn]);
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = \Auth::user()->id;
        $bahan = \App\bahan::orderBy('nama_bahan', 'ASC')->get();
        $cartbeli = DB::table('cart_beli')
            ->join('bahans', 'bahans.id', '=', 'cart_beli.id_bahan')
            ->select('bahans.nama_bahan as nama','bahans.satuan as satuan', 'cart_beli.*', db::raw('sum(cart_beli.qty) as qt'), db::raw('sum(cart_beli.total) as tot'))
            ->where('id_user',$user)
            ->groupBy('cart_beli.id_bahan')
            ->orderBy('cart_beli.created_at', 'ASC')
            ->get();
        return view('admin.beli_bahan.add-data', compact('bahan', 'cartbeli'));
    }
    public function delcartbeli($id)
    {
        DB::table('cart_beli')->where('id', $id)->delete();
        return redirect()->back();
    }
    public function delcart(){
        $user = \Auth::user()->id;
        DB::table('cart_beli')->where('id_user', $user)->delete();
        return redirect('/admin/bahan');
    }




    public function createid(bahan $bahan, $id_bahan)
    {
        // $bel = \App\beli_bahan::where('id_bahan', $id_bahan)->first();
        // $bahan = \App\bahan::where('id', $id)->first();
        // $bahan = \App\bahan::orderBy('id', 'ASC')->first();
        $bahan = \App\bahan::where('id', $id_bahan)->first();
        // $beli_bahan = \App\beli_bahan::orderBy('id', 'ASC')->get();
        // return view('admin.beli_bahan.add-data-id', compact('bahan','beli_bahan'));
        // $beli_bahan = \App\beli_bahan::where('id', $id)->first();
        $bahans = \App\bahan::orderBy('id', 'ASC')->get();

        return view('admin.beli_bahan.add-data-id', compact('bahan', 'bahans'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'tanggal_beli' => ['required'],

        ]);
    }
    public function validatorr(array $data)
    {
        return Validator::make($data, [
            'id_bahan' => ['required'],
            'qty' => ['required'],
            'total' => ['required'],

        ]);
    }
    public function store(Request $request)
    {
        
            // $id_bahan = \App\bahan::where('id_bahan',$request->id)->first();
            $this->validator($request->all())->validate();
            $usr = \Auth::user()->id;
            $bahan = DB::table('cart_beli')->where('id_user', $usr)->get();
            $current_date_time = Carbon::now()->toDateTimeString();
            if (count($bahan) == 0 || count($bahan) == null) {
                $notif = array(
                    'message' => 'Anda Belum memasukan List stok data bahan masuk',
                    'alert-type' => 'danger'
                );

                return redirect()->back()->with($notif);
            } else {
                foreach ($bahan as $bahn) {
                    $beli_bahan = beli_bahan::insert([
                        'tanggal_beli' => $request->get('tanggal_beli'),
                        'id_bahan' => $bahn->id_bahan,
                        'qty' => $bahn->qty,
                        'total' => $bahn->total,
                        'keterangan' => $request->get('keterangan'),
                        'created_at' => $current_date_time,
                        'updated_at' => $current_date_time,
                    ]);
                    $bhn = \App\bahan::findOrFail($bahn->id_bahan);
                    $bhn->stok += $bahn->qty;
                    $bhn->save();
                }
                
                DB::table('cart_beli')->where('id_user', $usr)->delete();
                $notif = array(
                    'message' => 'Input Stok Berhasil',
                    'alert-type' => 'success'
                );

                return redirect('/admin/bahan')->with($notif);
            }
            // return redirect()->back()->with($notif);
        
    }

    public function cartbeli(Request $request)
    {
        
            $this->validatorr($request->all())->validate();
            $crt = DB::table('cart_beli')
            ->select('*')
            ->where('id_bahan',$request->get('id_bahan'))
            ->first();
            $bahan = DB::table('cart_beli')->get();

            if($crt){
                cart_beli::findOrFail($crt->id)->update([
                    'id_bahan' => $request->get('id_bahan'),
                    'qty' => $crt->qty + $request->get('qty'),
                    'total' => $crt->total + $request->get('total'),
                    ]);
                
                    return redirect()->back();
            }else{
            $current_date_time = Carbon::now()->toDateTimeString();
            $cart_beli = cart_beli::insert([
                'id_bahan' => $request->get('id_bahan'),
                'qty' => $request->get('qty'),
                'total' => $request->get('total'),
                'id_user' => \Auth::user()->id,
                'created_at' => $current_date_time,
                        'updated_at' => $current_date_time,

            ]);
            return redirect()->back();
            }
    }
    public function validid(array $data, $id_bahan)
    {
        return Validator::make($data, [
            'tanggal_beli' => ['required'],
            'id_bahan' => ['required'],
            'qty' => ['required'],
            'total' => ['required'],

        ]);
    }

    public function storeid(Request $request, $id_bahan)
    {
        // $id_bahan = \App\bahan::where('id_bahan',$request->id)->first();
        $this->validid($request->all(), $id_bahan)->validate();
        $current_date_time = Carbon::now()->toDateTimeString();
        $id = bahan::findOrfail($id_bahan);
        $beli_bahan = beli_bahan::insert([
            'tanggal_beli' => $request->get('tanggal_beli'),
            'id_bahan' => $id_bahan,
            'qty' => $request->get('qty'),
            'total' => $request->get('total'),
            'keterangan' => $request->get('keterangan'),
            'created_at' => $current_date_time,
            'updated_at' => $current_date_time,
        ]);

        // beli_bahan::findOrFail($id)->insert([
        //     'tanggal_beli' => $request->get('tanggal_beli'),
        //     'id_bahan' => $request->get('id_bahan'),
        //     'qty' => $request->get('qty'),
        //     'total' => $request->get('total'),
        //     'keterangan' => $request->get('keterangan'),
        // ]);

        $bhn = \App\bahan::findOrFail($request->id_bahan);
        $bhn->stok += $request->qty;
        $bhn->save();


        $notif = array(
            'message' => 'Input Stok '.$bhn->nama_bahan.' Berhasil',
            'alert-type' => 'success'
        );

        return redirect('/admin/bahan')->with($notif);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\beli_bahan  $beli_bahan
     * @return \Illuminate\Http\Response
     */
    public function show(beli_bahan $beli_bahan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\beli_bahan  $beli_bahan
     * @return \Illuminate\Http\Response
     */
    public function edit(beli_bahan $beli_bahan, $id)
    {
        $beli_bahan = \App\beli_bahan::where('id', $id)->first();
        $bahan = \App\bahan::orderBy('id', 'ASC')->get();
        return view('admin.beli_bahan.edit-data', compact('bahan', 'beli_bahan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\beli_bahan  $beli_bahan
     * @return \Illuminate\Http\Response
     */
    public function valid(array $data, $id)
    {
        return Validator::make($data, [
            'tanggal_beli' => ['required'],
            'id_bahan' => ['required'],
            'qty' => ['required'],
            'total' => ['required'],

        ]);
    }
    public function update(Request $request, beli_bahan $beli_bahan, $id)
    {
        $this->valid($request->all(), $id)->validate();
        $jmllama = \App\beli_bahan::findOrFail($id);
        beli_bahan::findOrFail($id)->update([
            'tanggal_beli' => $request->get('tanggal_beli'),
            'id_bahan' => $request->get('id_bahan'),
            'qty' => $request->get('qty'),
            'total' => $request->get('total'),
            'keterangan' => $request->get('keterangan'),
        ]);

        $bhn = \App\bahan::findOrFail($request->id_bahan);
        $bhn->stok = ($bhn->stok - $jmllama->qty) + $request->qty;
        $bhn->save();
        // dd($bhn);
        $notif = array(
            'message' => 'Data Berhasil di ubah',
            'alert-type' => 'success'
        );
        // return redirect('kategori')->with('sukses', 'Data Berhasil Diubah');
        return redirect('/admin/stok_masuk')->with($notif);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\beli_bahan  $beli_bahan
     * @return \Illuminate\Http\Response
     */
    public function destroy(beli_bahan $beli_bahan)
    {
        //
    }
    public function delete($id)
    {   
        $bl = \App\beli_bahan::findOrFail($id);

        $bhn = \App\bahan::findOrFail($bl->id_bahan);
        $bhn->stok = $bhn->stok -  $bl->qty;
        $bhn->save();
        DB::table('beli_bahans')->where('id', $id)->delete();
        return redirect()->back();
    }
}
