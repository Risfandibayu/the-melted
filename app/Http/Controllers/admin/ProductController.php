<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Categories;
use Dompdf\Adapter\PDFLib;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use PDF;
class ProductController extends Controller
{
    public function index()
    {
        //membawa data produk yang di join dengan table kategori
        $products = DB::table('products')
            ->leftJoin('categories as a', 'a.id', '=', 'products.categories_id')
            ->select('products.*', 'a.name as nama_kategori')
            ->orderBy('products.created_at', 'DESC')
            ->get();
        
        $data = array(
            'products' => $products,
            'categories' => Categories::all(),
        );
        return view('admin.product.index', $data);
    }
    public function prdlaris()
    {
        //membawa data produk yang di join dengan table kategori
        $prod = DB::table('products')
            ->Join('categories as a', 'a.id', '=', 'products.categories_id')
            ->Join('detail_order as b', 'b.product_id', '=', 'products.id')
            ->join('order as c','c.id','=','b.order_id')
            ->select('products.*', 'a.name as nama_kategori', DB::raw('sum(b.qty) as jmlterjual'),DB::raw('sum(products.price * qty) as pendapatan'))
            ->whereRaw('c.created_at BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE()')
            ->groupBy('products.id')
            ->orderBy(db::raw('sum(b.qty)'), 'DESC')
            ->get();
        $data = array(
            'pro_laris' => $prod
        );
        return view('admin.laporan.produk-laris', $data);
    }

    public function prdrekom(){
        $rek1 = DB::select("SELECT a.name as pr, 
            (SELECT max(Year(Curdate())- Year(users.tgl_lahir)) FROM users 
            where users.id = b.user_id
            ORDER BY count(Year(Curdate())- Year(d.tgl_lahir)) desc
            ) as usia,
            (
            SELECT max(jk) from users
            WHERE users.id = b.user_id
            ORDER BY count(jk) desc
            )as gender
            FROM products a, `order` b, detail_order c, users d
            WHERE b.id = c.order_id
            and d.id = b.user_id
            and c.product_id = a.id
            and d.role = 'customer'
            GROUP BY a.name
            ORDER BY count(a.name) desc");

        $rek2 = DB::select("SELECT a.name as pr, (
            SELECT count(products_id) from keranjang 
            WHERE keranjang.products_id = a.id
        
            ) as masuk_keranjang,
            round(((
            SELECT count(log_customers.log) FROM log_customers
            where c.name like CONCAT('%',log_customers.log,'%')
            )+(
            SELECT count(log_customers.log) FROM log_customers
            where a.tag like CONCAT('%',log_customers.log,'%')
            )+(
            SELECT count(log_customers.log) FROM log_customers
            where a.name like CONCAT('%',log_customers.log,'%')
            ))/3) as cari
            from products a, log_customers b, categories c, keranjang d
            where a.categories_id = c.id
            and a.name like CONCAT('%',b.log,'%') 
            and a.tag like CONCAT('%',b.log,'%')
            and c.name like CONCAT('%',b.log,'%')
            or d.products_id = a.id
            GROUP BY a.name
            ORDER BY max(b.created_at) desc, count(a.name) desc");
        $data = array(
            'rek1' => $rek1,
            'rek2' => $rek2
        );
        return view('admin.laporan.prd-rekom', $data);
    }


    public function tambah()
    {
        //menampilkan form tambah kategori

        $data = array(
            'categories' => Categories::all(),
        );
        return view('admin.product.tambah', $data);
    }

    public function store(Request $request)
    {
        //menyimpan produk ke database
        if ($request->file('image')) {
            //simpan foto produk yang di upload ke direkteri public/storage/imageproduct
            $file = $request->file('image')->store('imageproduct', 'public');

            // $tags = explode(", ", $request->tag);
            Product::create([
                'name' => $request->name,
                'description' => $request->description,
                'price' => $request->price,
                'stok' => $request->stok,
                'weigth' => $request->weigth,
                'tag' => $request->tag,
                'categories_id' => $request->categories_id,

                'image'          => $file

            ]);
            $notif = array(
                'message' => 'Data Berhasil Di Tambahkan',
                'alert-type' => 'success'
            );

            return redirect()->route('admin.product')->with($notif);
        }
    }

    public function edit($id)
    {
        //menampilkan form edit
        //dan mengambil data produk sesuai id dari parameter
        $data = array(
            'product' => Product::findOrFail($id),
            'categories' => Categories::all(),
        );
        return view('admin.product.edit', $data);
    }
    public function detail($id)
    {

        // $data = array(
        //     'product' => Product::findOrFail($id),
        //     'categories' => Categories::findOrfail('products.categories_id')
        //     ->select('categories.name as namakat'),
        // );
        $product = DB::table('products')
            ->select('products.*', DB::raw('a.name as nama_kategori'))
            ->leftJoin('categories as a', 'a.id', '=', 'products.categories_id')
            ->where('products.id',$id)
            ->first();
        $data = array(
            'product' => $product
        );
        // $data = array(
        //     'product' => Product::findOrFail($id),
        //     'categories' => Categories::all(),
        // );
        
        return view('admin.product.detail', $data);
    }
    public function stok($id)
    {
        //menampilkan form edit
        //dan mengambil data produk sesuai id dari parameter

        $data = array(
            'product' => Product::findOrFail($id),
            'categories' => Categories::all(),
        );
        return view('admin.product.updatestok', $data);
    }

    public function update($id, Request $request)
    {
        //ambil data dulu sesuai parameter $Id
        $prod = Product::findOrFail($id);

        // Lalu update data nya ke database
        if ($request->file('image')) {

            Storage::delete('public/' . $prod->image);
            $file = $request->file('image')->store('imageproduct', 'public');
            $prod->image = $file;
        }

        $prod->name = $request->name;
        $prod->description = $request->description;
        $prod->price = $request->price;
        $prod->weigth = $request->weigth;
        $prod->tag = $request->tag;
        $prod->categories_id = $request->categories_id;
        $prod->stok = $request->stok;
        $prod->save();
        $notif = array(
            'message' => 'Data Berhasil Di Ubah',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.product')->with($notif);
    }
    public function updatestok($id, Request $request)
    {
        //ambil data dulu sesuai parameter $Id
        $prod = Product::findOrFail($id);

        // Lalu update data nya ke database
        if ($request->file('image')) {

            Storage::delete('public/' . $prod->image);
            $file = $request->file('image')->store('imageproduct', 'public');
            $prod->image = $file;
        }

        $prod->name = $request->name;
        $prod->description = $request->description;
        $prod->price = $request->price;
        $prod->weigth = $request->weigth;
        $prod->tag = $request->tag;
        $prod->categories_id = $request->categories_id;
        $prod->stok = $request->stok;

        $prod->save();
        $notif = array(
            'message' => 'Update stok '.$prod->name.' berhasil',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.product')->with($notif);
    }

    public function delete($id)
    {
        //mengahapus produk
        $prod = Product::findOrFail($id);
        Product::destroy($id);
        Storage::delete('public/' . $prod->image);
        return redirect()->route('admin.product')->with('status', 'Berhasil Mengahapus Produk');
    }

    public function cetak()
    {
        //membawa data produk yang di join dengan table kategori
        $products = DB::table('products')
            ->leftJoin('categories as a', 'a.id', '=', 'products.categories_id')
            ->select('products.*', 'a.name as nama_kategori')
            ->get();
        // $data = array(
        //     'products' => $products
        // );

        $pdf = PDF::loadview('admin.product.cetak',['products'=>$products]);
    	return $pdf->stream();
        // return view('admin.product.index', $data);
    }

    
}
