<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\LogCustomer;
use App\Categories;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Renaldy\PhpFPGrowth\FPGrowth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\URL;

class ProdukController extends Controller
{
    public function index()
    {
        //menampilkan data produk yang dijoin dengan table kategori
        //kemudian dikasih paginasi 9 data per halaman nya
        $kat = DB::table('categories')
            ->join('products', 'products.categories_id', '=', 'categories.id')
            ->select(DB::raw('count(products.categories_id) as jumlah, categories.*'))
            ->groupBy('categories.id')
            ->get();
        $data = array(
            'produks' => Product::paginate(12),
            'categories' => $kat
        );
        return view('user.produk', $data);
    }
    public function detail($id)
    {
        //mengambil detail produk
        $products = DB::table('products')
            ->leftJoin('categories as a', 'a.id', '=', 'products.categories_id')
            ->select('products.*', 'a.name as nama_kategori')
            ->where('products.id', $id)
            ->first();
        if (Auth::user() == NULL) {
            # code...
            $crt = Null;
        } else {
            $id_user = \Auth::user()->id;
            $crt = db::table('keranjang')
                ->select('*')
                ->where('user_id', $id_user)
                ->where('products_id', $id)
                ->first();
        }
        $kat = DB::table('products')
            ->select('categories_id')
            ->where('products.id', $id)
            ->value('products');

        $relkat = DB::table('categories')
            ->select('*')
            ->where('id', '=', $kat)
            ->get();

        $relate = DB::table('products')
            ->Join('categories as a', 'a.id', '=', 'products.categories_id')
            ->select('products.*', 'a.name as nama_kategori')
            ->where('a.id', $kat)
            ->where('products.id', '!=', $id)
            ->get();

        $data = array(
            'produk' => $products,
            'relate' => $relate,
            'relkat' => $relkat,
            'crt' => $crt
        );
        // $data = array(
        //     'produk' => Product::findOrFail($id)
        // );
        return view('user.produkdetail', $data);
    }

    public function cari(Request $request)
    {
        if (Auth::user() != NULL) {
            if (Categories::where('name', 'like', '%'.$request->cari.'%')->get()->count() == 0) {
                LogCustomer::create([
                    'id_customer' => Auth::user()->id,
                    'log' => $request->cari,
                    'by_categories' => 0
                ]);
            } else {
                LogCustomer::create([
                    'id_customer' => Auth::user()->id,
                    'log' => Categories::where('name', 'like', '%' . $request->cari . '%')->first('name')['name'],
                    'by_categories' => 1
                ]);
            }
        }

        $log = LogCustomer::where('by_categories', '1')->groupBy('id_customer')->selectRaw('*, sum(by_categories) as by_categories')->orderBy('by_categories', 'DESC')->get();
        foreach ($log as $key => $value) {
            // $y = $value['id_customer'];
            foreach (LogCustomer::where(array('by_categories' => 1, 'id_customer' => $value['id_customer']))->groupBy('log')->get('log') as $k => $v) {
                $x[] = $v['log'];
            }
            $log[$key]['log'] = $x;
            $y[$value['by_categories']] = array_unique($x);
        }

        $support = 0.8;
        $confidence = 0.75;
        // dump($y);

        $fpgrowth = new FPGrowth($y, $support, $confidence);
        $fpgrowth->run();
        $rules = $fpgrowth->getRules();
        $ky = array();

        foreach ($rules as $key => $value) {
            $a = explode(', ', $value['antecedent']);
            $b = array();
            foreach ($a as $k => $v) {
                if (array_key_exists($v, $ky)) {
                    $b = $ky[strtoupper($v)];
                    $c = explode(',', $value['consequent']);
                    foreach ($c as $ke => $val) {
                        $b[] = $val;
                    }
                    $ky[strtoupper($v)] = array_unique($b);
                } else {
                    $c = explode(',', $value['consequent']);
                    foreach ($c as $ke => $val) {
                        $ky[strtoupper($v)][] = $val;
                    }
                    $ky[strtoupper($v)] = array_unique($ky[strtoupper($v)]);
                }
            }
        }

        // dump($kk);
        // dd($ky);

        $pr  = Categories::where('name', $request->cari) ->get();
        // return count($pr);

        if (count($pr) == 0) {
            $prod = DB::table('categories')
                ->join('products', 'products.categories_id', '=', 'categories.id')
                ->where('categories.name','=', $request->cari)
                ->where('stok', '>', 0)
                ->limit(4)
                ->orderByRaw('Rand()')
                ->get();
            $total = $prod->count();
        } else {
            $prod = DB::table('categories')
                ->join('products', 'products.categories_id', '=', 'categories.id')
                ->where('categories.name','!=', $request->cari)
                ->where('stok', '>', 0)
                ->limit(4)
                ->orderByRaw('Rand()')
                ->get();

            $keys = array_keys($ky);
            $found = false;
            foreach ($keys as $key) {
                //If the key is found in your string, set $found to true
                if (str_contains(strtoupper($key), strtoupper($request->cari))) {
                    $found = true;
                }
            }

            // return $found;

            if (array_key_exists(strtoupper($request->cari), $ky)) {
                foreach ($ky[strtoupper($request->cari)] as $key => $value) {
                    $x = DB::table('categories')
                        ->join('products', 'products.categories_id', '=', 'categories.id')
                        ->where('categories.name', $value)
                        ->where('stok', '>', 0)
                        ->get();
                    if ($x != null) {
                        foreach ($x as $y => $z) {
                            $prod[] = $z;
                        }
                    }
                }
            }
            $total = count($prod);
        }

        $prodd  = DB::table('products')
            ->select('products.*')
            ->join('categories', 'products.categories_id', '=', 'categories.id')
            ->where('products.name', 'like', '%' . $request->cari . '%')
            ->orwhere('products.tag', 'like', '%' . $request->cari . '%')
            ->orwhere('categories.name', 'like', '%' . $request->cari . '%')
            ->paginate(124);
        $totall = DB::table('products')
            ->select('products.*')
            ->join('categories', 'products.categories_id', '=', 'categories.id')
            ->where('products.name', 'like', '%' . $request->cari . '%')
            ->orwhere('products.tag', 'like', '%' . $request->cari . '%')
            ->orwhere('categories.name', 'like', '%' . $request->cari . '%')
            ->count();
        // return $prod;

        $data  = array(
            'produks' => collect($prod)->paginate(8),
            'produkss' => $prodd,
            'cari' => $request->cari,
            'totall' => $totall,
            'total' => $total
        );
        return view('user.cariproduk', $data);
    }

    function array_sort($array, $on, $order = SORT_ASC)
    {

        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    // public function paginate($items, $perPage = 8, $page = null, $options = [])
    // {
    //     $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    //     $items = $items instanceof Collection ? $items : Collection::make($items);
    //     $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

    //     return [
    //         'current_page' => $lap->currentPage(),
    //         'data' => $lap->values(),
    //         'first_page_url' => $lap->url(1),
    //         'from' => $lap->firstItem(),
    //         'last_page' => $lap->lastPage(),
    //         'last_page_url' => $lap->url($lap->lastPage()),
    //         'next_page_url' => $lap->nextPageUrl(),
    //         'per_page' => $lap->perPage(),
    //         'prev_page_url' => $lap->previousPageUrl(),
    //         'to' => $lap->lastItem(),
    //         'total' => $lap->total(),
    //     ];
    // }

    public function loadData(Request $request)
    {
    }
    public function livesearch(Request $request)
    {
        $term = $request->get('term');
        if ($request->get('term') == null) {
            $data = DB::table('categories')->select('name as value', 'name as link')->get();
        } else {
            $data = DB::table('categories')->where("name", "LIKE", "%$term%")->select('name as value', 'name as link')->get();
        }
        return response()->json($data);
    }
}
