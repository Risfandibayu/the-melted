<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\Detailorder;
use App\Rekening;
use App\Product;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use PDF;

class OrderController extends Controller
{

    public function index()
    {
        //menampilkan semua data pesanan
        $user_id = \Auth::user()->id;

        $order = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->select('order.*', 'status_order.name')
            ->where('order.status_order_id', 1)
            ->where('order.user_id', $user_id)->get();
        $dicek = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->select('order.*', 'status_order.name')
            ->where('order.status_order_id', '!=', 1)
            ->Where('order.status_order_id', '!=', 5)
            ->Where('order.status_order_id', '!=', 6)
            ->where('order.user_id', $user_id)->get();
        $histori = DB::table('order')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->select('order.*', 'status_order.name')
            ->where('order.status_order_id', '!=', 1)
            ->Where('order.status_order_id', '!=', 2)
            ->Where('order.status_order_id', '!=', 3)
            ->Where('order.status_order_id', '!=', 4)
            ->where('order.user_id', $user_id)->get();
        $data = array(
            'order' => $order,
            'dicek' => $dicek,
            'histori' => $histori
        );
        return view('user.order.order', $data);
    }





    public function bayar()
    {
        $user_id = \Auth::user()->id;

        // $order = DB::table('order')
        //     ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
        //     ->select('order.*',DB::raw('date(order.created_at) as tanggal'), 'status_order.name')
        //     ->where('order.status_order_id', 1)
        //     ->where('order.user_id', $user_id)
        //     ->get();

        $order = DB::table('order')
        ->select( 'order.id as id','order.invoice as inv', 'a.name as status', 'order.subtotal as tot', DB::raw('date(order.created_at) as tgl'), DB::raw('max(c.name) as prd'), DB::raw('max(c.id) as prdid'), DB::raw('max(c.image) as prdim'),DB::raw('max(b.qty) as prdqty'), DB::raw('(count(c.name)-1) as item'))
        ->join('status_order as a', 'a.id', '=', 'order.status_order_id')
        ->join('detail_order as b', 'b.order_id','=','order.id')
        ->join('products as c', 'c.id','=','b.product_id')
        ->where('order.user_id', $user_id)
        ->where('order.status_order_id', 1)
        ->orwhere('order.user_id', $user_id)
        ->where('order.status_order_id', 8)
        ->groupBy('order.id')
        ->orderBy('order.created_at', 'DESC')
        ->get();
        

        
        
        $data = array(
            'order' => $order,
        );
        return view('user.order.belumbayar', $data);
    }

    public function proses()
    {
        $user_id = \Auth::user()->id;

        // $dicek = DB::table('order')
        //     ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
        //     ->select('order.*',DB::raw('date(order.created_at) as tanggal'), 'status_order.name')
        //     ->where('order.status_order_id', '!=', 1)
        //     ->Where('order.status_order_id', '!=', 5)
        //     ->Where('order.status_order_id', '!=', 6)
        //     ->where('order.user_id', $user_id)->get();
        // $data = array(
        //     'dicek' => $dicek,
        // );
        $order = DB::table('order')
        ->select( 'order.id as id','order.invoice as inv', 'a.name as status', 'order.subtotal as tot', DB::raw('date(order.created_at) as tgl'), DB::raw('max(c.name) as prd'), DB::raw('max(c.id) as prdid'), DB::raw('max(c.image) as prdim'),DB::raw('max(b.qty) as prdqty'), DB::raw('(count(c.name)-1) as item'))
        ->join('status_order as a', 'a.id', '=', 'order.status_order_id')
        ->join('detail_order as b', 'b.order_id','=','order.id')
        ->join('products as c', 'c.id','=','b.product_id')
        ->where('order.user_id', $user_id)
        ->where('order.status_order_id', '!=', 1)
        ->Where('order.status_order_id', '!=', 5)
        ->Where('order.status_order_id', '!=', 6)
        ->Where('order.status_order_id', '!=', 7)
        ->Where('order.status_order_id', '!=', 8)
        ->groupBy('order.id')
        ->orderBy('order.created_at', 'DESC')
        ->get();
        
        $data = array(
            'order' => $order,
        );
        return view('user.order.proses', $data);
    }

    public function riwayat(){

        $user_id = \Auth::user()->id;
        // $histori = DB::table('order')
        // ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
        // ->select('order.*',DB::raw('date(order.created_at) as tanggal'), 'status_order.name')
        // ->where('order.status_order_id', '!=', 1)
        // ->Where('order.status_order_id', '!=', 2)
        // ->Where('order.status_order_id', '!=', 3)
        // ->Where('order.status_order_id', '!=', 4)
        // ->where('order.user_id', $user_id)->get();
        // $data = array(
        //     'histori' => $histori
        // );
        $order = DB::table('order')
        ->select('order.id as id','order.invoice as inv', 'a.name as status', 'order.subtotal as tot', DB::raw('date(order.created_at) as tgl'), DB::raw('max(c.name) as prd'), DB::raw('max(c.id) as prdid'), DB::raw('max(c.image) as prdim'),DB::raw('max(b.qty) as prdqty'), DB::raw('(count(c.name)-1) as item'))
        ->join('status_order as a', 'a.id', '=', 'order.status_order_id')
        ->join('detail_order as b', 'b.order_id','=','order.id')
        ->join('products as c', 'c.id','=','b.product_id')
        ->where('order.user_id', $user_id)
        ->where('order.status_order_id', '!=', 1)
        ->Where('order.status_order_id', '!=', 2)
        ->Where('order.status_order_id', '!=', 3)
        ->Where('order.status_order_id', '!=', 4)
        ->Where('order.status_order_id', '!=', 8)
        ->groupBy('order.id')
        ->orderBy('order.created_at', 'DESC')
        ->get();
        
        $data = array(
            'order' => $order,
        );
        return view('user.order.riwayat', $data);
    }    




    public function detail($id)
    {
        //function menampilkan detail order
        $detail_order = DB::table('detail_order')
            ->join('products', 'products.id', '=', 'detail_order.product_id')
            ->join('order', 'order.id', '=', 'detail_order.order_id')
            ->select('products.name as nama_produk', 'products.image', 'products.id as prid', 'detail_order.*', 'products.price', 'order.*', db::raw('sum(detail_order.qty) as qt'))
            ->where('detail_order.order_id', $id)
            ->groupBy('detail_order.product_id')
            ->orderby('detail_order.product_id','ASC')
            ->get();
        $order = DB::table('order')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->select('order.*',DB::raw('date(order.created_at) as tgl'), 'users.name as nama_pelanggan', 'status_order.name as status')
            ->where('order.id', $id)
            ->first();
        $data = array(
            'detail' => $detail_order,
            'order'  => $order
        );
        return view('user.order.detail', $data);
    }

    public function sukses()
    {
        //menampilkan view terimakasih jika order berhasil dibuat
        return view('user.terimakasih');
    }

    public function kirimbukti($id, Request $request)
    {
        //mengupload bukti pembayaran
        $order = Order::findOrFail($id);
        if ($request->file('bukti_pembayaran')) {
            $file = $request->file('bukti_pembayaran')->store('buktibayar', 'public');

            $order->bukti_pembayaran = $file;
            $order->status_order_id  = 2;

            $order->save();
        }
        $notif = array(
            'message' => 'Upload bukti pembayaran berhasil!',
            'alert-type' => 'bisa'
        );
        return redirect()->route('user.order.proses')->with($notif);
    }

    public function pembayaran($id)
    {
        //menampilkan view pembayaran
        $data = array(
            'rekening' => Rekening::all(),
            'order' => Order::findOrFail($id)
        );
        return view('user.order.pembayaran', $data);
    }

    public function pesananditerima($id)
    {
        //function untuk menerima pesanan
        $order = Order::findOrFail($id);
        $order->status_order_id = 5;
        $order->save();
        
        return redirect()->route('user.order.riwayat');
    }

    public function pesanandibatalkan($id)
    {
        //function untuk membatalkan pesanan
        $order = Order::findOrFail($id);
        $order->status_order_id = 6;
        $order->save();

        return redirect()->route('user.order.bayar');
    }


    public function simpan(Request $request)
    {
        //untuk menyimpan pesanan ke table order
        $cek_invoice = DB::table('order')->where('invoice', $request->invoice)->count();
        if ($cek_invoice < 1) {
            $userid = \Auth::user()->id;
            //jika pelanggan memilih metode cod maka insert data yang ini

            $alamat_user = DB::table('alamat')
            ->join('cities','cities.city_id','=','alamat.cities_id')
            ->join('subdistricts','subdistricts.subdistrict_id','=','alamat.subdistrict_id')
            ->join('provinces','provinces.province_id','=','cities.province_id')
            ->select(Db::raw("Concat(alamat.detail,', ',cities.title,', ',subdistricts.subdistrict_name,', ',provinces.title) as almt"))
            ->where('alamat.user_id',$userid)
            ->first();  

            $almt = implode(',',(explode(',', $alamat_user->almt)));

            if ($request->no_hp == NULL) {
                $notif = array(
                    'message' => 'Nomor telepon penerima harus di isi',
                    'alert-type' => 'no'
                );
                return redirect()->back()->with($notif);
            } else {


                if ($request->metode_pembayaran == 'cod') {
                    Order::create([
                        'invoice' => $request->invoice,
                        'user_id' => $userid,
                        'subtotal' => $request->subtotal,
                        'status_order_id' => 1,
                        'metode_pembayaran' => $request->metode_pembayaran,
                        'ongkir' => $request->ongkir,
                        'biaya_cod' => 10000,
                        'no_hp' => $request->no_hp,
                        'pesan' => $request->pesan,
                        'alamat_pengiriman' => $almt,
                    ]);
                } else {
                    //jika memilih transfer maka data yang ini
                    Order::create([
                        'invoice' => $request->invoice,
                        'user_id' => $userid,
                        'subtotal' => $request->subtotal,
                        'status_order_id' => 1,
                        'metode_pembayaran' => $request->metode_pembayaran,
                        'ongkir' => $request->ongkir,
                        'no_hp' => $request->no_hp,
                        'pesan' => $request->pesan,
                        'alamat_pengiriman' => $almt,
                    ]);
                }
            }

            $order = DB::table('order')->where('invoice', $request->invoice)->first();

            $barang = DB::table('keranjang')->where('user_id', $userid)->get();
            //lalu masukan barang2 yang dibeli ke table detail order
            foreach ($barang as $brg) {
                Detailorder::create([
                    'order_id' => $order->id,
                    'product_id' => $brg->products_id,
                    'qty' => $brg->qty,
                ]);
                $prod = Product::findOrFail($brg->products_id);
                $prod->stok = $prod->stok - $brg->qty;
                $prod->save();

            }
            //lalu hapus data produk pada keranjang pembeli
            DB::table('keranjang')->where('user_id', $userid)->delete();
            $notif = array(
                'message' => 'Pesanan Berhasil di Buat',
                'alert-type' => 'ordsuk'
            );
            return redirect()->route('user.order.sukses')->with($notif);
        } else {
            return redirect()->route('user.keranjang');
        }
        // dd($request);

    }
    public function cetak($id){
        $detail_order = DB::table('detail_order')
            ->join('products', 'products.id', '=', 'detail_order.product_id')
            ->join('order', 'order.id', '=', 'detail_order.order_id')
            ->select('products.name as nama_produk', 'products.image', 'products.id as prid', 'detail_order.*', 'products.price', 'order.*', db::raw('sum(detail_order.qty) as qt'))
            ->where('detail_order.order_id', $id)
            ->groupBy('detail_order.product_id')
            ->get();
        $order = DB::table('order')
            ->join('users', 'users.id', '=', 'order.user_id')
            ->join('status_order', 'status_order.id', '=', 'order.status_order_id')
            ->select('order.*',DB::raw('date(order.created_at) as tgl'), 'users.name as nama_pelanggan', 'status_order.name as status')
            ->where('order.id', $id)
            ->first();
        $pdf = PDF::loadview('user.order.cetaknota', ['detail' => $detail_order, 'order' => $order ]);
            // $pdf = PDF::loadview('admin.product.cetak',['products'=>$products]);
        return $pdf->stream();
    }
}
