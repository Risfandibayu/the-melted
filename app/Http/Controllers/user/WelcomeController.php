<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
    public function index()
    {



        //menampilkan data produk dihalamam utama user dengan limit 10 data
        //untuk di carousel
        

        $data = array(
            // 'produks' => DB::table('products')->limit(10)->get(),
            'produks' => DB::table('products')
                ->Join('categories as a', 'a.id', '=', 'products.categories_id')
                ->Join('detail_order as b', 'b.product_id', '=', 'products.id')
                ->select('products.*', 'a.name as nama_kategori', DB::raw('sum(b.qty) as jmlterjual'), DB::raw('sum(products.price * qty) as pendapatan'))
                ->groupBy('products.id')
                ->orderBy(db::raw('sum(b.qty)'), 'DESC')
                ->get(),
            
            
            

                


          

        );
        return view('user.welcome', $data);
    }

    public function kontak()
    {
        return view('user.kontak');
    }
}
