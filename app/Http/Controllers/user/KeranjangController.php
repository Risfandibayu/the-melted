<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Keranjang;
use Illuminate\Support\Facades\DB;
class KeranjangController extends Controller
{

    public function index()
    {
        
        $id_user = \Auth::user()->id;
        $keranjangs = DB::table('keranjang')
                            ->join('users','users.id','=','keranjang.user_id')
                            ->join('products','products.id','=','keranjang.products_id')
                            ->select('products.name as nama_produk','products.image','users.name','keranjang.*',db::raw('sum(keranjang.qty) as qt'),'products.price','products.id as prdid', 'products.stok as stok')
                            ->where('keranjang.user_id','=',$id_user)
                            ->groupBy('keranjang.products_id')
                            ->get();
        $cekalamat = DB::table('alamat')->where('user_id',$id_user)->count();
        $index = 0;
        $cekstok = db::table('keranjang')
        ->select('*')
        ->join('products','products.id','=','keranjang.products_id')
        ->whereRaw('keranjang.qty > products.stok')
        ->where('keranjang.user_id',$id_user)
        ->count();

        $cekqty = db::table('keranjang')
        ->select('*')
        ->join('products','products.id','=','keranjang.products_id')
        ->where('products.stok','=',0)
        ->where('keranjang.user_id',$id_user)
        ->count();
        // dd($cekstok);
        $data = [
            'keranjangs' => $keranjangs,
            'cekalamat'  => $cekalamat,
            'cekqty' => $cekqty,
            'cekstok' =>$cekstok
            
        ];
        return view('user.keranjang',$data);
    }

    public function simpan(Request $request)
    {
        $prd = DB::table('products')
        ->select('*')
        ->where('id',$request->products_id)
        ->first();
        $id_user = \Auth::user()->id;
        $crt = Db::table('keranjang')
        ->select('*')
        ->where('products_id',$request->products_id)
        ->where('user_id',$id_user)
        ->first();
        

        if($request->qty <= $prd->stok){
            if($crt){
                if (($crt->qty + $request->qty) > $prd->stok) {
                    $notif = array(
                        'message' => 'Jumlah produk yang anda masukan sudah melebihi stok yang tersedia',
                        'alert-type' => 'fview'
                    );
            
                    return redirect()->back()->with($notif);
                }elseif($request->qty == 0){
                    $notif = array(
                        'message' => 'Jumlah produk yang anda masukan tidak boleh nol',
                        'alert-type' => 'fview'
                    );
            
                    return redirect()->back()->with($notif);
                }
                else{
                Keranjang::findOrFail($crt->id)->update([
                    'user_id' => $id_user,
                    'products_id' => $request->products_id,
                    'qty' => $crt->qty + $request->qty,
                    ]);
                    $notif = array(
                        'message' => 'Produk '.$crt->name.' berhasil di masukan dalam keranjang',
                        'alert-type' => 'bisa'
                    );
                return redirect()->route('user.keranjang')->with($notif);
                }
            }else{

                Keranjang::create([
                    'user_id' => $request->user_id,
                    'products_id' => $request->products_id,
                    'qty' => $request->qty
                    ]);
                $notif = array(
                        'message' => 'Produk '.$prd->name.' berhasil di masukan dalam keranjang',
                        'alert-type' => 'bisa'
                );
                return redirect()->route('user.keranjang')->with($notif);
            }
            
        }else{
            $notif = array(
                'message' => 'Jumlah produk yang anda masukan melebihi stok yang tersedia',
                'alert-type' => 'fview'
            );
    
            return redirect()->back()->with($notif);
        }

        
    }
    public function save(Request $request)
    {
        $prd = DB::table('products')
        ->select('*')
        ->where('id',$request->products_id)
        ->first();

        $id_user = \Auth::user()->id;
        $crt = Db::table('keranjang')
        ->select('*')
        ->where('products_id',$request->products_id)
        ->where('user_id',$id_user)
        ->first();

        if($request->qty <= $prd->stok){
            if($crt){
                if (($crt->qty + $request->qty) > $prd->stok) {
                    $notif = array(
                        'message' => 'Jumlah produk yang anda masukan melebihi stok yang tersedia',
                        'alert-type' => 'fview'
                    );
            
                    return redirect()->back()->with($notif);
                }else{
                Keranjang::findOrFail($crt->id)->update([
                    'user_id' => $id_user,
                    'products_id' => $request->products_id,
                    'qty' => $crt->qty + $request->qty,
                    ]);
                    $notif = array(
                        'message' => 'Berhasil Dimasukan dalam Keranjang',
                        'alert-type' => 'addcart'
                    );
                return redirect()->back()->with($notif);
                }
            }else{

                Keranjang::create([
                    'user_id' => $request->user_id,
                    'products_id' => $request->products_id,
                    'qty' => $request->qty
                    ]);
                $notif = array(
                        'message' => 'Berhasil Dimasukan dalam Keranjang',
                        'alert-type' => 'addcart'
                );
                return redirect()->back()->with($notif);
            }
            
        }else{
            $notif = array(
                'message' => 'Jumlah produk yang anda masukan melebihi stok yang tersedia',
                'alert-type' => 'fview'
            );
    
            return redirect()->back()->with($notif);
        }
    }

    function show_Names($n, $m)
    {
    return("The name is $n and email is $m, thank you");
    }
    public function update(Request $request)
    {
        $index = 0;
        $produk = db::table('keranjang')
        ->join('products','products.id','keranjang.products_id')
        ->select('*')
        ->where('keranjang.user_id',\Auth::user()->id)
        ->first();
        // if ($request->qty <= $produk->stok) {
            
        // }
        $k = 1;
        
        // $notif = [];
        foreach($request->id as $id){
                $keranjang = Keranjang::findOrFail($id);
                $pro = db::table('keranjang')
                ->join('products','products.id','keranjang.products_id')
                ->select('*')
                ->where('keranjang.user_id',\Auth::user()->id)
                ->where('keranjang.products_id',$keranjang->products_id)
                ->first();
                
                if ($pro->stok == 0) {
                    $keranjang->delete();
                }elseif($keranjang->qty > $pro->stok){
                    $keranjang->qty = $pro->stok;
                    $keranjang->save();
                }
                
                if($request->qty[$index] > $pro->stok || $request->qty[$index] == 0){
                    if($request->qty[$index] > $pro->stok ){
                        $k *= 1;
                    }elseif($request->qty[$index] == 0){
                        $k *= 1;
                        
                    }
                }else{
                    $keranjang->qty = $request->qty[$index];
                    $keranjang->save();
                    $k *= 0;
                    
                    
                }
                $index++;   

        }

        // dd($k);
        if ($k == 1) {
            $notif = array(
                'message' => 'Anda salah memasukan Qty!',
                'alert-type' => 'fail'
            );
        }else{
            $notif = array(
                'message' => 'Update keranjang berhasil!',
                'alert-type' => 'bisa'
            );
        }
        
        return redirect()->route('user.keranjang')->with($notif);
    }

    public function delete($id)
    {

        
        $prd = DB::table('keranjang')
        ->join('products','products.id','=','keranjang.products_id')
        ->select('products.name as nama_produk','products.image','keranjang.*','products.price')
        ->where('keranjang.id',$id)
        ->first();
        
        $notif = array(
            'message' => 'Produk '. $prd->nama_produk .' berhasil di hapus dari keranjang',
            'alert-type' => 'delcart'
        );
        Keranjang::destroy($id);
        // return redirect()->route('user.keranjang');
        return redirect()->back()->with($notif);
    }
}
