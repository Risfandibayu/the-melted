<?php

namespace App\Http\Controllers;

use App\user;
use Illuminate\Foundation\Auth\User as AuthUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $data = array(
            'admin' => DB::table('users')
                ->select('*')
                ->where('role', '=', 'admincus')
                ->orwhere('role', '=', 'adminkat')
                ->orwhere('role', '=', 'admintrx')
                ->orwhere('role', '=', 'adminstok')
                ->get()
        );
        //menampilkan view
        return view('admin.pengaturan.admin', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah()
    {
        //menampilkan form tambah kategori

        $data = array(
            'admin' => User::all(),
        );
        return view('admin.pengaturan.tambah-admin', $data);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    protected function store(Request $request)
    {
        $this->validator($request->all())->validate();
        User::create([
            'name' => $request->name,
            'role' => $request->role,
            'no_telp' => $request->no_telp,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        $notif = array(
            'message' => 'Tambah Admin Berhasil!',
            'alert-type' => 'success'
        );
        //lalu reireact ke route admin.categories dengan mengirim flashdata(session) berhasil tambah data untuk manampilkan alert succes tambah data
        return redirect()->route('admin.dataadmin')->with($notif);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user, $id)
    {
        $user = \App\User::where('id', $id)->first();
        return view('admin.user.edit-data', compact('user'));
    }

    public function editcus(user $user, $id)
    {
        $user = \App\User::where('id', $id)->first();
        return view('user.akun', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */

    public function valid(array $data, $id)
    {
        return Validator::make($data, [
            'ft_profil' => ['mimes:jpg,png,jpeg,gif'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
    public function update(Request $request, user $user, $id)
    {
        //
        $this->valid($request->all(), $id)->validate();
        if ($request->file('ft_profil')) {
            $up = $request->file('ft_profil')->store('fotouser', 'public');
            $ft_profil = User::findOrFail($id);
            if ($ft_profil->ft_profil) {
                Storage::delete('public/' . $ft_profil->ft_profil);
                $ft_profil->ft_profil = $up;
            } else {
                $ft_profil->ft_profil = $up;
            }
            $ft_profil->save();
        }
        user::findOrFail($id)->update([
            'name' => $request->get('name'),
            'no_telp' => $request->get('no_telp'),
            'role' => $request->get('role'),
            'jk' => $request->get('jk'),
            'tgl_lahir' => $request->get('tgl_lahir'),
            'email' => $request->get('email'),
            'status' => $request->get('status'),
            'username' => $request->get('username'),
            'password' => $request->get('password'),
        ]);

        $notif = array(
            'message' => 'Update profil berhasil',
            'alert-type' => 'updateprofil'
        );
        // return redirect('kategori')->with('sukses', 'Data Berhasil Diubah');


        if (auth()->user()->role == 'admin') {
            return redirect('/admin')->with($notif);
        } else if (auth()->user()->role == 'owner') {
            return redirect('/admin_owner')->with($notif);
        } else if (auth()->user()->role == 'admincus') {
            return redirect('/admin_admincust')->with($notif);
        } else if (auth()->user()->role == 'adminkat') {
            return redirect('/admin_adminkat')->with($notif);
        } else if (auth()->user()->role == 'admintrx') {
            return redirect('/admin_admintrx')->with($notif);
        } else if (auth()->user()->role == 'adminstok') {
            return redirect('/admin_adminstok')->with($notif);
        } else {
            return '/';
        }
    }
    public function updatecus(Request $request, user $user, $id)
    {
        //
        $this->valid($request->all(), $id)->validate();
        if ($request->file('ft_profil')) {
            $up = $request->file('ft_profil')->store('fotouser', 'public');
            $ft_profil = User::findOrFail($id);
            if ($ft_profil->ft_profil) {
                Storage::delete('public/' . $ft_profil->ft_profil);
                $ft_profil->ft_profil = $up;
            } else {
                $ft_profil->ft_profil = $up;
            }
            $ft_profil->save();
        }

        if ($request->get('newpassword') == NULL) {
            user::findOrFail($id)->update([
                'name' => $request->get('name'),
                'no_telp' => $request->get('no_telp'),
                'role' => $request->get('role'),
                'jk' => $request->get('jk'),
                'tgl_lahir' => $request->get('tgl_lahir'),
                'email' => $request->get('email'),
                'status' => $request->get('status'),
                'username' => $request->get('username'),
                'password' => $request->get('password'),
            ]);
        } else {

            if ($request->newpassword == $request->newpassword_confirmation) {
                
                user::findOrFail($id)->update([
                    'name' => $request->get('name'),
                    'no_telp' => $request->get('no_telp'),
                    'role' => $request->get('role'),
                    'jk' => $request->get('jk'),
                    'tgl_lahir' => $request->get('tgl_lahir'),
                    'email' => $request->get('email'),
                    'status' => $request->get('status'),
                    'username' => $request->get('username'),
                    'password' => hash::make($request->newpassword),
                ]);
                $notif = array(
                    'message' => 'Edit profil berhasil!',
                    'alert-type' => 'upprof'
                );
                // return redirect('kategori')->with('sukses', 'Data Berhasil Diubah');
                return redirect()->back()->with($notif);
            }else if( !empty($request->newpassword) & $request->newpassword_confirmation == NULL) {
                $notif = array(
                    'message' => 'Password Konformasi Belum Di isi',
                    'alert-type' => 'conkos'
                );
                // return redirect('kategori')->with('sukses', 'Data Berhasil Diubah');
                return redirect()->back()->with($notif);
            }else {
                $notif = array(
                    'message' => 'Password Konformasi Tidak Sama',
                    'alert-type' => 'con'
                );
                // return redirect('kategori')->with('sukses', 'Data Berhasil Diubah');
                return redirect()->back()->with($notif);
            }
        }





        $notif = array(
            'message' => 'Edit profil berhasil!',
            'alert-type' => 'upprof'
        );
        // return redirect('kategori')->with('sukses', 'Data Berhasil Diubah');


        return redirect()->back()->with($notif);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        //
    }
    public function delete(user $user, $id)
    {
        User::destroy($id);
        return redirect()->back();
    }

    public function usdash()
    {
        return view('user.usdash');
    }
}
