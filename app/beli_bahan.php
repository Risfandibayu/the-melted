<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class beli_bahan extends Model
{
    protected $table = 'beli_bahans';
    protected $fillable = [
        'tanggal_beli',
        'id_bahan',        
        'qty',        
        'total',        
        'keterangan',        
    ];

    public function bahan(){
        return $this->belongsTo(bahan::class);
    }

    public function cartbeli(){
        return $this->belongsTo(cart_beli::class);
    }
    public function crtbl(){
        return $this->belongsTo('App\cart_beli','id','id_bahan');
    }

    public function bhn(){
        return $this->belongsTo('App\bahan','id_bahan','id');
    }
    

}
