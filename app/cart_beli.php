<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class cart_beli extends Model
{
    protected $table = 'cart_beli';
    protected $fillable = [
        'id_bahan',        
        'qty',        
        'total',            
        'id_user',        
    ];

    public function bahan(){
        return $this->belongsTo(bahan::class);
    }

    public function bhn(){
        return $this->belongsTo('App\bahan','id_bahan','id');
    }
    

}
