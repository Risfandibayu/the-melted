<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kurang_bahan extends Model
{
    protected $table = 'kurang_bahans';
    protected $fillable = [
        'tanggal_beli',
        'id_bahan',        
        'jumlah',            
        'keterangan',        
    ];

    public function bahan(){
        return $this->belongsTo(bahan::class);
    }
}
