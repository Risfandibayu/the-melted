<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class cart_kurang extends Model
{
    protected $table = 'cart_kurang';
    protected $fillable = [
        'id_bahan',        
        'jumlah',
        'id_user'      
    ];

    public function bahan(){
        return $this->belongsTo(bahan::class);
    }

    public function bhn(){
        return $this->belongsTo('App\bahan','id_bahan','id');
    }
    

}
