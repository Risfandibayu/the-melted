<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tips extends Model
{
    protected $table = 'tips';
    protected $fillable = [
        'id_user',
        'tanggal_upload',        
        'judul',        
        'deskripsi',        
        'foto',        
    ];

}
